﻿using UnityEngine;
using System.Collections;
//using SIS;

public class ScrollView : MonoBehaviour {

	Vector2 scrollPosition;
	Touch touch;
	private Texture t, t1,t2,t3;
	private Rect tr, r, tr1, r1,tr2,r2,tr3,r3;
	private int cost, uphp,upatk;
	public GUISkin skin, skin2;
	private string currency= "gems";
	private int costgs1,costgs2,costother10,costother30;
	private int bulk10,bulk30;
	private int costfoot, costarcher, costsword, costgun, costspear, costmage, costshaman, costelephant, costtank;
	private int hpivfoot, hpivarcher, hpivsword, hpivgun, hpivspear, hpivmage, hpivshaman, hpivelephant, hpivtank;
	private int atkivfoot, atkivarcher, atkivsword, atkivgun, atkivspear, atkivmage, atkivshaman, atkivelephant, atkivtank;
	private int costarjuna, costbima, costsrikandi, costyudhistira, costnakulasadewa, costgatotkaca, costdrupada, costkresna, costhanoman;
	private int hpivarjuna, hpivbima, hpivsrikandi, hpivyudhistira, hpivnakulasadewa, hpivgatotkaca, hpivdrupada, hpivkresna, hpivhanoman;
	private int atkivarjuna, atkivbima, atkivsrikandi, atkivyudhistira, atkivnakulasadewa, atkivgatotkaca, atkivdrupada, atkivkresna, atkivhanoman;
	public GameObject footman,archerman,swordman,gunnerman,spearman,mageman,shamanman,elephantryman,tankman;
	public GameObject arjunaman,bimaman,srikandiman,yudhistiraman,nakulasadewaman,gatotkacaman,drupadaman,kresnaman,hanomanman;
	public int footcost,archercost,swordcost,gunnercost,spearcost,magecost,shamancost,elephantcost,tankcost;
	public Sprite coin, gem, heart, barbel;
	public int arjunacost,bimacost,srikandicost,yudhistiracost,nakulasadewacost,gatotkacacost,drupadacost,kresnacost,hanomancost;
	public Texture buy, upgrade, rate;
	public Texture foot,archer,sword,spear,gunner,mage,shaman,elephantry,tank;
	public Texture gamespeed2x, gamespeed3x, attack10, attack30, hp10, hp30, resource10, resource30, movespeed10, movespeed30, res10, res30;
	public Texture arjuna,bima,srikandi,yudhistira,nakulasadewa,gatotkaca,drupada,kresna,hanoman;
	public Texture footText, archerText, spearText, swordText, mageText, gunnerText, shamanText, elephantryText, tankText;
	public Texture arjunaText, bimaText, srikandiText, yudhistiraText, nakulasadewaText, gatotkacaText, drupadaText, kresnaText, hanomanText;
	public Texture emptyhealth,fullhealth,emptyatk,fullatk,progress,progressatk;
	public Texture gamespeed2xText, gamespeed3xText, attack10Text, attack30Text, hp10Text, hp30Text, res10Text, res30Text, movespeed10Text, movespeed30Text;
	public Texture OKButton, GoldWarning, GemWarning, GameSpeedWarning, LikePageWarning;
	public Texture ArcherWarning, SwordmanWarning, GunnerWarning, SpearmanWarning, MageWarning, ShamanWarning, ElephantryWarning, TankWarning;
	public GUIStyle style,smallstyle;
	public Font myFont;
	private bool showGoldNotification, showGemNotification, showLikePageNotification, showGameSpeedNotification, showArcherWarning, showSwordmanWarning, showGunnerWarning, showSpearmanWarning, showMageWarning, 
	showShamanWarning, showElephantryWarning, showTankWarning, showLockNotification, hasinitstyle=false;
	public static bool hasUpgrade;
	private float scrollViewXPosition;
	public GameObject Container;
	private bool showingNotification;

	void Start () {
		showingNotification = false;
		costgs1 = 5;
		costgs2 = 10;
		costother10 = 1;
		costother30 = 2;
		bulk10 = 6;
		bulk30 = 9;

		costfoot = 10;
		costarcher = 10;
		costsword = 12;
		costgun = 12;
		costspear = 14;
		costmage = 14;
		costshaman = 16;
		costelephant = 18;
		costtank = 20;
		costarjuna = 20;
		costbima = 20;
		costsrikandi = 20;
		costyudhistira = 20;
		costnakulasadewa = 20;
		costgatotkaca = 20;
		costdrupada = 20;
		costkresna = 20;
		costhanoman = 20;

		hpivfoot = 20;
		hpivarcher = 15;
		hpivsword = 30;
		hpivgun = 22;
		hpivspear = 30;
		hpivmage = 15;
		hpivshaman = 16;
		hpivelephant = 80;
		hpivtank = 40;
		hpivarjuna = 120;
		hpivbima = 150;
		hpivsrikandi = 115;
		hpivyudhistira = 135;
		hpivnakulasadewa = 125;
		hpivgatotkaca = 145;
		hpivdrupada = 220;
		hpivkresna = 145;
		hpivhanoman = 145;
		
		atkivfoot = 3;
		atkivarcher = 2;
		atkivsword = 6;
		atkivgun = 3;
		atkivspear = 6;
		atkivmage = 6;
		atkivshaman = 2;
		atkivelephant = 10;
		atkivtank = 8;
		atkivarjuna = 9;
		atkivbima = 24;
		atkivsrikandi = 5;
		atkivyudhistira = 12;
		atkivnakulasadewa = 8;
		atkivgatotkaca = 9;
		atkivdrupada = 7;
		atkivkresna = 7;
		atkivhanoman = 8;

		Scaling ();
		showGoldNotification = false;
		showGemNotification = false;
		hasUpgrade = false;
		t = coin.texture;
		tr = coin.textureRect;
		r = new Rect(tr.x / t.width, tr.y / t.height, tr.width / t.width, tr.height / t.height );
		t1 = gem.texture;
		tr1 = gem.textureRect;
		r1 = new Rect(tr1.x / t1.width, tr1.y / t1.height, tr1.width / t1.width, tr1.height / t1.height );
		t2 = heart.texture;
		tr2 = heart.textureRect;
		r2 = new Rect(tr2.x / t2.width, tr2.y / t2.height, tr2.width / t2.width, tr2.height / t2.height );
		t3 = barbel.texture;
		tr3 = barbel.textureRect;
		r3 = new Rect(tr3.x / t3.width, tr3.y / t3.height, tr3.width / t3.width, tr3.height / t3.height );

		if (RegenEnergy.secondTimePlaying == 0 && PlayerPrefs.GetInt("LevelFoot") == 0) {
			PlayerPrefs.SetInt("LevelFoot",1);
            PlayerPrefs.SetInt("LevelArcher",1);
			PlayerPrefs.SetInt("LevelSword",1);
			PlayerPrefs.SetInt("LevelSpear",1);
			PlayerPrefs.SetInt("LevelMage",1);
			PlayerPrefs.SetInt("LevelGunner",1);
			PlayerPrefs.SetInt("LevelArjuna",1);
			PlayerPrefs.SetInt("LevelBima",1);
			PlayerPrefs.SetInt("LevelSrikandi",1);
			PlayerPrefs.SetInt("LevelShaman",1);
			PlayerPrefs.SetInt("LevelElephantry",1);
			PlayerPrefs.SetInt("LevelTank",1);
			PlayerPrefs.SetInt("LevelYudhistira",1);
			PlayerPrefs.SetInt("LevelNakulaSadewa",1);
			PlayerPrefs.SetInt("LevelGatotKaca",1);
			PlayerPrefs.SetInt("LevelDrupada",1);
			PlayerPrefs.SetInt("LevelKresna",1);
			PlayerPrefs.SetInt("LevelHanoman",1);

			PlayerPrefs.SetFloat("ATKFoot",footman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPFoot",footman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKArcher",archerman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPArcher",archerman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKSword",swordman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPSword",swordman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKGunner",gunnerman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPGunner",gunnerman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKSpear",spearman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPSpear",spearman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKMage",mageman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPMage",mageman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKArjuna",arjunaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPArjuna",arjunaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKBima",bimaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPBima",bimaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKSrikandi",srikandiman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPSrikandi",srikandiman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKShaman",shamanman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPShaman",shamanman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKElephantry",elephantryman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPElephantry",elephantryman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKTank",tankman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPTank",tankman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKYudhistira",yudhistiraman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPYudhistira",yudhistiraman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKNakulaSadewa",nakulasadewaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPNakulaSadewa",nakulasadewaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKGatotKaca",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPGatotKaca",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKDrupada",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPDrupada",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKKresna",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPKresna",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKHanoman",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPHanoman",gatotkacaman.GetComponent<UnitAtt>().HP);

			PlayerPrefs.SetInt("Footcost",footcost);
			PlayerPrefs.SetInt("Swordcost",swordcost);
			PlayerPrefs.SetInt("Archercost",archercost);
			PlayerPrefs.SetInt("Spearcost",spearcost);
			PlayerPrefs.SetInt("Magecost",magecost);
			PlayerPrefs.SetInt("Gunnercost",gunnercost);
			PlayerPrefs.SetInt("Arjunacost",arjunacost);
			PlayerPrefs.SetInt("Bimacost",bimacost);
			PlayerPrefs.SetInt("Srikandicost",srikandicost);
			PlayerPrefs.SetInt("Shamancost",shamancost);
			PlayerPrefs.SetInt("Elephantrycost",elephantcost);
			PlayerPrefs.SetInt("Tankcost",tankcost);
			PlayerPrefs.SetInt("Yudhistiracost",yudhistiracost);
			PlayerPrefs.SetInt("NakulaSadewacost",nakulasadewacost);
			PlayerPrefs.SetInt("GatotKacacost",gatotkacacost);
			PlayerPrefs.SetInt("Drupadacost",drupadacost);
			PlayerPrefs.SetInt("Kresnacost",kresnacost);
			PlayerPrefs.SetInt("Hanomancost",hanomancost);
		}
		if(RegenEnergy.secondTimePlaying == 1 && PlayerPrefs.GetFloat("ATKSpear") == 0){
			PlayerPrefs.SetFloat("ATKSpear",spearman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPSpear",spearman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKMage",mageman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPMage",mageman.GetComponent<UnitAtt>().HP);
		}
		if(RegenEnergy.secondTimePlaying == 1 && PlayerPrefs.GetInt("LevelShaman") == 0){
			PlayerPrefs.SetInt("LevelShaman",1);
			PlayerPrefs.SetInt("LevelElephantry",1);
			PlayerPrefs.SetInt("LevelTank",1);
			PlayerPrefs.SetInt("LevelYudhistira",1);
			PlayerPrefs.SetInt("LevelNakulaSadewa",1);
			PlayerPrefs.SetInt("LevelGatotKaca",1);
			PlayerPrefs.SetInt("LevelDrupada",1);
			PlayerPrefs.SetInt("LevelKresna",1);
			PlayerPrefs.SetInt("LevelHanoman",1);

			PlayerPrefs.SetInt("Shamancost",shamancost);
			PlayerPrefs.SetInt("Elephantrycost",elephantcost);
			PlayerPrefs.SetInt("Tankcost",tankcost);
			PlayerPrefs.SetInt("Yudhistiracost",yudhistiracost);
			PlayerPrefs.SetInt("NakulaSadewacost",nakulasadewacost);
			PlayerPrefs.SetInt("GatotKacacost",gatotkacacost);
			PlayerPrefs.SetInt("Drupadacost",drupadacost);
			PlayerPrefs.SetInt("Kresnacost",kresnacost);
			PlayerPrefs.SetInt("Hanomancost",hanomancost);

			PlayerPrefs.SetFloat("ATKShaman",shamanman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPShaman",shamanman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKElephantry",elephantryman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPElephantry",elephantryman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKTank",tankman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPTank",tankman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKYudhistira",yudhistiraman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPYudhistira",yudhistiraman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKNakulaSadewa",nakulasadewaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPNakulaSadewa",nakulasadewaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKGatotKaca",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPGatotKaca",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKDrupada",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPDrupada",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKKresna",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPKresna",gatotkacaman.GetComponent<UnitAtt>().HP);
			PlayerPrefs.SetFloat("ATKHanoman",gatotkacaman.GetComponent<UnitAtt>().ATK);
			PlayerPrefs.SetFloat("HPHanoman",gatotkacaman.GetComponent<UnitAtt>().HP);
		}
	}
	void OnGUI () { 
		if (ShopClick.clickUnit == true) {
			if(ShopClick.reset == true){
				scrollPosition = new Vector2 (0,0);
				ShopClick.reset = false;
			}
			UnitUpgrade();
		}
		if (ShopClick.clickHero == true) {
			if(ShopClick.reset == true){
				scrollPosition = new Vector2 (0,0);
				ShopClick.reset = false;
			}
			HeroUpgrade();
		}
		if (ShopClick.clickItem == true) {
			if(ShopClick.reset == true){
				scrollPosition = new Vector2 (0,0);
				ShopClick.reset = false;
			}
			ItemShop();
		}
		if (showGoldNotification == true) {
			ShowGoldNotif();	
		}
		else if (showGemNotification == true) {
			ShowGemNotif();	
		}
		else if (showGameSpeedNotification == true) {
			ShowGameSpeedNotif();	
		}
		else if (showLikePageNotification == true) {
			ShowLikePageNotif();
		}
		else if (showArcherWarning == true) {
			ShowArcherNotif();	
		}
		else if (showSwordmanWarning == true) {
			ShowSwordmanNotif();	
		}
		else if (showGunnerWarning == true) {
			ShowGunnerNotif();	
		}
		else if (showSpearmanWarning == true) {
			ShowSpearmanNotif();	
		}
		else if (showMageWarning == true) {
			ShowMageNotif();	
		}
		else if (showShamanWarning == true) {
			ShowShamanNotif();	
		}
		else if (showElephantryWarning == true) {
			ShowElephantryNotif();	
		}
		else if (showTankWarning == true) {
			ShowTankNotif();	
		}
	}
	
	void Update()
	{
		if (hasinitstyle == false) {
			style.fontSize = Screen.width/40;
			smallstyle.fontSize = Screen.width/60;
			hasinitstyle = true;
		}
		if(Input.touchCount > 0)
		{
			touch = Input.touches[0];
			if (touch.phase == TouchPhase.Moved)
			{
				scrollPosition.y += touch.deltaPosition.y * 5.0f;
			}
		}
	}

	void ItemShop(){
		scrollPosition = GUI.BeginScrollView(new Rect(Screen.width*scrollViewXPosition,Screen.height*0.12f,Screen.width,Screen.height*0.7f),scrollPosition, new Rect(Screen.width*0.12f,Screen.height*0.1f,130,Screen.height*3.01f));
		GUI.skin = skin;
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.11f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.14f,Screen.width*0.3f,Screen.width*0.03f), gamespeed2xText);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*0.15f,Screen.width*0.1f,Screen.width*0.1f), gamespeed2x);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*0.15f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.18f,Screen.width*0.08f,Screen.height*0.3f),costgs1.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.2f,Screen.width*0.08f,Screen.height*0.12f),"Switch Game Speed (2x)",smallstyle);
        /*
		if (PlayerPrefs.GetInt ("GameSpeed") == 0) {
			if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*0.25f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
				hasUpgrade = true;
				if((DBManager.GetFunds(currency)-costgs1 >= 0)){
					PlayerPrefs.SetInt("GameSpeed", 2);
					DBManager.IncreaseFunds(currency,-costgs1);
					ItemShop();
				}
				else {
					showGemNotification = true;
					showingNotification = true;
				}
			}	
		}
        */
        GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.41f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.44f,Screen.width*0.3f,Screen.width*0.03f), gamespeed3xText);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*0.44f,Screen.width*0.1f,Screen.width*0.1f), gamespeed3x);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*0.45f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.48f,Screen.width*0.08f,Screen.height*0.3f),costgs2.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.5f,Screen.width*0.08f,Screen.height*0.12f),"Switch Game Speed (3x)",smallstyle);
        /*
		if (PlayerPrefs.GetInt ("GameSpeed") != 3) {
			if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*0.55f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
				if (PlayerPrefs.GetInt ("GameSpeed") == 2) {
					hasUpgrade = true;
					if((DBManager.GetFunds(currency)-costgs2 >= 0)){
						PlayerPrefs.SetInt("GameSpeed", 3);
						DBManager.IncreaseFunds(currency,-costgs2);
						ItemShop();
					}
					else {
						showGemNotification = true;
						showingNotification = true;
					}
				}
				else {
					showGameSpeedNotification = true;
					showingNotification = true;
				}
			}	
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.71f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.74f,Screen.width*0.3f,Screen.width*0.03f), movespeed10Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*0.74f,Screen.width*0.1f,Screen.width*0.1f), movespeed10);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*0.75f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*0.78f,Screen.width*0.08f,Screen.height*0.3f),costother10.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("MovementSpeed10"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.9f,Screen.width*0.08f,Screen.height*0.12f),"Increase Move Speed all ally units by 10%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*0.85f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother10 >= 0)){
				PlayerPrefs.SetInt("MovementSpeed10",PlayerPrefs.GetInt("MovementSpeed10")+bulk10);
				DBManager.IncreaseFunds(currency,-costother10);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.01f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.04f,Screen.width*0.3f,Screen.width*0.03f), movespeed30Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*1.04f,Screen.width*0.1f,Screen.width*0.1f), movespeed30);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*1.05f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*1.08f,Screen.width*0.08f,Screen.height*0.3f),costother30.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("MovementSpeed30"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.2f,Screen.width*0.08f,Screen.height*0.12f),"Increase Move Speed all ally units by 30%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*1.15f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother30 >= 0)){
				PlayerPrefs.SetInt("MovementSpeed30",PlayerPrefs.GetInt("MovementSpeed30")+bulk30);
				DBManager.IncreaseFunds(currency,-costother30);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.31f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.34f,Screen.width*0.3f,Screen.width*0.03f), attack10Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*1.34f,Screen.width*0.1f,Screen.width*0.1f), attack10);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*1.35f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*1.38f,Screen.width*0.08f,Screen.height*0.3f),costother10.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Attack10"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.5f,Screen.width*0.08f,Screen.height*0.12f),"Increase Attack Power all ally units by 10%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*1.45f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother10 >= 0)){
				PlayerPrefs.SetInt("Attack10",PlayerPrefs.GetInt("Attack10")+bulk10);
				DBManager.IncreaseFunds(currency,-costother10);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.61f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.64f,Screen.width*0.3f,Screen.width*0.03f), attack30Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*1.64f,Screen.width*0.1f,Screen.width*0.1f), attack30);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*1.65f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*1.68f,Screen.width*0.08f,Screen.height*0.3f),costother30.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Attack30"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.8f,Screen.width*0.08f,Screen.height*0.12f),"Increase Attack Power all ally units by 30%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*1.75f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother30 >= 0)){
				PlayerPrefs.SetInt("Attack30",PlayerPrefs.GetInt("Attack30")+bulk30);
				DBManager.IncreaseFunds(currency,-costother30);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}*/
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.91f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.94f,Screen.width*0.3f,Screen.width*0.03f),  hp10Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*1.94f,Screen.width*0.1f,Screen.width*0.1f), hp10);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*1.95f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*1.98f,Screen.width*0.08f,Screen.height*0.3f),costother10.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("HP10"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.1f,Screen.width*0.08f,Screen.height*0.12f),"Increase Health Point all ally units by 10%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.05f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother10 >= 0)){
				PlayerPrefs.SetInt("HP10",PlayerPrefs.GetInt("HP10")+bulk10);
				DBManager.IncreaseFunds(currency,-costother10);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.21f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.24f,Screen.width*0.3f,Screen.width*0.03f), hp30Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*2.24f,Screen.width*0.1f,Screen.width*0.1f), hp30);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*2.25f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*2.28f,Screen.width*0.08f,Screen.height*0.3f),costother30.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("HP30"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.4f,Screen.width*0.08f,Screen.height*0.12f),"Increase Health Point all ally units by 30%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.35f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother30 >= 0)){
				PlayerPrefs.SetInt("HP30",PlayerPrefs.GetInt("HP30")+bulk30);
				DBManager.IncreaseFunds(currency,-costother30);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.51f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.54f,Screen.width*0.3f,Screen.width*0.03f), res10Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*2.54f,Screen.width*0.1f,Screen.width*0.1f), res10);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*2.55f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*2.58f,Screen.width*0.08f,Screen.height*0.3f),costother10.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Resource10"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.7f,Screen.width*0.08f,Screen.height*0.12f),"Increase your silver coin's regeneration rate by 10%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.65f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother10 >= 0)){
				PlayerPrefs.SetInt("Resource10",PlayerPrefs.GetInt("Resource10")+bulk10);
				DBManager.IncreaseFunds(currency,-costother10);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.81f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.84f,Screen.width*0.3f,Screen.width*0.03f), res30Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*2.84f,Screen.width*0.1f,Screen.width*0.1f), res30);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*2.85f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*2.88f,Screen.width*0.08f,Screen.height*0.3f),costother30.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.92f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",smallstyle);
		GUI.Label(new Rect(Screen.width*0.43f,Screen.height*2.92f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Resource30"),smallstyle);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*3.0f,Screen.width*0.08f,Screen.height*0.12f),"Increase your silver coin's regeneration rate by 30%",smallstyle);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.95f,Screen.width*0.14f,Screen.width*0.07f),buy) && showingNotification == false) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother30 >= 0)){
				PlayerPrefs.SetInt("Resource30",PlayerPrefs.GetInt("Resource30")+bulk30);
				DBManager.IncreaseFunds(currency,-costother30);
				ItemShop();
			}
			else {
				showGemNotification = true;
				showingNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.51f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.29f,Screen.height*2.54f,Screen.width*0.3f,Screen.width*0.03f), res10Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*2.54f,Screen.width*0.1f,Screen.width*0.1f), resource10);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*2.55f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*2.58f,Screen.width*0.08f,Screen.height*0.3f),costother10.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.29f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",style);
		GUI.Label(new Rect(Screen.width*0.39f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Resource10"),style);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.65f,Screen.width*0.14f,Screen.width*0.07f),buy)) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother10 >= 0)){
				PlayerPrefs.SetInt("Resource10",PlayerPrefs.GetInt("Resource10")+bulk10);
				DBManager.IncreaseFunds(currency,-costother10);
				ItemShop();
			}
			else {
				showGemNotification = true;
			}
		}
        */
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.81f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.29f,Screen.height*2.84f,Screen.width*0.3f,Screen.width*0.03f), res30Text);
		GUI.DrawTexture (new Rect(Screen.width*0.19f,Screen.height*2.84f,Screen.width*0.1f,Screen.width*0.1f), resource30);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.82f,Screen.height*2.85f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);
		GUI.Label(new Rect(Screen.width*0.89f,Screen.height*2.88f,Screen.width*0.08f,Screen.height*0.3f),costother30.ToString(),style);
		GUI.Label(new Rect(Screen.width*0.29f,Screen.height*2.92f,Screen.width*0.08f,Screen.height*0.3f),"STOCK : ",style);
		GUI.Label(new Rect(Screen.width*0.39f,Screen.height*2.92f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Resource30"),style);
        /*
		if (GUI.Button(new Rect(Screen.width*0.8f,Screen.height*2.95f,Screen.width*0.14f,Screen.width*0.07f),buy)) {
			hasUpgrade = true;
			if((DBManager.GetFunds(currency)-costother30 >= 0)){
				PlayerPrefs.SetInt("Resource30",PlayerPrefs.GetInt("Resource30")+bulk30);
				DBManager.IncreaseFunds(currency,-costother30);
				ItemShop();
			}
			else {
				showGemNotification = true;
			}
		}
        */
        GUI.EndScrollView ();
	    }
        
	void UnitUpgrade(){
		scrollPosition = GUI.BeginScrollView(new Rect(Screen.width*scrollViewXPosition,Screen.height*0.12f,Screen.width,Screen.height*0.7f),scrollPosition, new Rect(Screen.width*0.12f,Screen.height*0.1f,130,Screen.height*2.71f));
		GUI.skin = skin;
		GUI.skin.font = myFont;
		if(PlayerPrefs.GetInt ("FootMax") == 0){
			upgradechange(hpivfoot,atkivfoot,costfoot,"Foot");
		}
		else{
			upgradechange(0,0,0,"Foot");
			PlayerPrefs.SetInt("Footcost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.11f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.14f,Screen.width*0.3f,Screen.width*0.03f), footText);
		GUI.Label(new Rect(Screen.width*0.54f,Screen.height*0.15f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelFoot")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.15f,Screen.width*0.1f,Screen.width*0.1f), foot);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.215f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.205f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPFoot"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.215f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.205f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKFoot"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.19f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Footcost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.17f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.29f,Screen.width*0.08f,Screen.height*0.12f),"Basic unit with cheapest cost",smallstyle);
		if(PlayerPrefs.GetInt ("FootMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.26f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupfoot == true){
					
				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Footcost")) >= 0){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelFoot") < 19) {
						upgradechange(hpivfoot,atkivfoot,costfoot,"Foot");
						PlayerPrefs.SetInt("LevelFoot", PlayerPrefs.GetInt("LevelFoot")+ 1);
						PlayerPrefs.SetFloat("ATKFoot",PlayerPrefs.GetFloat("ATKFoot")+ upatk);
						PlayerPrefs.SetFloat("HPFoot",PlayerPrefs.GetFloat("HPFoot")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Footcost"));
						PlayerPrefs.SetInt("Footcost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelFoot") == 19){
						upgradechange(hpivfoot,atkivfoot,costfoot,"Foot");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Footcost"));
						PlayerPrefs.SetInt("LevelFoot", 20);
						PlayerPrefs.SetInt ("FootMax",1);
						PlayerPrefs.SetFloat("ATKFoot",PlayerPrefs.GetFloat("ATKFoot")+ upatk);
						PlayerPrefs.SetFloat("HPFoot",PlayerPrefs.GetFloat("HPFoot")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					showGoldNotification = true;
					showingNotification = true;
				}
			}
		}
		if (Checkunit.archer == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.archer == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("ArcherMax") == 0){
			upgradechange(hpivarcher,atkivarcher,costarcher,"Archer");
		}
		else{
			upgradechange(0,0,0,"Archer");
			PlayerPrefs.SetInt("Archercost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.41f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.44f,Screen.width*0.3f,Screen.width*0.03f), archerText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*0.45f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelArcher")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.44f,Screen.width*0.1f,Screen.width*0.1f), archer);
		if (Checkunit.archer == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.515f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.505f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPArcher"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.515f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.505f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKArcher"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.49f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Archercost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.47f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.59f,Screen.width*0.08f,Screen.height*0.12f),"Basic unit with ranged attack",smallstyle);
		}
		if(PlayerPrefs.GetInt ("ArcherMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.55f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				
				if(Checkunit.lockuparcher == true){
					
				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Archercost")) >= 0 && Checkunit.archer == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelArcher") < 19) {
						upgradechange(hpivarcher,atkivarcher,costarcher,"Archer");
						PlayerPrefs.SetInt("LevelArcher", PlayerPrefs.GetInt("LevelArcher")+ 1);
						PlayerPrefs.SetFloat("ATKArcher",PlayerPrefs.GetFloat("ATKArcher")+ upatk);
						PlayerPrefs.SetFloat("HPArcher",PlayerPrefs.GetFloat("HPArcher")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Archercost"));
						PlayerPrefs.SetInt("Archercost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelArcher") == 19){
						upgradechange(hpivarcher,atkivarcher,costarcher,"Archer");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Archercost"));
						PlayerPrefs.SetInt("LevelArcher", 20);
						PlayerPrefs.SetInt ("ArcherMax",1);
						PlayerPrefs.SetFloat("ATKArcher",PlayerPrefs.GetFloat("ATKArcher")+ upatk);
						PlayerPrefs.SetFloat("HPArcher",PlayerPrefs.GetFloat("HPArcher")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.archer == false) {
						showArcherWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.swordman == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.swordman == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("SwordMax") == 0){
			upgradechange(hpivsword,atkivsword,costsword,"Sword");
		}
		else{
			upgradechange(0,0,0,"Sword");
			PlayerPrefs.SetInt("Swordcost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.71f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.74f,Screen.width*0.3f,Screen.width*0.03f), swordText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*0.75f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelSword")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.74f,Screen.width*0.1f,Screen.width*0.1f), sword);
		if (Checkunit.swordman == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.815f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.805f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPSword"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.815f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.805f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKSword"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.79f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Swordcost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.77f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.89f,Screen.width*0.08f,Screen.height*0.12f),"Melee unit with high defend and offense",smallstyle);
		}
		if(PlayerPrefs.GetInt ("SwordMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.87f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupsword == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Swordcost")) >= 0 && Checkunit.swordman == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelSword") < 19) {
						upgradechange(hpivsword,atkivsword,costsword,"Sword");
						PlayerPrefs.SetInt("LevelSword", PlayerPrefs.GetInt("LevelSword")+ 1);
						PlayerPrefs.SetFloat("ATKSword",PlayerPrefs.GetFloat("ATKSword")+ upatk);
						PlayerPrefs.SetFloat("HPSword",PlayerPrefs.GetFloat("HPSword")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Swordcost"));
						PlayerPrefs.SetInt("Swordcost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelSword") == 19){
						upgradechange(hpivsword,atkivsword,costsword,"Sword");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Swordcost"));
						PlayerPrefs.SetInt("LevelSword", 20);
						PlayerPrefs.SetInt ("SwordMax",1);
						PlayerPrefs.SetFloat("ATKSword",PlayerPrefs.GetFloat("ATKSword")+ upatk);
						PlayerPrefs.SetFloat("HPSword",PlayerPrefs.GetFloat("HPSword")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.swordman == false) {
						showSwordmanWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.gunner == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.gunner == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("GunnerMax") == 0){
			upgradechange(hpivgun,atkivgun,costgun,"Gunner");
		}
		else{
			upgradechange(0,0,0,"Gunner");
			PlayerPrefs.SetInt("Gunnercost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.01f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.04f,Screen.width*0.3f,Screen.width*0.03f), gunnerText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*1.05f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelGunner")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.04f,Screen.width*0.1f,Screen.width*0.1f), gunner);
		if (Checkunit.gunner == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.115f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.105f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPGunner"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.115f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.105f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKGunner"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.09f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Gunnercost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.07f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.19f,Screen.width*0.08f,Screen.height*0.12f),"Ranged unit with fast attack speed",smallstyle);
		}
		if(PlayerPrefs.GetInt ("GunnerMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.17f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupgun == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Gunnercost")) >= 0 && Checkunit.gunner == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelGunner") < 19) {
						upgradechange(hpivgun,atkivgun,costgun,"Gunner");
						PlayerPrefs.SetInt("LevelGunner", PlayerPrefs.GetInt("LevelGunner")+ 1);
						PlayerPrefs.SetFloat("ATKGunner",PlayerPrefs.GetFloat("ATKGunner")+ upatk);
						PlayerPrefs.SetFloat("HPGunner",PlayerPrefs.GetFloat("HPGunner")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Gunnercost"));
						PlayerPrefs.SetInt("Gunnercost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelGunner") == 19){
						upgradechange(hpivgun,atkivgun,costgun,"Gunner");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Gunnercost"));
						PlayerPrefs.SetInt("LevelGunner", 20);
						PlayerPrefs.SetInt ("GunnerMax",1);
						PlayerPrefs.SetFloat("ATKGunner",PlayerPrefs.GetFloat("ATKGunner")+ upatk);
						PlayerPrefs.SetFloat("HPGunner",PlayerPrefs.GetFloat("HPGunner")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.gunner == false) {
						showGunnerWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.spearman == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.spearman == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("SpearMax") == 0){
			upgradechange(hpivspear,atkivspear,costspear,"Spear");
		}
		else{
			upgradechange(0,0,0,"Spear");
			PlayerPrefs.SetInt("Spearcost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.31f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.34f,Screen.width*0.3f,Screen.width*0.03f), spearText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*1.35f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelSpear")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.34f,Screen.width*0.1f,Screen.width*0.1f), spear);
		if (Checkunit.spearman == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.415f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.405f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPSpear"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.415f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.405f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKSpear"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.39f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Spearcost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.37f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.49f,Screen.width*0.08f,Screen.height*0.12f),"Mid-range melee unit",smallstyle);
		}
		if(PlayerPrefs.GetInt ("SpearMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.47f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupspear == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Spearcost")) >= 0 && Checkunit.spearman == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelSpear") < 19) {
						upgradechange(hpivspear,atkivspear,costspear,"Spear");
						PlayerPrefs.SetInt("LevelSpear", PlayerPrefs.GetInt("LevelSpear")+ 1);
						PlayerPrefs.SetFloat("ATKSpear",PlayerPrefs.GetFloat("ATKSpear")+ upatk);
						PlayerPrefs.SetFloat("HPSpear",PlayerPrefs.GetFloat("HPSpear")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Spearcost"));
						PlayerPrefs.SetInt("Spearcost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelSpear") == 19){
						upgradechange(hpivspear,atkivspear,costspear,"Spear");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Spearcost"));
						PlayerPrefs.SetInt("LevelSpear", 20);
						PlayerPrefs.SetInt ("SpearMax",1);
						PlayerPrefs.SetFloat("ATKSpear",PlayerPrefs.GetFloat("ATKSpear")+ upatk);
						PlayerPrefs.SetFloat("HPSpear",PlayerPrefs.GetFloat("HPSpear")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.spearman == false) {
						showSpearmanWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.mage == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.mage == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("MageMax") == 0){
			upgradechange(hpivmage,atkivmage,costmage,"Mage");
		}
		else{
			upgradechange(0,0,0,"Mage");
			PlayerPrefs.SetInt("Magecost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.61f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.64f,Screen.width*0.3f,Screen.width*0.03f), mageText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*1.65f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelMage")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.64f,Screen.width*0.1f,Screen.width*0.1f), mage);
		if (Checkunit.mage == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.715f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.705f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPMage"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.715f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.705f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKMage"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.69f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Magecost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.67f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.79f,Screen.width*0.08f,Screen.height*0.12f),"Ranged unit with high damage and splash attack",smallstyle);
		}
		if(PlayerPrefs.GetInt ("MageMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.77f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupmage == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Magecost")) >= 0 && Checkunit.mage == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelMage") < 19) {
						upgradechange(hpivmage,atkivmage,costmage,"Mage");
						PlayerPrefs.SetInt("LevelMage", PlayerPrefs.GetInt("LevelMage")+ 1);
						PlayerPrefs.SetFloat("ATKMage",PlayerPrefs.GetFloat("ATKMage")+ upatk);
						PlayerPrefs.SetFloat("HPMage",PlayerPrefs.GetFloat("HPMage")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Magecost"));
						PlayerPrefs.SetInt("Magecost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelMage") == 19){
						upgradechange(hpivmage,atkivmage,costmage,"Mage");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Magecost"));
						PlayerPrefs.SetInt("LevelMage", 20);
						PlayerPrefs.SetInt ("MageMax",1);
						PlayerPrefs.SetFloat("ATKMage",PlayerPrefs.GetFloat("ATKMage")+ upatk);
						PlayerPrefs.SetFloat("HPMage",PlayerPrefs.GetFloat("HPMage")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.mage == false) {
						showMageWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.shaman == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.shaman == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("ShamanMax") == 0){
			upgradechange(hpivshaman,atkivshaman,costshaman,"Shaman");
		}
		else{
			upgradechange(0,0,0,"Shaman");
			PlayerPrefs.SetInt("Shamancost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.91f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.94f,Screen.width*0.3f,Screen.width*0.03f), shamanText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*1.95f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelShaman")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.94f,Screen.width*0.1f,Screen.width*0.1f), shaman);
		if (Checkunit.mage == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.015f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.005f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPShaman"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.015f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.005f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKShaman"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.99f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Shamancost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.97f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.09f,Screen.width*0.08f,Screen.height*0.12f),"Support unit that can heal nearby allies",smallstyle);
		}
		if(PlayerPrefs.GetInt ("ShamanMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.07f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupshaman == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Shamancost")) >= 0 && Checkunit.shaman == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelShaman") < 19) {
						upgradechange(hpivshaman,atkivshaman,costshaman,"Shaman");
						PlayerPrefs.SetInt("LevelShaman", PlayerPrefs.GetInt("LevelShaman")+ 1);
						PlayerPrefs.SetFloat("ATKShaman",PlayerPrefs.GetFloat("ATKShaman")+ upatk);
						PlayerPrefs.SetFloat("HPShaman",PlayerPrefs.GetFloat("HPShaman")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Shamancost"));
						PlayerPrefs.SetInt("Shamancost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelShaman") == 19){
						upgradechange(hpivmage,atkivmage,costmage,"Shaman");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Shamancost"));
						PlayerPrefs.SetInt("LevelShaman", 20);
						PlayerPrefs.SetInt ("ShamanMax",1);
						PlayerPrefs.SetFloat("ATKShaman",PlayerPrefs.GetFloat("ATKShaman")+ upatk);
						PlayerPrefs.SetFloat("HPShaman",PlayerPrefs.GetFloat("HPShaman")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.shaman == false) {
						showShamanWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.elephant == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.elephant == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("ElephantryMax") == 0){
			upgradechange(hpivelephant,atkivelephant,costelephant,"Elephantry");
		}
		else{
			upgradechange(0,0,0,"Elephantry");
			PlayerPrefs.SetInt("Elephantrycost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.21f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.24f,Screen.width*0.3f,Screen.width*0.03f), elephantryText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*2.25f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelElephantry")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*2.24f,Screen.width*0.1f,Screen.width*0.1f), elephantry);
		if (Checkunit.mage == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.315f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.305f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPElephantry"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.315f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.305f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKElephantry"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*2.29f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Elephantrycost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*2.27f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.39f,Screen.width*0.08f,Screen.height*0.12f),"High cost melee unit with high defence,",smallstyle);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.43f,Screen.width*0.08f,Screen.height*0.12f),"high damage and knockback attack",smallstyle);
		}
		if(PlayerPrefs.GetInt ("ElephantryMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.37f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockupelephantry == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Elephantrycost")) >= 0 && Checkunit.elephant == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelElephantry") < 19) {
						upgradechange(hpivelephant,atkivelephant,costelephant,"Elephantry");
						PlayerPrefs.SetInt("LevelElephantry", PlayerPrefs.GetInt("LevelElephantry")+ 1);
						PlayerPrefs.SetFloat("ATKElephantry",PlayerPrefs.GetFloat("ATKElephantry")+ upatk);
						PlayerPrefs.SetFloat("HPElephantry",PlayerPrefs.GetFloat("HPElephantry")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Elephantrycost"));
						PlayerPrefs.SetInt("Elephantrycost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelElephantry") == 19){
						upgradechange(hpivmage,atkivmage,costmage,"Elephantry");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Elephantrycost"));
						PlayerPrefs.SetInt("LevelElephantry", 20);
						PlayerPrefs.SetInt ("ElephantryMax",1);
						PlayerPrefs.SetFloat("ATKElephantry",PlayerPrefs.GetFloat("ATKElephantry")+ upatk);
						PlayerPrefs.SetFloat("HPElephantry",PlayerPrefs.GetFloat("HPElephantry")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.elephant == false) {
						showElephantryWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		if (Checkunit.tank == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.tank == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("TankMax") == 0){
			upgradechange(hpivtank,atkivtank,costtank,"Tank");
		}
		else{
			upgradechange(0,0,0,"Tank");
			PlayerPrefs.SetInt("Tankcost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.51f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.54f,Screen.width*0.3f,Screen.width*0.03f), tankText);
		GUI.Label(new Rect(Screen.width*0.53f,Screen.height*2.55f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelTank")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*2.54f,Screen.width*0.1f,Screen.width*0.1f), tank);
		if (Checkunit.mage == true) {
			GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.615f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.605f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
			GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPTank"),style);
			GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
			GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.615f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.605f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
			GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKTank"),style);
			GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
			GUI.Label(new Rect(Screen.width*0.85f,Screen.height*2.59f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Tankcost"),style);
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*2.57f,Screen.width*0.05f,Screen.width*0.04f), t, r);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.69f,Screen.width*0.08f,Screen.height*0.12f),"High cost ranged unit with ultra high defence,",smallstyle);
			GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.73f,Screen.width*0.08f,Screen.height*0.12f),"high damage and splash attack",smallstyle);
		}
		if(PlayerPrefs.GetInt ("TankMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.67f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if(Checkunit.lockuptank == true){

				}
				else if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Tankcost")) >= 0 && Checkunit.tank == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelTank") < 19) {
						upgradechange(hpivtank,atkivtank,costtank,"Tank");
						PlayerPrefs.SetInt("LevelTank", PlayerPrefs.GetInt("LevelTank")+ 1);
						PlayerPrefs.SetFloat("ATKTank",PlayerPrefs.GetFloat("ATKTank")+ upatk);
						PlayerPrefs.SetFloat("HPTank",PlayerPrefs.GetFloat("HPTank")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Tankcost"));
						PlayerPrefs.SetInt("Tankcost",cost);
						Checkunit.playOnce = false;
					}
					else if(PlayerPrefs.GetInt("LevelTank") == 19){
						upgradechange(hpivmage,atkivmage,costmage,"Tank");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Tankcost"));
						PlayerPrefs.SetInt("LevelTank", 20);
						PlayerPrefs.SetInt ("TankMax",1);
						PlayerPrefs.SetFloat("ATKTank",PlayerPrefs.GetFloat("ATKTank")+ upatk);
						PlayerPrefs.SetFloat("HPTank",PlayerPrefs.GetFloat("HPTank")+ uphp);
						Checkunit.playOnce = false;
					}
					UnitUpgrade();
				}
				else {
					if (Checkunit.tank == false) {
						showTankWarning = true;
						showingNotification = true;
					}
					else {
						showGoldNotification = true;
						showingNotification = true;
					}
				}
			}
		}
		GUI.EndScrollView ();
	}

	void HeroUpgrade(){
        scrollPosition = GUI.BeginScrollView(new Rect(Screen.width*scrollViewXPosition,Screen.height*0.12f,Screen.width,Screen.height*0.7f),scrollPosition, new Rect(Screen.width*0.12f,Screen.height*0.1f,130,Screen.height*2.71f)); //0.91
		GUI.skin = skin;
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.11f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.14f,Screen.width*0.3f,Screen.width*0.03f), arjunaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*0.15f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelArjuna")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.15f,Screen.width*0.1f,Screen.width*0.1f), arjuna);
		if(PlayerPrefs.GetInt ("ArjunaMax") == 0){
			upgradechange(hpivarjuna,atkivarjuna,costarjuna,"Arjuna");
		}
		else{
			upgradechange(0,0,0,"Arjuna");
			PlayerPrefs.SetInt("Arjunacost",0);
		}
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.215f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.205f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPArjuna"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.215f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.205f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKArjuna"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.22f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.19f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Arjunacost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.17f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.29f,Screen.width*0.08f,Screen.height*0.12f),"Third son of Pandu with the grace\nfrom god Indra",smallstyle);
		if(PlayerPrefs.GetInt ("ArjunaMax") == 0){
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.25f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Arjunacost")) >= 0 && Checkunit.arjuna == true){
					hasUpgrade = true;
					if (PlayerPrefs.GetInt("LevelArjuna") < 19) {
						upgradechange(hpivarjuna,atkivarjuna,costarjuna,"Arjuna");
						PlayerPrefs.SetInt("LevelArjuna", PlayerPrefs.GetInt("LevelArjuna")+ 1);
						PlayerPrefs.SetFloat("ATKArjuna",PlayerPrefs.GetFloat("ATKArjuna")+ upatk);
						PlayerPrefs.SetFloat("HPArjuna",PlayerPrefs.GetFloat("HPArjuna")+ uphp);
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Arjunacost"));
						PlayerPrefs.SetInt("Arjunacost",cost);
						PlayerPrefs.SetInt("ArjunaUpgrade",1);
						Checkunit.playOnce = false;
					}
					else if (PlayerPrefs.GetInt("LevelArjuna") == 19){
						upgradechange(hpivarjuna,atkivarjuna,costarjuna,"Arjuna");
						PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Arjunacost"));
						PlayerPrefs.SetInt("LevelArjuna", 20);
						PlayerPrefs.SetInt ("ArjunaMax",1);
						PlayerPrefs.SetFloat("ATKArjuna",PlayerPrefs.GetFloat("ATKArjuna")+ upatk);
						PlayerPrefs.SetFloat("HPArjuna",PlayerPrefs.GetFloat("HPArjuna")+ uphp);
						Checkunit.playOnce = false;
					}
					HeroUpgrade();
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.bima == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.bima == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("BimaMax") == 0){
			upgradechange(hpivbima,atkivbima,costbima,"Bima");
		}
		else{
			upgradechange(0,0,0,"Bima");
			PlayerPrefs.SetInt("Bimacost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.41f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.44f,Screen.width*0.3f,Screen.width*0.03f), bimaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*0.45f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelBima")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.45f,Screen.width*0.1f,Screen.width*0.1f), bima);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.515f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.505f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPBima"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.515f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.505f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKBima"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.52f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.49f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Bimacost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.47f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.59f,Screen.width*0.08f,Screen.height*0.12f),"Second son of Pandu with the grace\nfrom god Vayu",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.bima == true) {
			if(PlayerPrefs.GetInt ("BimaMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.55f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Bimacost")) >= 0 && Checkunit.bima == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelBima") < 19) {
							upgradechange(hpivbima,atkivbima,costbima,"Bima");
							PlayerPrefs.SetInt("LevelBima", PlayerPrefs.GetInt("LevelBima")+ 1);
							PlayerPrefs.SetFloat("ATKBima",PlayerPrefs.GetFloat("ATKBima")+ upatk);
							PlayerPrefs.SetFloat("HPBima",PlayerPrefs.GetFloat("HPBima")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Bimacost"));
							PlayerPrefs.SetInt("Bimacost",cost);
							PlayerPrefs.SetInt("BimaUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelBima") == 19){
							upgradechange(hpivbima,atkivbima,costbima,"Bima");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Bimacost"));
							PlayerPrefs.SetInt("LevelBima", 20);
							PlayerPrefs.SetInt ("BimaMax",1);
							PlayerPrefs.SetFloat("ATKBima",PlayerPrefs.GetFloat("ATKBima")+ upatk);
							PlayerPrefs.SetFloat("HPBima",PlayerPrefs.GetFloat("HPBima")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.53f,Screen.width*0.15f,Screen.width*0.1f),rate)) {
				showLikePageNotification = true;
			}
		}
		if (Checkunit.srikandi == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.srikandi == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("SrikandiMax") == 0){
			upgradechange(hpivsrikandi,atkivsrikandi,costsrikandi,"Srikandi");
		}
		else{
			upgradechange(0,0,0,"Srikandi");
			PlayerPrefs.SetInt("Srikandicost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*0.71f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*0.74f,Screen.width*0.3f,Screen.width*0.03f), srikandiText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*0.75f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelSrikandi")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*0.75f,Screen.width*0.1f,Screen.width*0.1f), srikandi);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*0.815f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*0.805f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPSrikandi"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*0.815f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*0.805f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKSrikandi"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*0.82f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*0.79f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Srikandicost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*0.77f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*0.89f,Screen.width*0.08f,Screen.height*0.12f),"Reincarnation of Amba, she has sworn\nto kill Bheesma with her own hands",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.srikandi == true) {
			if(PlayerPrefs.GetInt ("SrikandiMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.85f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Srikandicost")) >= 0 && Checkunit.srikandi == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelSrikandi") < 19) {
							upgradechange(hpivsrikandi,atkivsrikandi,costsrikandi,"Srikandi");
							PlayerPrefs.SetInt("LevelSrikandi", PlayerPrefs.GetInt("LevelSrikandi")+ 1);
							PlayerPrefs.SetFloat("ATKSrikandi",PlayerPrefs.GetFloat("ATKSrikandi")+ upatk);
							PlayerPrefs.SetFloat("HPSrikandi",PlayerPrefs.GetFloat("HPSrikandi")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Srikandicost"));
							PlayerPrefs.SetInt("Srikandicost",cost);
							PlayerPrefs.SetInt("SrikandiUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelSrikandi") == 19){
							upgradechange(hpivsrikandi,atkivsrikandi,costsrikandi,"Srikandi");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Srikandicost"));
							PlayerPrefs.SetInt("LevelSrikandi", 20);
							PlayerPrefs.SetInt ("SrikandiMax",1);
							PlayerPrefs.SetFloat("ATKSrikandi",PlayerPrefs.GetFloat("ATKSrikandi")+ upatk);
							PlayerPrefs.SetFloat("HPSrikandi",PlayerPrefs.GetFloat("HPSrikandi")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*0.83f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Srikandicost")) >= 0 && Checkunit.srikandi == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockSrikandi",1);
					PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Srikandicost"));
					PlayerPrefs.SetInt("Srikandicost",150);
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.yudhistira == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.yudhistira == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("YudhistiraMax") == 0){
			upgradechange(hpivyudhistira,atkivyudhistira,costyudhistira,"Yudhistira");
		}
		else{
			upgradechange(0,0,0,"Yudhistira");
			PlayerPrefs.SetInt("Yudhistiracost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.01f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.04f,Screen.width*0.3f,Screen.width*0.03f), yudhistiraText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*1.05f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelYudhistira")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.05f,Screen.width*0.1f,Screen.width*0.1f), yudhistira);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.115f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.105f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPYudhistira"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.115f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.105f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKYudhistira"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.12f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.09f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Yudhistiracost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.07f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.19f,Screen.width*1.08f,Screen.height*0.12f),"First Son of Pandu with the grace\nfrom God Dharma.",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.yudhistira == true) {
			if(PlayerPrefs.GetInt ("YudhistiraMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.15f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Yudhistiracost")) >= 0 && Checkunit.yudhistira == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelYudhistira") < 19) {
							upgradechange(hpivyudhistira,atkivyudhistira,costyudhistira,"Yudhistira");
							PlayerPrefs.SetInt("LevelYudhistira", PlayerPrefs.GetInt("LevelYudhistira")+ 1);
							PlayerPrefs.SetFloat("ATKYudhistira",PlayerPrefs.GetFloat("ATKYudhistira")+ upatk);
							PlayerPrefs.SetFloat("HPYudhistira",PlayerPrefs.GetFloat("HPYudhistira")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Yudhistiracost"));
							PlayerPrefs.SetInt("Yudhistiracost",cost);
							PlayerPrefs.SetInt("YudhistiraUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelYudhistira") == 19){
							upgradechange(hpivyudhistira,atkivyudhistira,costyudhistira,"Yudhistira");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Yudhistiracost"));
							PlayerPrefs.SetInt("LevelYudhistira", 20);
							PlayerPrefs.SetInt ("YudhistiraMax",1);
							PlayerPrefs.SetFloat("ATKYudhistira",PlayerPrefs.GetFloat("ATKYudhistira")+ upatk);
							PlayerPrefs.SetFloat("HPYudhistira",PlayerPrefs.GetFloat("HPYudhistira")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.13f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Yudhistiracost")) >= 0 && Checkunit.yudhistira == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockYudhistira",1);
					PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Yudhistiracost"));
					PlayerPrefs.SetInt("Yudhistiracost",150);
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.nakulasadewa == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.nakulasadewa == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("NakulaSadewaMax") == 0){
			upgradechange(hpivnakulasadewa,atkivnakulasadewa,costnakulasadewa,"NakulaSadewa");
		}
		else{
			upgradechange(0,0,0,"NakulaSadewa");
			PlayerPrefs.SetInt("NakulaSadewacost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.31f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.34f,Screen.width*0.3f,Screen.width*0.03f), nakulasadewaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*1.35f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelNakulaSadewa")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.35f,Screen.width*0.1f,Screen.width*0.1f), nakulasadewa);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.415f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.405f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPNakulaSadewa"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.415f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.405f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKNakulaSadewa"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.42f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.39f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("NakulaSadewacost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.37f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.49f,Screen.width*1.08f,Screen.height*0.12f),"Youngest Sons of Pandu. Twin Brother with the grace\nfrom God Ashvins.",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.nakulasadewa == true) {
			if(PlayerPrefs.GetInt ("NakulaSadewaMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.45f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("NakulaSadewacost")) >= 0 && Checkunit.nakulasadewa == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelNakulaSadewa") < 19) {
							upgradechange(hpivnakulasadewa,atkivnakulasadewa,costnakulasadewa,"NakulaSadewa");
							PlayerPrefs.SetInt("LevelNakulaSadewa", PlayerPrefs.GetInt("LevelNakulaSadewa")+ 1);
							PlayerPrefs.SetFloat("ATKNakulaSadewa",PlayerPrefs.GetFloat("ATKNakulaSadewa")+ upatk);
							PlayerPrefs.SetFloat("HPNakulaSadewa",PlayerPrefs.GetFloat("HPNakulaSadewa")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("NakulaSadewacost"));
							PlayerPrefs.SetInt("NakulaSadewacost",cost);
							PlayerPrefs.SetInt("NakulaSadewaUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelNakulaSadewa") == 19){
							upgradechange(hpivnakulasadewa,atkivnakulasadewa,costnakulasadewa,"NakulaSadewa");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("NakulaSadewacost"));
							PlayerPrefs.SetInt("LevelNakulaSadewa", 20);
							PlayerPrefs.SetInt ("NakulaSadewaMax",1);
							PlayerPrefs.SetFloat("ATKNakulaSadewa",PlayerPrefs.GetFloat("ATKNakulaSadewa")+ upatk);
							PlayerPrefs.SetFloat("HPNakulaSadewa",PlayerPrefs.GetFloat("HPNakulaSadewa")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.43f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("NakulaSadewacost")) >= 0 && Checkunit.nakulasadewa == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockNakulaSadewa",1);
					PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("NakulaSadewacost"));
					PlayerPrefs.SetInt("NakulaSadewacost",150);
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.gatotkaca == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.gatotkaca == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("GatotKacaMax") == 0){
			upgradechange(hpivgatotkaca,atkivgatotkaca,costgatotkaca,"GatotKaca");
		}
		else{
			upgradechange(0,0,0,"GatotKaca");
			PlayerPrefs.SetInt("GatotKacacost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.61f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.64f,Screen.width*0.3f,Screen.width*0.03f), gatotkacaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*1.65f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelGatotKaca")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.65f,Screen.width*0.1f,Screen.width*0.1f), gatotkaca);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*1.715f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*1.705f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPGatotKaca"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*1.715f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*1.705f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKGatotKaca"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*1.72f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.69f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("GatotKacacost"),style);
		if (PlayerPrefs.GetInt ("LockGatotKaca") == 0) {
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.67f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);	
		}
		else {
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.67f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		}
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*1.79f,Screen.width*1.08f,Screen.height*0.12f),"Second son of Bima. Many people call him\nWire Muscle and Iron Bone",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.gatotkaca == true) {
			if(PlayerPrefs.GetInt ("GatotKacaMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.75f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("GatotKacacost")) >= 0 && Checkunit.gatotkaca == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelGatotKaca") < 19) {
							upgradechange(hpivgatotkaca,atkivgatotkaca,costgatotkaca,"GatotKaca");
							PlayerPrefs.SetInt("LevelGatotKaca", PlayerPrefs.GetInt("LevelGatotKaca")+ 1);
							PlayerPrefs.SetFloat("ATKGatotKaca",PlayerPrefs.GetFloat("ATKGatotKaca")+ upatk);
							PlayerPrefs.SetFloat("HPGatotKaca",PlayerPrefs.GetFloat("HPGatotKaca")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("GatotKacacost"));
							PlayerPrefs.SetInt("GatotKacacost",cost);
							PlayerPrefs.SetInt("GatotKacaUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelGatotKaca") == 19){
							upgradechange(hpivgatotkaca,atkivgatotkaca,costgatotkaca,"GatotKaca");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("GatotKacacost"));
							PlayerPrefs.SetInt("LevelGatotKaca", 20);
							PlayerPrefs.SetInt ("GatotKacaMax",1);
							PlayerPrefs.SetFloat("ATKGatotKaca",PlayerPrefs.GetFloat("ATKGatotKaca")+ upatk);
							PlayerPrefs.SetFloat("HPGatotKaca",PlayerPrefs.GetFloat("HPGatotKaca")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*1.73f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("GatotKacacost")) >= 0 && Checkunit.gatotkaca == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockGatotKaca",1);
					//DBManager.IncreaseFunds(currency,-PlayerPrefs.GetInt("GatotKacacost"));
					//PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("GatotKacacost"));
					PlayerPrefs.SetInt("GatotKacacost",150);
					while (PlayerPrefs.GetInt("LevelGatotKaca") < 10) {
						upgradechange(hpivgatotkaca,atkivgatotkaca,costgatotkaca,"GatotKaca");
						PlayerPrefs.SetInt("LevelGatotKaca", PlayerPrefs.GetInt("LevelGatotKaca")+ 1);
						PlayerPrefs.SetFloat("ATKGatotKaca",PlayerPrefs.GetFloat("ATKGatotKaca")+ upatk);
						PlayerPrefs.SetFloat("HPGatotKaca",PlayerPrefs.GetFloat("HPGatotKaca")+ uphp);
						//PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("GatotKacacost"));
						PlayerPrefs.SetInt("GatotKacacost",cost);
						PlayerPrefs.SetInt("GatotKacaUpgrade",1);
					}
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGemNotification = true;
				}
			}
		}
		if (Checkunit.drupada == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.drupada == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("DrupadaMax") == 0){
			upgradechange(hpivdrupada,atkivdrupada,costdrupada,"Drupada");
		}
		else{
			upgradechange(0,0,0,"Drupada");
			PlayerPrefs.SetInt("Drupadacost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*1.91f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*1.94f,Screen.width*0.3f,Screen.width*0.03f), drupadaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*1.95f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelDrupada")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*1.95f,Screen.width*0.1f,Screen.width*0.1f), drupada);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.015f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.005f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPDrupada"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.015f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.005f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKDrupada"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.02f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*1.99f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Drupadacost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*1.97f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.09f,Screen.width*1.08f,Screen.height*0.12f),"King of Panchala and father of\nShikhandi, Dhrishtadyumna and Draupadi.",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.drupada == true) {
			if(PlayerPrefs.GetInt ("DrupadaMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.05f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Drupadacost")) >= 0 && Checkunit.drupada == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelDrupada") < 19) {
							upgradechange(hpivdrupada,atkivdrupada,costdrupada,"Drupada");
							PlayerPrefs.SetInt("LevelDrupada", PlayerPrefs.GetInt("LevelDrupada")+ 1);
							PlayerPrefs.SetFloat("ATKDrupada",PlayerPrefs.GetFloat("ATKDrupada")+ upatk);
							PlayerPrefs.SetFloat("HPDrupada",PlayerPrefs.GetFloat("HPDrupada")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Drupadacost"));
							PlayerPrefs.SetInt("Drupadacost",cost);
							PlayerPrefs.SetInt("DrupadaUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelDrupada") == 19){
							upgradechange(hpivdrupada,atkivdrupada,costdrupada,"Drupada");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Drupadacost"));
							PlayerPrefs.SetInt("LevelDrupada", 20);
							PlayerPrefs.SetInt ("DrupadaMax",1);
							PlayerPrefs.SetFloat("ATKDrupada",PlayerPrefs.GetFloat("ATKDrupada")+ upatk);
							PlayerPrefs.SetFloat("HPDrupada",PlayerPrefs.GetFloat("HPDrupada")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.03f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Drupadacost")) >= 0 && Checkunit.drupada == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockDrupada",1);
					PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Drupadacost"));
					PlayerPrefs.SetInt("Drupadacost",150);
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.kresna == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.kresna == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("KresnaMax") == 0){
			upgradechange(hpivkresna,atkivkresna,costkresna,"Kresna");
		}
		else{
			upgradechange(0,0,0,"Kresna");
			PlayerPrefs.SetInt("Kresnacost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.21f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.24f,Screen.width*0.3f,Screen.width*0.03f), kresnaText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*2.25f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelKresna")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*2.25f,Screen.width*0.1f,Screen.width*0.1f), kresna);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.315f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.305f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPKresna"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.315f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.305f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKKresna"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.32f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*2.29f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Kresnacost"),style);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*2.27f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.39f,Screen.width*1.08f,Screen.height*0.12f),"Pandava strategist in Kurukshetra war.\nIncarnation of God Vishnu.",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.kresna == true) {
			if(PlayerPrefs.GetInt ("KresnaMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.35f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Kresnacost")) >= 0 && Checkunit.kresna == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelKresna") < 19) {
							upgradechange(hpivkresna,atkivkresna,costkresna,"Kresna");
							PlayerPrefs.SetInt("LevelKresna", PlayerPrefs.GetInt("LevelKresna")+ 1);
							PlayerPrefs.SetFloat("ATKKresna",PlayerPrefs.GetFloat("ATKKresna")+ upatk);
							PlayerPrefs.SetFloat("HPKresna",PlayerPrefs.GetFloat("HPKresna")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Kresnacost"));
							PlayerPrefs.SetInt("Kresnacost",cost);
							PlayerPrefs.SetInt("KresnaUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelKresna") == 19){
							upgradechange(hpivkresna,atkivkresna,costkresna,"Kresna");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Kresnacost"));
							PlayerPrefs.SetInt("LevelKresna", 20);
							PlayerPrefs.SetInt ("KresnaMax",1);
							PlayerPrefs.SetFloat("ATKKresna",PlayerPrefs.GetFloat("ATKKresna")+ upatk);
							PlayerPrefs.SetFloat("HPKresna",PlayerPrefs.GetFloat("HPKresna")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.33f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Kresnacost")) >= 0 && Checkunit.kresna == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockKresna",1);
					PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Kresnacost"));
					PlayerPrefs.SetInt("Kresnacost",150);
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGoldNotification = true;
				}
			}
		}
		if (Checkunit.hanoman == true) {
			GUI.color = Color.white;
		}
		else if(Checkunit.hanoman == false){
			GUI.color = Color.gray;
		}
		if(PlayerPrefs.GetInt ("HanomanMax") == 0){
			upgradechange(hpivhanoman,atkivhanoman,costhanoman,"Hanoman");
		}
		else{
			upgradechange(0,0,0,"Hanoman");
			PlayerPrefs.SetInt("Hanomancost",0);
		}
		GUI.Box(new Rect(Screen.width*0.16f,Screen.height*2.51f,Screen.width*0.8f,Screen.height*0.3f)," ");
		GUI.DrawTexture (new Rect(Screen.width*0.31f,Screen.height*2.54f,Screen.width*0.3f,Screen.width*0.03f), hanomanText);
		GUI.Label(new Rect(Screen.width*0.61f,Screen.height*2.55f,Screen.width*0.08f,Screen.height*0.12f),"(Lv. "+PlayerPrefs.GetInt("LevelHanoman")+"/20)",style);
		GUI.DrawTexture (new Rect(Screen.width*0.185f,Screen.height*2.55f,Screen.width*0.1f,Screen.width*0.1f), hanoman);
		GUI.DrawTexture (new Rect(Screen.width*0.349f,Screen.height*2.615f,Screen.width*0.1f,Screen.width*0.026f), emptyhealth);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.31f,Screen.height*2.605f,Screen.width*0.048f,Screen.width*0.048f), t2, r2);
		GUI.Label(new Rect(Screen.width*0.359f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("HPHanoman"),style);
		GUI.Label(new Rect(Screen.width*0.445f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),"+"+uphp,style);
		GUI.DrawTexture (new Rect(Screen.width*0.569f,Screen.height*2.615f,Screen.width*0.1f,Screen.width*0.026f), emptyatk);
		GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.53f,Screen.height*2.605f,Screen.width*0.048f,Screen.width*0.048f), t3, r3);
		GUI.Label(new Rect(Screen.width*0.578f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),""+PlayerPrefs.GetFloat("ATKHanoman"),style);
		GUI.Label(new Rect(Screen.width*0.67f,Screen.height*2.62f,Screen.width*0.08f,Screen.height*0.1f),"+"+upatk,style);
		GUI.Label(new Rect(Screen.width*0.85f,Screen.height*2.59f,Screen.width*0.08f,Screen.height*0.3f),""+PlayerPrefs.GetInt("Hanomancost"),style);
		if (PlayerPrefs.GetInt ("LockHanoman") == 0) {
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*2.57f,Screen.width*0.03f,Screen.width*0.05f), t1, r1);	
		}
		else {
			GUI.DrawTextureWithTexCoords (new Rect(Screen.width*0.8f,Screen.height*2.57f,Screen.width*0.05f,Screen.width*0.04f), t, r);
		}
		GUI.Label(new Rect(Screen.width*0.32f,Screen.height*2.69f,Screen.width*1.08f,Screen.height*0.12f),"A vanara that help Rama's war againts Ravana.\nIn Mahabharata, he helps strengthen Arjuna's chariot",smallstyle);
		GUI.color = Color.white;
		if (Checkunit.hanoman == true) {
			if(PlayerPrefs.GetInt ("HanomanMax") == 0){
				if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.65f,Screen.width*0.15f,Screen.width*0.055f),upgrade) && showingNotification == false) {
					if((PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Hanomancost")) >= 0 && Checkunit.hanoman == true){
						hasUpgrade = true;
						if (PlayerPrefs.GetInt("LevelHanoman") < 19) {
							upgradechange(hpivhanoman,atkivhanoman,costhanoman,"Hanoman");
							PlayerPrefs.SetInt("LevelHanoman", PlayerPrefs.GetInt("LevelHanoman")+ 1);
							PlayerPrefs.SetFloat("ATKHanoman",PlayerPrefs.GetFloat("ATKHanoman")+ upatk);
							PlayerPrefs.SetFloat("HPHanoman",PlayerPrefs.GetFloat("HPHanoman")+ uphp);
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Hanomancost"));
							PlayerPrefs.SetInt("Hanomancost",cost);
							PlayerPrefs.SetInt("HanomanUpgrade",1);
						}
						else if (PlayerPrefs.GetInt("LevelHanoman") == 19){
							upgradechange(hpivhanoman,atkivhanoman,costhanoman,"Hanoman");
							PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Hanomancost"));
							PlayerPrefs.SetInt("LevelHanoman", 20);
							PlayerPrefs.SetInt ("HanomanMax",1);
							PlayerPrefs.SetFloat("ATKHanoman",PlayerPrefs.GetFloat("ATKHanoman")+ upatk);
							PlayerPrefs.SetFloat("HPHanoman",PlayerPrefs.GetFloat("HPHanoman")+ uphp);
						}
						HeroUpgrade();
					}
					else {
						showGoldNotification = true;
					}
				}
			}
		}
		else{
			if (GUI.Button(new Rect(Screen.width*0.78f,Screen.height*2.63f,Screen.width*0.15f,Screen.width*0.1f),buy)) {
				if((PlayerPrefs.GetInt("Hanomancost")) >= 0 && Checkunit.hanoman == false){
					hasUpgrade = true;
					PlayerPrefs.SetInt("LockHanoman",1);
					//DBManager.IncreaseFunds(currency,-PlayerPrefs.GetInt("Hanomancost"));
					//PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Hanomancost"));
					PlayerPrefs.SetInt("Hanomancost",150);
					while (PlayerPrefs.GetInt("LevelHanoman") < 10) {
						upgradechange(hpivhanoman,atkivhanoman,costhanoman,"Hanoman");
						PlayerPrefs.SetInt("LevelHanoman", PlayerPrefs.GetInt("LevelHanoman")+ 1);
						PlayerPrefs.SetFloat("ATKHanoman",PlayerPrefs.GetFloat("ATKHanoman")+ upatk);
						PlayerPrefs.SetFloat("HPHanoman",PlayerPrefs.GetFloat("HPHanoman")+ uphp);
						//PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold")-PlayerPrefs.GetInt("Hanomancost"));
						PlayerPrefs.SetInt("Hanomancost",cost);
						PlayerPrefs.SetInt("HanomanUpgrade",1);
					}
					HeroUpgrade();
					Checkunit.playOnce = false;
				}
				else {
					showGemNotification = true;
				}
			}
		}
		GUI.EndScrollView ();
	}
	void ShowGoldNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.325f,Screen.height*0.325f,Screen.width*0.35f,Screen.height*0.15f), GoldWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.51f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showGoldNotification = false;
			showingNotification = false;
		}
	}
	void ShowGemNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.325f,Screen.height*0.325f,Screen.width*0.35f,Screen.height*0.15f), GemWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.51f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showGemNotification = false;
			showingNotification = false;
		}
	}
	void ShowLikePageNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.325f,Screen.height*0.325f,Screen.width*0.35f,Screen.height*0.15f), LikePageWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.51f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showLikePageNotification = false;
			showingNotification = false;
			hasUpgrade = true;
			PlayerPrefs.SetInt("LockBima",1);
			PlayerPrefs.SetInt("Bimacost",150);
			HeroUpgrade();
			Checkunit.playOnce = false;
			Application.OpenURL("https://www.facebook.com/MahabharatWars");
		}
	}
	void ShowGameSpeedNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.26f,Screen.height*0.3f,Screen.width*0.48f,Screen.height*0.175f), GameSpeedWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.51f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showGameSpeedNotification = false;
			showingNotification = false;
		}
	}
	void ShowArcherNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), ArcherWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showArcherWarning = false;
			showingNotification = false;
		}
	}
	void ShowSwordmanNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), SwordmanWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showSwordmanWarning = false;
			showingNotification = false;
		}
	}
	void ShowGunnerNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), GunnerWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showGunnerWarning = false;
			showingNotification = false;
		}
	}
	void ShowSpearmanNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), SpearmanWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showSpearmanWarning = false;
			showingNotification = false;
		}
	}
	void ShowMageNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), MageWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showMageWarning = false;
			showingNotification = false;
		}
	}
	void ShowShamanNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), ShamanWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showShamanWarning = false;
			showingNotification = false;
		}
	}
	void ShowElephantryNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), ElephantryWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showElephantryWarning = false;
			showingNotification = false;
		}
	}
	void ShowTankNotif() {
		GUI.skin = skin2;
		GUI.color = Color.white;
		GUI.Box(new Rect(Screen.width*0.25f,Screen.height*0.25f,Screen.width*0.5f,Screen.height*0.5f),"");
		GUI.DrawTexture (new Rect(Screen.width*0.34f,Screen.height*0.275f,Screen.width*0.325f,Screen.height*0.225f), TankWarning);
		if (GUI.Button(new Rect(Screen.width*0.425f,Screen.height*0.54f,Screen.width*0.15f,Screen.height*0.15f),OKButton)) {
			showTankWarning = false;
			showingNotification = false;
		}
	}

	void upgradechange(int hpch,int atkch, int costch, string name){
		uphp = PlayerPrefs.GetInt ("HP"+name) + ((PlayerPrefs.GetInt ("Level" + name))*hpch);
		upatk = PlayerPrefs.GetInt ("ATK"+name) + ((PlayerPrefs.GetInt ("Level" + name))*atkch);
		cost = PlayerPrefs.GetInt (name + "cost") + ((PlayerPrefs.GetInt ("Level" + name)+2)*costch);
	}

	void Scaling() {
		if (Camera.main.aspect >= 1.7) {
			//16 : 9
			scrollViewXPosition = 0.12f;
			Container.transform.localScale = new Vector3 (0.98f, 0.73f, 0);
		}
		else if (Camera.main.aspect >= 1.5) {
			//16 : 10
			scrollViewXPosition = 0.14f;
			Container.transform.localScale = new Vector3 (0.87f, 0.73f, 0);
			Container.transform.position -= new Vector3 (0.44f, 0, 0);
		}
		else if (Camera.main.aspect >= 1.3){
			//4 : 3
			scrollViewXPosition = 0.16f;
		}
	}
}
