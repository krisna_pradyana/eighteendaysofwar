﻿using UnityEngine;
using System.Collections;

public class ConfirmClick : MonoBehaviour {
    
	private int gemIncrease, goldIncrease, energyIncrease;
	private int goldpack1 = 1000, goldpack2 = 6000, goldpack3 = 15000, goldpack4 = 35000;
	private int energypack1 = 40, energypack2 = 100, energypack3 = 120;
	private GemSystem gemSystem;
	private GoldSystem goldSystem;
	private RegenEnergy regenEnergy;
	public GameObject buyPopupUI, gemPopupUI;
	private GameObject square;
	public AudioClip buttonSound;
	// Use this for initialization
	void Start () {
		gemSystem = GameObject.Find ("GemSystem").GetComponent<GemSystem> ();
		goldSystem = GameObject.Find ("GoldSystem").GetComponent<GoldSystem> ();
		regenEnergy = GameObject.Find ("EnergyText").GetComponent<RegenEnergy> ();
		//square = GameObject.Find ("Square2");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown () {
		AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
		if (gameObject.name == "Buy Container" && Application.loadedLevelName == "Purchase Gold") {
			WarningClick.stillShowingWarnings = false;
			if (GoldGemClick.hasBuyPack1 == true) {
				gemSystem.SendMessage ("DecreaseGem", 1);
				goldSystem.SendMessage ("IncreaseGold", goldpack1);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack1 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack2 == true) {
				gemSystem.SendMessage ("DecreaseGem", 5);
				goldSystem.SendMessage ("IncreaseGold", goldpack2);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack2 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack3 == true) {
				gemSystem.SendMessage ("DecreaseGem", 10);
				goldSystem.SendMessage ("IncreaseGold", goldpack3);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack3 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack4 == true) {
				gemSystem.SendMessage ("DecreaseGem", 20);
				goldSystem.SendMessage ("IncreaseGold", goldpack4);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack4 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		else if (gameObject.name == "Buy Container" && Application.loadedLevelName == "Purchase Energy") {
			WarningClick.stillShowingWarnings = false;
			if (GoldGemClick.hasBuyPack2 == true) {
				gemSystem.SendMessage ("DecreaseGem", 1);
				regenEnergy.SendMessage ("IncreaseEnergy", energypack1);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack1 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack3 == true) {
				gemSystem.SendMessage ("DecreaseGem", 2);
				regenEnergy.SendMessage ("IncreaseEnergy", energypack2);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack2 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack4 == true) {
				gemSystem.SendMessage ("DecreaseGem", 3);
				regenEnergy.SendMessage ("IncreaseEnergy", energypack3);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack3 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		else if (gameObject.name == "Buy Container" && Application.loadedLevelName == "Purchase Gem") {
			WarningClick.stillShowingWarnings = false;
			if (GoldGemClick.hasBuyPack1 == true) {
				goldSystem.SendMessage ("DecreaseGold", 10);
				gemSystem.SendMessage ("IncreaseGem", 100);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack1 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack2 == true) {
				goldSystem.SendMessage ("DecreaseGold", 25);
				gemSystem.SendMessage ("IncreaseGem", 250);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack2 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack3 == true) {
				goldSystem.SendMessage ("DecreaseGold", 50);
				gemSystem.SendMessage ("IncreaseGem", 500);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack3 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (GoldGemClick.hasBuyPack4 == true) {
				goldSystem.SendMessage ("DecreaseGold", 100);
				gemSystem.SendMessage ("IncreaseGem", 1000);
				buyPopupUI.SetActive (false);
				GoldGemClick.hasBuyPack4 = false;
				//square.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		else if (gameObject.name == "Gem Container") {
			WarningClick.stillShowingWarnings = false;
			Application.LoadLevel ("Purchase Gem");
		}
		else if (gameObject.name == "Cancel Container") {
			WarningClick.stillShowingWarnings = false;
			buyPopupUI.SetActive (false);
			//square.GetComponent<SpriteRenderer>().enabled = false;
			gemPopupUI.SetActive (false);

		}
	}
}
