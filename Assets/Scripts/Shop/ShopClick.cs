﻿using UnityEngine;
using System.Collections;

public class ShopClick : MonoBehaviour {
	public string scene;
	public bool isHero;
	public bool isUnit;
	public bool isItem;
	public static bool clickHero;
	public static bool clickUnit;
	public static bool clickItem;
	public static bool reset = false;
	public Sprite containerNonActive, containerActive;
	private int clearStage6, clearStage12, clearStage18, clearStage24, clearStage30, clearStage36, clearStage42, clearStage48;
	// Use this for initialization
	void Start () {
		clickHero = false;
		clickUnit = true;
		clickItem = false;
		clearStage6 = PlayerPrefs.GetInt ("ClearStage6");
		clearStage12 = PlayerPrefs.GetInt ("ClearStage12");
		clearStage18 = PlayerPrefs.GetInt ("ClearStage18");
		clearStage24 = PlayerPrefs.GetInt ("ClearStage24");
		clearStage30 = PlayerPrefs.GetInt ("ClearStage30");
		clearStage36 = PlayerPrefs.GetInt ("ClearStage36");
		clearStage42 = PlayerPrefs.GetInt ("ClearStage42");
		clearStage48 = PlayerPrefs.GetInt ("ClearStage48");
	}
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			if (Application.loadedLevelName == "Shop") {
				Application.LoadLevel ("Menu");
			}
			else if (Application.loadedLevelName == "Purchase Gem" || Application.loadedLevelName == "Purchase Gold" || Application.loadedLevelName == "Purchase Energy") {
				if (clearStage6 == 1) {
					GetComponent<AudioSource>().Play ();
					Application.LoadLevel ("Stage Selection2");
				}
				if (clearStage12 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection3");
				}
				if (clearStage18 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection4");
				}
				if (clearStage24 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection5");
				}
				if (clearStage30 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection6");
				}
				if (clearStage36 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection7");
				}
				if (clearStage42 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection8");
				}
				if (clearStage48 == 1) {
					GetComponent<AudioSource>().Play();
					Application.LoadLevel ("Stage Selection9");
				}
				else {
					GetComponent<AudioSource>().Play ();
					Application.LoadLevel ("Stage Selection");
				}
			}
		}
	}
	// Update is called once per frame
	void OnMouseDown () {
		if (isHero == true) {
			GetComponent<AudioSource>().Play ();
			clickHero = true;
			clickUnit = false;
			clickItem = false;
			reset = true;
			if(gameObject.name == "ContainerHero") {
				gameObject.GetComponent<SpriteRenderer>().sprite = containerActive;
				GameObject.Find("ContainerHero/ContainerUnit").GetComponent<SpriteRenderer>().sprite = containerNonActive;
				GameObject.Find("ContainerHero/ContainerItem").GetComponent<SpriteRenderer>().sprite = containerNonActive;
			}
		}
		else if (isUnit == true) {
			GetComponent<AudioSource>().Play ();
			clickUnit = true;
			clickHero = false;
			clickItem = false;
			reset = true;
			if(gameObject.name == "ContainerUnit") {
				gameObject.GetComponent<SpriteRenderer>().sprite = containerActive;
				GameObject.Find("ContainerHero").GetComponent<SpriteRenderer>().sprite = containerNonActive;
				GameObject.Find("ContainerHero/ContainerItem").GetComponent<SpriteRenderer>().sprite = containerNonActive;
			}	
		}
		else if (isItem == true) {
			GetComponent<AudioSource>().Play ();
			clickItem = true;
			clickHero = false;
			clickUnit = false;
			reset = true;
			if(gameObject.name == "ContainerItem") {
				gameObject.GetComponent<SpriteRenderer>().sprite = containerActive;
				GameObject.Find("ContainerHero").GetComponent<SpriteRenderer>().sprite = containerNonActive;
				GameObject.Find("ContainerHero/ContainerUnit").GetComponent<SpriteRenderer>().sprite = containerNonActive;
			}	
		}
		else if (gameObject.name == "ContainerNext") {
			if (clearStage6 == 1) {
				GetComponent<AudioSource>().Play ();
				Application.LoadLevel ("Stage Selection2");
			}
			if (clearStage12 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection3");
			}
			if (clearStage18 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection4");
			}
			if (clearStage24 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection5");
			}
			if (clearStage30 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection6");
			}
			if (clearStage36 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection7");
			}
			if (clearStage42 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection8");
			}
			if (clearStage48 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection9");
			}
			else {
				GetComponent<AudioSource>().Play ();
				Application.LoadLevel ("Stage Selection");
			}
		}
		else if (gameObject.name == "ContainerBack" && (Application.loadedLevelName == "Purchase Gem" || Application.loadedLevelName == "Purchase Gold" || Application.loadedLevelName == "Purchase Energy") ) {
			if (clearStage6 == 1) {
				GetComponent<AudioSource>().Play ();
				Application.LoadLevel ("Stage Selection2");
			}
			if (clearStage12 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection3");
			}
			if (clearStage18 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection4");
			}
			if (clearStage24 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection5");
			}
			if (clearStage30 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection6");
			}
			if (clearStage36 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection7");
			}
			if (clearStage42 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection8");
			}
			if (clearStage48 == 1) {
				GetComponent<AudioSource>().Play();
				Application.LoadLevel ("Stage Selection9");
			}
			else {
				GetComponent<AudioSource>().Play ();
				Application.LoadLevel ("Stage Selection");
			}
		}
		else {
			GetComponent<AudioSource>().Play ();
			Application.LoadLevel (scene);
		}

	}

}
