﻿using UnityEngine;
using System.Collections;
using System;
//using System.Timers;
public class RegenEnergy : MonoBehaviour {
	//private static System.Timers.Timer aTimer;
	DateTime oldDate;
	DateTime currentDate;
	DateTime lastDate;
	
	public int maxEnergy;
	private int energyGiven;
	public int energy;
	//private int energyTick;
	public static int secondTimePlaying;
	private GameObject[] energyArray;
	//private ELANNotification notification;
	void Awake () {
		//notification = GameObject.Find ("Notification").GetComponent<ELANNotification> ();
		energyArray = GameObject.FindGameObjectsWithTag ("Energy");
		if (energyArray.Length <= 1) {
			DontDestroyOnLoad(gameObject);	
		}
		else {
			DestroyObject(gameObject);
		}
	}
	void Start () 
	{
        /*
		ELANManager.CancelAllNotifications ();
		if (notification != null) {
			notification.cancel();
		}
		notification.title = "Full Energy";
		notification.message = "Your energy has full! Come play Mahabharat War!";
		notification.delayTypeTime = EnumTimeType.Hours;
		notification.delay = 4;
		secondTimePlaying = PlayerPrefs.GetInt ("SecondTimePlaying");

		if (secondTimePlaying == 0) {
			if(PlayerPrefs.GetInt("Energy")== 0 && PlayerPrefs.GetInt("ClearStage1") == 0){
				energy = 120;
				energyGiven = 1;
				PlayerPrefs.SetInt ("Energy", energy);
			}
			else{
				energy = PlayerPrefs.GetInt("Energy");
			}
			//energyTick = energy;
			oldDate = System.DateTime.Now;
		}
		else {
			energy = PlayerPrefs.GetInt("Energy");
			energyGiven = 1;
			lastDate = Convert.ToDateTime(PlayerPrefs.GetString ("CloseTime"));
			oldDate = System.DateTime.Now;
			TimeSpan span = oldDate.Subtract ( lastDate );
			int totalenergy = (int)span.TotalSeconds / 120;
			energy += totalenergy;
			//energyTick = energy;
			if (energy >= maxEnergy) {
				energy = maxEnergy;
			}
			if (energy <= 0) {
				energy = 0;
			}
			PlayerPrefs.SetInt ("Energy", energy);
			if (Application.loadedLevelName == "Survival Mode" || Application.loadedLevelName == "Stage Selection" || Application.loadedLevelName == "Stage Selection2" || Application.loadedLevelName == "Stage Selection3" || 
			    Application.loadedLevelName == "Stage Selection4" || Application.loadedLevelName == "Stage Selection5" || Application.loadedLevelName == "Stage Selection6" ||
			    Application.loadedLevelName == "Stage Selection7" || Application.loadedLevelName == "Stage Selection8" || Application.loadedLevelName == "Stage Selection9" ||
			    Application.loadedLevelName == "Purchase Energy") {
				GameObject.Find("ContainerNext/Energy Container/EnergyText").GetComponent<TextMesh>().text = energy.ToString ()+"/"+maxEnergy;	
			}
		}
		/*aTimer = new System.Timers.Timer(5 * 1000);
		aTimer.Elapsed += new ElapsedEventHandler(OnTick);
		aTimer.Stop (); */

	}
	/*private void OnTick(object source, ElapsedEventArgs e) {
		energyTick += 1;
		Debug.Log (energyTick);
		if (energyTick >= maxEnergy) {
			energyTick = maxEnergy;
			notification.send();
			//aTimer.Stop ();
		}
	}*/
	void OnApplicationQuit() {
		PlayerPrefs.SetString ("CloseTime", currentDate.ToString());
		PlayerPrefs.SetInt ("Energy", energy);
		PlayerPrefs.SetInt ("SecondTimePlaying", 1);
		//notification.send ();
		//aTimer.Start();
		//energyTick = energy;
	}
	void OnApplicationFocus(bool focusStatus) {
		if (focusStatus != true) {
			PlayerPrefs.SetString ("CloseTime", currentDate.ToString());
			PlayerPrefs.SetInt ("SecondTimePlaying", 1);
			PlayerPrefs.SetInt ("Energy", energy);
			//notification.send ();
		}
	}
	void Update()
	{
		currentDate = System.DateTime.Now;
		TimeSpan span = currentDate.Subtract ( oldDate );
		if(span.TotalSeconds/120 >= energyGiven) {
			energy++;
			oldDate = System.DateTime.Now;
			if (energy >= maxEnergy) {
				energy = maxEnergy;
			}
			if (energy <= 0) {
				energy = 0;
			}

		}
		if (Application.loadedLevelName == "Survival Mode" || Application.loadedLevelName == "Stage Selection" || Application.loadedLevelName == "Stage Selection2" || Application.loadedLevelName == "Stage Selection3" || 
		    Application.loadedLevelName == "Stage Selection4" || Application.loadedLevelName == "Stage Selection5" || Application.loadedLevelName == "Stage Selection6" ||
		    Application.loadedLevelName == "Stage Selection7" || Application.loadedLevelName == "Stage Selection8" || Application.loadedLevelName == "Stage Selection9" ||
		    Application.loadedLevelName == "Purchase Energy") {
			GameObject.Find("ContainerNext/Energy Container/EnergyText").GetComponent<TextMesh>().text = energy.ToString ()+"/"+maxEnergy;	
		}

	}
	void IncreaseEnergy (int energyIncrease) {
		energy += energyIncrease;
		if (energy >= maxEnergy) {
			energy = maxEnergy;	
		}
		if (energy <= 0) {
			energy = 0;
		}
		PlayerPrefs.SetInt ("Energy", energy);
	}
}