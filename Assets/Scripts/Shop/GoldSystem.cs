﻿using UnityEngine;
using System.Collections;

public class GoldSystem : MonoBehaviour {
	public int gold;
	private int increaseGold;
	private int rewardGold;
	private GameObject[] goldArray;
	private int secondTimePlaying;
	// Use this for initialization
	void Awake () {
		goldArray = GameObject.FindGameObjectsWithTag ("Gold");
		if (goldArray.Length == 1) {
			DontDestroyOnLoad(gameObject);	
		}
		else {
			DestroyObject(gameObject);
		}
	}
	void Start () {
		//PlayerPrefs.DeleteAll ();
		secondTimePlaying = PlayerPrefs.GetInt ("SecondTimePlaying");
		if (Application.loadedLevelName == "Menu") {
			if (secondTimePlaying == 1) {
				gold = PlayerPrefs.GetInt ("Gold");
				GameObject cointest = GameObject.Find ("ContainerBack/Coin Container/CoinText");
				if(cointest != null){
					cointest.GetComponent<TextMesh> ().text = gold.ToString ();
				}
			}
			else {
				PlayerPrefs.SetInt ("Gold", gold);
				GameObject cointest = GameObject.Find ("ContainerBack/Coin Container/CoinText");
				if(cointest != null){
					cointest.GetComponent<TextMesh> ().text = gold.ToString ();
				}				
			}	
		}
	}
	
	// Update is called once per frame
	void Update () {
//		if (Application.loadedLevel < 10 && Application.loadedLevel >= 3 && Application.loadedLevelName != "Purchase Gem") {
//			if (ScrollView.hasUpgrade == true) {
//				gold = PlayerPrefs.GetInt ("Gold");
//				GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
//			}
//			else {
//				GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
//			}
//		}
//		if (secondTimePlaying == 1 && (Application.loadedLevel < 10 && Application.loadedLevel >= 3 && Application.loadedLevelName != "Purchase Gem")) {
//			gold = PlayerPrefs.GetInt ("Gold");
//			GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
//		}
        if (Application.loadedLevel < 16 && Application.loadedLevel >= 3 && Application.loadedLevelName != "Purchase Gem") {
            if (ScrollView.hasUpgrade == true) {
                gold = PlayerPrefs.GetInt ("Gold");
                GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
            }
            else {
                GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
            }
        }
        if (secondTimePlaying == 1 && (Application.loadedLevel < 16 && Application.loadedLevel >= 3 && Application.loadedLevelName != "Purchase Gem")) {
            gold = PlayerPrefs.GetInt ("Gold");
            GameObject.Find ("ContainerBack/Coin Container/CoinText").GetComponent<TextMesh> ().text = gold.ToString ();
        }
	}
	void IncreaseGold (int coin) {
		gold += coin;
		PlayerPrefs.SetInt ("Gold", gold);
	}
	void DecreaseGold (int coin) {
		gold -= coin;
		PlayerPrefs.SetInt ("Gold", gold);
	}
	void OnApplicationQuit() {
		PlayerPrefs.SetInt ("SecondTimePlaying", 1);
		PlayerPrefs.SetInt ("Gold", gold);
	}
	void OnApplicationFocus(bool focusStatus) {
		if (focusStatus != true) {
			PlayerPrefs.SetInt ("SecondTimePlaying", 1);
			PlayerPrefs.SetInt ("Gold", gold);
		}
	}
}
