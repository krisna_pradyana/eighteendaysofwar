﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyContainer : MonoBehaviour {

    #region Delegates
    public delegate void GainEnergy(int _EnergyGain);
    public delegate void DepeletsEnergy(int _EnergyLoss);
    #endregion

    #region Delegate Functions
    public static GainEnergy gainEnergy;
    public static DepeletsEnergy depeletsEnergy;
    #endregion

    GameObject EnergyUpdate;
    public static int EnergyValue;
    //int CurrentEnergy;
    private static bool created = false;
    //public int MaxEnergy;

    private void OnEnable()
    {
        gainEnergy += IncreaseEnergy;
        depeletsEnergy += DecreaseEnergy;
    }

    private void OnDisable()
    {
        gainEnergy -= IncreaseEnergy;
        depeletsEnergy -= DecreaseEnergy;
    }

    void Awake()
    {
       // DontDestroyOnLoad(this.gameObject);
        //EnergyUpdate = GameObject.Find("Energy Update");   
    }

    // Update is called once per frame
    void Update()
    {
        EnergyValue = PlayerPrefs.GetInt("Energy");
    }

    public void IncreaseEnergy(int _EnergyGain)
    {
        EnergyValue = EnergyValue + _EnergyGain;
        PlayerPrefs.SetInt("Energy", EnergyValue);
        PlayerPrefs.Save();
        //ResetValue();
        RefreshValue();
    }
    public void DecreaseEnergy(int _product)
    {     
        EnergyValue = EnergyValue - _product;
        PlayerPrefs.SetInt("Energy", EnergyValue);
        PlayerPrefs.Save();
        //ResetValue();
        RefreshValue();
    }

    private void ResetValue()
    {
        EnergyValue = 0;
    }

    private void RefreshValue()
    {
        EnergyValue = PlayerPrefs.GetInt("Energy");
    }
}
