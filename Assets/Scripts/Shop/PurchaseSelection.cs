﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseSelection : MonoBehaviour {


    public GameObject WindowSelection;
    public GameObject TelkomselPoinSelection;
    public GameObject GooglePlaySelection;
    public GameObject SelectedTelkomsel;
    public GameObject SelectedGooglePlay;
    public GameObject SelectedAlternateMethod;
    public GameObject AlternateMethodSelection;
  
    public void isTelkomPoin()
    {
        if (SelectedGooglePlay.activeInHierarchy == false)
        {
            SelectedTelkomsel.SetActive(true);
        }
    }

    public void isGooglePlay()
    {
        if(SelectedTelkomsel.activeInHierarchy == false)
        {
           SelectedGooglePlay.SetActive(true);
        }
    }

    public void isAlternate()
    {
        if (SelectedAlternateMethod.activeInHierarchy == false)
        {
            SelectedAlternateMethod.SetActive(true);
        }
    }
}
