﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using FourthSky.Android;
using FourthSky.Android.Services;
using Newtonsoft.Json;
using UnityEngine.UI;

public class APISakti : MonoBehaviour
{
    //[SerializeField]
    private InputField inputProvider, inputMsisdn, inputPrice, inputItem;
    private Text ReceivedNumber, ReceivedMessage, Status;
    public GameObject PopUpSendPin, PinField, WaitingMessage, SuccessMessage, FailedMessage, WaitAfterSubmit;
    public Dropdown ProviderLists;

    //int Occurence;
    bool AcquiredPin;

    private string URL;
    private string providerNumber;
    string AppID, AppSecret, Provider;
    string Msisdn, Price, Item;
    string Converted_datetime;

    string phoneNumber; 
    string message = "Testing SMS";
    string messageStatus = "No messages";

    System.DateTime dateTime = System.DateTime.Now;

    private void Start()
    {
        dateTime = System.DateTime.Now;
        AppID = "18b8ad15b78647ae8f9ea7898b182cc9";     //predefined appID
        AppSecret = "1625db3f72d76fcc7e608ce1fcc4fe1e";     //predefined secret number     
        AcquiredPin = false;
        Debug.Log(ProviderLists.value);
    }

    // Update is called once per frame
    void Update()
    {
        Converted_datetime = dateTime.ToString("yyyy-MM-dd\\THH:mm:ss\\Z");  //convert time to specsific format
        ListenSMS();
    }

    #region Unity UI Based
    public void GetMSISDN()
    {
        Msisdn = GameObject.Find("InputPhoneNumber").GetComponent<InputField>().text;
    }

    public void GetPrice(string _price)
    {
        Price = _price;
    }

    public void GetProvider()
    {
        switch(ProviderLists.value)
        {
            case 0:
                {
                    Provider = "xl";
                    phoneNumber = "99888";
                    break;
                }
            case 1:
                {
                    Provider = "h3i";
                    phoneNumber = "96333";
                    break;
                }
            case 2:
                {
                    Provider = "isat";
                    break;
                }
        }
    }

    public void GetItem(string _item)
    {
        Item = _item;
    }

    public void SubmitField()
    {
        GetMSISDN();
        GetProvider();
        if (Provider != "isat")
        {
            StartCoroutine(AccessUrl());
        }
        else
            StartCoroutine(IndosatAccessURL());
    }

    public void SubmitPin()
    {
        SendPin();
    }
    #endregion

    #region Axis,Xl & Tri 
    IEnumerator CatchPINNumber(string _message)
    {
        yield return new WaitForSeconds(1f);
        string sentence = _message;
        int number;
        string[] digits = Regex.Split(sentence, @"\b\D+\b");

        if (int.TryParse(digits[3], out number))
        {
            PinField.GetComponent<InputField>().text = ""+number;
        }
    }
   
    IEnumerator AccessUrl()
    {
        yield return new WaitForSeconds(1f);
        URL = "http://api-pg.saktimobile.com/api.php" + "?app_id=" + AppID + "&app_secret=" + AppSecret + "&msisdn=" + Msisdn + "&price=" + Price + "&operator=" + Provider + "&request_time=" + Converted_datetime + "&item=" + Item;

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            yield return www.Send();
            Debug.Log(www.url);
            PopUpSendPin.SetActive(true);
            WaitAfterSubmit.SetActive(false);
        }
    }

    public void SendPin()
    {
        //WWWForm form = new WWWForm();
    #if UNITY_ANDROID
        Telephony.SendSMS(phoneNumber, PinField.GetComponent<InputField>().text,
                            (sentOK) =>
                            {
                                if (sentOK)
                                {
                                    //messageStatus = "Purchase in Proccess";
                                    //GameObject.Find("Waiting Message").SetActive(true);
                                }
                                else
                                {
                                    //messageStatus = "Not enough ballance";
                                    //GameObject.Find("Failed Message").SetActive(true);
                                }
                            },
                            (deliveredOK) =>
                            {
                                if (deliveredOK)
                                {
                                    //messageStatus = "Purchase Completed";
                                }
                                else
                                {
                                    //messageStatus = "Purchase Failed";
                                    //GameObject.Find("Failed Message").SetActive(true);
                                }
                            });
    #endif
        PopUpSendPin.SetActive(false);
        WaitingMessage.SetActive(true);
        AcquiredPin = true;
    }

    #endregion

    #region Indosat
    IEnumerator IndosatAccessURL()
    {
        URL = "http://api-pg.saktimobile.com/api.php" + "?app_id=" + AppID + "&app_secret=" + AppSecret + "&msisdn=" + Msisdn + "&price=" + Price + "&operator=" + Provider + "&request_time=" + Converted_datetime + "&item=" + Item;
        //WWWForm form = new WWWForm();
        WaitAfterSubmit.SetActive(true);

        using (UnityWebRequest www = UnityWebRequest.Get(URL))
        {
            yield return www.Send();
            Debug.Log(www.url);
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    string JsonOutput = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                    Debug.Log(JsonOutput);
                    yield return new WaitForSeconds(2f);
                    JsonResult Result = (JsonConvert.DeserializeObject<IDictionary<string, JsonResult>>(JsonOutput))["result"];

                    string key = Result.keyword;
                    string shortcode = Result.shortcode;

                    Debug.Log(key);
                    Debug.Log(shortcode);
                    SendKeyword(shortcode, key);
                }
            }
        }
    }

    void SendKeyword(string _shordcode, string _keyword)
    {
    #if UNITY_ANDROID
        Telephony.SendSMS(_shordcode, _keyword,
                         (SendOK) =>
                         {
                            //left it blank if there's no feedback needed
                         },
                         (DeliveredOK) =>
                         {
                             //left it blank if there's no feedback needed
                         }
                         );
#endif
        //Debug.Log("Sending Keyword");
        WaitAfterSubmit.SetActive(false);
        WaitingMessage.SetActive(true);
        AcquiredPin = true;
    }
    #endregion

    void ListenSMS()
    {
        // Text to send
        Telephony.ListenForSms((msg) =>
        {
            messageStatus = "Receive SMS from " + msg[0].OriginAddress + ": " + msg[0].MessageBody;
            if (AcquiredPin == false)
            {
                StartCoroutine(CatchPINNumber(messageStatus));
            }
            else if (AcquiredPin == true)
            {
                StartCoroutine(ConfirmSuccess(messageStatus));
            }
        });
        messageStatus = "listening for message";
    }

    IEnumerator ConfirmSuccess(string _message)
    {
        int CurrentGem, GemValue;
        string keyword = _message;
        Match m = Regex.Match(keyword, @"\bberhasil", RegexOptions.IgnoreCase);

        yield return new WaitForSeconds(1f);
        Debug.Log(keyword);

        if (m.Success)
        {
            Debug.Log(m.Length);
            if (m.Value == "berhasil")
            {
                Debug.Log("Found " + m.Value + " at Position " + m.Index);
                WaitingMessage.SetActive(false);
                SuccessMessage.SetActive(true);
                AcquiredPin = false;

                //this line is where you implement internal eceonomy change
                switch (Item)
                {
                    case "Test":
                        CurrentGem = PlayerPrefs.GetInt("Gem");
                        GemValue = CurrentGem + 5;
                        PlayerPrefs.SetInt("Gem", GemValue);
                        PlayerPrefs.Save();
                        AcquiredPin = false;
                        break;
                    case "Gem8":
                        CurrentGem = PlayerPrefs.GetInt("Gem");
                        GemValue = CurrentGem + 8;
                        PlayerPrefs.SetInt("Gem", GemValue);
                        PlayerPrefs.Save();
                        AcquiredPin = false;
                        break;
                    case "Gem15":
                        CurrentGem = PlayerPrefs.GetInt("Gem");
                        GemValue = CurrentGem + 15;
                        PlayerPrefs.SetInt("Gem", GemValue);
                        PlayerPrefs.Save();
                        AcquiredPin = false;
                        break;
                    case "Gem20":
                        CurrentGem = PlayerPrefs.GetInt("Gem");
                        GemValue = CurrentGem + 20;
                        PlayerPrefs.SetInt("Gem", GemValue);
                        PlayerPrefs.Save();
                        AcquiredPin = false;
                        break;
                    case "Gem25":
                        CurrentGem = PlayerPrefs.GetInt("Gem");
                        GemValue = CurrentGem + 25;
                        PlayerPrefs.SetInt("Gem", GemValue);
                        PlayerPrefs.Save();
                        AcquiredPin = false;
                        break;
                }
            }
            else
                FailedMessage.SetActive(true);
            WaitingMessage.SetActive(false);
            AcquiredPin = false;
        }
    }

    //this where json variables encapsulated
    [System.Serializable]
    public class JsonResult
    {
        public string tid;
        public string status;
        public string msisdn;
        public string price;
        public string keyword;
        public string shortcode;
    }

}