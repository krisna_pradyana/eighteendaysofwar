﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyUpdate : MonoBehaviour {

    #region Delegates
    public delegate void DepeleteEnergy(int cost);
    public delegate void RepelnishEnergy(int cost);

    public static DepeleteEnergy depeleteEnergy;
    public static RepelnishEnergy repelnishEnergy;
    #endregion

    public static int EnergyValue;
    //public TextMesh EnergyText;
    public Text EnergyText;
    private const int MaxEnergy = 120;

    private void OnEnable()
    {
        depeleteEnergy += DecreaseEnergy;
        repelnishEnergy += IncreaseEnergy;
    }

    private void OnDisable()
    {
        depeleteEnergy -= DecreaseEnergy;
        repelnishEnergy -= IncreaseEnergy;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        EnergyValue = PlayerPrefs.GetInt("Energy");
        if(EnergyValue >= MaxEnergy)
        {
            EnergyValue = MaxEnergy;
        }
        else if(EnergyValue <0)
        {
            EnergyValue = 0;
        }

        EnergyText.text = "" + EnergyValue +"/"+ MaxEnergy;

        PlayerPrefs.SetInt("Energy", EnergyValue);
        PlayerPrefs.Save();
    }

    void DecreaseEnergy(int cost)
    {
        EnergyValue = EnergyValue - cost;
        PlayerPrefs.SetInt("Energy", EnergyValue);
        PlayerPrefs.Save();
    }

    void IncreaseEnergy(int cost)
    {
        EnergyValue = EnergyValue + cost;
        PlayerPrefs.SetInt("Energy", EnergyValue);
        PlayerPrefs.Save();
    }
}
