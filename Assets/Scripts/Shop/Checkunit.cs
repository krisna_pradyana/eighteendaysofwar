﻿using UnityEngine;
using System.Collections;

public class Checkunit : MonoBehaviour {
	public static bool footman, archer, swordman, gunner, spearman, mage, shaman, elephant, tank;
	public static bool arjuna=true, bima=false, srikandi=false, yudhistira=false, nakulasadewa=false, gatotkaca=false, drupada=false, kresna=false, hanoman=false;
	public static bool lockupfoot,lockuparcher,lockupsword,lockupgun,lockupspear,lockupmage,lockupshaman,lockupelephantry, lockuptank;
	private GameObject[] check;
	public static bool playOnce;
	public static string hero1select="",hero2select="",hero3select="";
	private int unlockUnit, unlockHero, secondTimePlaying;
	private int unlockBima, unlockSrikandi, unlockYudhistira, unlockNakulaSadewa, unlockGatotKaca, unlockDrupada, unlockKresna, unlockHanoman;

	void Start() {
        hero1select = "";
        hero2select = "";
        hero3select = "";

        footman = true;
		archer = false;
		swordman = false;
		gunner = false;
		spearman = false;
		mage = false;
		shaman = false;
		elephant = false;
		tank = false;
		lockupfoot = false;
		lockuparcher = false;
		lockupsword = false;
		lockupgun = false;
		lockupspear = false;
		lockupmage = false;
		lockupshaman = false;
		lockupelephantry = false;
		lockuptank = false;
		secondTimePlaying = PlayerPrefs.GetInt ("SecondTimePlaying");
		if (secondTimePlaying == 0) {
			unlockUnit = 1;
			unlockHero = 1;
			PlayerPrefs.SetInt ("UnlockUnit", unlockUnit);
			PlayerPrefs.SetInt ("UnlockHero", unlockHero);
		}
		else {
			unlockUnit = PlayerPrefs.GetInt ("UnlockUnit");
			unlockHero = PlayerPrefs.GetInt ("UnlockHero");
		}
		playOnce = false;
    }
    void Update()
    {
        unlockBima = PlayerPrefs.GetInt("HasUnlockBima");
        unlockSrikandi = PlayerPrefs.GetInt("HasUnlockSrikandi");
        unlockYudhistira = PlayerPrefs.GetInt("HasUnlockYudhistira");
        unlockNakulaSadewa = PlayerPrefs.GetInt("HasUnlockNakulaSadewa");
        unlockGatotKaca = PlayerPrefs.GetInt("HasUnlockGatotKaca");
        unlockDrupada = PlayerPrefs.GetInt("HasUnlockDrupada");
        unlockKresna = PlayerPrefs.GetInt("HasUnlockKresna");
        unlockHanoman = PlayerPrefs.GetInt("HasUnlockHanoman");
        //Debug.Log("More Than One");

        if (Application.loadedLevel == 3) {
			hero1select = "";
			hero2select = "";
			hero3select = "";
			InitUnit.readytoinit = false;
        
		} else 
        {
            lockupfoot = false;
            lockuparcher = false;
            lockupsword = false;
            lockupgun = false;
            lockupspear = false;
            lockupmage = false;
            lockupshaman = false;
            lockupelephantry = false;
            lockuptank = false;
        }

        //Debug.Log("More Than One");
        /*
        if (PlayerPrefs.GetInt ("ClearStage3") == 1) {
            PlayerPrefs.SetInt("LockArcher", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage6") == 1) {
            PlayerPrefs.SetInt("LockSword", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage9") == 1) {
            PlayerPrefs.SetInt("LockGunner", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage12") == 1) {
            PlayerPrefs.SetInt("LockSpear", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage15") == 1) {
            PlayerPrefs.SetInt("LockMage", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage18") == 1) {
            PlayerPrefs.SetInt("LockShaman", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage24") == 1) {
            PlayerPrefs.SetInt("LockElephantry", 1);
        }
        if (PlayerPrefs.GetInt ("ClearStage30") == 1) {
            PlayerPrefs.SetInt("LockTank", 1);
        }
        */

        if (PlayerPrefs.GetInt("LockArcher") == 1)
        {
            unlockUnit = 2;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            archer = true;
        }
        if (PlayerPrefs.GetInt("LockSword") == 1)
        {
            unlockUnit = 3;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            swordman = true;
        }
        if (PlayerPrefs.GetInt("LockGunner") == 1)
        {
            unlockUnit = 4;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            gunner = true;
        }
        if (PlayerPrefs.GetInt("LockSpear") == 1)
        {
            unlockUnit = 5;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            spearman = true;
        }
        if (PlayerPrefs.GetInt("LockMage") == 1)
        {
            unlockUnit = 6;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            mage = true;
        }
        if (PlayerPrefs.GetInt("LockShaman") == 1)
        {
            unlockUnit = 7;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            shaman = true;
        }
        if (PlayerPrefs.GetInt("LockElephantry") == 1)
        {
            unlockUnit = 8;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            elephant = true;
        }
        if (PlayerPrefs.GetInt("LockTank") == 1)
        {
            unlockUnit = 9;
            PlayerPrefs.SetInt("UnlockUnit", unlockUnit);
            tank = true;
        }
        if (PlayerPrefs.GetInt("LockBima") == 1)
        {
            if (unlockBima == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockBima", 1);
                Debug.Log("BimaUnlocked");
            }
            bima = true;
        }
        if (PlayerPrefs.GetInt("LockSrikandi") == 1)
        {
            if (unlockSrikandi == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockSrikandi", 1);
            }
            srikandi = true;
        }
        if (PlayerPrefs.GetInt("LockYudhistira") == 1)
        {
            if (unlockYudhistira == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockYudhistira", 1);
            }
            yudhistira = true;
        }
        if (PlayerPrefs.GetInt("LockNakulaSadewa") == 1)
        {
            if (unlockNakulaSadewa == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockNakulaSadewa", 1);
            }
            nakulasadewa = true;
        }
        if (PlayerPrefs.GetInt("LockGatotKaca") == 1)
        {
            if (unlockGatotKaca == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockGatotKaca", 1);
                PlayerPrefs.SetInt("IronFist", 1);
            }
            gatotkaca = true;
        }
        if (PlayerPrefs.GetInt("LockDrupada") == 1)
        {
            if (unlockYudhistira == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockDrupada", 1);
            }
            drupada = true;
        }
        if (PlayerPrefs.GetInt("LockKresna") == 1)
        {
            if (unlockNakulaSadewa == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockKresna", 1);
            }
            kresna = true;
        }
        if (PlayerPrefs.GetInt("LockHanoman") == 1)
        {
            if (unlockGatotKaca == 0)
            {
                unlockHero += 1;
                PlayerPrefs.SetInt("UnlockHero", unlockHero);
                PlayerPrefs.SetInt("HasUnlockHanoman", 1);
                PlayerPrefs.SetInt("BlazingMonkey", 1);
            }
            hanoman = true;
        }
        /*
        if (PlayerPrefs.GetInt ("ClearStage3") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 4){
                lockupfoot = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage6") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 6){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 6){
                lockuparcher = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage9") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 8){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 8){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 6){
                lockupsword = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage12") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 10){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 10){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 6){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 6){
                lockupgun = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage15") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 12){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 12){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 8){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 8){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 8){
                lockupspear = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage18") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 14){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 14){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 12){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 12){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 10){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 10){
                lockupmage = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage21") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 16){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 16){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 14){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 14){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 12){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 12){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 10){
                lockupshaman = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage24") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 18){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 18){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 16){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 16){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 14){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 14){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 12){
                lockupshaman = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage27") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 18){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 18){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 16){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 16){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 14){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 12){
                lockupelephantry = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage30") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 20){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 20){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 18){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 18){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 16){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 14){
                lockupelephantry = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage33") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 20){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 20){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 20){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 20){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 18){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 16){
                lockupelephantry = true;
            }
            if(PlayerPrefs.GetInt("LevelTank") >= 14){
                lockuptank = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage36") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 20){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 20){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 20){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 20){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 20){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 18){
                lockupelephantry = true;
            }
            if(PlayerPrefs.GetInt("LevelTank") >= 16){
                lockuptank = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage39") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 20){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 20){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 20){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 20){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 20){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 20){
                lockupelephantry = true;
            }
            if(PlayerPrefs.GetInt("LevelTank") >= 18){
                lockuptank = true;
            }
        }
        if (PlayerPrefs.GetInt ("ClearStage42") == 0) {
            if(PlayerPrefs.GetInt("LevelFoot") >= 20){
                lockupfoot = true;
            }
            if(PlayerPrefs.GetInt("LevelArcher") >= 20){
                lockuparcher = true;
            }
            if(PlayerPrefs.GetInt("LevelSword") >= 20){
                lockupsword = true;
            }
            if(PlayerPrefs.GetInt("LevelGunner") >= 20){
                lockupgun = true;
            }
            if(PlayerPrefs.GetInt("LevelSpear") >= 20){
                lockupspear = true;
            }
            if(PlayerPrefs.GetInt("LevelMage") >= 20){
                lockupmage = true;
            }
            if(PlayerPrefs.GetInt("LevelShaman") >= 20){
                lockupshaman = true;
            }
            if(PlayerPrefs.GetInt("LevelElephantry") >= 20){
                lockupelephantry = true;
            }
            if(PlayerPrefs.GetInt("LevelTank") >= 20){
                lockuptank = true;
            }
        }
        */
    }
}
