﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using SIS;
//using ChartboostSDK;
public class Gemshard : MonoBehaviour {

    public delegate void CheckingShard();
    public static CheckingShard checkingShard;

    public Text shardNumber;

    private int shards;

    private void Start()
    {
        CheckShard();
        checkingShard += CheckShard;
    }

    private void OnDisable()
    {
        checkingShard -= CheckShard;
    }

    public void CheckShard()
    {
        Debug.Log("SHARD CHECKED PROPERLY");
        if(PlayerPrefs.HasKey("GemShard"))
        {
            shards = PlayerPrefs.GetInt("GemShard");
            shardNumber.text = shards + "/5";
        }
        else
        {
            PlayerPrefs.SetInt("GemShard",0);
            shards = PlayerPrefs.GetInt("GemShard");
        }
    }    

    public void GetShard()
    {
        AdInterfaceScript.ClickAdButton();
        CheckShard();
    }
}
