﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstTime : MonoBehaviour
{
    void Awake()
    {
        if (!PlayerPrefs.HasKey("Gold"))
        {
            PlayerPrefs.SetInt("Gold", 200);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Gem"))
        {
            PlayerPrefs.SetInt("Gem", 10);
            PlayerPrefs.Save();
        }
        if (!PlayerPrefs.HasKey("Energy"))
        {
            PlayerPrefs.SetInt("Energy", 120);
            PlayerPrefs.Save();
        }
        else
        {
            return;
        }
    }
}