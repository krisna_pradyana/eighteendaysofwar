﻿using UnityEngine;
using System.Collections;
//using SIS;

public class GoldGemClick : MonoBehaviour {
    
	public int gemCost;
	public int goldCost;
	private string currency="gems";
	public static bool 
		hasBuyPack1 = false, 
		hasBuyPack2 = false, 
		hasBuyPack3 = false, 
		hasBuyPack4 = false;
	private GemSystem gemSystem;
	private GoldSystem goldSystem;
	private RegenEnergy regenEnergy;
	public GameObject BuyPopupUI, GemPopupUI, AdsRewardUI;
	private GameObject square;
	// Use this for initialization
	void Start () {
		//gemSystem = GameObject.Find ("GemSystem").GetComponent<GemSystem> ();
		//goldSystem = GameObject.Find ("GoldSystem").GetComponent<GoldSystem> ();
		//regenEnergy = GameObject.Find ("EnergyText").GetComponent<RegenEnergy> ();
		square = GameObject.Find ("Square2");
		if (UnityAdsLoading.hasFinishWatchingAds == true) {
			WarningClick.stillShowingWarnings = true;
			square.GetComponent<SpriteRenderer>().enabled = true;
			AdsRewardUI.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (WarningClick.stillShowingWarnings == true) {
			gameObject.GetComponent<BoxCollider2D>().enabled = false;	
		}
		else {
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (Application.loadedLevelName == "Purchase Gold") {
			WarningClick.stillShowingWarnings = true;
			if (gameObject.name == "Purchase1") {
				BuyPopupUI.SetActive(true);
				//.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack1 = true;
			}
			else if (gameObject.name == "Purchase2") {
				BuyPopupUI.SetActive(true);
				//square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack2 = true;
			}
			else if (gameObject.name == "Purchase3") {
				BuyPopupUI.SetActive(true);
				//square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack3 = true;
			}
			else if (gameObject.name == "Purchase4") {
				BuyPopupUI.SetActive(true);
				//square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack4 = true;
			}
			else if (gameObject.name == "WatchAds") {
				UnityAdsLoading.adsReward = "Gold";
				Application.LoadLevel ("AdMobLoading");
			}
            /*
			else if (DBManager.GetFunds(currency) < gemCost) {
				GemPopupUI.SetActive (true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				BuyPopupUI.SetActive (false);
                
			}*/
		}
		else if (Application.loadedLevelName == "Purchase Gem") {
			WarningClick.stillShowingWarnings = true;
			if (gameObject.name == "Purchase1") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack1 = true;
			}
			else if (gameObject.name == "Purchase2") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack2 = true;
			}
			else if (gameObject.name == "Purchase3") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack3 = true;
			}
			else if (gameObject.name == "Purchase4") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack4 = true;
			}
            /*
			else if (goldSystem.gold >= goldCost) {
				GemPopupUI.SetActive (true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				BuyPopupUI.SetActive (false);
			}
            */
		}
		else if (Application.loadedLevelName == "Purchase Energy") {
			WarningClick.stillShowingWarnings = true;
			if (gameObject.name == "Purchase1") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack1 = true;
			}
			else if (gameObject.name == "Purchase2") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack2 = true;
			}
			else if (gameObject.name == "Purchase3") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack3 = true;
			}
			else if (gameObject.name == "Purchase4") {
				BuyPopupUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				hasBuyPack4 = true;
			}
			else if (gameObject.name == "WatchAds") {
				UnityAdsLoading.adsReward = "Energy";
				Application.LoadLevel ("AdMobLoading");
			}
            /*
			else if (DBManager.GetFunds(currency) < gemCost) {
				GemPopupUI.SetActive (true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				BuyPopupUI.SetActive (false);
			}
            */
		}
	}
}
