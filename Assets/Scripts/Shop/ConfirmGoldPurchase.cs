﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmGoldPurchase : MonoBehaviour {

    public int[] gemPrices;
    public int GemValue;
    public GameObject unableWarning;
    public GameObject purchaseUI;
    public int PurchaseCode;
    //GoldContainer GoldCntr;
    //GemContainer GemCntr;

    private void Awake()
    {
        //GoldCntr = new GoldContainer();
        //GemCntr = new GemContainer();
    }
 
	void Update () {
        //GemValue = PlayerPrefs.GetInt("Gem");
    }

    public void SetPurchaseCode(int _purchaseCode)
    {
        PurchaseCode = _purchaseCode; 
    }

    public void purchase()
    {

        if (PurchaseCode == 1 && GemContainer.GemValue >= gemPrices[1])
        {
            Debug.Log("Purchase Code " + PurchaseCode + " called");
            //GoldCntr.IncreaseGold(1000);
            //GemCntr.DecreaseGem(1);
            GoldContainer.addGold(1000);
            GemContainer.purchaseWithGem(1);
            purchaseUI.SetActive(true);
            ResetValue();
        }
        else if (PurchaseCode == 2 && GemContainer.GemValue >= gemPrices[2])
        {
            Debug.Log("Purchase Code " + PurchaseCode + " called");
            //GoldCntr.IncreaseGold(6000);
            //GemCntr.DecreaseGem(5);
            GoldContainer.addGold(6000);
            GemContainer.purchaseWithGem(5);
            purchaseUI.SetActive(true);
            ResetValue();
        }
        else if (PurchaseCode == 3 && GemContainer.GemValue >= gemPrices[3])
        {
            Debug.Log("Purchase Code " + PurchaseCode + " called");
            //GoldCntr.IncreaseGold(15000);
            //GemCntr.DecreaseGem(10);
            GoldContainer.addGold(15000);
            GemContainer.purchaseWithGem(10);
            purchaseUI.SetActive(true);
            ResetValue();
        }
        else if (PurchaseCode == 4 && GemContainer.GemValue >= gemPrices[4])
        {
            Debug.Log("Purchase Code " + PurchaseCode + " called");
            //GoldCntr.IncreaseGold(35000);
            //GemCntr.DecreaseGem(20);
            GoldContainer.addGold(35000);
            GemContainer.purchaseWithGem(20);
            purchaseUI.SetActive(true);
            ResetValue();
        }
        else if (PurchaseCode == 0)
        {
            //GoldCntr.DecreaseGold(10000);
        }
        else unableWarning.SetActive(true);
    }

    private void ResetValue()
    {
        PurchaseCode = 0;
    }
}
