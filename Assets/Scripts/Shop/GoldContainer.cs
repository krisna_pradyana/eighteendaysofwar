﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldContainer : MonoBehaviour {

    #region Delegate 
    public delegate void PurchaseWithGold(int _purchase);
    public delegate void AddGold(int _incement);
    #endregion

    public static PurchaseWithGold purchaseWithGold;
    public static AddGold addGold;

    GameObject GoldUpdate;
    public static int GoldValue;
    //public GameObject PurchaseConfirmation;
    private static bool created = false;
    //public GameObject GoldText;

    private void OnEnable()
    {
        purchaseWithGold += DecreaseGold;
        addGold += IncreaseGold;
    }

    private void OnDisable()
    {
        purchaseWithGold -= DecreaseGold;
        addGold -= IncreaseGold;
    }

    void Awake()
    {
        //GoldUpdate = GameObject.Find("Gold Update");
        if (!created)
        {
            // this is the first instance - make it persist
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else
        {
            // this must be a duplicate from a scene reload - DESTROY!
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    { 
       // GoldText.GetComponent<TextMesh>().text = PlayerPrefs.GetInt("Gold").ToString();
    }

    public void IncreaseGold(int _GoldGain)
    {
        Debug.Log(_GoldGain);
        int CurrentGold = PlayerPrefs.GetInt("Gold");
        GoldValue = CurrentGold + _GoldGain;
        PlayerPrefs.SetInt("Gold", GoldValue);
        PlayerPrefs.Save();
        ResetValue();
    }
    public void DecreaseGold(int _product)
    {
        Debug.Log(_product);
        int CurrentGold = PlayerPrefs.GetInt("Gold");
        GoldValue = CurrentGold - _product;
        PlayerPrefs.SetInt("Gold", GoldValue);
        PlayerPrefs.Save();
        ResetValue();
    }

    private void ResetValue()
    {
        GoldValue = 0;
    }
}
