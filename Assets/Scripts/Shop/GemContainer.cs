﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemContainer : MonoBehaviour {

    #region Delegate 
    public delegate void PurchaseWithGem(int _purchase);
    public delegate void AddGem(int _incement);
    #endregion

    public static PurchaseWithGem purchaseWithGem;
    public static AddGem addGem;

    //GameObject GemUpdate;
    public static int GemValue;
    private static bool created = false;

    private void OnEnable()
    {
        purchaseWithGem += DecreaseGem;
        addGem += IncreaseGem;
    }

    private void OnDisable()
    {
        purchaseWithGem -= DecreaseGem;
        addGem -= IncreaseGem;
    }

    void Awake()
    {
        //GemUpdate = GameObject.Find("Gem Update");
    }

	// Use this for initialization
	void Start () {
        RefreshValue();
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void IncreaseGem(int _GemGain)
    {
        Debug.Log("Purchase Gem");
        int CurrentGem = PlayerPrefs.GetInt("Gem");
        GemValue = CurrentGem + _GemGain;
        PlayerPrefs.SetInt("Gem",GemValue);
        PlayerPrefs.Save();
        RefreshValue();
        //ResetValue();
    }
    public void DecreaseGem(int _product)
    {
        int CurrentGem = PlayerPrefs.GetInt("Gem");
        GemValue = CurrentGem - _product;
        PlayerPrefs.SetInt("Gem", GemValue);
        PlayerPrefs.Save();
        RefreshValue();
        //ResetValue();
    }

    private void ResetValue()
    {
        GemValue = 0;
    }

    void RefreshValue()
    {
        GemValue = PlayerPrefs.GetInt("Gem");
    }
}

