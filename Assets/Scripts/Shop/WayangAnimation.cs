﻿using UnityEngine;
using System.Collections;

public class WayangAnimation : MonoBehaviour {
	public GameObject wayang1;
	public GameObject wayang2;
	public float wayangSpeed;
	private float wayang1xPosition, wayang2xPosition, wayang1zRotation, wayang2zRotation;
	// Use this for initialization
	void Start () {
		wayang1zRotation = 60;
	}
	
	// Update is called once per frame
	void Update () {
		if (wayang1.transform.position.x <= -3.5f) {
			wayang1xPosition = wayang1.transform.position.x;
			wayang1xPosition += wayangSpeed * Time.deltaTime;
			wayang1.transform.position = new Vector3 (wayang1xPosition, 0, 0);
			if (wayang1.transform.rotation.z >= -60) {
				wayang1zRotation -= wayangSpeed * Time.deltaTime * 12;
				wayang1.transform.rotation = Quaternion.Euler (0, 0, wayang1zRotation);
			}	
		}
		if (wayang2.transform.position.x >= 3.5f) {
			wayang2xPosition = wayang1.transform.position.x;
			wayang2xPosition += wayangSpeed * Time.deltaTime;
			wayang2.transform.position = new Vector3 (wayang1xPosition, 0, 0);
			if (wayang2.transform.rotation.z >= -60) {
				wayang2.transform.Translate (-Vector2.right * wayangSpeed * Time.deltaTime);
				wayang2.transform.Rotate (-Vector2.right * wayangSpeed * Time.deltaTime);
			}	
		}

	}
}
