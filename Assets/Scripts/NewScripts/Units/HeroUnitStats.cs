﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroUnitStats : MonoBehaviour
{
    GameObject levelText;
    GameObject healthStatText;
    GameObject powerStatText;
    GameObject costStatText;
    GameObject lockedImage;
    GameObject upgradeButton;
    Button upgradeListener;
    Text maxLevel;

    GameObject unlockButton;
    GameObject lockIcon;
    Text requirementText;
    Text price;
    public UnlockType unlockType;
    
    public int gemCost;

    #region Default Stats
    public int level;
    public float atk;
    public float hp;
    public int cost;
    #endregion

    #region Stat Changes
    public float upAtk;
    public float upHp;
    public int upCost;
    #endregion

    #region CurrentStats
    public int currAtk;
    public int currHp;
    public int currCost;
    #endregion

    public bool lockState;
    public int currentUnlockedLevel;
    public int currentMaxLevel;


    [Header("Hero Identifier (Fill noState in Unit dropdown)")]
    public Heroes heroUnit;
    [Header("Strings that call Playerprefs String addresses")]
    public string plPrefs;

    #region Hero Default Attribute
    const int costarjuna = 100;
    const int costbima = 100;
    const int costsrikandi = 80;
    const int costyudhistira = 90;
    const int costnakulasadewa = 100;
    const int costgatotkaca = 200;
    const int costdrupada = 250;
    const int costkresna = 270;
    const int costhanoman = 300;

    const int hpivarjuna = 1200;
    const int hpivbima = 1500;
    const int hpivsrikandi = 800;
    const int hpivyudhistira = 1350;
    const int hpivnakulasadewa = 1250;
    const int hpivgatotkaca = 1450;
    const int hpivdrupada = 2200;
    const int hpivkresna = 1450;
    const int hpivhanoman = 2450;

    const int atkivarjuna = 90;
    const int atkivbima = 120;
    const int atkivsrikandi = 50;
    const int atkivyudhistira = 70;
    const int atkivnakulasadewa = 80;
    const int atkivgatotkaca = 90;
    const int atkivdrupada = 70;
    const int atkivkresna = 70;
    const int atkivhanoman = 80;
    #endregion

    private void Awake()
    {
        lockedImage = transform.Find("LockPanel").gameObject;
        upgradeButton = transform.Find("Upgrade").gameObject;
        upgradeListener = upgradeButton.GetComponent<Button>();
        unlockButton = transform.Find("LockPanel/PurchaseButton").gameObject;
        requirementText = transform.Find("LockPanel/RequirementText").GetComponent<Text>();
        lockIcon = transform.Find("LockPanel/LockIcon").gameObject;
        price = transform.Find("LockPanel/PurchaseButton/GemImage/Price").GetComponent<Text>();

        #region unlock default Unit and Hero
        PlayerPrefs.SetInt("LockArjuna", 1);
        #endregion

        CompareCompletedLevels();
        DefineUnlockType();
        DefineUnit();
        LevelCapControl();
        LockStatus();
    }

    private void OnEnable()
    {
        //Debug.Log(gameObject.name);
        upgradeListener.onClick.AddListener(delegate { UpgradeUnit(); });
        //DefineUnit();
    }

    private void OnDisable()
    {
        upgradeListener.onClick.RemoveAllListeners();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        DisplayText();
    }

    /// <summary>
    /// Run at Starts and Events
    /// </summary>
    void DefineUnit()
    {
        switch (heroUnit)
        {
            //Only Arjuna can be unlocked on start of every game
            case Heroes.arjuna:
                atk = atkivarjuna;
                hp = hpivarjuna;
                cost = costarjuna;
                LoadStats(UnitLists.arjuna);
                unlockType = UnlockType.progression;
                UnlockingCharacter();
                break;
            case Heroes.bima:
                atk = atkivbima;
                hp = hpivbima;
                cost = costbima;
                unlockType = UnlockType.progression;
                LoadStats(UnitLists.bima);
                break;
            case Heroes.shrikandi:
                atk = atkivsrikandi;
                hp = hpivsrikandi;
                cost = costsrikandi;
                LoadStats(UnitLists.shrikandi);
                unlockType = UnlockType.progression;
                break;
            case Heroes.yudhistira:
                atk = atkivyudhistira;
                hp = hpivyudhistira;
                cost = costyudhistira;
                LoadStats(UnitLists.yudhistira);
                unlockType = UnlockType.progression;
                break;
            case Heroes.nakulaSadewa:
                atk = atkivnakulasadewa;
                hp = hpivnakulasadewa;
                cost = costnakulasadewa;
                LoadStats(UnitLists.nakulaSadewa);
                unlockType = UnlockType.purchase;
                gemCost = 4;
                break;
            case Heroes.ghatotkacha:
                atk = atkivgatotkaca;
                hp = hpivgatotkaca;
                cost = costgatotkaca;
                LoadStats(UnitLists.ghatotkacha);
                unlockType = UnlockType.purchase;
                gemCost = 8;
                break;
            case Heroes.drupada:
                atk = atkivdrupada;
                hp = hpivdrupada;
                cost = costdrupada;
                LoadStats(UnitLists.drupada);
                unlockType = UnlockType.purchase;
                gemCost = 12;
                break;
            case Heroes.khresna:
                atk = atkivkresna;
                hp = hpivkresna;
                cost = costkresna;
                LoadStats(UnitLists.khresna);
                unlockType = UnlockType.purchase;
                gemCost = 14;
                break;
            case Heroes.anoman:
                atk = atkivhanoman;
                hp = hpivhanoman;
                cost = costhanoman;
                LoadStats(UnitLists.anoman);
                unlockType = UnlockType.purchase;
                gemCost = 16;
                break;
        }
    }

    void LoadStats(UnitLists unitName)
    {
       
        currAtk = (int)atk;
        currHp = (int)hp;
        currCost = cost;
        level = PlayerPrefs.GetInt("Level" + plPrefs);

        if (level == 0)
        {
            level = 1;
            PlayerPrefs.SetFloat("ATK" + plPrefs, currAtk);
            PlayerPrefs.SetFloat("HP" + plPrefs, currHp);
            PlayerPrefs.SetInt(plPrefs + "cost", currCost);

            PlayerPrefs.SetInt("CurrATK" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.SetInt("CurrHP" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.Save();
        }

        else if (level >= 2)
        {
            //return;
            //Debug.Log("Access this");
            upAtk = (PlayerPrefs.GetInt("Level" + plPrefs) * atk) * 0.35f;
            upHp = (PlayerPrefs.GetInt("Level" + plPrefs) * hp) * 0.35f;
            upCost = PlayerPrefs.GetInt(plPrefs + "cost") + ((PlayerPrefs.GetInt("Level" + plPrefs) + 2) * cost);
            //PlayerPrefs.SetInt(plPrefs + "Max", 0); // some buggy code needed hehe

            currAtk = (int)(atk + upAtk);
            currHp = (int)(hp + upHp);
            currCost = cost + upCost;

            PlayerPrefs.SetInt("CurrATK" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.SetInt("CurrHP" + plPrefs, currHp); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.Save();
        }
        CompareCompletedLevels();

        Debug.Log(PlayerPrefs.GetInt("CurrHP" + plPrefs));
    }

    public void UpgradeUnit()
    {
        //Debug.Log("Upgrading Character");
        if (level < currentMaxLevel)
        {
            if (GoldUpdate.goldValue >= currCost)
            {
                GoldContainer.purchaseWithGold(currCost);

                level += 1;

                upAtk = (PlayerPrefs.GetInt("Level" + plPrefs) * atk);
                upHp = (PlayerPrefs.GetInt("Level" + plPrefs) * hp);
                upCost = PlayerPrefs.GetInt(plPrefs + "cost") + ((PlayerPrefs.GetInt("Level" + plPrefs) + 2) * cost);

                currAtk = (int)(atk + upAtk);
                currHp = (int)(hp + upHp);
                currCost = cost + upCost;

                //PlayerPrefs.SetInt(plPrefs + "Upgrade", 1);
                PlayerPrefs.SetFloat("ATK" + plPrefs, currAtk);
                PlayerPrefs.SetFloat("HP" + plPrefs, currHp);
                PlayerPrefs.SetInt("Level" + plPrefs, level);
                PlayerPrefs.SetInt(plPrefs + "cost", currCost);
                PlayerPrefs.Save();

                DefineUnit(); //Refresh Menu
            }
            else
            {
                WindowManager.CallWindow("Not Enough Coin", WindowProtectionLevel.open);
            }
        }
        else
        {
            WindowManager.CallWindow("Level has maxed out", WindowProtectionLevel.open);
            PlayerPrefs.SetInt(plPrefs + "Max", 1);
            PlayerPrefs.Save();
        }
    }

    public void LockStatus()
    {
        price.text = gemCost.ToString();
        if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
        {
            lockState = false;
            upgradeButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            lockState = true;
            upgradeButton.GetComponent<Button>().interactable = false;
        }
    }

    /// <summary>
    /// Check if this character is unlocked by curremt level
    /// </summary>
    void CompareCompletedLevels()
    {
        for (int i = 0; i < 53; i++)
        {
            if (PlayerPrefs.GetInt("ClearStage" + i) == 1)
            {
                currentUnlockedLevel = i;
            }
        }

        switch (heroUnit)
        {
            case Heroes.bima:
                if (currentUnlockedLevel >= 3)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.shrikandi:
                if (currentUnlockedLevel >= 4)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.yudhistira:
                if (currentUnlockedLevel >= 5)
                {
                    UnlockingCharacter();
                }
                break;
                
            case Heroes.nakulaSadewa:
                if (PlayerPrefs.GetInt("Lock"+plPrefs) == 1)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.ghatotkacha:
                if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.drupada:
                if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.khresna:
                if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
                {
                    UnlockingCharacter();
                }
                break;
            case Heroes.anoman:
                if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
                {
                    UnlockingCharacter();
                }
                break;
                
        }
    }

    /// <summary>
    /// Check if level upgrade is approriate for current level
    /// </summary>
    void LevelCapControl()
    {
        for (int i = 0; i < 53; i++)
        {
            if (PlayerPrefs.GetInt("ClearStage" + i) == 1)
            {
                currentUnlockedLevel = i;
            }
        }

        if (currentUnlockedLevel >= 0)
        {
            currentMaxLevel = 7;
        }
        if (currentUnlockedLevel >= 5)
        {
            currentMaxLevel = 9;
        }
        if (currentUnlockedLevel >= 11)
        {
            currentMaxLevel = 11;
        }
        if (currentUnlockedLevel >= 16)
        {
            currentMaxLevel = 14;
        }
        if (currentUnlockedLevel >= 21)
        {
            currentMaxLevel = 17;
        }
        if (currentUnlockedLevel >= 33)
        {
            currentMaxLevel = 20;
        }
    }

    /// <summary>
    /// Unlock Character by saving it data to player prefs
    /// </summary>
    public void UnlockingCharacter()
    {
        if (unlockType == UnlockType.progression)
        {
            Debug.Log("Unlocking");
            PlayerPrefs.SetInt("Lock" + plPrefs, 1);
            PlayerPrefs.Save();

            CheckLocked();
            LockStatus();
        }
        else if (unlockType == UnlockType.purchase)
        {
            if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
            {
                Debug.Log("Unlocking");
                PlayerPrefs.SetInt("Lock" + plPrefs, 1);
                PlayerPrefs.Save();

                CheckLocked();
                LockStatus();
            }
            else if (!PlayerPrefs.HasKey("Lock" + plPrefs) && GemContainer.GemValue >= gemCost)
            {
                WindowManager.CallWindow("Are you sure want to purchase this Hero?", NotificationType.questions, delegate
                {
                    GemContainer.purchaseWithGem(gemCost);
                    Debug.Log("Unlocking");
                    PlayerPrefs.SetInt("Lock" + plPrefs, 1);
                    PlayerPrefs.Save();

                    CheckLocked();
                    LockStatus();
                });

            }
            else
            {
                WindowManager.CallWindow("Not Enough gem to purchase Hero", WindowProtectionLevel.open);
            }
        }
    }

    /// <summary>
    /// Double Check lock character
    /// </summary>
    void CheckLocked()
    {
        if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
        {
            lockState = false;
        }
        else
        {
            lockState = true;
        }

        if (lockState == true)
        {
            lockedImage.SetActive(true);
        }
        else
        {
            lockedImage.SetActive(false);
        }

    }

    /// <summary>
    /// Function that run at the latest order to display attribute 
    /// </summary>
    void DisplayText()
    {
        levelText = transform.Find("Level").gameObject;
        powerStatText = transform.Find("Strength" + "/StrVar").gameObject; // get grandchild component
        healthStatText = transform.Find("HP" + "/HealthVar").gameObject; // get grandchild component
        costStatText = transform.Find("Cost" + "/CostVar").gameObject; // get grandchild component
        maxLevel = transform.Find("Level/LimitLevel").GetComponent<Text>();

        levelText.GetComponent<Text>().text = level.ToString();
        powerStatText.GetComponent<Text>().text = currAtk.ToString();
        healthStatText.GetComponent<Text>().text = currHp.ToString();
        costStatText.GetComponent<Text>().text = currCost.ToString();
        maxLevel.text = "/ " + currentMaxLevel.ToString();
    }

    void DefineUnlockType()
    {
        string continueProgress = "Continue Progression to unlock hero";
        string purchaseHero = "Purchase using Gem to unlock Hero";
        switch(unlockType)
        {
            case UnlockType.progression:
                unlockButton.SetActive(false);
                requirementText.text = continueProgress;
                lockIcon.SetActive(true);

                break;
            case UnlockType.purchase:
                unlockButton.SetActive(true);
                requirementText.text = purchaseHero;
                lockIcon.SetActive(false);
                unlockButton.GetComponent<Button>().onClick.AddListener(delegate { UnlockingCharacter(); });
                break;
        }
    }
}
