﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasicUnitStats : MonoBehaviour
{
    GameObject levelText;
    GameObject healthStatText;
    GameObject powerStatText;
    GameObject costStatText;
    Text maxLevel;
    public GameObject lockedImage;
    GameObject upgradeButton;
    Button upgradeListener;

    #region Default Stats
    public int level;
    public float atk;
    public float hp;
    public int cost;
    #endregion

    #region Stat Changes
    public float upAtk;
    public float upHp;
    public int upCost;
    #endregion

    #region CurrentStats
    public int currAtk;
    public int currHp;
    public int currCost;
    #endregion

    public bool lockState;
    public int currentUnlockedLevel;
    public int currentMaxLevel;

    [Header("Unit Identifier (Fill noState in Hero dropdown)")] 
    public BasicUnits basicUnitsName;
    [Header("Strings that call Playerprefs String addresses")]
    public string plPrefs;

    #region Units Default Attribute
    //region unit UPGRADE COST
    const int costfoot = 10;
    const int costarcher = 10;
    const int costsword = 12;
    const int costgun = 12;
    const int costspear = 14;
    const int costmage = 14;
    const int costshaman = 16;
    const int costelephant = 18;
    const int costtank = 20;
    //region unit HP
    const int hpivfoot = 200;
    const int hpivarcher = 150;
    const int hpivsword = 300;
    const int hpivgun = 220;
    const int hpivspear = 300;
    const int hpivmage = 150;
    const int hpivshaman = 160;
    const int hpivelephant = 800;
    const int hpivtank = 400;
    //region unit ATK
    const int atkivfoot = 30;
    const int atkivarcher = 20;
    const int atkivsword = 60;
    const int atkivgun = 30;
    const int atkivspear = 60;
    const int atkivmage = 60;
    const int atkivshaman = 20;
    const int atkivelephant = 100;
    const int atkivtank = 80;
    #endregion

    private void Awake()
    {
        lockedImage = transform.Find("LockPanel").gameObject;
        upgradeButton = transform.Find("Upgrade").gameObject;
        upgradeListener = upgradeButton.GetComponent<Button>();

        #region unlock default Unit and Hero
        PlayerPrefs.SetInt("LockFoot", 1);
        #endregion

        CompareCompletedLevels();
        DefineUnit();
        LevelCapControl();
        LockStatus();
    }

    private void OnEnable()
    {
        upgradeListener.onClick.AddListener(delegate { UpgradeUnit(); });
    }

    private void OnDisable()
    {
        upgradeListener.onClick.RemoveAllListeners();
    }
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void LateUpdate()
    {
        DisplayText();
    }

    /// <summary>
    /// Run at Starts and Events
    /// </summary>
    void DefineUnit()
    {
        switch (basicUnitsName)
        {
            case BasicUnits.footman:
                atk = atkivfoot;
                hp = hpivfoot;
                cost = costfoot;
                LoadStats(UnitLists.footman);
                break;
            case BasicUnits.archer:
                atk = atkivarcher;
                hp = hpivarcher;
                cost = costarcher;
                LoadStats(UnitLists.archer);
                break;
            case BasicUnits.swordman:
                atk = atkivsword;
                hp = hpivsword;
                cost = costsword;
                LoadStats(UnitLists.swordman);
                break;
            case BasicUnits.gunner:
                atk = atkivgun;
                hp = hpivgun;
                cost = costgun;
                LoadStats(UnitLists.gunner);
                break;
            case BasicUnits.spearman:
                atk = atkivspear;
                hp = hpivspear;
                cost = costspear;
                LoadStats(UnitLists.spearman);
                break;
            case BasicUnits.mage:
                atk = atkivmage;
                hp = hpivmage;
                cost = costmage;
                LoadStats(UnitLists.mage);
                break;
            case BasicUnits.shaman:
                atk = atkivshaman;
                hp = hpivshaman;
                cost = costshaman;
                LoadStats(UnitLists.shaman);
                break;
            case BasicUnits.elephantry:
                atk = atkivelephant;
                hp = hpivelephant;
                cost = costelephant;
                LoadStats(UnitLists.elephantry);
                break;
            case BasicUnits.tank:
                atk = atkivtank;
                hp = hpivtank;
                cost = costtank;
                LoadStats(UnitLists.tank);
                break;
        }
    }

    void LoadStats(UnitLists unitName)
    {
        Debug.Log("Loading Stats" + basicUnitsName);

        //fix needed when after build
        #region Loading DefaultStats 
        //atk = GameObject.Find("HeroItemPrefabContainer").GetComponent<HeroItemPrefabContainer>().unitsPrefabs[(int)unitName].GetComponent<UnitAtt>().ATK;
        //hp = GameObject.Find("HeroItemPrefabContainer").GetComponent<HeroItemPrefabContainer>().unitsPrefabs[(int)unitName].GetComponent<UnitAtt>().HP;
        //cost = GameObject.Find("HeroItemPrefabContainer").GetComponent<HeroItemPrefabContainer>().unitsPrefabs[(int)unitName].GetComponent<UnitAtt>().COST;
        #endregion

        currAtk = (int)atk;
        currHp = (int)hp;
        currCost = cost;
        level = PlayerPrefs.GetInt("Level" + plPrefs);
        
        if (level == 0)
        {
            level = 1;
            PlayerPrefs.SetFloat("ATK" + plPrefs, currAtk);
            PlayerPrefs.SetFloat("HP" + plPrefs, currHp);
            PlayerPrefs.SetInt(plPrefs + "cost", currCost);

            PlayerPrefs.SetInt("CurrATK" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.SetInt("CurrHP" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.Save();
        }
        
        else if(level >= 2)
        { 
            //return;
            //Debug.Log("Access this");
            upAtk =  (PlayerPrefs.GetInt("Level" + plPrefs) * atk) * 0.35f;
            upHp = (PlayerPrefs.GetInt("Level" + plPrefs) * hp) * 0.35f;
            upCost = PlayerPrefs.GetInt(plPrefs + "cost") + ((PlayerPrefs.GetInt("Level" + plPrefs) + 2) * cost);
            //PlayerPrefs.SetInt(plPrefs + "Max", 0); // some buggy code needed hehe

            currAtk = (int)(atk + upAtk);
            currHp = (int)(hp + upHp);
            currCost = cost + upCost;

            PlayerPrefs.SetInt("CurrATK" + plPrefs, currAtk); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.SetInt("CurrHP" + plPrefs, currHp); //these variables pack in prefs to be passed to LoadAtt.cs
            PlayerPrefs.Save();
        }
        CompareCompletedLevels();

        Debug.Log(PlayerPrefs.GetInt("CurrHP"+plPrefs));
    }

    public void UpgradeUnit()
    {
        Debug.Log("Upgrading Character");
        if (level < currentMaxLevel)
        {
            if (GoldUpdate.goldValue >= currCost)
            {
                GoldContainer.purchaseWithGold(currCost);

                level += 1;

                upAtk = (PlayerPrefs.GetInt("Level" + plPrefs) * atk);
                upHp = (PlayerPrefs.GetInt("Level" + plPrefs) * hp);
                upCost = PlayerPrefs.GetInt(plPrefs + "cost") + ((PlayerPrefs.GetInt("Level" + plPrefs) + 2) * cost);

                currAtk = (int)(atk + upAtk) ;
                currHp = (int)(hp + upHp);
                currCost = cost + upCost;

                //PlayerPrefs.SetInt(plPrefs + "Upgrade", 1);
                PlayerPrefs.SetFloat("ATK" + plPrefs, currAtk);
                PlayerPrefs.SetFloat("HP" + plPrefs, currHp);
                PlayerPrefs.SetInt("Level" + plPrefs, level);
                PlayerPrefs.SetInt(plPrefs + "cost", currCost);
                PlayerPrefs.Save();

                DefineUnit(); //Refresh Menu
            }
            else
            {
                WindowManager.CallWindow("Not Enough Coin", WindowProtectionLevel.open);
            }
        }
        else
        {
            WindowManager.CallWindow("Level Max", WindowProtectionLevel.open);
            PlayerPrefs.SetInt(plPrefs + "Max", 1);
            PlayerPrefs.Save();
        }
    }

    public void LockStatus()
    {
        if (PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
        {
            lockState = false;
            upgradeButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            lockState = true;
            upgradeButton.GetComponent<Button>().interactable = false;
        }
    }
   
    /// <summary>
    /// Check if this character is unlocked by curremt level
    /// </summary>
    void CompareCompletedLevels()
    {
        for(int i = 0; i < 53; i++)
        {
            if(PlayerPrefs.GetInt("ClearStage" + i) == 1)
            {
                currentUnlockedLevel = i; 
            }
        }
        switch (basicUnitsName)
        {
            case BasicUnits.archer:
                if(currentUnlockedLevel >= 3)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.swordman:
                if (currentUnlockedLevel >= 6)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.gunner:
                if (currentUnlockedLevel >= 9)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.spearman:
                if (currentUnlockedLevel >= 12)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.mage:
                if (currentUnlockedLevel >= 15)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.shaman:
                if (currentUnlockedLevel >= 18)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.elephantry:
                if (currentUnlockedLevel >= 21)
                {
                    UnlockingCharacter();
                }
                break;
            case BasicUnits.tank:
                if (currentUnlockedLevel >= 24)
                {
                    UnlockingCharacter();
                }
                break;
        }
    }

    /// <summary>
    /// Check if level upgrade is approriate for current level
    /// </summary>
    void LevelCapControl()
    {
        for (int i = 0; i < 53; i++)
        {
            if (PlayerPrefs.GetInt("ClearStage" + i) == 1)
            {
                currentUnlockedLevel = i;
                Debug.Log("UNLOCKED LEVELS : " + currentUnlockedLevel);
            }
        }

        if (currentUnlockedLevel >= 0)
        {
            currentMaxLevel = 7;
        }
        if (currentUnlockedLevel >= 5)
        {
            currentMaxLevel = 9;
        }
        if (currentUnlockedLevel >= 11)
        {
            currentMaxLevel = 11;
        }
        if (currentUnlockedLevel >= 16)
        {
            currentMaxLevel = 14;
        }
        if (currentUnlockedLevel >= 21)
        {
            currentMaxLevel = 17;
        }
        if (currentUnlockedLevel >= 33)
        {
            currentMaxLevel = 20;
        }
    }

    /// <summary>
    /// Unlock Character by saving it data to player prefs
    /// </summary>
    private void UnlockingCharacter()
    {
        Debug.Log("Unlocking");
        PlayerPrefs.SetInt("Lock" + plPrefs, 1);
        PlayerPrefs.Save();

        CheckLocked();
    }

    /// <summary>
    /// Double Check lock character
    /// </summary>
    void CheckLocked()
    {
        if(PlayerPrefs.GetInt("Lock" + plPrefs) == 1)
        {
            lockState = false;
        }
        else
        {
            lockState = true;
        }

        if (lockState == true)
        {
            lockedImage.SetActive(true);
        }
        else
        {
            lockedImage.SetActive(false);
        }

    }

    /// <summary>
    /// Function that run at the latest order to display attribute 
    /// </summary>
    void DisplayText()
    {
        levelText = transform.Find("Level").gameObject;
        powerStatText = transform.Find("Strength" + "/StrVar").gameObject; // get grandchild component
        healthStatText = transform.Find("HP" + "/HealthVar").gameObject; // get grandchild component
        costStatText = transform.Find("Cost" + "/CostVar").gameObject; // get grandchild component
        maxLevel = transform.Find("Level/LimitLevel").GetComponent<Text>();

        levelText.GetComponent<Text>().text = level.ToString();
        powerStatText.GetComponent<Text>().text = currAtk.ToString();
        healthStatText.GetComponent<Text>().text = currHp.ToString();
        costStatText.GetComponent<Text>().text = currCost.ToString();
        maxLevel.text = "/ " + currentMaxLevel.ToString();
    }
}
