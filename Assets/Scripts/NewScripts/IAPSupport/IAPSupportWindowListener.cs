﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPSupportWindowListener : MonoBehaviour
{
    public void InIAPSuccess()
    {
        WindowManager.CallWindow("Purchase Succeed!", WindowProtectionLevel.open);
    }

    public void InIAPCancelled()
    {
        WindowManager.CallWindow("Purchase Cancelled!", WindowProtectionLevel.open);
    }

    public void InIAPFailed()
    {
        WindowManager.CallWindow("Purchase Failed! Or Cancelled \r\n Please try again later", WindowProtectionLevel.open);
    }
}
