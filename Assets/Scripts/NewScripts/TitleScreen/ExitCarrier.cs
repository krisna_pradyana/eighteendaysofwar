﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitCarrier : MonoBehaviour
{
    private void Update()
    {
        if (WindowManager.protectionLevel == WindowProtectionLevel.open)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                WindowManager.CallWindow("Do you want to exit?", NotificationType.questions, (delegate
                {
                    Application.Quit();
                }));
            }
        }
    }
}

