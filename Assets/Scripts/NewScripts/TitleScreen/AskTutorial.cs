﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AskTutorial : MonoBehaviour
{
    public void CallTutorial()
    {
        LO_LoadScene _LoadScene;
        LO_SelectStyle _SelectStyle;

        _LoadScene = GameObject.FindObjectOfType<LO_LoadScene>();
        _SelectStyle = GameFlow.FindObjectOfType<LO_SelectStyle>();

        _SelectStyle.SetStyle("Stock_Style");

        WindowManager.CallWindow("Do you want to play tutorial?", NotificationType.questions, (delegate { _LoadScene.ChangeToScene("Tutorial"); }));
    }
}
