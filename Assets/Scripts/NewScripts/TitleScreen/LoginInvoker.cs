﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoginInvoker : MonoBehaviour
{
    private void Start()
    {
        PlayGamesInstance.SignIn();   
    }

    public void ShowAchivementGoogle()
    {
        PlayGamesInstance.ShowAchivement();
    }

    public void SigOut()
    {
        PlayGamesInstance.SignOut();
    }
}
