﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtworkScript : MonoBehaviour
{
    public GameObject canvasToShow;
    public Animator animatorStateToChange;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SkipArtworkIntro()
    {
        canvasToShow.SetActive(true);
        animatorStateToChange.SetTrigger("Skipped");
    }
}
