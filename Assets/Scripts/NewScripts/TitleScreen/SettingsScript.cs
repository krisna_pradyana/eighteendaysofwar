﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using antilunchbox;

public class SettingsScript : MonoBehaviour
{
    public delegate void DelegateEnableBGM();
    public delegate void DelegateEnableSFX();
    public delegate void DelegateDisableBGM(bool state);
    public delegate void DelegateDisableSFX(bool state);

    public static DelegateEnableBGM delegateEnableBGM;
    public static DelegateEnableSFX delegateEnableSFX;
    public static DelegateDisableBGM bgmIcon;
    public static DelegateDisableSFX sfxIcon;

    public Button buttonSFX;
    public Button buttonBGM;

    public Image disabledBGM;
    public Image disabledSFX;
    SoundManager audioSourcePro;

    public static bool enabledBGM;
    public static bool enabledSFX;
    
    private void OnEnable()
    {
        delegateEnableSFX += EnableSFX;
        delegateEnableBGM += EnableBGM;
        bgmIcon += InitDisableBGMIcon;
        sfxIcon += InitDisableSFXIcon;
    }

    private void OnDisable()
    {
        delegateEnableSFX -= EnableSFX;
        delegateEnableBGM -= EnableBGM;
        bgmIcon -= InitDisableBGMIcon;
        sfxIcon -= InitDisableSFXIcon;
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSourcePro = FindObjectOfType<SoundManager>();

        if (!PlayerPrefs.HasKey("EnabledBGM"))
        {
            PlayerPrefs.SetInt("EnabledBGM", 1);
            PlayerPrefs.Save();
            enabledBGM = true;
            InitDisableBGMIcon(false);
        }
        else
        {
            if(PlayerPrefs.GetInt("EnabledBGM") == 1)
            {
                enabledBGM = true;
                audioSourcePro.mutedMusic = false;
                InitDisableBGMIcon(false);
            }
            else
            {
                enabledBGM = false;
                audioSourcePro.mutedMusic = true;
                InitDisableBGMIcon(true);
            }
        }
    
        if (!PlayerPrefs.HasKey("EnabledSFX"))
        {
            PlayerPrefs.SetInt("EnabledSFX", 1);
            PlayerPrefs.Save();
            enabledSFX = true;
            InitDisableSFXIcon(false);
        }
        else
        {
            if(PlayerPrefs.GetInt("EnabledSFX") == 1)
            {
                enabledSFX = true;
                OnClickEvents.sfxCheck();
                InitDisableSFXIcon(false);
            }
            else
            {
                enabledSFX = false;
                OnClickEvents.sfxCheck();
                InitDisableSFXIcon(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitDisableBGMIcon(bool state)
    {
        disabledBGM.enabled = state;
    }

    void InitDisableSFXIcon(bool state)
    {
        disabledSFX.enabled = state;
    }

    public void EnableBGM()
    {
        Debug.Log("ChangedBGM");
        if(enabledBGM == true)
        {
            enabledBGM = false;
            PlayerPrefs.SetInt("EnabledBGM", 0);
            PlayerPrefs.Save();
            audioSourcePro.mutedMusic = true;
            InitDisableBGMIcon(true);   
        }
        else
        {
            enabledBGM = true;
            PlayerPrefs.SetInt("EnabledBGM", 1);
            PlayerPrefs.Save();
            audioSourcePro.mutedMusic = false;
            InitDisableBGMIcon(false);
        }
    }

    public void EnableSFX()
    {
        if (enabledSFX == true)
        {
            enabledSFX = false;
            PlayerPrefs.SetInt("EnabledSFX", 0);
            PlayerPrefs.Save();
            OnClickEvents.sfxCheck();
            InitDisableSFXIcon(true);
        }
        else
        {
            enabledSFX = true;
            PlayerPrefs.SetInt("EnabledSFX", 1);
            PlayerPrefs.Save();
            OnClickEvents.sfxCheck();
            InitDisableSFXIcon(false);
        }
    }
}
