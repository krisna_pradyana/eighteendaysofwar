﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadingScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }


}
