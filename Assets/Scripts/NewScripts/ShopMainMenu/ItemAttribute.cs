﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemAttribute : MonoBehaviour
{
    Text stock;
    public ItemCategory itemCategory;
    int itemStock;
    public int cost;
    string prefsName;
    int purchased;

    LO_SelectStyle selectLoadStyle;
    LO_LoadScene loadScene;

    public Button purchaseButton;
    public Text displayStock;

    private void OnDisable()
    {
        if (purchaseButton != null)
        {
            purchaseButton.onClick.RemoveAllListeners();
        }
        else
        {
            return;
        }
    }

    private void OnEnable()
    {
        InitializeObjects();
        IdentifyItemCategory();
    }

    private void Start()
    {
        selectLoadStyle = GameObject.FindObjectOfType<LO_SelectStyle>();
        loadScene = GameObject.FindObjectOfType<LO_LoadScene>();
    }

    void InitializeObjects()
    {
        purchaseButton = GameObject.Find(this.gameObject.name + "/Purchase").GetComponent<Button>();
        displayStock = GameObject.Find(this.gameObject.name + "/Stock").GetComponent<Text>();

        purchaseButton.onClick.AddListener(delegate { PurchaseItem(); });
    }

    void IdentifyItemCategory()
    {
        switch(itemCategory)
        {
            case ItemCategory.gameSpeed2x:
                prefsName = "Gamespeed";
                purchased = PlayerPrefs.GetInt(prefsName);
  
                break;
            case ItemCategory.gameSpeed3x:
                prefsName = "Gamespeed";
                purchased = PlayerPrefs.GetInt(prefsName);
    
                break;
            ////////////////////////////////////////////////////
            ////////////////////////////////////////////////////
            case ItemCategory.atk10:
                prefsName = "Attack10";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.atk30:
                prefsName = "Attack30";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.mov10:
                prefsName = "MovementSpeed10";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.mov30:
                prefsName = "MovementSpeed30";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.hp10:
                prefsName = "HP10";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.hp30:
                prefsName = "HP30";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.res10:
                prefsName = "Resource10";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;

            case ItemCategory.res30:
                prefsName = "Resource30";
                itemStock = PlayerPrefs.GetInt(prefsName);
                displayStock.text = "Stock : " + itemStock;
                break;
        }
    }

    public void PurchaseItem()
    {
        if(GemContainer.GemValue >= cost)
        {
            WindowManager.CallWindow("Do you want to purchase this item?", NotificationType.questions, (delegate
            {
                PlayerPrefs.SetInt(prefsName, itemStock + 1);
                PlayerPrefs.Save();
                IdentifyItemCategory();
                GemContainer.purchaseWithGem(cost);
            }));
        }
        else
        {
            WindowManager.CallWindow("Not enough Gem to purchase", NotificationType.confirmation, (delegate
            {
                WindowManager.CallWindow("Do you want to purchase some Gem?", NotificationType.questions, (delegate 
                {
                    selectLoadStyle.SetStyle("Stock_Style");
                    loadScene.ChangeToScene("Purchase Gem");      
                }));
            }));
        }
    }
}
