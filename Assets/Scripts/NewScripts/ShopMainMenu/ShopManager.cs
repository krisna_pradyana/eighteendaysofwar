﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopManager : MonoBehaviour
{
    public Button unitButton;
    public Button heroButton;
    public Button itemButton;

    public GameObject[] scrollsMenu;

    MenuStates menuStates;

    // Start is called before the first frame update
    void Start()
    {
        scrollsMenu[0] = null;
        UpdateWindow((int)MenuStates.showUnit);
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        InteractableButtons();
    }

    public void UpdateWindow(int state)
    {
        menuStates = (MenuStates)state;
        for (int i = 0; i < scrollsMenu.Length; i ++)
        {
            if(scrollsMenu[i] == null)
            {
                continue;
            }
            scrollsMenu[i].SetActive(false);
        }
        switch((MenuStates)state)
        {
            case MenuStates.showUnit:
                Debug.Log("ShowingUnit");
                scrollsMenu[state].SetActive(true);
                break;
            case MenuStates.showHero:
                Debug.Log("ShowingHero");
                scrollsMenu[state].SetActive(true);
                break;
            case MenuStates.showItem:
                Debug.Log("ShowingItems"); 
                scrollsMenu[state].SetActive(true);
                break;
        }
    }

    void InteractableButtons()
    {
        if(menuStates == MenuStates.showUnit)
        {
            unitButton.interactable = false;
        }
        else
        {
            unitButton.interactable = true;
        }

        if (menuStates == MenuStates.showHero)
        {
            heroButton.interactable = false;
        }
        else
        {
            heroButton.interactable = true;
        }

        if (menuStates == MenuStates.showItem)
        {
            itemButton.interactable = false;
        }
        else
        {
            itemButton.interactable = true;
        }
    }

    public void LoadDesiredScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
