﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeAchivement : MonoBehaviour
{
    public void UnlockAchivements(string achivementId)
    {
        PlayGamesInstance.AchivementCompleted(achivementId);
    }
}
