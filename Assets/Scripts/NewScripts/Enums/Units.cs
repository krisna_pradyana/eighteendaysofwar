﻿public enum BasicUnits
{
    noState, footman, archer, swordman, gunner, spearman, mage, shaman, elephantry, tank
}

public enum Heroes
{
    noState, arjuna, bima, shrikandi, yudhistira, nakulaSadewa, ghatotkacha, drupada, khresna, anoman 
}

public enum UnitLists
{
    footman = 0, archer = 1, swordman = 2, gunner = 3, spearman = 4, mage = 5, shaman = 6, elephantry = 7, tank = 8,
    arjuna = 9, bima = 10, shrikandi = 11, yudhistira = 12, nakulaSadewa = 13, ghatotkacha = 14, drupada = 15, khresna = 16, anoman =17
}

public enum UnlockType
{
    progression, purchase
}