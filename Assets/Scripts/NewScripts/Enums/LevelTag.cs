﻿public enum LevelTag
{
    noState = 0,
    stage1, stage2, stage3, stage4, stage5, stage6, stage7, stage8, stage9, stage10,
    stage11, stage12, stage13, stage14, stage15, stage16, stage17, stage18, stage19, stage20,
    stage21, stage22, stage23, stage24, stage25, stage26, stage27, stage28, stage29, stage30,
    stage31, stage32, stage33, stage34, stage35, stage36, stage37, stage38, stage39, stage40,
    stage41, stage42, stage43, stage44, stage45, stage46, stage47, stage48, stage49, stage50,
    stage51, stage52, stage53, stage54
}