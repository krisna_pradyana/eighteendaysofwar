﻿public enum Choices
{
    confirm = 0,
    accept = 1,
    cancel = 2
}

public enum NotificationType
{
    disabled = 0,
    confirmation = 1,
    questions = 2,
}

public enum WindowProtectionLevel
{
    open = 0,
    protect = 1,
}