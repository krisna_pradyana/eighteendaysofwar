﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using ChartboostSDK;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AdInterfaceScript : MonoBehaviour
{
    private static AdInterfaceScript _instance;

    public Chartboost chartboost;
    public static int watchCount;
    public static bool hasRewardedVideo;
    Button adButton;

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        SetupDelegates();
        SceneManager.sceneLoaded += CheckAdsButton;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= CheckAdsButton;
        // Remove event handlers
        Chartboost.didInitialize -= didInitialize;
        Chartboost.didFailToLoadInterstitial -= didFailToLoadInterstitial;
        Chartboost.didDismissInterstitial -= didDismissInterstitial;
        Chartboost.didCloseInterstitial -= didCloseInterstitial;
        Chartboost.didClickInterstitial -= didClickInterstitial;
        Chartboost.didCacheInterstitial -= didCacheInterstitial;
        //Chartboost.shouldDisplayInterstitial -= shouldDisplayInterstitial;
        //Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
        Chartboost.didFailToRecordClick -= didFailToRecordClick;
        Chartboost.didFailToLoadRewardedVideo -= didFailToLoadRewardedVideo;
        Chartboost.didDismissRewardedVideo -= didDismissRewardedVideo;
        Chartboost.didCloseRewardedVideo -= didCloseRewardedVideo;
        Chartboost.didClickRewardedVideo -= didClickRewardedVideo;
        Chartboost.didCacheRewardedVideo -= didCacheRewardedVideo;
        //Chartboost.shouldDisplayRewardedVideo -= shouldDisplayRewardedVideo;
        Chartboost.didCompleteRewardedVideo -= didCompleteRewardedVideo;
        //Chartboost.didDisplayRewardedVideo -= didDisplayRewardedVideo;
        Chartboost.didPauseClickForConfirmation -= didPauseClickForConfirmation;
        //Chartboost.willDisplayVideo -= willDisplayVideo;
#if UNITY_IPHONE
		Chartboost.didCompleteAppStoreSheetFlow -= didCompleteAppStoreSheetFlow;
#endif
    }

    void CheckAdsButton(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "PurhcaseGem")
        {
            adButton = GameObject.Find("Window - IAPPlaystore/IAPItemGemshard/buyButton").GetComponent<Button>();
        }
    }

    private void Start()
    {
        Chartboost.setAutoCacheAds(true);
        CBExternal.cacheRewardedVideo(CBLocation.Default);

        if (!PlayerPrefs.HasKey("GemShard"))
        {
            PlayerPrefs.SetInt("GemShard", 0);
            PlayerPrefs.Save();
        }
    }

    public static void ClickAdButton()
    {
        if (Chartboost.hasRewardedVideo(CBLocation.Default))
        {
            Chartboost.showRewardedVideo(CBLocation.Default);
            return;
        }
        else
        {
            WindowManager.CallWindow("There's no Ads right now", WindowProtectionLevel.open);
        }
    }

    void SetupDelegates()
    {
        // Listen to all impression-related events
        Chartboost.didInitialize += didInitialize;
        Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
        Chartboost.didDismissInterstitial += didDismissInterstitial;
        Chartboost.didCloseInterstitial += didCloseInterstitial;
        Chartboost.didClickInterstitial += didClickInterstitial;
        Chartboost.didCacheInterstitial += didCacheInterstitial;
        //Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
        //Chartboost.didDisplayInterstitial += didDisplayInterstitial;
        Chartboost.didFailToRecordClick += didFailToRecordClick;
        Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
        Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
        Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
        Chartboost.didClickRewardedVideo += didClickRewardedVideo;
        Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
        //Chartboost.shouldDisplayRewardedVideo += shouldDisplayRewardedVideo;
        Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
        //Chartboost.didDisplayRewardedVideo += didDisplayRewardedVideo;
        Chartboost.didPauseClickForConfirmation += didPauseClickForConfirmation;
        //Chartboost.willDisplayVideo += willDisplayVideo;
#if UNITY_IPHONE
		Chartboost.didCompleteAppStoreSheetFlow += didCompleteAppStoreSheetFlow;
#endif
    }


    void didInitialize(bool status)
    {
        
    }

    void didFailToLoadInterstitial(CBLocation location, CBImpressionError error)
    {
        hasRewardedVideo = false;
        WindowManager.CallWindow("No Ads available right now", WindowProtectionLevel.open);
    }

    void didDismissInterstitial(CBLocation location)
    {
       
    }

    void didCloseInterstitial(CBLocation location)
    {

    }

    void didClickInterstitial(CBLocation location)
    {
      
    }

    void didCacheInterstitial(CBLocation location)
    {

    }

    /*
    bool shouldDisplayInterstitial(CBLocation location)
    {
        // return true if you want to allow the interstitial to be displayed
    
    }
    */

    void didDisplayInterstitial(CBLocation location)
    {
    
    }

    void didFailToRecordClick(CBLocation location, CBClickError error)
    {
      
    }

    void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error)
    {
        if (adButton != null)
        {
            adButton.interactable = false;
        }
    }

    void didDismissRewardedVideo(CBLocation location)
    {
        
    }

    void didCloseRewardedVideo(CBLocation location)
    {
      
    }

    void didClickRewardedVideo(CBLocation location)
    {
        
    }

    void didCacheRewardedVideo(CBLocation location)
    {
        //enable button here
        if (adButton != null)
        {
            adButton.interactable = true;
        }
    }

    /*
    bool shouldDisplayRewardedVideo(CBLocation location)
    {
      
    }
    */

    void didCompleteRewardedVideo(CBLocation location, int reward)
    {
        int shards = PlayerPrefs.GetInt("GemShard");
        if (shards == 4)
        {
            GemContainer.addGem(1);
            PlayerPrefs.SetInt("GemShard", 0);
            PlayerPrefs.Save();
            WindowManager.CallWindow("Congrats! you just Got 2 Gem", WindowProtectionLevel.open);
        }
        else
        {
            PlayerPrefs.SetInt("GemShard", shards + 1);
            PlayerPrefs.Save();
        }
        Gemshard.checkingShard();
    }

    void didPauseClickForConfirmation()
    {
#if UNITY_IPHONE
		AddLog("didPauseClickForConfirmation called");
		activeAgeGate = true;
#endif
    }
}
