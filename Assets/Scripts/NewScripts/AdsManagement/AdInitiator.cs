﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartboostSDK;

public class AdInitiator : MonoBehaviour
{
    private void Start()
    {
        LoadIntersitial();    
    }

    public void LoadIntersitial()
    {
        //check if user already purchased ad skip
        if(PlayerPrefs.HasKey("SkipAd"))
        {
            return;
        }
        else
        {
            //AdInterfaceScript.InitAddCache();
            Chartboost.cacheInterstitial(CBLocation.LevelStart);
        }
    }
}
