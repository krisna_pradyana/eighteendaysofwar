﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitParticle : MonoBehaviour
{
    public ParticleSystem particleEmitter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!particleEmitter.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }
}
