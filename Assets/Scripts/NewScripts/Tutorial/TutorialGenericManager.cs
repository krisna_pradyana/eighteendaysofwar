﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Control Tutorial, Check Playprefs is tutorial done or not
/// </summary>
public class TutorialGenericManager : MonoBehaviour
{
    static TutorialGenericManager _instance;

    public delegate void SettutorState(TutorialState state);
    public static SettutorState setTutorState;

    public GameObject mainMenuTutor;
    public GameObject stageSelectTutor;
    public GameObject warTutor;
    public GameObject bossTutor;
    Image background;
    Scrollbar currentTutorScrollbar;
    public GameObject dissmissButton;

    public TutorialState tutorialState;

    private int stageCostToPass;

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            _instance = this;
        }
    }

    private void OnEnable()
    {
        if (!PlayerPrefs.HasKey("DoneTutorial"))
        {
            SceneManager.sceneLoaded += SetTutorialState;
        }
        setTutorState += SetTutorVarInstance;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SetTutorialState;
        setTutorState -= SetTutorVarInstance;
        currentTutorScrollbar.onValueChanged.RemoveAllListeners();
    }

    private void Start()
    {
        background = this.GetComponent<Image>();
        dissmissButton.GetComponent<Button>().onClick.AddListener(() => DissmissTutorial());
    }

    private void CheckValue()
    {
        if (currentTutorScrollbar.value > 0.95f)
        {
            dissmissButton.gameObject.SetActive(true);
        }
        else
        {
            dissmissButton.gameObject.SetActive(false);
        }
    }    

    void SetTutorVarInstance(TutorialState state)
    {
        tutorialState = state;
    }

    void SetTutorialState(Scene scene, LoadSceneMode mode)
    {
        if (currentTutorScrollbar != null)
        {
            currentTutorScrollbar.value = 0;
        }
        switch(scene.name)
        {
            case "MainMenu":
                tutorialState = TutorialState.MainMenu; break;
            case "StageSelect":
                tutorialState = TutorialState.Stage; break;
            default:
                tutorialState = TutorialState.NoState; break;
        }
        SetVisibleTutorial();
    }

    void SetVisibleTutorial()
    {
        DisableTutorials();
        this.transform.SetAsLastSibling();
        switch (tutorialState)
        {
            case TutorialState.MainMenu:
                if (!PlayerPrefs.HasKey("DoneMenuTutorial"))
                {
                    mainMenuTutor.SetActive(true);
                    currentTutorScrollbar = GameObject.Find("MenuTutorPanel/Scrollbar").GetComponent<Scrollbar>();
                    currentTutorScrollbar.onValueChanged.RemoveAllListeners();
                    background.enabled = true;
                    currentTutorScrollbar.onValueChanged.AddListener(delegate { CheckValue(); });
                }
                else
                {
                    DisableTutorials();
                }
                break;

            case TutorialState.Stage:
                if (!PlayerPrefs.HasKey("DoneStageTutorial"))
                {
                    stageSelectTutor.SetActive(true);
                    currentTutorScrollbar = GameObject.Find("StageTutorPanel/Scrollbar").GetComponent<Scrollbar>();
                    currentTutorScrollbar.onValueChanged.RemoveAllListeners();
                    background.enabled = true;
                    currentTutorScrollbar.onValueChanged.AddListener(delegate { CheckValue(); });
                }
                else
                {
                    DisableTutorials();
                }
                break;
                /*
            case TutorialState.War:
                if (!PlayerPrefs.HasKey("DoneWarTutorial"))
                {
                    warTutor.SetActive(true);
                    currentTutorScrollbar = GameObject.Find("WarTutorPanel/Scrollbar").GetComponent<Scrollbar>();
                }
                else
                {
                    DisableTutorials();
                }
                break;
                */
            case TutorialState.NoState:
                DisableTutorials();
                break;
        }
    }

    void DisableTutorials()
    {
        if (currentTutorScrollbar != null)
        {
            currentTutorScrollbar.onValueChanged.RemoveAllListeners();
        }

        mainMenuTutor.SetActive(false);
        stageSelectTutor.SetActive(false);
        bossTutor.SetActive(false);
        warTutor.SetActive(false);
        dissmissButton.SetActive(false);
        background.enabled = false;
    }

    /// <summary>
    /// intercepts Stage Loading, giving level tutorial before start
    /// <para"costTopass"></para>
    /// </summary>
    public static void DisplaySpecsificTutorial(int costToPass)
    {
        _instance.stageCostToPass = costToPass;
        if (_instance.tutorialState == TutorialState.War)
        {
            if (!PlayerPrefs.HasKey("DoneWarTutorial"))
            {
                _instance.DisableTutorials();
                _instance.gameObject.transform.SetAsLastSibling();
                _instance.warTutor.SetActive(true);
                _instance.background.enabled = true;
                _instance.currentTutorScrollbar = GameObject.Find("WarTutorPanel/Scrollbar").GetComponent<Scrollbar>();
                _instance.currentTutorScrollbar.onValueChanged.RemoveAllListeners();
                _instance.currentTutorScrollbar.onValueChanged.AddListener(delegate { _instance.CheckValue(); });
            }
            else
            {
                StageManager.initializingLevel(LevelTag.stage1);
                StageManager.settingCost(_instance.stageCostToPass);
            }
        }
        if (_instance.tutorialState == TutorialState.Boss)
        {
            if (!PlayerPrefs.HasKey("DoneBossTutorial"))
            {
                _instance.DisableTutorials();
                _instance.gameObject.transform.SetAsLastSibling();
                _instance.bossTutor.SetActive(true);
                _instance.background.enabled = true;
                _instance.currentTutorScrollbar = GameObject.Find("BossTutorPanel/Scrollbar").GetComponent<Scrollbar>();
                _instance.currentTutorScrollbar.onValueChanged.RemoveAllListeners();
                _instance.currentTutorScrollbar.onValueChanged.AddListener(delegate { _instance.CheckValue(); });
            }
            else
            {
                StageManager.initializingLevel(LevelTag.stage6);
                StageManager.settingCost(_instance.stageCostToPass);
            }
        }
    }

    void DissmissTutorial()
    {      
        switch(tutorialState)
        {
            case TutorialState.MainMenu: PlayerPrefs.SetInt("DoneMenuTutorial", 0); break;
            case TutorialState.Stage: PlayerPrefs.SetInt("DoneStageTutorial", 0); break;
            case TutorialState.War: PlayerPrefs.SetInt("DoneWarTutorial", 0); StageManager.initializingLevel(LevelTag.stage1); StageManager.settingCost(_instance.stageCostToPass); break;
            case TutorialState.Boss: PlayerPrefs.SetInt("DoneBossTutorial", 0); StageManager.initializingLevel(LevelTag.stage6); StageManager.settingCost(_instance.stageCostToPass); break;
        }
        DisableTutorials();
        tutorialState = TutorialState.NoState;
        PlayerPrefs.Save();
    }
}
