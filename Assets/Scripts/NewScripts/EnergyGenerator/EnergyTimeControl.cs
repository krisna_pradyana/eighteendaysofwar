﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Notifications.Android;

public class EnergyTimeControl : MonoBehaviour
{
    static EnergyTimeControl _instance;
    AndroidNotification notificationEnergy = new AndroidNotification();
    AndroidNotification notificationGem = new AndroidNotification();

    private void Awake()
    {
        if(_instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        AndroidNotificationCenter.NotificationReceivedCallback receivedNotificationHandler =
        delegate (AndroidNotificationIntentData data)
        {           
            var msg = "Notification received : " + data.Id + "\n";
            msg += "\n Notification received: ";
            msg += "\n .Title: " + data.Notification.Title;
            msg += "\n .Body: " + data.Notification.Text;
            msg += "\n .Channel: " + data.Channel;

            switch (data.Channel)
            {
                case "energy_Mahabarat":
                    if (EnergyContainer.EnergyValue <= 120)
                    {
                        EnergyContainer.gainEnergy(120);
                        if (EnergyContainer.EnergyValue > 120)
                        {
                            EnergyContainer.EnergyValue = 120;
                        }
                    }
                    break;
                case "gem_Mahabarat":
                    GemContainer.addGem(1);
                    break;
            }
            Debug.Log(msg);
        };
        AndroidNotificationCenter.OnNotificationReceived += receivedNotificationHandler;
    }

    private void Start()
    {
#if UNITY_ANDROID
        var notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();

        if (notificationIntentData != null)
        {
            var id = notificationIntentData.Id;
            var channel = notificationIntentData.Channel;
            var notification = notificationIntentData.Notification;

            //return notification.IntentData;
            //Debug.Log("Debug Notification : " + notification.IntentData);
            /*
            switch(channel)
            {
                case "energy_Mahabarat":
                    if(EnergyContainer.EnergyValue <= 120)
                    {
                        EnergyContainer.gainEnergy(120);
                        if(EnergyContainer.EnergyValue > 120)
                        {
                            EnergyContainer.EnergyValue = 120;
                        }
                    }
                    break;
                case "gem_Mahabarat": GemContainer.addGem(1);
                    break;
            }
            */
        }
#endif
    }

    private void OnApplicationQuit()
    {
#if UNITY_ANDROID
        var channelEnergy = new AndroidNotificationChannel()
        {
            Id = "energy_Mahabarat",
            Name = "Mahabarat Warriors",
            Importance = Importance.Default,
            Description = "Generic notifications",
        };

        var channelGem = new AndroidNotificationChannel()
        {
            Id = "gem_Mahabarat",
            Name = "Mahabarat Warriors",
            Importance = Importance.Default,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(channelEnergy);
        AndroidNotificationCenter.RegisterNotificationChannel(channelGem);

        notificationEnergy.Title = "Soldiers are ready to war";
        notificationEnergy.Text = "Your soldiers energy are repelnished, Get ready for the next War!";
        notificationEnergy.FireTime = System.DateTime.Now.AddMinutes(30);
        notificationEnergy.IntentData = "{\"GetGem\": \"Notification 1\", \"data\": \"200\"}";
        AndroidNotificationCenter.SendNotification(notificationEnergy, "energy_Mahabarat");

        notificationGem.Title = "Fortune for followers of Dharma";
        notificationGem.Text = "You got 2 Gems";
        notificationGem.FireTime = System.DateTime.Now.AddDays(2);
        notificationGem.IntentData = "{\"GetGem\": \"Notification 1\", \"data\": \"200\"}";
        AndroidNotificationCenter.SendNotification(notificationGem, "gem_Mahabarat");
#endif
    }
}
