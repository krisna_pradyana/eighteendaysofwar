﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartboostSDK;

public class StageManager : MonoBehaviour
{
    static StageManager _instance;

    public delegate void SetStage(LevelTag level);
    public static SetStage initializingLevel;

    public delegate void SetCost(int cost);
    public static SetCost settingCost;

    LO_LoadScene sceneLoader;
    public static string targetLevel;
    public static int currentCost;
    public static LevelTag loadedLevel;

    void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        initializingLevel += TargetLevel;
        settingCost += InitiateLoadStage;
    }

    private void OnDisable()
    {
        initializingLevel -= TargetLevel;
        settingCost -= InitiateLoadStage;
    }

    private void Start()
    {
        targetLevel = "NullStage";
        sceneLoader = GameObject.FindGameObjectWithTag("LoaderManager").GetComponent<LO_LoadScene>();
    }

    public void TargetLevel(LevelTag level)
    {
        loadedLevel = level;

        switch(level)
        {
            #region Select Page 1
            case LevelTag.stage1: targetLevel = "Stage1-1"; break;
            case LevelTag.stage2: targetLevel = "Stage1-2"; break;
            case LevelTag.stage3: targetLevel = "Stage1-3"; break;
            case LevelTag.stage4: targetLevel = "Stage1-4"; break;
            case LevelTag.stage5: targetLevel = "Stage1-5"; break;
            case LevelTag.stage6: targetLevel = "Stage1-6"; break;
            #endregion

            #region Select Page 2
            case LevelTag.stage7: targetLevel = "Stage2-1"; break;
            case LevelTag.stage8: targetLevel = "Stage2-2"; break;
            case LevelTag.stage9: targetLevel = "Stage2-3"; break;
            case LevelTag.stage10: targetLevel = "Stage2-4"; break;
            case LevelTag.stage11: targetLevel = "Stage2-5"; break;
            case LevelTag.stage12: targetLevel = "Stage2-6"; break;
            #endregion

            #region Select Page 3
            case LevelTag.stage13: targetLevel = "Stage3-1"; break;
            case LevelTag.stage14: targetLevel = "Stage3-2"; break;
            case LevelTag.stage15: targetLevel = "Stage3-3"; break;
            case LevelTag.stage16: targetLevel = "Stage3-4";  break;
            case LevelTag.stage17: targetLevel = "Stage3-5";  break;
            case LevelTag.stage18: targetLevel = "Stage3-6";  break;
            #endregion

            #region Select Page 4
            case LevelTag.stage19: targetLevel = "Stage4-1"; break;
            case LevelTag.stage20: targetLevel = "Stage4-2"; break;
            case LevelTag.stage21: targetLevel = "Stage4-3"; break;
            case LevelTag.stage22: targetLevel = "Stage4-4"; break;
            case LevelTag.stage23: targetLevel = "Stage4-5"; break;
            case LevelTag.stage24: targetLevel = "Stage4-6"; break;
            #endregion

            #region Select Page 5
            case LevelTag.stage25: targetLevel = "Stage5-1"; break;
            case LevelTag.stage26: targetLevel = "Stage5-2"; break;
            case LevelTag.stage27: targetLevel = "Stage5-3"; break;
            case LevelTag.stage28: targetLevel = "Stage5-4"; break;
            case LevelTag.stage29: targetLevel = "Stage5-5"; break;
            case LevelTag.stage30: targetLevel = "Stage5-6"; break;
            #endregion

            #region Select Page 6
            case LevelTag.stage31: targetLevel = "Stage6-1"; break;
            case LevelTag.stage32: targetLevel = "Stage6-2"; break;
            case LevelTag.stage33: targetLevel = "Stage6-3"; break;
            case LevelTag.stage34: targetLevel = "Stage6-4"; break;
            case LevelTag.stage35: targetLevel = "Stage6-5"; break;
            case LevelTag.stage36: targetLevel = "Stage6-6"; break;
            #endregion

            #region Select Page 7
            case LevelTag.stage37: targetLevel = "Stage7-1"; break;
            case LevelTag.stage38: targetLevel = "Stage7-2"; break;
            case LevelTag.stage39: targetLevel = "Stage7-3"; break;
            case LevelTag.stage40: targetLevel = "Stage7-4"; break;
            case LevelTag.stage41: targetLevel = "Stage7-5"; break;
            case LevelTag.stage42: targetLevel = "Stage7-6"; break;
            #endregion        

            #region Select Page 8
            case LevelTag.stage43: targetLevel = "Stage8-1"; break;
            case LevelTag.stage44: targetLevel = "Stage8-2"; break;
            case LevelTag.stage45: targetLevel = "Stage8-3"; break;
            case LevelTag.stage46: targetLevel = "Stage8-4"; break;
            case LevelTag.stage47: targetLevel = "Stage8-5"; break;
            case LevelTag.stage48: targetLevel = "Stage8-6"; break;
            #endregion

            #region Select Page 9
            case LevelTag.stage49: targetLevel = "Stage9-1"; break;
            case LevelTag.stage50: targetLevel = "Stage9-2"; break;
            case LevelTag.stage51: targetLevel = "Stage9-3"; break;
            case LevelTag.stage52: targetLevel = "Stage9-4"; break;
            case LevelTag.stage53: targetLevel = "Stage9-5"; break;
            case LevelTag.stage54: targetLevel = "Stage9-6"; break;
            #endregion
        }
    }

    /// <summary>
    /// Loadstage With Cost
    /// </summary>
    /// <param name="stageCost"></param>
    public void InitiateLoadStage(int stageCost)
    {
        currentCost = stageCost;
        if (stageCost > EnergyUpdate.EnergyValue)
        {
            // set notification later
            WindowManager.CallWindow("Not enough energy!", NotificationType.confirmation, (delegate 
            {
                WindowManager.CallWindow("Do you want to buy Energy", NotificationType.questions, (delegate { sceneLoader.ChangeToScene("Purchase Energy"); }));

            }));
            return;
        }
        else
        {
            if(PlayerPrefs.HasKey("SkipAd"))
            {
                EnergyUpdate.depeleteEnergy(stageCost);
                sceneLoader.ChangeToScene(targetLevel);
            }
            else
            {
                CBExternal.showInterstitial(CBLocation.LevelStart);
                EnergyUpdate.depeleteEnergy(stageCost);
                sceneLoader.ChangeToScene(targetLevel);
            }
        }
    }
}
