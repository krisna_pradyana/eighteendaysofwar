﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundChanges : MonoBehaviour
{
    public Image displayedBackground;
    public Image imageToFade;
    public Scrollbar stageScrollbar;
    int index;

    public Button Next;
    public Button Back;

    public Sprite[] backgroundCollections;
    bool Changing;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        stageScrollbar.onValueChanged.AddListener(delegate { ChangeBackgoround(); });
        Next.onClick.AddListener(delegate { PrepareImage(); Changing = true; });
        Back.onClick.AddListener(delegate { PrepareImage(); Changing = true; });
        LoadLastPage();
    }

    // Update is called once per frame
    void Update()
    {
        FadingImage();
    }

    void ChangeBackgoround()
    {
        float currentScrollValue = Mathf.Round(stageScrollbar.value * 1000);
        //Debug.Log(currentScrollValue);
        
        switch (currentScrollValue)
        {
            case 0: index = 0; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 125: index = 1; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 250: index = 2; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 375: index = 3; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 500: index = 4; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 625: index = 5; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 750: index = 6; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 875: index = 7; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
            case 1000: index = 8; PlayerPrefs.SetInt("LastPage", index); displayedBackground.sprite = backgroundCollections[index];
                break;
        }
    }

    void LoadLastPage()
    {
        Debug.Log("Loading Last Page");
        switch (PlayerPrefs.GetInt("LastPage"))
        {
            case 0: stageScrollbar.value = 0 / 1000; break;
            case 1: stageScrollbar.value = 125 / 1000; break;
            case 2: stageScrollbar.value = 250 / 1000; break;
            case 3: stageScrollbar.value = 375 / 1000; break;
            case 4: stageScrollbar.value = 600 / 1000; break;
            case 5: stageScrollbar.value = 625 / 1000; break;
            case 6: stageScrollbar.value = 750 / 1000; break;
            case 7: stageScrollbar.value = 875 / 1000; break;
            case 8: stageScrollbar.value = 1000 / 1000; break;
        }
    }

    void PrepareImage()
    {
        imageToFade.sprite = displayedBackground.sprite;
        imageToFade.CrossFadeAlpha(1, 0, true);
    }

    void FadingImage()
    {
        if (Changing == true)
        {
            imageToFade.CrossFadeAlpha(0, 1f, false);
            Changing = false;
        }
    }
}
