﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageContentAttribute : MonoBehaviour
{
    public delegate void LoadedLevel();
    public static LoadedLevel loadedLevel;

    GameObject levelLock; //for lock image
    GameObject starLogo;
    GameObject levelStar; //for star image
    public LevelTag levelTag;
    public int stageCost;
    public bool isLocked;
    public float levelStarValue;

    private const int highestLevel = 53;
    public static int currentLevel;

    private void OnEnable()
    {

    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveAllListeners();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("CurrentLevel");
        //PlayerPrefs.DeleteAll();
        #region defaults value
        currentLevel = 0;
        #endregion

        InitButtonComponents();
        GetStageStatus();
        LevelLockCheck();

        Debug.Log(currentLevel);
    }

    // Update is called once per frame
    void LateUpdate()
    {

    }

    void InitButtonComponents()
    {
        levelLock = transform.Find("Lock").gameObject;
        starLogo = transform.Find("Star").gameObject;
        levelStar = transform.Find("Star/Fill").gameObject;

        if (levelTag == LevelTag.stage1 )
        {
            GetComponent<Button>().onClick.AddListener(delegate 
            {
                TutorialGenericManager.setTutorState(TutorialState.War);
                TutorialGenericManager.DisplaySpecsificTutorial(stageCost);
            }); 
        }
        else if (levelTag == LevelTag.stage6)
        {
            GetComponent<Button>().onClick.AddListener(delegate 
            {
                TutorialGenericManager.setTutorState(TutorialState.Boss);
                TutorialGenericManager.DisplaySpecsificTutorial(stageCost);
            });
        }
        else
        {
            GetComponent<Button>().onClick.AddListener(delegate { StageManager.initializingLevel(levelTag); StageManager.settingCost(stageCost); });
        }
    }

    void GetStageStatus()
    {
        for (int i = 0; i < highestLevel; i++)
        {
            if (PlayerPrefs.GetInt("ClearStage" + i) == 1)
            {
                currentLevel = i;
            }
        }

        if (currentLevel + 1 >= (int)levelTag)
        {
            isLocked = false;
            levelStarValue = PlayerPrefs.GetInt("StarStage" + (int)levelTag);
            switch(levelStarValue)
            {
                case 0: levelStar.GetComponent<Image>().fillAmount = 0; break;
                case 1: levelStar.GetComponent<Image>().fillAmount = 0.32f; break;
                case 2: levelStar.GetComponent<Image>().fillAmount = 0.69f; break;
                case 3: levelStar.GetComponent<Image>().fillAmount = 1; break;
            }
        }
        else
        {
            isLocked = true;
        }
    }

    void LevelLockCheck()
    {
        //Debug.Log("checked");
        if(isLocked == true)
        {
            levelLock.SetActive(true);
            starLogo.SetActive(false);
            GetComponent<Button>().interactable = false;
        }
        else
        {
            levelLock.SetActive(false);
            starLogo.SetActive(true);
            GetComponent<Button>().interactable = true;
        }
    }
}
