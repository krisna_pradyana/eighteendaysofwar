﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    private static WindowManager _instance;

    public delegate void FunctionAddition();
    public static FunctionAddition functionAddition;

    public GameObject mainPanel;
    Animator animator;
    public Text windowMessage;
    public List<GameObject> button;
    public static WindowProtectionLevel protectionLevel;

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    private void OnEnable()
    {
        animator = mainPanel.GetComponent<Animator>();
    }

    private void Start()
    {
        //Examples
        //CallWindow("Testing window to display", WindowProtectionLevel.open);
    }

    private void Update()
    {
        
    }

    /// <summary>
    /// This Function will call Confirmation window
    /// </summary>
    /// <param name="message"></param>
    /// <param name="protection"></param>
    public static void CallWindow(string message, WindowProtectionLevel protection)
    {
        DefaultWindow();

        _instance.windowMessage.text = message;
        protectionLevel = protection;

        SetDisplayChoice();
    }

    /// <summary>
    /// This Function will call Choice window, require function void non argument to add to true Choice's
    /// </summary>
    /// <param name="message"></param>
    /// <param name="addition"></param>
    public static void CallWindow(string message, NotificationType type, FunctionAddition addition)
    {
        DefaultWindow();

        functionAddition += addition;

        _instance.windowMessage.text = message;
        protectionLevel = WindowProtectionLevel.open;
        functionAddition += addition;

        _instance.StartCoroutine( SetDisplayChoice(type, addition));
    }

    /// <summary>
    /// Default Set display, non argument mode for Confrimation window
    /// </summary>
    private static void SetDisplayChoice()
    {
        //set cancle button to disbale window with animation
        _instance.button[(int)Choices.cancel].GetComponent<Button>().onClick.AddListener(delegate { _instance.animator.SetBool("isEnabled", false); });

        _instance.mainPanel.SetActive(true);

        _instance.button[(int)Choices.confirm].SetActive(true);
        _instance.button[(int)Choices.accept].SetActive(false);
        _instance.button[(int)Choices.cancel].SetActive(false);

        _instance.button[(int)Choices.confirm].GetComponent<Button>().onClick.AddListener(delegate { _instance.animator.SetBool("isEnabled", false); });

        _instance.animator.SetBool("isEnabled", true);
    }

    /// <summary>
    /// Set Display of choice window with delegate void argument for true choice
    /// </summary>
    /// <param name="functionAddition"></param>
    private static IEnumerator SetDisplayChoice(NotificationType type ,FunctionAddition functionAddition)
    {
        //set cancle button to disbale window with animation
        _instance.button[(int)Choices.cancel].GetComponent<Button>().onClick.AddListener(delegate { _instance.animator.SetBool("isEnabled", false); });

        _instance.mainPanel.SetActive(true);

        switch(type)
        {
            ///this only when you have window that should be displyed after another
            case NotificationType.questions:

                yield return new WaitingAnimation();

                _instance.button[(int)Choices.confirm].SetActive(false);
                _instance.button[(int)Choices.accept].SetActive(true);
                _instance.button[(int)Choices.cancel].SetActive(true);

                _instance.button[(int)Choices.accept].GetComponent<Button>().onClick.AddListener(delegate { functionAddition(); _instance.animator.SetBool("isEnabled", false); });
                break;

            ///this only when you have window that should be displyed after another
            case NotificationType.confirmation:

                yield return new WaitingAnimation();

                _instance.button[(int)Choices.confirm].SetActive(true);
                _instance.button[(int)Choices.accept].SetActive(false);
                _instance.button[(int)Choices.cancel].SetActive(false);

                _instance.button[(int)Choices.confirm].GetComponent<Button>().onClick.AddListener(delegate { functionAddition(); _instance.animator.SetBool("isEnabled", false); });
                break;
        }

        _instance.animator.SetBool("isEnabled", true);
        yield return null;
    }

    /// <summary>
    /// Reset Window to default before display
    /// </summary>
    private static void DefaultWindow()
    {
        protectionLevel = WindowProtectionLevel.open;
        _instance.mainPanel.SetActive(false);
        for (int i = 0; i < _instance.button.Count; i ++)
        {
            _instance.button[i].SetActive(false);
            _instance.button[i].GetComponent<Button>().onClick.RemoveAllListeners();
        }
    }
}
