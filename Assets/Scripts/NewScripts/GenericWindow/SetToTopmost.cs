﻿using UnityEngine;
using UnityEngine.UI;

public class SetToTopmost : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<RectTransform>().SetAsFirstSibling();
    }
}
