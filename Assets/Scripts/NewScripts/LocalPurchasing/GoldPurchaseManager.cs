﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldPurchaseManager : MonoBehaviour
{
    public int[] gemCost;
    public int purchaseCode;

    LO_SelectStyle selectLoadStyle;
    LO_LoadScene loadScene;

    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        selectLoadStyle = GameObject.FindObjectOfType<LO_SelectStyle>();
        loadScene = GameObject.FindObjectOfType<LO_LoadScene>();
    }

    public void SetPurchaseCode(int _code)
    {
        purchaseCode = _code;
        WindowManager.CallWindow("Do you want to purchase this item?", NotificationType.questions, (delegate { purchase(); }));
    }

    private void purchase()
    {
        if (purchaseCode == 1 && GemContainer.GemValue >= gemCost[1])
        {
            Debug.Log("Purchase Code " + purchaseCode + " called");
            GoldContainer.addGold(1000);
            GemContainer.purchaseWithGem(1);

            ResetValue();
        }
        else if (purchaseCode == 2 && GemContainer.GemValue >= gemCost[2])
        {
            Debug.Log("Purchase Code " + purchaseCode + " called");
            GoldContainer.addGold(6000);
            GemContainer.purchaseWithGem(5);

            ResetValue();
        }
        else if (purchaseCode == 3 && GemContainer.GemValue >= gemCost[3])
        {
            Debug.Log("Purchase Code " + purchaseCode + " called");

            GoldContainer.addGold(15000);
            GemContainer.purchaseWithGem(10);
            ResetValue();
        }
        else if (purchaseCode == 4 && GemContainer.GemValue >= gemCost[4])
        {
            Debug.Log("Purchase Code " + purchaseCode + " called");

            GoldContainer.addGold(35000);
            GemContainer.purchaseWithGem(20);
            ResetValue();
        }
        else if (purchaseCode == 0)
        {
            return;
        }
        else
        {
            WindowManager.CallWindow("Not Enough Gem", NotificationType.confirmation,
                (delegate
                    {
                        WindowManager.CallWindow("Do you want to purchase some gem?", NotificationType.questions,
                            (delegate
                                {
                                    selectLoadStyle.SetStyle("Stock_Style");
                                    loadScene.ChangeToScene("Purchase Gem");
                                }
                            ));
                    }
                ));
        }
    }

    private void ResetValue()
    {
        purchaseCode = 0;
    }
}
