﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPurchaseManager : MonoBehaviour
{
    public int[] gemCost;
    public int purchaseCode;

    LO_SelectStyle selectLoadStyle;
    LO_LoadScene loadScene;

    private void Start()
    {
        Debug.Log(EnergyContainer.EnergyValue);
        selectLoadStyle = GameObject.FindObjectOfType<LO_SelectStyle>();
        loadScene = GameObject.FindObjectOfType<LO_LoadScene>();
    }

    public void SetPurchaseCode(int _code)
    {
        purchaseCode = _code;
        WindowManager.CallWindow("Do you want to purchase this item?", NotificationType.questions, (delegate { purchase(); }));
    }

    private void purchase()
    {
        if (EnergyContainer.EnergyValue < 120)
        {
            if (purchaseCode == 1 && GemContainer.GemValue >= gemCost[0])
            {
                Debug.Log("Purchase Code " + purchaseCode + " called");
                EnergyContainer.gainEnergy(40);
                GemContainer.purchaseWithGem(gemCost[0]);

                ResetValue();
            }
            else if (purchaseCode == 2 && GemContainer.GemValue >= gemCost[1])
            {
                Debug.Log("Purchase Code " + purchaseCode + " called");
                EnergyContainer.gainEnergy(100);
                GemContainer.purchaseWithGem(gemCost[1]);

                ResetValue();
            }
            else if (purchaseCode == 3 && GemContainer.GemValue >= gemCost[2])
            {
                Debug.Log("Purchase Code " + purchaseCode + " called");

                EnergyContainer.gainEnergy(120);
                GemContainer.purchaseWithGem(gemCost[2]);
                ResetValue();
            }
            else if (purchaseCode == 0)
            {
                return;
            }
            else
            {
                WindowManager.CallWindow("Not Enough Gem", NotificationType.confirmation,
                    (delegate
                    {
                        WindowManager.CallWindow("Do you want to purchase some gem?", NotificationType.questions,
                            (delegate
                            {
                                selectLoadStyle.SetStyle("Stock_Style");
                                loadScene.ChangeToScene("Purchase Gem");
                            }
                            ));
                    }
                    ));
            }
        }
        else
        {
            WindowManager.CallWindow("Your Energy already at Maximum \r\n No need to Purchase more energy", NotificationType.confirmation, (delegate { }));
        }
    }

    private void ResetValue()
    {
        purchaseCode = 0;
    }
}
