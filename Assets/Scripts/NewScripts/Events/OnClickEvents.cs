﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnClickEvents : MonoBehaviour
{
    private static OnClickEvents _instance;

    public delegate void SFXCheck();
    public static SFXCheck sfxCheck;

    public GraphicRaycaster[] m_Raycaster = new GraphicRaycaster[2];
    public PointerEventData m_PointerEventData;
    public EventSystem m_EventSystem;
    public AudioSource[] clickSound;

    int currentScene;

    private void OnEnable()
    {
        sfxCheck += CheckEnabledSFX;
    }

    private void OnDisable()
    {
        sfxCheck -= CheckEnabledSFX;
    }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        currentScene = SceneManager.GetActiveScene().buildIndex;
        ReInitializerRaycaster();
        CheckEnabledSFX();
    }

    void Update()
    {
        ReInitializerRaycaster();
        //Check if the left Mouse button is clicked
        if (Input.GetMouseButtonDown(0))
        {
            //Set up the new Pointer Event
            m_PointerEventData = new PointerEventData(m_EventSystem);
            //Set the Pointer Event Position to that of the mouse position
            m_PointerEventData.position = Input.mousePosition;

            //Create a list of Raycast Results
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            for (int i = 0; i < m_Raycaster.Length; i++)
            {
                m_Raycaster[i].Raycast(m_PointerEventData, results);
            }

            //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
            foreach (RaycastResult result in results)
            {
                //Debug.Log("Hit " + result.gameObject.name);
                CheckClickCategory(result);
            }
        }
    }

    void CheckClickCategory(RaycastResult result)
    {
        if (result.gameObject.GetComponent<Button>())
        {
            if (result.gameObject.GetComponent<Button>().interactable == true)
            {
                clickSound[(int)SFXCategory.Confirm].Play();
            }
            else
            {
                clickSound[(int)SFXCategory.Cancel].Play();
            }
        }
    }
   
    void ReInitializerRaycaster()
    {
        for(int i = 0; i < m_Raycaster.Length; i++)
        {
            m_Raycaster[i] = null;
        }

        if (currentScene == SceneManager.GetActiveScene().buildIndex)
        {
            m_Raycaster = GameObject.FindObjectsOfType<GraphicRaycaster>();
            m_EventSystem = GameObject.FindObjectOfType<EventSystem>();
        }
        else
        {
            currentScene = SceneManager.GetActiveScene().buildIndex;
        }
    }

    void CheckEnabledSFX()
    {
        Debug.Log("ChangedSFX");
        if (SettingsScript.enabledSFX == true)
        {
            for(int i = 0; i < clickSound.Length; i++)
            {
                clickSound[i].volume = 1;
            }
        }
        else if (SettingsScript.enabledSFX == false)
        {
            for (int i = 0; i < clickSound.Length; i++)
            {
                clickSound[i].volume = 0;
            }
        }
    }
}
