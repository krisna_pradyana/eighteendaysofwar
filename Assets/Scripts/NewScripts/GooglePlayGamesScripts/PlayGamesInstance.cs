﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class PlayGamesInstance : MonoBehaviour
{
    PlayGamesClientConfiguration config = new PlayGamesClientConfiguration();
    PlayGamesClientConfiguration.Builder builderConfig = new PlayGamesClientConfiguration.Builder();

    private void Start()
    {
        //builderConfig.EnableSavedGames();
        builderConfig.RequestIdToken();
        builderConfig.Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }

    public static void SignIn()
    {
        PlayGamesPlatform.Instance.Authenticate((bool success) =>
        {
            //handle success or failure
            if (success == true)
            {
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.TOP);
            }
            else
            {
                WindowManager.CallWindow("Cannot Sign In to Google Play Games. Check your connection", WindowProtectionLevel.open);
            }
        });
    }

    public static void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    public static void AchivementCompleted(string achivementID)
    {
        PlayGamesPlatform.Instance.UnlockAchievement(achivementID, (bool success) =>
        {
            if (success)
            {
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.TOP);
            }
            else
            {
                WindowManager.CallWindow("Cannot Sign Unlock achivement", WindowProtectionLevel.open);
            }
        });

        /*
        PlayGamesPlatform.Instance.ReportProgress(achivementID, 100f, (bool success) =>
        {
            //handle success or failure
            if(success)
            {
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.TOP);
            }
            else
            {
                WindowManager.CallWindow("Cannot Sign Unlock achivement (You still get your reward). Check your connection", WindowProtectionLevel.open);
            }
        });
        */
    }

    public static void ShowAchivement()
    {
        PlayGamesPlatform.Instance.ShowAchievementsUI();
    }
}
