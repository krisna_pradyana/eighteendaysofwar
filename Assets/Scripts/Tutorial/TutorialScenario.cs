﻿using UnityEngine;
using System.Collections;

public class TutorialScenario : MonoBehaviour {
	public static bool tutorialStepOne;
	public static bool tutorialStepTwo;
	public static bool tutorialStepThree;
	public static bool tutorialStepFour;
	public static bool tutorialStepFive;
	public static bool tutorialStepSix;
	public static bool tutorialHasSummonUnit;
	public static bool tutorialCanSummonUnit;
	public static bool tutorialHasSummonHero;
	public static bool tutorialCanSummonHero;
	public static bool tutorialCanEnemySpawn;
	public static bool tutorialEnemyFinishSpawning;
	public static bool tutorialHasPlayerWin;
	public static bool tutorialOnlyShowOnce;
	public static bool tutorialMoveCameraOnce;
	public static bool tutorialSummon1stTime;
	public GameObject tutorialUI;
	// Use this for initialization
	void Start () {
		tutorialStepOne = false;
		tutorialStepTwo = false;
		tutorialStepThree = false;
		tutorialStepFour = false;
		tutorialStepFive = false;
		tutorialStepSix = false;
		tutorialHasSummonUnit = false;
		tutorialCanSummonUnit = false;
		tutorialHasSummonHero = false;
		tutorialCanSummonHero = false;
		tutorialCanEnemySpawn = false;
		tutorialEnemyFinishSpawning = false;
		tutorialHasPlayerWin = false;
		tutorialOnlyShowOnce = false;
		tutorialMoveCameraOnce = false;
		tutorialSummon1stTime = false;
		Time.timeScale = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Generate.money >= 170 && tutorialStepOne == false) {
			Time.timeScale = 0.1f;
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
			tutorialStepOne = true;
		}
		if (tutorialHasSummonUnit == true && tutorialOnlyShowOnce == false) {
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
			tutorialStepThree = true;
		}

		if (tutorialEnemyFinishSpawning == true) {
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
			tutorialStepFour = true;

		}
		if (tutorialHasSummonHero == true) {
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
			tutorialStepFive = true;	
		}
		if (tutorialHasPlayerWin == true) {
			Time.timeScale = 0f;
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
			tutorialStepSix = true;	
		}
	}
}
