﻿using UnityEngine;
using System.Collections;

public class TutorialClick : MonoBehaviour {
	public GameObject tutorialUI, welcomeText, resourceText, minerText, unitText, enemyText, warriorText, skillText, winText;
	public GameObject signResource, signMiner, signUnit, signHero, signCastle;
	private int tutorialStep = 1;
	private Animator animSignCastle, animSignHero;
	public GameObject camIndicator;
	// Use this for initialization
	void Start () {
		animSignCastle = signCastle.GetComponent<Animator> ();
		animSignHero = signHero.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (TutorialScenario.tutorialStepOne == true && TutorialScenario.tutorialStepTwo == false) {
			signUnit.SetActive (true);
			unitText.SetActive (true);	
		}
		else if (TutorialScenario.tutorialStepThree == true) {
			signCastle.SetActive (true);
			enemyText.SetActive (true);
			TutorialScenario.tutorialStepThree = false;
			animSignCastle.SetBool ("Slowmotion", true);

		}
		else if (TutorialScenario.tutorialStepFour == true) {
			animSignHero.SetBool ("Slowmotion", true);
			signHero.SetActive (true);
			warriorText.SetActive (true);
		}
		else if (TutorialScenario.tutorialStepFive == true) {
			animSignHero.SetBool ("Slowmotion", true);
			signHero.SetActive (true);
			skillText.SetActive (true);
		}
		else if (TutorialScenario.tutorialStepSix == true) {
			winText.SetActive (true);
		}
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (gameObject.name == "Confirm Container") {
			if (tutorialStep == 1) {
				welcomeText.SetActive (false);
				resourceText.SetActive (true);
				signResource.SetActive (true);
				tutorialStep = 2;
			}	
			else if (tutorialStep == 2) {
				signResource.SetActive (false);
				resourceText.SetActive (false);
				minerText.SetActive (true);
				signMiner.SetActive (true);
				tutorialStep = 3;
			}
			else if (tutorialStep == 3) {
				signMiner.SetActive (false);
				minerText.SetActive (false);
				tutorialStep = 4;
				GameFlow.hasPaused = false;
				tutorialUI.SetActive (false);
				Time.timeScale = 1;
			}
			else if (tutorialStep == 4) {
				signUnit.SetActive (false);
				unitText.SetActive (false);
				tutorialStep = 5;
				GameFlow.hasPaused = false;
				tutorialUI.SetActive (false);
				Time.timeScale = 1;
				TutorialScenario.tutorialStepTwo = true;
				TutorialScenario.tutorialCanSummonUnit = true;
				TutorialScenario.tutorialCanEnemySpawn = true;
			}
			else if (tutorialStep == 5) {
				signCastle.SetActive (false);
				enemyText.SetActive (false);
				tutorialStep = 6;
				GameFlow.hasPaused = false;
				tutorialUI.SetActive (false);
				Time.timeScale = 1;
				TutorialScenario.tutorialStepThree = false;
				TutorialScenario.tutorialOnlyShowOnce = true;

			}
			else if (tutorialStep == 6) {
				signHero.SetActive (false);
				warriorText.SetActive (false);
				tutorialStep = 7;
				GameFlow.hasPaused = false;
				tutorialUI.SetActive (false);
				Time.timeScale = 1;
				TutorialScenario.tutorialCanSummonHero = true;
				TutorialScenario.tutorialStepFour = false;
				TutorialScenario.tutorialEnemyFinishSpawning = false;
				TutorialScenario.tutorialCanEnemySpawn = false;
				TutorialScenario.tutorialMoveCameraOnce = false;

			}
			else if (tutorialStep == 7) {
				signHero.SetActive (false);
				skillText.SetActive (false);
				tutorialStep = 8;
				GameFlow.hasPaused = false;
				tutorialUI.SetActive (false);
				Time.timeScale = 1;
				TutorialScenario.tutorialStepFive = false;
				TutorialScenario.tutorialHasSummonHero = false;
				TutorialScenario.tutorialMoveCameraOnce = false;
			}
			else if (tutorialStep == 8) {
				winText.SetActive(false);
				Time.timeScale = 1;
				Generate.money = 0;
				Generate.increaseFromMiner = 0;
				IncreaseSilver.counterLimit = 0;
				GameFlow.hasPaused = false;
				GameFlow.hasResumed = false;
				Unitcooldown.hasdied = "";
				Unitcooldown.hasSummonHero = false;
				Unitcooldown.herosummoned = "";
				PlayerPrefs.SetInt ("FinishTutorial", 1);
				PlayerPrefs.SetInt ("Graduate", 1);
				Application.LoadLevel("TitleScreen");
			}
		}
	}
}
