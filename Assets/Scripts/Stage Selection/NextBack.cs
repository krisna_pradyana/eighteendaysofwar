﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextBack : MonoBehaviour {

    int sceneCode, currentSceneCode;
    string Scene;
    public GameObject Next, Back;

    void Awake()
    {
        
    }

    void Start()
    {
        Next = GameObject.Find("NextButton");
        Back = GameObject.Find("BackButton");
        
    }

    void Update()
    {
        PlayerPrefs.GetInt("CurrentStage", currentSceneCode);
    }

    void OnMouseDown()
    {
        if(Next)
        {
            currentSceneCode += 1;
            //Debug.Log(currentSceneCode);
        }
        else if (Back)
        {
            currentSceneCode -= 1;
            //PlayerPrefs.SetInt("CurrentStage", currentSceneCode -1);
        }
        switch (PlayerPrefs.GetInt("CurrentStage"))
        {
            case 0:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 1:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection1";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 2:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection2";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 3:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection3";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 4:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection4";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 5:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection5";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 6:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection6";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 7:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection7";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
            case 8:
                Debug.Log("Scene Loaded");
                Scene = "StageSelection8";
                Debug.Log(currentSceneCode);
                loadStage();
                break;
        }
    }

    IEnumerator loadStage()
    {
        AsyncOperation async = Application.LoadLevelAsync(Scene);
        yield return async;
    }

}
