﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadAtt : MonoBehaviour {
    #region Delegates
    public delegate void LoadAttributes();
    public static LoadAttributes loadAttributes;
    #endregion

    private void OnEnable()
    {
        loadAttributes += LoadingAllAttribute;
    }

    private void OnDisable()
    {
        loadAttributes -= LoadingAllAttribute;
    }

    public GameObject Foot,Archer,Sword,Gunner,Spear,Mage,Shaman,Elephantry,Tank;
	public GameObject Arjuna,Bima,Srikandi,Yudhistira,NakulaSadewa,GatotKaca,Drupada,Kresna,Hanoman;
	private GameObject[] check;
	private int uphp, upatk;
	// Use this for initialization

	void Start ()
    {
        LoadingAllAttribute();
	}

    public void LoadingAllAttribute()
    {
        DebugSpear("Spear", 44, 440);
        DebugSpear("Bima", 150, 6000);
        /*
        LoadAttribute("Foot", 2, Foot);
        LoadAttribute("Archer", 2, Archer);
        LoadAttribute("Sword", 2, Sword);
        LoadAttribute("Gunner", 2, Gunner);
        LoadAttribute("Spear", 2, Spear);
        LoadAttribute("Mage", 2, Mage);
        LoadAttribute("Shaman", 2, Shaman);
        LoadAttribute("Elephantry", 0, Elephantry);
        LoadAttribute("Tank", 0, Tank);
        LoadAttribute("Arjuna", 2, Arjuna);
        LoadAttribute("Bima", 4, Bima);
        LoadAttribute("Srikandi", 1, Srikandi);
        LoadAttribute("Yudhistira", 0.5f, Yudhistira);
        LoadAttribute("NakulaSadewa", 2, NakulaSadewa);
        LoadAttribute("GatotKaca", 2, GatotKaca);
        LoadAttribute("Drupada", 3, Drupada);
        LoadAttribute("Kresna", 2.5f, Kresna);
        LoadAttribute("Hanoman", 0, Hanoman);
        */
        LoadAttribute("Foot",  Foot);
        LoadAttribute("Archer",  Archer);
        LoadAttribute("Sword", Sword);
        LoadAttribute("Gunner",  Gunner);
        LoadAttribute("Spear",  Spear);
        LoadAttribute("Mage", Mage);
        LoadAttribute("Shaman", Shaman);
        LoadAttribute("Elephantry", Elephantry);
        LoadAttribute("Tank", Tank);
        LoadAttribute("Arjuna", Arjuna);
        LoadAttribute("Bima", Bima);
        LoadAttribute("Srikandi", Srikandi);
        LoadAttribute("Yudhistira", Yudhistira);
        LoadAttribute("NakulaSadewa", NakulaSadewa);
        LoadAttribute("GatotKaca", GatotKaca);
        LoadAttribute("Drupada", Drupada);
        LoadAttribute("Kresna", Kresna);
        LoadAttribute("Hanoman", Hanoman);

    }

	void DebugSpear(string unit,int initatk, int inithp){
		GameObject unitchanged = (GameObject)this.GetType().GetField(unit).GetValue(this);
		UnitAtt att = unitchanged.GetComponent<UnitAtt>();
		uphp = inithp;
		upatk = initatk;
		if (unit == "Spear") {
			if((PlayerPrefs.GetInt("Level"+unit)>4 && att.ATK < 198) || (PlayerPrefs.GetInt("Level"+unit)<4 && att.ATK < 44) || (PlayerPrefs.GetInt("Level"+unit)==4 && att.ATK < 98)){
				for (int i = 2; i<=PlayerPrefs.GetInt("Level"+unit)-1; i++) {
					uphp = uphp + (i*30);
					upatk = upatk + (i*6);
				}
				PlayerPrefs.SetFloat("HP"+unit,uphp);
				PlayerPrefs.SetFloat("ATK"+unit,upatk);
				att.HP = uphp;
				att.ATK = upatk;
			}	
		}
		else if(unit == "Bima"){
			if((PlayerPrefs.GetInt("Level"+unit)>=15 && att.ATK < 3000) || (PlayerPrefs.GetInt("Level"+unit)<15 && PlayerPrefs.GetInt("Level"+unit)>=10 && att.ATK < 1400) || 
			   (PlayerPrefs.GetInt("Level"+unit)<10 && PlayerPrefs.GetInt("Level"+unit)>=7 && att.ATK < 798)||
			   (PlayerPrefs.GetInt("Level"+unit)==6 && att.ATK != 630)||(PlayerPrefs.GetInt("Level"+unit)==5 && att.ATK != 486)||
			   (PlayerPrefs.GetInt("Level"+unit)==4 && att.ATK != 366)||(PlayerPrefs.GetInt("Level"+unit)==3 && att.ATK != 270)||(PlayerPrefs.GetInt("Level"+unit)==2 && att.ATK != 198)){
				for (int i = 2; i<=PlayerPrefs.GetInt("Level"+unit); i++) {
					upatk = upatk + (i*24);
				}
				PlayerPrefs.SetFloat("ATK"+unit,upatk);
				att.ATK = upatk;
			}
		}
	}

    void LoadAttribute(string unit, GameObject unitObject)
    {
        //Debug.Log("Loading Attribute");
        UnitAtt att = unitObject.GetComponent<UnitAtt>();
        float change = (PlayerPrefs.GetInt("Level" + unit));

        att.skillduration += (int)change;
        att.HP = PlayerPrefs.GetInt("CurrHP" + unit);
        att.ATK = PlayerPrefs.GetInt("CurrATK" + unit);
    }
}
