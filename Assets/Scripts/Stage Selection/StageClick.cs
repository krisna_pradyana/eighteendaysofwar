﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class StageClick : MonoBehaviour {

	//public string scene;
	private int clearStage1, clearStage2, clearStage3, clearStage4, clearStage5, clearStage6;
	private int clearStage7, clearStage8, clearStage9, clearStage10, clearStage11, clearStage12;
	private int clearStage13, clearStage14, clearStage15, clearStage16, clearStage17, clearStage18;
	private int clearStage19, clearStage20, clearStage21, clearStage22, clearStage23, clearStage24;
	private int clearStage25, clearStage26, clearStage27, clearStage28, clearStage29, clearStage30;
	private int clearStage31, clearStage32, clearStage33, clearStage34, clearStage35, clearStage36;
	private int clearStage37, clearStage38, clearStage39, clearStage40, clearStage41, clearStage42;
	private int clearStage43, clearStage44, clearStage45, clearStage46, clearStage47, clearStage48;
	private int clearStage49, clearStage50, clearStage51, clearStage52, clearStage53;
    //private RegenEnergy regenEnergy;
    //public int energyCost;
    //private EnergyContainer EnContainer;

    int[] RequiredEnergy;
    int SelectedLevelEnergy;

	//public GameObject energyPopUpUI;
	//public GameObject clearStagePopUpUI;
	//public GameObject comingSoonPopUpUI;
	//public GameObject exitGamePopUpUI;
	public static string clickStage;
	//private GameObject square;
	// Use this for initialization
	void Awake () {
        //EnContainer = new EnergyContainer();
        //GameObject.Find ("EnergyText").SetActive(true);
    }

    void RequiredEnergyExecution()
    {
        #region Day 1
        RequiredEnergy[0] = 10;
        RequiredEnergy[1] = 10;
        RequiredEnergy[2] = 10;
        #endregion

        #region Day 2
        RequiredEnergy[3] = 10;
        RequiredEnergy[4] = 10;
        RequiredEnergy[5] = 15;
        #endregion

        #region Day 3
        RequiredEnergy[6] = 12;
        RequiredEnergy[7] = 12;
        RequiredEnergy[8] = 12;
        #endregion

        #region Day 4
        RequiredEnergy[9] = 12;
        RequiredEnergy[10] = 12;
        RequiredEnergy[11] = 18;
        #endregion

        #region Day 5
        RequiredEnergy[12] = 12;
        RequiredEnergy[13] = 12;
        RequiredEnergy[14] = 12;
        #endregion

        #region Day 6
        RequiredEnergy[15] = 12;
        RequiredEnergy[16] = 12;
        RequiredEnergy[17] = 18;
        #endregion

        #region Day 7
        RequiredEnergy[18] = 17;
        RequiredEnergy[19] = 17;
        RequiredEnergy[20] = 17;
        #endregion

        #region Day 8
        RequiredEnergy[21] = 17;
        RequiredEnergy[22] = 17;
        RequiredEnergy[23] = 24;
        #endregion

        #region Day 9
        RequiredEnergy[24] = 19;
        RequiredEnergy[25] = 19;
        RequiredEnergy[26] = 19;
        #endregion

        #region Day 10
        RequiredEnergy[27] = 19;
        RequiredEnergy[28] = 19;
        RequiredEnergy[29] = 27;
        #endregion

        #region Day 11
        RequiredEnergy[30] = 24;
        RequiredEnergy[31] = 24;
        RequiredEnergy[32] = 24;
        #endregion

        #region Day 12
        RequiredEnergy[33] = 24;
        RequiredEnergy[34] = 24;
        RequiredEnergy[35] = 33;
        #endregion

        #region Day 13
        RequiredEnergy[36] = 24;
        RequiredEnergy[37] = 24;
        RequiredEnergy[38] = 24;
        #endregion

        #region Day 14
        RequiredEnergy[39] = 24;
        RequiredEnergy[40] = 24;
        RequiredEnergy[41] = 33;
        #endregion

        #region Day 15
        RequiredEnergy[42] = 27;
        RequiredEnergy[43] = 27;
        RequiredEnergy[44] = 27;
        #endregion

        #region Day 16
        RequiredEnergy[45] = 27;
        RequiredEnergy[46] = 27;
        RequiredEnergy[46] = 36;
        #endregion

        #region Day 17
        RequiredEnergy[48] = 10;
        RequiredEnergy[49] = 10;
        RequiredEnergy[50] = 15;
        #endregion

        #region Day 18
        RequiredEnergy[51] = 30;
        RequiredEnergy[52] = 30;
        RequiredEnergy[53] = 40;
        RequiredEnergy[54] = 40;
        #endregion

        RequiredEnergy[55] = 50;

    }

    void Start ()
    { 
        //EnContainer = GameObject.Find("Energy Container").GetComponent<EnergyContainer>();
        RequiredEnergy = new int[56];
        RequiredEnergyExecution();

        //square = GameObject.Find ("Square2");
		//PlayerPrefs.SetInt ("ClearStage2", 1);
		clearStage1 = PlayerPrefs.GetInt ("ClearStage1");
		clearStage2 = PlayerPrefs.GetInt ("ClearStage2");
		clearStage3 = PlayerPrefs.GetInt ("ClearStage3");
		clearStage4 = PlayerPrefs.GetInt ("ClearStage4");
		clearStage5 = PlayerPrefs.GetInt ("ClearStage5");
		clearStage6 = PlayerPrefs.GetInt ("ClearStage6");

		clearStage7 = PlayerPrefs.GetInt ("ClearStage7");
		clearStage8 = PlayerPrefs.GetInt ("ClearStage8");
		clearStage9 = PlayerPrefs.GetInt ("ClearStage9");
		clearStage10 = PlayerPrefs.GetInt ("ClearStage10");
		clearStage11 = PlayerPrefs.GetInt ("ClearStage11");
		clearStage12 = PlayerPrefs.GetInt ("ClearStage12");

		clearStage13 = PlayerPrefs.GetInt ("ClearStage13");
		clearStage14 = PlayerPrefs.GetInt ("ClearStage14");
		clearStage15 = PlayerPrefs.GetInt ("ClearStage15");
		clearStage16 = PlayerPrefs.GetInt ("ClearStage16");
		clearStage17 = PlayerPrefs.GetInt ("ClearStage17");

		clearStage18 = PlayerPrefs.GetInt ("ClearStage18");
		clearStage19 = PlayerPrefs.GetInt ("ClearStage19");
		clearStage20 = PlayerPrefs.GetInt ("ClearStage20");
		clearStage21 = PlayerPrefs.GetInt ("ClearStage21");
		clearStage22 = PlayerPrefs.GetInt ("ClearStage22");
		clearStage23 = PlayerPrefs.GetInt ("ClearStage23");
		
		clearStage24 = PlayerPrefs.GetInt ("ClearStage24");
		clearStage25 = PlayerPrefs.GetInt ("ClearStage25");
		clearStage26 = PlayerPrefs.GetInt ("ClearStage26");
		clearStage27 = PlayerPrefs.GetInt ("ClearStage27");
		clearStage28 = PlayerPrefs.GetInt ("ClearStage28");
		clearStage29 = PlayerPrefs.GetInt ("ClearStage29");
		
		clearStage30 = PlayerPrefs.GetInt ("ClearStage30");
		clearStage31 = PlayerPrefs.GetInt ("ClearStage31");
		clearStage32 = PlayerPrefs.GetInt ("ClearStage32");
		clearStage33 = PlayerPrefs.GetInt ("ClearStage33");
		clearStage34 = PlayerPrefs.GetInt ("ClearStage34");
		clearStage35 = PlayerPrefs.GetInt ("ClearStage35");

		clearStage36 = PlayerPrefs.GetInt ("ClearStage36");
		clearStage37 = PlayerPrefs.GetInt ("ClearStage37");
		clearStage38 = PlayerPrefs.GetInt ("ClearStage38");
		clearStage39 = PlayerPrefs.GetInt ("ClearStage39");
		clearStage40 = PlayerPrefs.GetInt ("ClearStage40");
		clearStage41 = PlayerPrefs.GetInt ("ClearStage41");
		
		clearStage42 = PlayerPrefs.GetInt ("ClearStage42");
		clearStage43 = PlayerPrefs.GetInt ("ClearStage43");
		clearStage44 = PlayerPrefs.GetInt ("ClearStage44");
		clearStage45 = PlayerPrefs.GetInt ("ClearStage45");
		clearStage46 = PlayerPrefs.GetInt ("ClearStage46");
		clearStage47 = PlayerPrefs.GetInt ("ClearStage47");
		
		clearStage48 = PlayerPrefs.GetInt ("ClearStage48");
		clearStage49 = PlayerPrefs.GetInt ("ClearStage49");
		clearStage50 = PlayerPrefs.GetInt ("ClearStage50");
		clearStage51 = PlayerPrefs.GetInt ("ClearStage51");
		clearStage52 = PlayerPrefs.GetInt ("ClearStage52");
		clearStage53 = PlayerPrefs.GetInt ("ClearStage53");

        /*
		if (GameObject.Find("EnergyText") != null)
		{
			regenEnergy = GameObject.Find ("EnergyText").GetComponent<RegenEnergy> ();
		}
        */
	}
	
	// Update is called once per frame
	void OnMouseDown ()
    {
        /*
        Debug.Log(gameObject.name);
		if (gameObject.name.Contains ("Stage1-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[0]) {
			GetComponent<AudioSource>().Play();
            EnergyContainer.depeletsEnergy(  RequiredEnergy[0]);
			clickStage = "Day1-1";
			StartCoroutine ("loadStage");	
		}
		else if (gameObject.name.Contains ("Stage1-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[1]) {
			if (clearStage1 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[1]);
				clickStage = "Day1-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage1-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[2]) {
			if (clearStage2 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[2]);
				clickStage = "Day1-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage1-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[3]) {
			if (clearStage3 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[3]);
				clickStage = "Day2-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage1-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[4]) {
			if (clearStage4 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[4]);
				clickStage = "Day2-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage1-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[5]) {
			if (clearStage5 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[5]);
				clickStage = "Day2-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage2-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[6]) {
			if (clearStage6 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[6]);
				clickStage = "Day3-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage2-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[7]) {
			if (clearStage7 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[7]);
				clickStage = "Day3-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage2-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[8]) {
			if (clearStage8 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[8]);
				clickStage = "Day3-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage2-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[9]) {
			if (clearStage9 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[9]);
				clickStage = "Day4-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage2-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[10]) {
			if (clearStage10 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[10]);
				clickStage = "Day4-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage2-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[11]) {
			if (clearStage11 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[11]);
				clickStage = "Day4-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage3-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[12]) {
			if (clearStage12 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[12]);
				clickStage = "Day5-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage3-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[13]) {
			if (clearStage13 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[13]);
				clickStage = "Day5-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage3-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[14]) {
			if (clearStage14 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[14]);
				clickStage = "Day5-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage3-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[15]) {
			if (clearStage15 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[15]);
				clickStage = "Day6-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage3-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[16]) {
			if (clearStage16 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[16]);
				clickStage = "Day6-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage3-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[17]) {
			if (clearStage17 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[17]);
				clickStage = "Day6-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage4-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[18]) {
			if (clearStage18 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[18]);
				clickStage = "Day7-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage4-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[19]) {
			if (clearStage19 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[19]);
				clickStage = "Day7-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage4-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[20]) {
			if (clearStage20 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[20]);
				clickStage = "Day7-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage4-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[21]) {
			if (clearStage21 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[21]);
				clickStage = "Day8-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage4-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[22]) {
			if (clearStage22 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[22]);
				clickStage = "Day8-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage4-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[23]) {
			if (clearStage23 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[23]);
				clickStage = "Day8-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage5-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[24]) {
			if (clearStage24 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[24]);
				clickStage = "Day9-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage5-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[25]) {
			if (clearStage25 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[25]);
				clickStage = "Day9-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage5-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[26]) {
			if (clearStage26 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[26]);
				clickStage = "Day9-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage5-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[27]) {
			if (clearStage27 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[27]);
				clickStage = "Day10-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage5-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[28]) {
			if (clearStage28 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[28]);
				clickStage = "Day10-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage5-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[29]) {
			if (clearStage29 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[29]);
				clickStage = "Day10-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage6-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[30]) {
			if (clearStage30 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[30]);
				clickStage = "Day11-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage6-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[31]) {
			if (clearStage31 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[32]);
				clickStage = "Day11-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage6-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[33]) {
			if (clearStage32 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[33]);
				clickStage = "Day11-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage6-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[34]) {
			if (clearStage33 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[34]);
				clickStage = "Day12-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage6-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[35]) {
			if (clearStage34 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[35]);
				clickStage = "Day12-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage6-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[36]) {
			if (clearStage35 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[36]);
				clickStage = "Day12-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage7-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[37]) {
			if (clearStage36 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[37]);
				clickStage = "Day13-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage7-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[38]) {
			if (clearStage37 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[38]);
				clickStage = "Day13-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage7-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[39]) {
			if (clearStage38 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[39]);
				clickStage = "Day13-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage7-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[40]) {
			if (clearStage39 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[40]);
				clickStage = "Day14-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage7-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[41]) {
			if (clearStage40 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[41]);
				clickStage = "Day14-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage7-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[42]) {
			if (clearStage41 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[42]);
				clickStage = "Day14-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage8-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[43]) {
			if (clearStage42 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[43]);
				clickStage = "Day15-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage8-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[44]) {
			if (clearStage43 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[44]);
				clickStage = "Day15-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage8-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[45]) {
			if (clearStage44 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[45]);
				clickStage = "Day15-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage8-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[46]) {
			if (clearStage45 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[46]);
				clickStage = "Day16-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage8-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[47]) {
			if (clearStage46 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[47]);
				clickStage = "Day16-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage8-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[48]) {
			if (clearStage47 == 1) {
				GetComponent<AudioSource>().Play();
				 EnergyContainer.depeletsEnergy(  RequiredEnergy[48]);
				clickStage = "Day16-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage9-1") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[49]) {
			if (clearStage48 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[49]);
				clickStage = "Day17-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage9-2") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[50]) {
			if (clearStage49 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[50]);
				clickStage = "Day17-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}	
		}
		else if (gameObject.name.Contains ("Stage9-3") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[51]) {
			if (clearStage50 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[51]);
				clickStage = "Day17-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage9-4") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[52]) {
			if (clearStage51 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[52]);
				clickStage = "Day18-1";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage9-5") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[53]) {
			if (clearStage52 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[53]);
				clickStage = "Day18-2";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name.Contains ("Stage9-6") && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[54]) {
			if (clearStage53 == 1) {
				GetComponent<AudioSource>().Play();
				EnergyContainer.depeletsEnergy(  RequiredEnergy[54]);
				clickStage = "Day18-3";
				StartCoroutine ("loadStage");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = true;
				WarningClick.stillShowingWarnings = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(true);
				comingSoonPopUpUI.SetActive(false);
				exitGamePopUpUI.SetActive(false);
			}		
		}
		else if (gameObject.name == "NextButton") {
			if (Application.loadedLevelName == "Stage Selection10") {
				WarningClick.stillShowingWarnings = true;
				//square.GetComponent<SpriteRenderer>().enabled = true;
				GetComponent<AudioSource>().Play();
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(false);
				comingSoonPopUpUI.SetActive(true);
				exitGamePopUpUI.SetActive(false);
			}
			else {
				GetComponent<AudioSource>().Play();
				StartCoroutine ("loadStage");
			}
		}
		else if (gameObject.name == "PreviousButton") {
			GetComponent<AudioSource>().Play();
			StartCoroutine ("loadStage");
		}
		else if (gameObject.name == "Survival Mode" && PlayerPrefs.GetInt("Energy") >=   RequiredEnergy[55]) {
			GetComponent<AudioSource>().Play();
			 EnergyContainer.depeletsEnergy(  RequiredEnergy[55]);
			clickStage = "SurvivalMode";
			StartCoroutine ("loadStage");
		}
		else {
			WarningClick.stillShowingWarnings = true;
			GetComponent<AudioSource>().Play();
			energyPopUpUI.SetActive(true);
			clearStagePopUpUI.SetActive(false);
			comingSoonPopUpUI.SetActive(false);
			exitGamePopUpUI.SetActive(false);
		}
        */

	}

    public void SetLevelReqEnergy(int levelIndex)
    {
        SelectedLevelEnergy = RequiredEnergy[levelIndex];
    }

    public void RunLevels(string levelname)
    {
        EnergyContainer.depeletsEnergy(SelectedLevelEnergy);
        Debug.Log(levelname);
        //clickStage = levelname;
        SceneManager.LoadScene(levelname);
        //StartCoroutine(loadStage(levelname));
    }

	void Update() {
        /*
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.LoadLevel ("Shop");
		}
		if (WarningClick.stillShowingWarnings == true) {
			gameObject.GetComponent<BoxCollider2D>().enabled = false;	
		}
		else {
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
        */
	}

	IEnumerator loadStage(string scenename) {
        //Debug.Log(SceneManager.GetSceneByName(scenename).buildIndex);
        SceneManager.LoadSceneAsync(SceneManager.GetSceneByName(scenename).buildIndex, LoadSceneMode.Single);
		yield break;
	}
}
