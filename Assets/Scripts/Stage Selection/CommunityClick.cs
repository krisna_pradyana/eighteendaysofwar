﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CommunityClick : MonoBehaviour {
    public string scene;

    // Your app’s unique identifier.
    private string AppID = "1518031601790532";

    // The link attached to this post.
    private string Link = "https://www.facebook.com/MahabharatWars";

    // The URL of a picture attached to this post. The picture must be at least 200px by 200px.
    private string Picture = "http://falkanastudio.com/mwicon.png";

    // The name of the link attachment.
    private string Name = "Mahabharat Warriors";

    // The caption of the link (appears beneath the link name).
    public string Caption = "I just beat day 1-1. Can you beat it?";

    // The description of the link (appears beneath the link caption). 
    private string Description = "Pandawa warriors are waiting for you. Click this link to download Mahabharat Warriors and let us defeat Kurawa Armies together.";

    private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
    private const string TWEET_LANGUAGE = "en"; 

    void OnMouseDown () {
        GetComponent<AudioSource>().Play();
        if (gameObject.name.Contains("Back") || gameObject.name.Contains("Community Button"))
        {
            SceneManager.LoadScene(scene);
        }
        else if (gameObject.name.Contains("FB"))
        {
            ShareScoreOnFB();
        }
        else if (gameObject.name.Contains("Twitter"))
        {
            ShareToTwitter(Caption + "#mahabharatwarriors");
        }
    }
    void ShareToTwitter (string textToDisplay)
    {
        Application.OpenURL(TWITTER_ADDRESS +
            "?text=" + WWW.EscapeURL(textToDisplay) +
            "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
    }
    void ShareScoreOnFB(){
        Application.OpenURL("https://www.facebook.com/dialog/feed?"+
            "app_id="+AppID+
            "&link="+Link+
            "&picture="+Picture+
            "&name="+ReplaceSpace(Name)+
            "&caption="+ReplaceSpace(Caption)+
            "&description="+ReplaceSpace(Description)+
            "&redirect_uri=https://facebook.com/");
    }
    string ReplaceSpace (string val) {
        return val.Replace(" ", "%20");    
    }
}
