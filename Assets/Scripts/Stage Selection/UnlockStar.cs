﻿using UnityEngine;
using System.Collections;

public class UnlockStar : MonoBehaviour {
	private int starStage1, starStage2, starStage3, starStage4, starStage5, starStage6;
	private int starStage7, starStage8, starStage9, starStage10, starStage11, starStage12;
	private int starStage13, starStage14, starStage15, starStage16, starStage17, starStage18;
	private int starStage19, starStage20, starStage21, starStage22, starStage23, starStage24;
	private int starStage25, starStage26, starStage27, starStage28, starStage29, starStage30;
	private int starStage31, starStage32, starStage33, starStage34, starStage35, starStage36;
	private int starStage37, starStage38, starStage39, starStage40, starStage41, starStage42;
	private int starStage43, starStage44, starStage45, starStage46, starStage47, starStage48;
	private int starStage49, starStage50, starStage51, starStage52, starStage53, starStage54;
	private int star3_Stage1, star3_Stage2, star3_Stage3, star3_Stage4, star3_Stage5, star3_Stage6;
	private int star3_Stage7, star3_Stage8, star3_Stage9, star3_Stage10, star3_Stage11, star3_Stage12;
	private int star3_Stage13, star3_Stage14, star3_Stage15, star3_Stage16, star3_Stage17, star3_Stage18;
	private int star3_Stage19, star3_Stage20, star3_Stage21, star3_Stage22, star3_Stage23, star3_Stage24;
	private int star3_Stage25, star3_Stage26, star3_Stage27, star3_Stage28, star3_Stage29, star3_Stage30;
	private int star3_Stage31, star3_Stage32, star3_Stage33, star3_Stage34, star3_Stage35, star3_Stage36;
	private int star3_Stage37, star3_Stage38, star3_Stage39, star3_Stage40, star3_Stage41, star3_Stage42;
	private int star3_Stage43, star3_Stage44, star3_Stage45, star3_Stage46, star3_Stage47, star3_Stage48;
	private int star3_Stage49, star3_Stage50, star3_Stage51, star3_Stage52, star3_Stage53, star3_Stage54;
	private SpriteRenderer[] texStarStage1_1, texStarStage1_2, texStarStage1_3, texStarStage1_4, texStarStage1_5, texStarStage1_6;
	private SpriteRenderer[] texStarStage2_1, texStarStage2_2, texStarStage2_3, texStarStage2_4, texStarStage2_5, texStarStage2_6;
	private SpriteRenderer[] texStarStage3_1, texStarStage3_2, texStarStage3_3, texStarStage3_4, texStarStage3_5, texStarStage3_6;
	private SpriteRenderer[] texStarStage4_1, texStarStage4_2, texStarStage4_3, texStarStage4_4, texStarStage4_5, texStarStage4_6;
	private SpriteRenderer[] texStarStage5_1, texStarStage5_2, texStarStage5_3, texStarStage5_4, texStarStage5_5, texStarStage5_6;
	private SpriteRenderer[] texStarStage6_1, texStarStage6_2, texStarStage6_3, texStarStage6_4, texStarStage6_5, texStarStage6_6;
	private SpriteRenderer[] texStarStage7_1, texStarStage7_2, texStarStage7_3, texStarStage7_4, texStarStage7_5, texStarStage7_6;
	private SpriteRenderer[] texStarStage8_1, texStarStage8_2, texStarStage8_3, texStarStage8_4, texStarStage8_5, texStarStage8_6;
	private SpriteRenderer[] texStarStage9_1, texStarStage9_2, texStarStage9_3, texStarStage9_4, texStarStage9_5, texStarStage9_6;
	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName == "Stage Selection") {
			starStage1 = PlayerPrefs.GetInt ("StarStage1");
			starStage2 = PlayerPrefs.GetInt ("StarStage2");
			starStage3 = PlayerPrefs.GetInt ("StarStage3");
			starStage4 = PlayerPrefs.GetInt ("StarStage4");
			starStage5 = PlayerPrefs.GetInt ("StarStage5");	
			starStage6 = PlayerPrefs.GetInt ("StarStage6");

			if (starStage1 == 3) {
				PlayerPrefs.SetInt ("Star3Stage1", 1);		
			}
			else if (starStage2 == 3) {
				PlayerPrefs.SetInt ("Star3Stage2", 1);		
			}
			else if (starStage3 == 3) {
				PlayerPrefs.SetInt ("Star3Stage3", 1);		
			}
			else if (starStage4 == 3) {
				PlayerPrefs.SetInt ("Star3Stage4", 1);		
			}
			else if (starStage5 == 3) {
				PlayerPrefs.SetInt ("Star3Stage5", 1);		
			}
			else if (starStage6 == 3) {
				PlayerPrefs.SetInt ("Star3Stage6", 1);		
			}

			star3_Stage1 = PlayerPrefs.GetInt ("Star3Stage1");
			star3_Stage2 = PlayerPrefs.GetInt ("Star3Stage2");
			star3_Stage3 = PlayerPrefs.GetInt ("Star3Stage3");
			star3_Stage4 = PlayerPrefs.GetInt ("Star3Stage4");
			star3_Stage5 = PlayerPrefs.GetInt ("Star3Stage5");
			star3_Stage6 = PlayerPrefs.GetInt ("Star3Stage6");

			starStage1_1 (star3_Stage1, starStage1);
			starStage1_2 (star3_Stage2, starStage2);
			starStage1_3 (star3_Stage3, starStage3);
			starStage1_4 (star3_Stage4, starStage4);
			starStage1_5 (star3_Stage5, starStage5);
			starStage1_6 (star3_Stage6, starStage6);
		}
		else if (Application.loadedLevelName == "Stage Selection2") {
			starStage7 = PlayerPrefs.GetInt ("StarStage7");
			starStage8 = PlayerPrefs.GetInt ("StarStage8");
			starStage9 = PlayerPrefs.GetInt ("StarStage9");
			starStage10 = PlayerPrefs.GetInt ("StarStage10");
			starStage11 = PlayerPrefs.GetInt ("StarStage11");	
			starStage12 = PlayerPrefs.GetInt ("StarStage12");

			if (starStage7 == 3) {
				PlayerPrefs.SetInt ("Star3Stage7", 1);		
			}
			else if (starStage8 == 3) {
				PlayerPrefs.SetInt ("Star3Stage8", 1);		
			}
			else if (starStage9 == 3) {
				PlayerPrefs.SetInt ("Star3Stage9", 1);		
			}
			else if (starStage10 == 3) {
				PlayerPrefs.SetInt ("Star3Stage10", 1);		
			}
			else if (starStage11 == 3) {
				PlayerPrefs.SetInt ("Star3Stage11", 1);		
			}
			else if (starStage12 == 3) {
				PlayerPrefs.SetInt ("Star3Stage12", 1);		
			}

			star3_Stage7 = PlayerPrefs.GetInt ("Star3Stage7");
			star3_Stage8 = PlayerPrefs.GetInt ("Star3Stage8");
			star3_Stage9 = PlayerPrefs.GetInt ("Star3Stage9");
			star3_Stage10 = PlayerPrefs.GetInt ("Star3Stage10");
			star3_Stage11 = PlayerPrefs.GetInt ("Star3Stage11");
			star3_Stage12 = PlayerPrefs.GetInt ("Star3Stage12");

			starStage2_1 (star3_Stage7, starStage7);
			starStage2_2 (star3_Stage8, starStage8);
			starStage2_3 (star3_Stage9, starStage9);
			starStage2_4 (star3_Stage10, starStage10);
			starStage2_5 (star3_Stage11, starStage11);
			starStage2_6 (star3_Stage12, starStage12);
		}
		else if (Application.loadedLevelName == "Stage Selection3") {
			starStage13 = PlayerPrefs.GetInt ("StarStage13");
			starStage14 = PlayerPrefs.GetInt ("StarStage14");
			starStage15 = PlayerPrefs.GetInt ("StarStage15");
			starStage16 = PlayerPrefs.GetInt ("StarStage16");
			starStage17 = PlayerPrefs.GetInt ("StarStage17");	
			starStage18 = PlayerPrefs.GetInt ("StarStage18");

			if (starStage13 == 3) {
				PlayerPrefs.SetInt ("Star3Stage13", 1);		
			}
			else if (starStage14 == 3) {
				PlayerPrefs.SetInt ("Star3Stage14", 1);		
			}
			else if (starStage15 == 3) {
				PlayerPrefs.SetInt ("Star3Stage15", 1);		
			}
			else if (starStage16 == 3) {
				PlayerPrefs.SetInt ("Star3Stage16", 1);		
			}
			else if (starStage17 == 3) {
				PlayerPrefs.SetInt ("Star3Stage17", 1);		
			}
			else if (starStage18 == 3) {
				PlayerPrefs.SetInt ("Star3Stage18", 1);		
			}

			star3_Stage13 = PlayerPrefs.GetInt ("Star3Stage13");
			star3_Stage14 = PlayerPrefs.GetInt ("Star3Stage14");
			star3_Stage15 = PlayerPrefs.GetInt ("Star3Stage15");
			star3_Stage16 = PlayerPrefs.GetInt ("Star3Stage16");
			star3_Stage17 = PlayerPrefs.GetInt ("Star3Stage17");
			star3_Stage18 = PlayerPrefs.GetInt ("Star3Stage18");

			starStage3_1 (star3_Stage13, starStage13);
			starStage3_2 (star3_Stage14, starStage14);
			starStage3_3 (star3_Stage15, starStage15);
			starStage3_4 (star3_Stage16, starStage16);
			starStage3_5 (star3_Stage17, starStage17);
			starStage3_6 (star3_Stage18, starStage18);
		}
		else if (Application.loadedLevelName == "Stage Selection4") {
			starStage19 = PlayerPrefs.GetInt ("StarStage19");
			starStage20 = PlayerPrefs.GetInt ("StarStage20");
			starStage21 = PlayerPrefs.GetInt ("StarStage21");
			starStage22 = PlayerPrefs.GetInt ("StarStage22");
			starStage23 = PlayerPrefs.GetInt ("StarStage23");	
			starStage24 = PlayerPrefs.GetInt ("StarStage24");
			
			if (starStage19 == 3) {
				PlayerPrefs.SetInt ("Star3Stage19", 1);		
			}
			else if (starStage20 == 3) {
				PlayerPrefs.SetInt ("Star3Stage20", 1);		
			}
			else if (starStage21 == 3) {
				PlayerPrefs.SetInt ("Star3Stage21", 1);		
			}
			else if (starStage22 == 3) {
				PlayerPrefs.SetInt ("Star3Stage22", 1);		
			}
			else if (starStage23 == 3) {
				PlayerPrefs.SetInt ("Star3Stage23", 1);		
			}
			else if (starStage24 == 3) {
				PlayerPrefs.SetInt ("Star3Stage24", 1);		
			}
			
			star3_Stage19 = PlayerPrefs.GetInt ("Star3Stage19");
			star3_Stage20 = PlayerPrefs.GetInt ("Star3Stage20");
			star3_Stage21 = PlayerPrefs.GetInt ("Star3Stage21");
			star3_Stage22 = PlayerPrefs.GetInt ("Star3Stage22");
			star3_Stage23 = PlayerPrefs.GetInt ("Star3Stage23");
			star3_Stage24 = PlayerPrefs.GetInt ("Star3Stage24");
			
			starStage4_1 (star3_Stage19, starStage19);
			starStage4_2 (star3_Stage20, starStage20);
			starStage4_3 (star3_Stage21, starStage21);
			starStage4_4 (star3_Stage22, starStage22);
			starStage4_5 (star3_Stage23, starStage23);
			starStage4_6 (star3_Stage24, starStage24);
		}
		else if (Application.loadedLevelName == "Stage Selection5") {
			starStage25 = PlayerPrefs.GetInt ("StarStage25");
			starStage26 = PlayerPrefs.GetInt ("StarStage26");
			starStage27 = PlayerPrefs.GetInt ("StarStage27");
			starStage28 = PlayerPrefs.GetInt ("StarStage28");
			starStage29 = PlayerPrefs.GetInt ("StarStage29");	
			starStage30 = PlayerPrefs.GetInt ("StarStage30");
			
			if (starStage25 == 3) {
				PlayerPrefs.SetInt ("Star3Stage25", 1);		
			}
			else if (starStage26 == 3) {
				PlayerPrefs.SetInt ("Star3Stage26", 1);		
			}
			else if (starStage27 == 3) {
				PlayerPrefs.SetInt ("Star3Stage27", 1);		
			}
			else if (starStage28 == 3) {
				PlayerPrefs.SetInt ("Star3Stage28", 1);		
			}
			else if (starStage29 == 3) {
				PlayerPrefs.SetInt ("Star3Stage29", 1);		
			}
			else if (starStage30 == 3) {
				PlayerPrefs.SetInt ("Star3Stage30", 1);		
			}
			
			star3_Stage25 = PlayerPrefs.GetInt ("Star3Stage25");
			star3_Stage26 = PlayerPrefs.GetInt ("Star3Stage26");
			star3_Stage27 = PlayerPrefs.GetInt ("Star3Stage27");
			star3_Stage28 = PlayerPrefs.GetInt ("Star3Stage28");
			star3_Stage29 = PlayerPrefs.GetInt ("Star3Stage29");
			star3_Stage30 = PlayerPrefs.GetInt ("Star3Stage30");
			
			starStage5_1 (star3_Stage25, starStage25);
			starStage5_2 (star3_Stage26, starStage26);
			starStage5_3 (star3_Stage27, starStage27);
			starStage5_4 (star3_Stage28, starStage28);
			starStage5_5 (star3_Stage29, starStage29);
			starStage5_6 (star3_Stage30, starStage30);
		}
		else if (Application.loadedLevelName == "Stage Selection6") {
			starStage31 = PlayerPrefs.GetInt ("StarStage31");
			starStage32 = PlayerPrefs.GetInt ("StarStage32");
			starStage33 = PlayerPrefs.GetInt ("StarStage33");
			starStage34 = PlayerPrefs.GetInt ("StarStage34");
			starStage35 = PlayerPrefs.GetInt ("StarStage35");	
			starStage36 = PlayerPrefs.GetInt ("StarStage36");
			
			if (starStage31 == 3) {
				PlayerPrefs.SetInt ("Star3Stage31", 1);		
			}
			else if (starStage32 == 3) {
				PlayerPrefs.SetInt ("Star3Stage32", 1);		
			}
			else if (starStage33 == 3) {
				PlayerPrefs.SetInt ("Star3Stage33", 1);		
			}
			else if (starStage34 == 3) {
				PlayerPrefs.SetInt ("Star3Stage34", 1);		
			}
			else if (starStage35 == 3) {
				PlayerPrefs.SetInt ("Star3Stage35", 1);		
			}
			else if (starStage36 == 3) {
				PlayerPrefs.SetInt ("Star3Stage36", 1);		
			}
			
			star3_Stage31 = PlayerPrefs.GetInt ("Star3Stage31");
			star3_Stage32 = PlayerPrefs.GetInt ("Star3Stage32");
			star3_Stage33 = PlayerPrefs.GetInt ("Star3Stage33");
			star3_Stage34 = PlayerPrefs.GetInt ("Star3Stage34");
			star3_Stage35 = PlayerPrefs.GetInt ("Star3Stage35");
			star3_Stage36 = PlayerPrefs.GetInt ("Star3Stage36");
			
			starStage6_1 (star3_Stage31, starStage31);
			starStage6_2 (star3_Stage32, starStage32);
			starStage6_3 (star3_Stage33, starStage33);
			starStage6_4 (star3_Stage34, starStage34);
			starStage6_5 (star3_Stage35, starStage35);
			starStage6_6 (star3_Stage36, starStage36);
		}
		else if (Application.loadedLevelName == "Stage Selection7") {
			starStage37 = PlayerPrefs.GetInt ("StarStage37");
			starStage38 = PlayerPrefs.GetInt ("StarStage38");
			starStage39 = PlayerPrefs.GetInt ("StarStage39");
			starStage40 = PlayerPrefs.GetInt ("StarStage40");
			starStage41 = PlayerPrefs.GetInt ("StarStage41");	
			starStage42 = PlayerPrefs.GetInt ("StarStage42");
			
			if (starStage37 == 3) {
				PlayerPrefs.SetInt ("Star3Stage37", 1);		
			}
			else if (starStage38 == 3) {
				PlayerPrefs.SetInt ("Star3Stage38", 1);		
			}
			else if (starStage39 == 3) {
				PlayerPrefs.SetInt ("Star3Stage39", 1);		
			}
			else if (starStage40 == 3) {
				PlayerPrefs.SetInt ("Star3Stage40", 1);		
			}
			else if (starStage41 == 3) {
				PlayerPrefs.SetInt ("Star3Stage41", 1);		
			}
			else if (starStage42 == 3) {
				PlayerPrefs.SetInt ("Star3Stage42", 1);		
			}
			
			star3_Stage37 = PlayerPrefs.GetInt ("Star3Stage37");
			star3_Stage38 = PlayerPrefs.GetInt ("Star3Stage38");
			star3_Stage39 = PlayerPrefs.GetInt ("Star3Stage39");
			star3_Stage40 = PlayerPrefs.GetInt ("Star3Stage40");
			star3_Stage41 = PlayerPrefs.GetInt ("Star3Stage41");
			star3_Stage42 = PlayerPrefs.GetInt ("Star3Stage42");
			
			starStage7_1 (star3_Stage37, starStage37);
			starStage7_2 (star3_Stage38, starStage38);
			starStage7_3 (star3_Stage39, starStage39);
			starStage7_4 (star3_Stage40, starStage40);
			starStage7_5 (star3_Stage41, starStage41);
			starStage7_6 (star3_Stage42, starStage42);
		}
		else if (Application.loadedLevelName == "Stage Selection8") {
			starStage43 = PlayerPrefs.GetInt ("StarStage43");
			starStage44 = PlayerPrefs.GetInt ("StarStage44");
			starStage45 = PlayerPrefs.GetInt ("StarStage45");
			starStage46 = PlayerPrefs.GetInt ("StarStage46");
			starStage47 = PlayerPrefs.GetInt ("StarStage47");	
			starStage48 = PlayerPrefs.GetInt ("StarStage48");
			
			if (starStage43 == 3) {
				PlayerPrefs.SetInt ("Star3Stage43", 1);		
			}
			else if (starStage44 == 3) {
				PlayerPrefs.SetInt ("Star3Stage44", 1);		
			}
			else if (starStage45 == 3) {
				PlayerPrefs.SetInt ("Star3Stage45", 1);		
			}
			else if (starStage46 == 3) {
				PlayerPrefs.SetInt ("Star3Stage46", 1);		
			}
			else if (starStage47 == 3) {
				PlayerPrefs.SetInt ("Star3Stage47", 1);		
			}
			else if (starStage48 == 3) {
				PlayerPrefs.SetInt ("Star3Stage48", 1);		
			}
			
			star3_Stage43 = PlayerPrefs.GetInt ("Star3Stage43");
			star3_Stage44 = PlayerPrefs.GetInt ("Star3Stage44");
			star3_Stage45 = PlayerPrefs.GetInt ("Star3Stage45");
			star3_Stage46 = PlayerPrefs.GetInt ("Star3Stage46");
			star3_Stage47 = PlayerPrefs.GetInt ("Star3Stage47");
			star3_Stage48 = PlayerPrefs.GetInt ("Star3Stage48");
			
			starStage8_1 (star3_Stage43, starStage43);
			starStage8_2 (star3_Stage44, starStage44);
			starStage8_3 (star3_Stage45, starStage45);
			starStage8_4 (star3_Stage46, starStage46);
			starStage8_5 (star3_Stage47, starStage47);
			starStage8_6 (star3_Stage48, starStage48);
		}
		else if (Application.loadedLevelName == "Stage Selection9") {
			starStage49 = PlayerPrefs.GetInt ("StarStage49");
			starStage50 = PlayerPrefs.GetInt ("StarStage50");
			starStage51 = PlayerPrefs.GetInt ("StarStage51");
			starStage52 = PlayerPrefs.GetInt ("StarStage52");
			starStage53 = PlayerPrefs.GetInt ("StarStage53");	
			starStage54 = PlayerPrefs.GetInt ("StarStage54");
			
			if (starStage49 == 3) {
				PlayerPrefs.SetInt ("Star3Stage49", 1);		
			}
			else if (starStage50 == 3) {
				PlayerPrefs.SetInt ("Star3Stage50", 1);		
			}
			else if (starStage51 == 3) {
				PlayerPrefs.SetInt ("Star3Stage51", 1);		
			}
			else if (starStage52 == 3) {
				PlayerPrefs.SetInt ("Star3Stage52", 1);		
			}
			else if (starStage53 == 3) {
				PlayerPrefs.SetInt ("Star3Stage53", 1);		
			}
			else if (starStage54 == 3) {
				PlayerPrefs.SetInt ("Star3Stage54", 1);		
			}
			
			star3_Stage49 = PlayerPrefs.GetInt ("Star3Stage49");
			star3_Stage50 = PlayerPrefs.GetInt ("Star3Stage50");
			star3_Stage51 = PlayerPrefs.GetInt ("Star3Stage51");
			star3_Stage52 = PlayerPrefs.GetInt ("Star3Stage52");
			star3_Stage53 = PlayerPrefs.GetInt ("Star3Stage53");
			star3_Stage54 = PlayerPrefs.GetInt ("Star3Stage54");
			
			starStage9_1 (star3_Stage49, starStage49);
			starStage9_2 (star3_Stage50, starStage50);
			starStage9_3 (star3_Stage51, starStage51);
			starStage9_4 (star3_Stage52, starStage52);
			starStage9_5 (star3_Stage53, starStage53);
			starStage9_6 (star3_Stage54, starStage54);
		}
	}

	void starStage1_1(int star3Stage1, int starStage1) {
		texStarStage1_1 = GameObject.Find ("Stars1-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage1 == 1) {
			texStarStage1_1[0].enabled = true;
			texStarStage1_1[1].enabled = true;
			texStarStage1_1[2].enabled = true;	
		}
		else {
			if (starStage1 == 1) {
				texStarStage1_1[0].enabled = true;	
			}
			if (starStage1 == 2) {
				texStarStage1_1[0].enabled = true;
				texStarStage1_1[2].enabled = true;
			}
			if (starStage1 == 3) {
				texStarStage1_1[0].enabled = true;
				texStarStage1_1[1].enabled = true;
				texStarStage1_1[2].enabled = true;
			}
		}

	}
	void starStage1_2(int star3Stage2, int starStage2) {
		texStarStage1_2 = GameObject.Find ("Stars1-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage2 == 1) {
			texStarStage1_2[0].enabled = true;
			texStarStage1_2[1].enabled = true;
			texStarStage1_2[2].enabled = true;	
		}
		else {
			if (starStage2 == 1) {
				texStarStage1_2[0].enabled = true;	
			}
			if (starStage2 == 2) {
				texStarStage1_2[0].enabled = true;
				texStarStage1_2[2].enabled = true;
			}
			if (starStage2 == 3) {
				texStarStage1_2[0].enabled = true;
				texStarStage1_2[1].enabled = true;
				texStarStage1_2[2].enabled = true;
			}
		}
		
	}
	void starStage1_3(int star3Stage3, int starStage3) {
		texStarStage1_3 = GameObject.Find ("Stars1-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage3 == 1) {
			texStarStage1_3[0].enabled = true;
			texStarStage1_3[1].enabled = true;
			texStarStage1_3[2].enabled = true;	
		}
		else {
			if (starStage3 == 1) {
				texStarStage1_3[0].enabled = true;	
			}
			if (starStage3 == 2) {
				texStarStage1_3[0].enabled = true;
				texStarStage1_3[2].enabled = true;
			}
			if (starStage3 == 3) {
				texStarStage1_3[0].enabled = true;
				texStarStage1_3[1].enabled = true;
				texStarStage1_3[2].enabled = true;
			}
		}
		
	}
	void starStage1_4(int star3Stage4, int starStage4) {
		texStarStage1_4 = GameObject.Find ("Stars1-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage4 == 1) {
			texStarStage1_4[0].enabled = true;
			texStarStage1_4[1].enabled = true;
			texStarStage1_4[2].enabled = true;	
		}
		else {
			if (starStage4 == 1) {
				texStarStage1_4[0].enabled = true;	
			}
			if (starStage4 == 2) {
				texStarStage1_4[0].enabled = true;
				texStarStage1_4[2].enabled = true;
			}
			if (starStage4 == 3) {
				texStarStage1_4[0].enabled = true;
				texStarStage1_4[1].enabled = true;
				texStarStage1_4[2].enabled = true;
			}
		}
		
	}
	void starStage1_5(int star3Stage5, int starStage5) {
		texStarStage1_5 = GameObject.Find ("Stars1-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage5 == 1) {
			texStarStage1_5[0].enabled = true;
			texStarStage1_5[1].enabled = true;
			texStarStage1_5[2].enabled = true;	
		}
		else {
			if (starStage5 == 1) {
				texStarStage1_5[0].enabled = true;	
			}
			if (starStage5 == 2) {
				texStarStage1_5[0].enabled = true;
				texStarStage1_5[2].enabled = true;
			}
			if (starStage5 == 3) {
				texStarStage1_5[0].enabled = true;
				texStarStage1_5[1].enabled = true;
				texStarStage1_5[2].enabled = true;
			}
		}
		
	}
	void starStage1_6(int star3Stage6, int starStage6) {
		texStarStage1_6 = GameObject.Find ("Stars1-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage6 == 1) {
			texStarStage1_6[0].enabled = true;
			texStarStage1_6[1].enabled = true;
			texStarStage1_6[2].enabled = true;	
		}
		else {
			if (starStage6 == 1) {
				texStarStage1_6[0].enabled = true;	
			}
			if (starStage6 == 2) {
				texStarStage1_6[0].enabled = true;
				texStarStage1_6[2].enabled = true;
			}
			if (starStage6 == 3) {
				texStarStage1_6[0].enabled = true;
				texStarStage1_6[1].enabled = true;
				texStarStage1_6[2].enabled = true;
			}
		}
		
	}
	void starStage2_1(int star3Stage7, int starStage7) {
		texStarStage2_1 = GameObject.Find ("Stars2-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage7 == 1) {
			texStarStage2_1[0].enabled = true;
			texStarStage2_1[1].enabled = true;
			texStarStage2_1[2].enabled = true;	
		}
		else {
			if (starStage7 == 1) {
				texStarStage2_1[0].enabled = true;	
			}
			if (starStage7 == 2) {
				texStarStage2_1[0].enabled = true;
				texStarStage2_1[2].enabled = true;
			}
			if (starStage7 == 3) {
				texStarStage2_1[0].enabled = true;
				texStarStage2_1[1].enabled = true;
				texStarStage2_1[2].enabled = true;
			}
		}
		
	}
	void starStage2_2(int star3Stage8, int starStage8) {
		texStarStage2_2 = GameObject.Find ("Stars2-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage8 == 1) {
			texStarStage2_2[0].enabled = true;
			texStarStage2_2[1].enabled = true;
			texStarStage2_2[2].enabled = true;	
		}
		else {
			if (starStage8 == 1) {
				texStarStage2_2[0].enabled = true;	
			}
			if (starStage8 == 2) {
				texStarStage2_2[0].enabled = true;
				texStarStage2_2[2].enabled = true;
			}
			if (starStage8 == 3) {
				texStarStage2_2[0].enabled = true;
				texStarStage2_2[1].enabled = true;
				texStarStage2_2[2].enabled = true;
			}
		}
		
	}
	void starStage2_3(int star3Stage9, int starStage9) {
		texStarStage2_3 = GameObject.Find ("Stars2-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage9 == 1) {
			texStarStage2_3[0].enabled = true;
			texStarStage2_3[1].enabled = true;
			texStarStage2_3[2].enabled = true;	
		}
		else {
			if (starStage9 == 1) {
				texStarStage2_3[0].enabled = true;	
			}
			if (starStage9 == 2) {
				texStarStage2_3[0].enabled = true;
				texStarStage2_3[2].enabled = true;
			}
			if (starStage9 == 3) {
				texStarStage2_3[0].enabled = true;
				texStarStage2_3[1].enabled = true;
				texStarStage2_3[2].enabled = true;
			}
		}
		
	}
	void starStage2_4(int star3Stage10, int starStage10) {
		texStarStage2_4 = GameObject.Find ("Stars2-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage10 == 1) {
			texStarStage2_4[0].enabled = true;
			texStarStage2_4[1].enabled = true;
			texStarStage2_4[2].enabled = true;	
		}
		else {
			if (starStage10 == 1) {
				texStarStage2_4[0].enabled = true;	
			}
			if (starStage10 == 2) {
				texStarStage2_4[0].enabled = true;
				texStarStage2_4[2].enabled = true;
			}
			if (starStage10 == 3) {
				texStarStage2_4[0].enabled = true;
				texStarStage2_4[1].enabled = true;
				texStarStage2_4[2].enabled = true;
			}
		}
		
	}
	void starStage2_5(int star3Stage11, int starStage11) {
		texStarStage2_5 = GameObject.Find ("Stars2-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage11 == 1) {
			texStarStage2_5[0].enabled = true;
			texStarStage2_5[1].enabled = true;
			texStarStage2_5[2].enabled = true;	
		}
		else {
			if (starStage11 == 1) {
				texStarStage2_5[0].enabled = true;	
			}
			if (starStage11 == 2) {
				texStarStage2_5[0].enabled = true;
				texStarStage2_5[2].enabled = true;
			}
			if (starStage11 == 3) {
				texStarStage2_5[0].enabled = true;
				texStarStage2_5[1].enabled = true;
				texStarStage2_5[2].enabled = true;
			}
		}
		
	}
	void starStage2_6(int star3Stage12, int starStage12) {
		texStarStage2_6 = GameObject.Find ("Stars2-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage12 == 1) {
			texStarStage2_6[0].enabled = true;
			texStarStage2_6[1].enabled = true;
			texStarStage2_6[2].enabled = true;	
		}
		else {
			if (starStage12 == 1) {
				texStarStage2_6[0].enabled = true;	
			}
			if (starStage12 == 2) {
				texStarStage2_6[0].enabled = true;
				texStarStage2_6[2].enabled = true;
			}
			if (starStage12 == 3) {
				texStarStage2_6[0].enabled = true;
				texStarStage2_6[1].enabled = true;
				texStarStage2_6[2].enabled = true;
			}
		}
		
	}
	void starStage3_1(int star3Stage13, int starStage13) {
		texStarStage3_1 = GameObject.Find ("Stars3-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage13 == 1) {
			texStarStage3_1[0].enabled = true;
			texStarStage3_1[1].enabled = true;
			texStarStage3_1[2].enabled = true;	
		}
		else {
			if (starStage13 == 1) {
				texStarStage3_1[0].enabled = true;	
			}
			if (starStage13 == 2) {
				texStarStage3_1[0].enabled = true;
				texStarStage3_1[2].enabled = true;
			}
			if (starStage13 == 3) {
				texStarStage3_1[0].enabled = true;
				texStarStage3_1[1].enabled = true;
				texStarStage3_1[2].enabled = true;
			}
		}
		
	}
	void starStage3_2(int star3Stage14, int starStage14) {
		texStarStage3_2 = GameObject.Find ("Stars3-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage14 == 1) {
			texStarStage3_2[0].enabled = true;
			texStarStage3_2[1].enabled = true;
			texStarStage3_2[2].enabled = true;	
		}
		else {
			if (starStage14 == 1) {
				texStarStage3_2[0].enabled = true;	
			}
			if (starStage14 == 2) {
				texStarStage3_2[0].enabled = true;
				texStarStage3_2[2].enabled = true;
			}
			if (starStage14 == 3) {
				texStarStage3_2[0].enabled = true;
				texStarStage3_2[1].enabled = true;
				texStarStage3_2[2].enabled = true;
			}
		}
		
	}
	void starStage3_3(int star3Stage15, int starStage15) {
		texStarStage3_3 = GameObject.Find ("Stars3-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage15 == 1) {
			texStarStage3_3[0].enabled = true;
			texStarStage3_3[1].enabled = true;
			texStarStage3_3[2].enabled = true;	
		}
		else {
			if (starStage15 == 1) {
				texStarStage3_3[0].enabled = true;	
			}
			if (starStage15 == 2) {
				texStarStage3_3[0].enabled = true;
				texStarStage3_3[2].enabled = true;
			}
			if (starStage15 == 3) {
				texStarStage3_3[0].enabled = true;
				texStarStage3_3[1].enabled = true;
				texStarStage3_3[2].enabled = true;
			}
		}
		
	}
	void starStage3_4(int star3Stage16, int starStage16) {
		texStarStage3_4 = GameObject.Find ("Stars3-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage16 == 1) {
			texStarStage3_4[0].enabled = true;
			texStarStage3_4[1].enabled = true;
			texStarStage3_4[2].enabled = true;	
		}
		else {
			if (starStage16 == 1) {
				texStarStage3_4[0].enabled = true;	
			}
			if (starStage16 == 2) {
				texStarStage3_4[0].enabled = true;
				texStarStage3_4[2].enabled = true;
			}
			if (starStage16 == 3) {
				texStarStage3_4[0].enabled = true;
				texStarStage3_4[1].enabled = true;
				texStarStage3_4[2].enabled = true;
			}
		}
		
	}
	void starStage3_5(int star3Stage17, int starStage17) {
		texStarStage3_5 = GameObject.Find ("Stars3-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage17 == 1) {
			texStarStage3_5[0].enabled = true;
			texStarStage3_5[1].enabled = true;
			texStarStage3_5[2].enabled = true;	
		}
		else {
			if (starStage17 == 1) {
				texStarStage3_5[0].enabled = true;	
			}
			if (starStage17 == 2) {
				texStarStage3_5[0].enabled = true;
				texStarStage3_5[2].enabled = true;
			}
			if (starStage17 == 3) {
				texStarStage3_5[0].enabled = true;
				texStarStage3_5[1].enabled = true;
				texStarStage3_5[2].enabled = true;
			}
		}
		
	}
	void starStage3_6(int star3Stage18, int starStage18) {
		texStarStage3_6 = GameObject.Find ("Stars3-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage18 == 1) {
			texStarStage3_6[0].enabled = true;
			texStarStage3_6[1].enabled = true;
			texStarStage3_6[2].enabled = true;	
		}
		else {
			if (starStage18 == 1) {
				texStarStage3_6[0].enabled = true;	
			}
			if (starStage18 == 2) {
				texStarStage3_6[0].enabled = true;
				texStarStage3_6[2].enabled = true;
			}
			if (starStage18 == 3) {
				texStarStage3_6[0].enabled = true;
				texStarStage3_6[1].enabled = true;
				texStarStage3_6[2].enabled = true;
			}
		}
		
	}
	void starStage4_1(int star3Stage19, int starStage19) {
		texStarStage4_1 = GameObject.Find ("Stars4-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage19 == 1) {
			texStarStage4_1[0].enabled = true;
			texStarStage4_1[1].enabled = true;
			texStarStage4_1[2].enabled = true;	
		}
		else {
			if (starStage19 == 1) {
				texStarStage4_1[0].enabled = true;	
			}
			if (starStage19 == 2) {
				texStarStage4_1[0].enabled = true;
				texStarStage4_1[2].enabled = true;
			}
			if (starStage19 == 3) {
				texStarStage4_1[0].enabled = true;
				texStarStage4_1[1].enabled = true;
				texStarStage4_1[2].enabled = true;
			}
		}
		
	}
	void starStage4_2(int star3Stage20, int starStage20) {
		texStarStage4_2 = GameObject.Find ("Stars4-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage20 == 1) {
			texStarStage4_2[0].enabled = true;
			texStarStage4_2[1].enabled = true;
			texStarStage4_2[2].enabled = true;	
		}
		else {
			if (starStage20 == 1) {
				texStarStage4_2[0].enabled = true;	
			}
			if (starStage20 == 2) {
				texStarStage4_2[0].enabled = true;
				texStarStage4_2[2].enabled = true;
			}
			if (starStage20 == 3) {
				texStarStage4_2[0].enabled = true;
				texStarStage4_2[1].enabled = true;
				texStarStage4_2[2].enabled = true;
			}
		}
		
	}
	void starStage4_3(int star3Stage21, int starStage21) {
		texStarStage4_3 = GameObject.Find ("Stars4-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage21 == 1) {
			texStarStage4_3[0].enabled = true;
			texStarStage4_3[1].enabled = true;
			texStarStage4_3[2].enabled = true;	
		}
		else {
			if (starStage21 == 1) {
				texStarStage4_3[0].enabled = true;	
			}
			if (starStage21 == 2) {
				texStarStage4_3[0].enabled = true;
				texStarStage4_3[2].enabled = true;
			}
			if (starStage21 == 3) {
				texStarStage4_3[0].enabled = true;
				texStarStage4_3[1].enabled = true;
				texStarStage4_3[2].enabled = true;
			}
		}
		
	}
	void starStage4_4(int star3Stage22, int starStage22) {
		texStarStage4_4 = GameObject.Find ("Stars4-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage22 == 1) {
			texStarStage4_4[0].enabled = true;
			texStarStage4_4[1].enabled = true;
			texStarStage4_4[2].enabled = true;	
		}
		else {
			if (starStage22 == 1) {
				texStarStage4_4[0].enabled = true;	
			}
			if (starStage22 == 2) {
				texStarStage4_4[0].enabled = true;
				texStarStage4_4[2].enabled = true;
			}
			if (starStage22 == 3) {
				texStarStage4_4[0].enabled = true;
				texStarStage4_4[1].enabled = true;
				texStarStage4_4[2].enabled = true;
			}
		}
		
	}
	void starStage4_5(int star3Stage23, int starStage23) {
		texStarStage4_5 = GameObject.Find ("Stars4-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage23 == 1) {
			texStarStage4_5[0].enabled = true;
			texStarStage4_5[1].enabled = true;
			texStarStage4_5[2].enabled = true;	
		}
		else {
			if (starStage23 == 1) {
				texStarStage4_5[0].enabled = true;	
			}
			if (starStage23 == 2) {
				texStarStage4_5[0].enabled = true;
				texStarStage4_5[2].enabled = true;
			}
			if (starStage23 == 3) {
				texStarStage4_5[0].enabled = true;
				texStarStage4_5[1].enabled = true;
				texStarStage4_5[2].enabled = true;
			}
		}
		
	}
	void starStage4_6(int star3Stage24, int starStage24) {
		texStarStage4_6 = GameObject.Find ("Stars4-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage24 == 1) {
			texStarStage4_6[0].enabled = true;
			texStarStage4_6[1].enabled = true;
			texStarStage4_6[2].enabled = true;	
		}
		else {
			if (starStage24 == 1) {
				texStarStage4_6[0].enabled = true;	
			}
			if (starStage24 == 2) {
				texStarStage4_6[0].enabled = true;
				texStarStage4_6[2].enabled = true;
			}
			if (starStage24 == 3) {
				texStarStage4_6[0].enabled = true;
				texStarStage4_6[1].enabled = true;
				texStarStage4_6[2].enabled = true;
			}
		}
	}
	void starStage5_1(int star3Stage25, int starStage25) {
		texStarStage5_1 = GameObject.Find ("Stars5-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage25 == 1) {
			texStarStage5_1[0].enabled = true;
			texStarStage5_1[1].enabled = true;
			texStarStage5_1[2].enabled = true;	
		}
		else {
			if (starStage25 == 1) {
				texStarStage5_1[0].enabled = true;	
			}
			if (starStage25 == 2) {
				texStarStage5_1[0].enabled = true;
				texStarStage5_1[2].enabled = true;
			}
			if (starStage25 == 3) {
				texStarStage5_1[0].enabled = true;
				texStarStage5_1[1].enabled = true;
				texStarStage5_1[2].enabled = true;
			}
		}
		
	}
	void starStage5_2(int star3Stage26, int starStage26) {
		texStarStage5_2 = GameObject.Find ("Stars5-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage26 == 1) {
			texStarStage5_2[0].enabled = true;
			texStarStage5_2[1].enabled = true;
			texStarStage5_2[2].enabled = true;	
		}
		else {
			if (starStage26 == 1) {
				texStarStage5_2[0].enabled = true;	
			}
			if (starStage26 == 2) {
				texStarStage5_2[0].enabled = true;
				texStarStage5_2[2].enabled = true;
			}
			if (starStage26 == 3) {
				texStarStage5_2[0].enabled = true;
				texStarStage5_2[1].enabled = true;
				texStarStage5_2[2].enabled = true;
			}
		}
		
	}
	void starStage5_3(int star3Stage27, int starStage27) {
		texStarStage5_3 = GameObject.Find ("Stars5-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage27 == 1) {
			texStarStage5_3[0].enabled = true;
			texStarStage5_3[1].enabled = true;
			texStarStage5_3[2].enabled = true;	
		}
		else {
			if (starStage27 == 1) {
				texStarStage5_3[0].enabled = true;	
			}
			if (starStage27 == 2) {
				texStarStage5_3[0].enabled = true;
				texStarStage5_3[2].enabled = true;
			}
			if (starStage27 == 3) {
				texStarStage5_3[0].enabled = true;
				texStarStage5_3[1].enabled = true;
				texStarStage5_3[2].enabled = true;
			}
		}
		
	}
	void starStage5_4(int star3Stage28, int starStage28) {
		texStarStage5_4 = GameObject.Find ("Stars5-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage28 == 1) {
			texStarStage5_4[0].enabled = true;
			texStarStage5_4[1].enabled = true;
			texStarStage5_4[2].enabled = true;	
		}
		else {
			if (starStage28 == 1) {
				texStarStage5_4[0].enabled = true;	
			}
			if (starStage28 == 2) {
				texStarStage5_4[0].enabled = true;
				texStarStage5_4[2].enabled = true;
			}
			if (starStage28 == 3) {
				texStarStage5_4[0].enabled = true;
				texStarStage5_4[1].enabled = true;
				texStarStage5_4[2].enabled = true;
			}
		}
		
	}
	void starStage5_5(int star3Stage29, int starStage29) {
		texStarStage5_5 = GameObject.Find ("Stars5-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage29 == 1) {
			texStarStage5_5[0].enabled = true;
			texStarStage5_5[1].enabled = true;
			texStarStage5_5[2].enabled = true;	
		}
		else {
			if (starStage29 == 1) {
				texStarStage5_5[0].enabled = true;	
			}
			if (starStage29 == 2) {
				texStarStage5_5[0].enabled = true;
				texStarStage5_5[2].enabled = true;
			}
			if (starStage29 == 3) {
				texStarStage5_5[0].enabled = true;
				texStarStage5_5[1].enabled = true;
				texStarStage5_5[2].enabled = true;
			}
		}
		
	}
	void starStage5_6(int star3Stage30, int starStage30) {
		texStarStage5_6 = GameObject.Find ("Stars5-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage30 == 1) {
			texStarStage5_6[0].enabled = true;
			texStarStage5_6[1].enabled = true;
			texStarStage5_6[2].enabled = true;	
		}
		else {
			if (starStage30 == 1) {
				texStarStage5_6[0].enabled = true;	
			}
			if (starStage30 == 2) {
				texStarStage5_6[0].enabled = true;
				texStarStage5_6[2].enabled = true;
			}
			if (starStage30 == 3) {
				texStarStage5_6[0].enabled = true;
				texStarStage5_6[1].enabled = true;
				texStarStage5_6[2].enabled = true;
			}
		}	
	}
	void starStage6_1(int star3Stage31, int starStage31) {
		texStarStage6_1 = GameObject.Find ("Stars6-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage31 == 1) {
			texStarStage6_1[0].enabled = true;
			texStarStage6_1[1].enabled = true;
			texStarStage6_1[2].enabled = true;	
		}
		else {
			if (starStage31 == 1) {
				texStarStage6_1[0].enabled = true;	
			}
			if (starStage31 == 2) {
				texStarStage6_1[0].enabled = true;
				texStarStage6_1[2].enabled = true;
			}
			if (starStage31 == 3) {
				texStarStage6_1[0].enabled = true;
				texStarStage6_1[1].enabled = true;
				texStarStage6_1[2].enabled = true;
			}
		}
		
	}
	void starStage6_2(int star3Stage32, int starStage32) {
		texStarStage6_2 = GameObject.Find ("Stars6-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage32 == 1) {
			texStarStage6_2[0].enabled = true;
			texStarStage6_2[1].enabled = true;
			texStarStage6_2[2].enabled = true;	
		}
		else {
			if (starStage32 == 1) {
				texStarStage6_2[0].enabled = true;	
			}
			if (starStage32 == 2) {
				texStarStage6_2[0].enabled = true;
				texStarStage6_2[2].enabled = true;
			}
			if (starStage32 == 3) {
				texStarStage6_2[0].enabled = true;
				texStarStage6_2[1].enabled = true;
				texStarStage6_2[2].enabled = true;
			}
		}
		
	}
	void starStage6_3(int star3Stage33, int starStage33) {
		texStarStage6_3 = GameObject.Find ("Stars6-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage33 == 1) {
			texStarStage6_3[0].enabled = true;
			texStarStage6_3[1].enabled = true;
			texStarStage6_3[2].enabled = true;	
		}
		else {
			if (starStage33 == 1) {
				texStarStage6_3[0].enabled = true;	
			}
			if (starStage33 == 2) {
				texStarStage6_3[0].enabled = true;
				texStarStage6_3[2].enabled = true;
			}
			if (starStage33 == 3) {
				texStarStage6_3[0].enabled = true;
				texStarStage6_3[1].enabled = true;
				texStarStage6_3[2].enabled = true;
			}
		}
		
	}
	void starStage6_4(int star3Stage34, int starStage34) {
		texStarStage6_4 = GameObject.Find ("Stars6-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage34 == 1) {
			texStarStage6_4[0].enabled = true;
			texStarStage6_4[1].enabled = true;
			texStarStage6_4[2].enabled = true;	
		}
		else {
			if (starStage34 == 1) {
				texStarStage6_4[0].enabled = true;	
			}
			if (starStage34 == 2) {
				texStarStage6_4[0].enabled = true;
				texStarStage6_4[2].enabled = true;
			}
			if (starStage34 == 3) {
				texStarStage6_4[0].enabled = true;
				texStarStage6_4[1].enabled = true;
				texStarStage6_4[2].enabled = true;
			}
		}
		
	}
	void starStage6_5(int star3Stage35, int starStage35) {
		texStarStage6_5 = GameObject.Find ("Stars6-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage35 == 1) {
			texStarStage6_5[0].enabled = true;
			texStarStage6_5[1].enabled = true;
			texStarStage6_5[2].enabled = true;	
		}
		else {
			if (starStage35 == 1) {
				texStarStage6_5[0].enabled = true;	
			}
			if (starStage35 == 2) {
				texStarStage6_5[0].enabled = true;
				texStarStage6_5[2].enabled = true;
			}
			if (starStage35 == 3) {
				texStarStage6_5[0].enabled = true;
				texStarStage6_5[1].enabled = true;
				texStarStage6_5[2].enabled = true;
			}
		}
		
	}
	void starStage6_6(int star3Stage36, int starStage36) {
		texStarStage6_6 = GameObject.Find ("Stars6-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage36 == 1) {
			texStarStage6_6[0].enabled = true;
			texStarStage6_6[1].enabled = true;
			texStarStage6_6[2].enabled = true;	
		}
		else {
			if (starStage36 == 1) {
				texStarStage6_6[0].enabled = true;	
			}
			if (starStage36 == 2) {
				texStarStage6_6[0].enabled = true;
				texStarStage6_6[2].enabled = true;
			}
			if (starStage36 == 3) {
				texStarStage6_6[0].enabled = true;
				texStarStage6_6[1].enabled = true;
				texStarStage6_6[2].enabled = true;
			}
		}	
	}
	void starStage7_1(int star3Stage37, int starStage37) {
		texStarStage7_1 = GameObject.Find ("Stars7-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage37 == 1) {
			texStarStage7_1[0].enabled = true;
			texStarStage7_1[1].enabled = true;
			texStarStage7_1[2].enabled = true;	
		}
		else {
			if (starStage37 == 1) {
				texStarStage7_1[0].enabled = true;	
			}
			if (starStage37 == 2) {
				texStarStage7_1[0].enabled = true;
				texStarStage7_1[2].enabled = true;
			}
			if (starStage37 == 3) {
				texStarStage7_1[0].enabled = true;
				texStarStage7_1[1].enabled = true;
				texStarStage7_1[2].enabled = true;
			}
		}
		
	}
	void starStage7_2(int star3Stage38, int starStage38) {
		texStarStage7_2 = GameObject.Find ("Stars7-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage38 == 1) {
			texStarStage7_2[0].enabled = true;
			texStarStage7_2[1].enabled = true;
			texStarStage7_2[2].enabled = true;	
		}
		else {
			if (starStage38 == 1) {
				texStarStage7_2[0].enabled = true;	
			}
			if (starStage38 == 2) {
				texStarStage7_2[0].enabled = true;
				texStarStage7_2[2].enabled = true;
			}
			if (starStage38 == 3) {
				texStarStage7_2[0].enabled = true;
				texStarStage7_2[1].enabled = true;
				texStarStage7_2[2].enabled = true;
			}
		}
		
	}
	void starStage7_3(int star3Stage39, int starStage39) {
		texStarStage7_3 = GameObject.Find ("Stars7-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage39 == 1) {
			texStarStage7_3[0].enabled = true;
			texStarStage7_3[1].enabled = true;
			texStarStage7_3[2].enabled = true;	
		}
		else {
			if (starStage39 == 1) {
				texStarStage7_3[0].enabled = true;	
			}
			if (starStage39 == 2) {
				texStarStage7_3[0].enabled = true;
				texStarStage7_3[2].enabled = true;
			}
			if (starStage39 == 3) {
				texStarStage7_3[0].enabled = true;
				texStarStage7_3[1].enabled = true;
				texStarStage7_3[2].enabled = true;
			}
		}
		
	}
	void starStage7_4(int star3Stage40, int starStage40) {
		texStarStage7_4 = GameObject.Find ("Stars7-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage40 == 1) {
			texStarStage7_4[0].enabled = true;
			texStarStage7_4[1].enabled = true;
			texStarStage7_4[2].enabled = true;	
		}
		else {
			if (starStage40 == 1) {
				texStarStage7_4[0].enabled = true;	
			}
			if (starStage40 == 2) {
				texStarStage7_4[0].enabled = true;
				texStarStage7_4[2].enabled = true;
			}
			if (starStage40 == 3) {
				texStarStage7_4[0].enabled = true;
				texStarStage7_4[1].enabled = true;
				texStarStage7_4[2].enabled = true;
			}
		}
		
	}
	void starStage7_5(int star3Stage41, int starStage41) {
		texStarStage7_5 = GameObject.Find ("Stars7-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage41 == 1) {
			texStarStage7_5[0].enabled = true;
			texStarStage7_5[1].enabled = true;
			texStarStage7_5[2].enabled = true;	
		}
		else {
			if (starStage41 == 1) {
				texStarStage7_5[0].enabled = true;	
			}
			if (starStage41 == 2) {
				texStarStage7_5[0].enabled = true;
				texStarStage7_5[2].enabled = true;
			}
			if (starStage41 == 3) {
				texStarStage7_5[0].enabled = true;
				texStarStage7_5[1].enabled = true;
				texStarStage7_5[2].enabled = true;
			}
		}
		
	}
	void starStage7_6(int star3Stage42, int starStage42) {
		texStarStage7_6 = GameObject.Find ("Stars7-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage42 == 1) {
			texStarStage7_6[0].enabled = true;
			texStarStage7_6[1].enabled = true;
			texStarStage7_6[2].enabled = true;	
		}
		else {
			if (starStage42 == 1) {
				texStarStage7_6[0].enabled = true;	
			}
			if (starStage42 == 2) {
				texStarStage7_6[0].enabled = true;
				texStarStage7_6[2].enabled = true;
			}
			if (starStage42 == 3) {
				texStarStage7_6[0].enabled = true;
				texStarStage7_6[1].enabled = true;
				texStarStage7_6[2].enabled = true;
			}
		}	
	}
	void starStage8_1(int star3Stage43, int starStage43) {
		texStarStage8_1 = GameObject.Find ("Stars8-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage43 == 1) {
			texStarStage8_1[0].enabled = true;
			texStarStage8_1[1].enabled = true;
			texStarStage8_1[2].enabled = true;	
		}
		else {
			if (starStage43 == 1) {
				texStarStage8_1[0].enabled = true;	
			}
			if (starStage43 == 2) {
				texStarStage8_1[0].enabled = true;
				texStarStage8_1[2].enabled = true;
			}
			if (starStage43 == 3) {
				texStarStage8_1[0].enabled = true;
				texStarStage8_1[1].enabled = true;
				texStarStage8_1[2].enabled = true;
			}
		}
		
	}
	void starStage8_2(int star3Stage44, int starStage44) {
		texStarStage8_2 = GameObject.Find ("Stars8-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage44 == 1) {
			texStarStage8_2[0].enabled = true;
			texStarStage8_2[1].enabled = true;
			texStarStage8_2[2].enabled = true;	
		}
		else {
			if (starStage44 == 1) {
				texStarStage8_2[0].enabled = true;	
			}
			if (starStage44 == 2) {
				texStarStage8_2[0].enabled = true;
				texStarStage8_2[2].enabled = true;
			}
			if (starStage44 == 3) {
				texStarStage8_2[0].enabled = true;
				texStarStage8_2[1].enabled = true;
				texStarStage8_2[2].enabled = true;
			}
		}
		
	}
	void starStage8_3(int star3Stage45, int starStage45) {
		texStarStage8_3 = GameObject.Find ("Stars8-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage45 == 1) {
			texStarStage8_3[0].enabled = true;
			texStarStage8_3[1].enabled = true;
			texStarStage8_3[2].enabled = true;	
		}
		else {
			if (starStage45 == 1) {
				texStarStage8_3[0].enabled = true;	
			}
			if (starStage45 == 2) {
				texStarStage8_3[0].enabled = true;
				texStarStage8_3[2].enabled = true;
			}
			if (starStage45 == 3) {
				texStarStage8_3[0].enabled = true;
				texStarStage8_3[1].enabled = true;
				texStarStage8_3[2].enabled = true;
			}
		}
		
	}
	void starStage8_4(int star3Stage46, int starStage46) {
		texStarStage8_4 = GameObject.Find ("Stars8-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage46 == 1) {
			texStarStage8_4[0].enabled = true;
			texStarStage8_4[1].enabled = true;
			texStarStage8_4[2].enabled = true;	
		}
		else {
			if (starStage46 == 1) {
				texStarStage8_4[0].enabled = true;	
			}
			if (starStage46 == 2) {
				texStarStage8_4[0].enabled = true;
				texStarStage8_4[2].enabled = true;
			}
			if (starStage46 == 3) {
				texStarStage8_4[0].enabled = true;
				texStarStage8_4[1].enabled = true;
				texStarStage8_4[2].enabled = true;
			}
		}
		
	}
	void starStage8_5(int star3Stage47, int starStage47) {
		texStarStage8_5 = GameObject.Find ("Stars8-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage47 == 1) {
			texStarStage8_5[0].enabled = true;
			texStarStage8_5[1].enabled = true;
			texStarStage8_5[2].enabled = true;	
		}
		else {
			if (starStage47 == 1) {
				texStarStage8_5[0].enabled = true;	
			}
			if (starStage47 == 2) {
				texStarStage8_5[0].enabled = true;
				texStarStage8_5[2].enabled = true;
			}
			if (starStage47 == 3) {
				texStarStage8_5[0].enabled = true;
				texStarStage8_5[1].enabled = true;
				texStarStage8_5[2].enabled = true;
			}
		}
		
	}
	void starStage8_6(int star3Stage48, int starStage48) {
		texStarStage8_6 = GameObject.Find ("Stars8-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage48 == 1) {
			texStarStage8_6[0].enabled = true;
			texStarStage8_6[1].enabled = true;
			texStarStage8_6[2].enabled = true;	
		}
		else {
			if (starStage48 == 1) {
				texStarStage8_6[0].enabled = true;	
			}
			if (starStage48 == 2) {
				texStarStage8_6[0].enabled = true;
				texStarStage8_6[2].enabled = true;
			}
			if (starStage48 == 3) {
				texStarStage8_6[0].enabled = true;
				texStarStage8_6[1].enabled = true;
				texStarStage8_6[2].enabled = true;
			}
		}	
	}
	void starStage9_1(int star3Stage49, int starStage49) {
		texStarStage9_1 = GameObject.Find ("Stars9-1").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage49 == 1) {
			texStarStage9_1[0].enabled = true;
			texStarStage9_1[1].enabled = true;
			texStarStage9_1[2].enabled = true;	
		}
		else {
			if (starStage49 == 1) {
				texStarStage9_1[0].enabled = true;	
			}
			if (starStage49 == 2) {
				texStarStage9_1[0].enabled = true;
				texStarStage9_1[2].enabled = true;
			}
			if (starStage49 == 3) {
				texStarStage9_1[0].enabled = true;
				texStarStage9_1[1].enabled = true;
				texStarStage9_1[2].enabled = true;
			}
		}
		
	}
	void starStage9_2(int star3Stage50, int starStage50) {
		texStarStage9_2 = GameObject.Find ("Stars9-2").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage50 == 1) {
			texStarStage9_2[0].enabled = true;
			texStarStage9_2[1].enabled = true;
			texStarStage9_2[2].enabled = true;	
		}
		else {
			if (starStage50 == 1) {
				texStarStage9_2[0].enabled = true;	
			}
			if (starStage50 == 2) {
				texStarStage9_2[0].enabled = true;
				texStarStage9_2[2].enabled = true;
			}
			if (starStage50 == 3) {
				texStarStage9_2[0].enabled = true;
				texStarStage9_2[1].enabled = true;
				texStarStage9_2[2].enabled = true;
			}
		}
		
	}
	void starStage9_3(int star3Stage51, int starStage51) {
		texStarStage9_3 = GameObject.Find ("Stars9-3").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage51 == 1) {
			texStarStage9_3[0].enabled = true;
			texStarStage9_3[1].enabled = true;
			texStarStage9_3[2].enabled = true;	
		}
		else {
			if (starStage51 == 1) {
				texStarStage9_3[0].enabled = true;	
			}
			if (starStage51 == 2) {
				texStarStage9_3[0].enabled = true;
				texStarStage9_3[2].enabled = true;
			}
			if (starStage51 == 3) {
				texStarStage9_3[0].enabled = true;
				texStarStage9_3[1].enabled = true;
				texStarStage9_3[2].enabled = true;
			}
		}
		
	}
	void starStage9_4(int star3Stage52, int starStage52) {
		texStarStage9_4 = GameObject.Find ("Stars9-4").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage52 == 1) {
			texStarStage9_4[0].enabled = true;
			texStarStage9_4[1].enabled = true;
			texStarStage9_4[2].enabled = true;	
		}
		else {
			if (starStage52 == 1) {
				texStarStage9_4[0].enabled = true;	
			}
			if (starStage52 == 2) {
				texStarStage9_4[0].enabled = true;
				texStarStage9_4[2].enabled = true;
			}
			if (starStage52 == 3) {
				texStarStage9_4[0].enabled = true;
				texStarStage9_4[1].enabled = true;
				texStarStage9_4[2].enabled = true;
			}
		}
		
	}
	void starStage9_5(int star3Stage53, int starStage53) {
		texStarStage9_5 = GameObject.Find ("Stars9-5").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage53 == 1) {
			texStarStage9_5[0].enabled = true;
			texStarStage9_5[1].enabled = true;
			texStarStage9_5[2].enabled = true;	
		}
		else {
			if (starStage53 == 1) {
				texStarStage9_5[0].enabled = true;	
			}
			if (starStage53 == 2) {
				texStarStage9_5[0].enabled = true;
				texStarStage9_5[2].enabled = true;
			}
			if (starStage53 == 3) {
				texStarStage9_5[0].enabled = true;
				texStarStage9_5[1].enabled = true;
				texStarStage9_5[2].enabled = true;
			}
		}
		
	}
	void starStage9_6(int star3Stage54, int starStage54) {
		texStarStage9_6 = GameObject.Find ("Stars9-6").GetComponentsInChildren<SpriteRenderer> ();
		if (star3Stage54 == 1) {
			texStarStage9_6[0].enabled = true;
			texStarStage9_6[1].enabled = true;
			texStarStage9_6[2].enabled = true;	
		}
		else {
			if (starStage54 == 1) {
				texStarStage9_6[0].enabled = true;	
			}
			if (starStage54 == 2) {
				texStarStage9_6[0].enabled = true;
				texStarStage9_6[2].enabled = true;
			}
			if (starStage54 == 3) {
				texStarStage9_6[0].enabled = true;
				texStarStage9_6[1].enabled = true;
				texStarStage9_6[2].enabled = true;
			}
		}	
	}
}
