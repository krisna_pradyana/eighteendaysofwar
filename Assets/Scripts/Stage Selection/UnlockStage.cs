﻿using UnityEngine;
using System.Collections;

public class UnlockStage : MonoBehaviour {
	public Sprite stageTexture;
	public Sprite bossTexture;
	private SpriteRenderer[] stageText;
	private SpriteRenderer[] stageButton;
	private SpriteRenderer[] starEmpty;
	private int clearStage1, clearStage2, clearStage3, clearStage4, clearStage5, clearStage6;
	private int clearStage7, clearStage8, clearStage9, clearStage10, clearStage11, clearStage12;
	private int clearStage13, clearStage14, clearStage15, clearStage16, clearStage17, clearStage18;
	private int clearStage19, clearStage20, clearStage21, clearStage22, clearStage23, clearStage24;
	private int clearStage25, clearStage26, clearStage27, clearStage28, clearStage29, clearStage30;
	private int clearStage31, clearStage32, clearStage33, clearStage34, clearStage35, clearStage36;
	private int clearStage37, clearStage38, clearStage39, clearStage40, clearStage41, clearStage42;
	private int clearStage43, clearStage44, clearStage45, clearStage46, clearStage47, clearStage48;
	private int clearStage49, clearStage50, clearStage51, clearStage52, clearStage53;
	void Awake () {

	}
	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName != "Survival Mode") {
			stageText = GameObject.Find ("Text").GetComponentsInChildren<SpriteRenderer> ();
			stageButton = GameObject.Find ("ButtonStage").GetComponentsInChildren<SpriteRenderer> ();
			starEmpty = GameObject.Find ("StarEmpty").GetComponentsInChildren<SpriteRenderer> ();
			clearStage1 = PlayerPrefs.GetInt ("ClearStage1");
			clearStage2 = PlayerPrefs.GetInt ("ClearStage2");
			clearStage3 = PlayerPrefs.GetInt ("ClearStage3");
			clearStage4 = PlayerPrefs.GetInt ("ClearStage4");
			clearStage5 = PlayerPrefs.GetInt ("ClearStage5");
			clearStage6 = PlayerPrefs.GetInt ("ClearStage6");
			
			clearStage7 = PlayerPrefs.GetInt ("ClearStage7");
			clearStage8 = PlayerPrefs.GetInt ("ClearStage8");
			clearStage9 = PlayerPrefs.GetInt ("ClearStage9");
			clearStage10 = PlayerPrefs.GetInt ("ClearStage10");
			clearStage11 = PlayerPrefs.GetInt ("ClearStage11");
			clearStage12 = PlayerPrefs.GetInt ("ClearStage12");
			
			clearStage13 = PlayerPrefs.GetInt ("ClearStage13");
			clearStage14 = PlayerPrefs.GetInt ("ClearStage14");
			clearStage15 = PlayerPrefs.GetInt ("ClearStage15");
			clearStage16 = PlayerPrefs.GetInt ("ClearStage16");
			clearStage17 = PlayerPrefs.GetInt ("ClearStage17");
			
			clearStage18 = PlayerPrefs.GetInt ("ClearStage18");
			clearStage19 = PlayerPrefs.GetInt ("ClearStage19");
			clearStage20 = PlayerPrefs.GetInt ("ClearStage20");
			clearStage21 = PlayerPrefs.GetInt ("ClearStage21");
			clearStage22 = PlayerPrefs.GetInt ("ClearStage22");
			clearStage23 = PlayerPrefs.GetInt ("ClearStage23");
			
			clearStage24 = PlayerPrefs.GetInt ("ClearStage24");
			clearStage25 = PlayerPrefs.GetInt ("ClearStage25");
			clearStage26 = PlayerPrefs.GetInt ("ClearStage26");
			clearStage27 = PlayerPrefs.GetInt ("ClearStage27");
			clearStage28 = PlayerPrefs.GetInt ("ClearStage28");
			clearStage29 = PlayerPrefs.GetInt ("ClearStage29");
			
			clearStage30 = PlayerPrefs.GetInt ("ClearStage30");
			clearStage31 = PlayerPrefs.GetInt ("ClearStage31");
			clearStage32 = PlayerPrefs.GetInt ("ClearStage32");
			clearStage33 = PlayerPrefs.GetInt ("ClearStage33");
			clearStage34 = PlayerPrefs.GetInt ("ClearStage34");
			clearStage35 = PlayerPrefs.GetInt ("ClearStage35");
			
			clearStage36 = PlayerPrefs.GetInt ("ClearStage36");
			clearStage37 = PlayerPrefs.GetInt ("ClearStage37");
			clearStage38 = PlayerPrefs.GetInt ("ClearStage38");
			clearStage39 = PlayerPrefs.GetInt ("ClearStage39");
			clearStage40 = PlayerPrefs.GetInt ("ClearStage40");
			clearStage41 = PlayerPrefs.GetInt ("ClearStage41");
			
			clearStage42 = PlayerPrefs.GetInt ("ClearStage42");
			clearStage43 = PlayerPrefs.GetInt ("ClearStage43");
			clearStage44 = PlayerPrefs.GetInt ("ClearStage44");
			clearStage45 = PlayerPrefs.GetInt ("ClearStage45");
			clearStage46 = PlayerPrefs.GetInt ("ClearStage46");
			clearStage47 = PlayerPrefs.GetInt ("ClearStage47");
			
			clearStage48 = PlayerPrefs.GetInt ("ClearStage48");
			clearStage49 = PlayerPrefs.GetInt ("ClearStage49");
			clearStage50 = PlayerPrefs.GetInt ("ClearStage50");
			clearStage51 = PlayerPrefs.GetInt ("ClearStage51");
			clearStage52 = PlayerPrefs.GetInt ("ClearStage52");
			clearStage53 = PlayerPrefs.GetInt ("ClearStage53");	
		}

		if (Application.loadedLevelName == "Stage Selection") {
			if (clearStage1 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage2 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage3 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage4 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage5 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}	
		}
		else if (Application.loadedLevelName == "Stage Selection2") {
			if (clearStage6 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage7 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage8 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage9 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage10 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage11 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection3") {
			if (clearStage12 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage13 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage14 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage15 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage16 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage17 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection4") {
			if (clearStage18 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage19 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage20 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage21 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage22 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage23 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection5") {
			if (clearStage24 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage25 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage26 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage27 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage28 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage29 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection6") {
			if (clearStage30 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage31 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage32 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage33 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage34 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage35 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection7") {
			if (clearStage36 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage37 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage38 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage39 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage40 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage41 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection8") {
			if (clearStage42 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage43 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage44 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage45 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage46 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage47 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
		else if (Application.loadedLevelName == "Stage Selection9") {
			if (clearStage48 == 1) {
				stageButton[0].sprite = stageTexture;
				stageButton[6].enabled = false;	
				stageText[0].enabled = true;
				for (int i = 0; i<3; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage49 == 1) {
				stageButton[1].sprite = stageTexture;
				stageButton[7].enabled = false;	
				stageText[1].enabled = true;
				for (int i = 0; i<6; i++) {
					starEmpty[i].enabled = true;
				}
			}
			if (clearStage50 == 1) {
				stageButton[2].sprite = stageTexture;
				stageButton[8].enabled = false;	
				stageText[2].enabled = true;
				for (int i = 0; i<9; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage51 == 1) {
				stageButton[3].sprite = stageTexture;
				stageButton[9].enabled = false;	
				stageText[3].enabled = true;
				for (int i = 0; i<12; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage52 == 1) {
				stageButton[4].sprite = stageTexture;
				stageButton[10].enabled = false;	
				stageText[4].enabled = true;
				for (int i = 0; i<15; i++) {
					starEmpty[i].enabled = true;
				}	
			}
			if (clearStage53 == 1) {
				stageButton[5].sprite = bossTexture;
				stageButton[11].enabled = false;	
				stageText[5].enabled = true;
				for (int i = 0; i<18; i++) {
					starEmpty[i].enabled = true;
				}	
			}
		}
	}
}
