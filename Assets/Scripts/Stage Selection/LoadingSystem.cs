﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
//using ChartboostSDK;
public class LoadingSystem : MonoBehaviour {
	public GameObject[] arrayLoadingIcon;
	public GameObject[] arrayLoadingTips;
	private int randomLoading;
	// Use this for initialization
	void Start () {
		//Chartboost.showInterstitial (CBLocation.Default);
		randomLoading = Random.Range (0, arrayLoadingIcon.Length);
		arrayLoadingIcon[randomLoading].SetActive(true);
		arrayLoadingTips[randomLoading].SetActive(true);

		if (StageClick.clickStage == "Tutorial") {
			StartCoroutine (loadStage("Tutorial"));	
		}
		else if (StageClick.clickStage == "SurvivalMode") {
			StartCoroutine (loadStage("Survival"));
		}
		else if (StageClick.clickStage == "Day1-1") {
			StartCoroutine (loadStage("Stage1-1"));	
		}
		else if (StageClick.clickStage == "Day1-2") {
			StartCoroutine (loadStage("Stage1-2"));
		}
		else if (StageClick.clickStage == "Day1-3") {
			StartCoroutine (loadStage("Stage1-3"));
		}
		else if (StageClick.clickStage == "Day2-1") {
			StartCoroutine (loadStage("Stage1-4"));
		}
		else if (StageClick.clickStage == "Day2-2") {
			StartCoroutine (loadStage("Stage1-5"));
		}
		else if (StageClick.clickStage == "Day2-3") {
			StartCoroutine (loadStage("Stage1-6"));
		}
		else if (StageClick.clickStage == "Day3-1") {
			StartCoroutine (loadStage("Stage2-1"));	
		}
		else if (StageClick.clickStage == "Day3-2") {
			StartCoroutine (loadStage("Stage2-2"));
		}
		else if (StageClick.clickStage == "Day3-3") {
			StartCoroutine (loadStage("Stage2-3"));
		}
		else if (StageClick.clickStage == "Day4-1") {
			StartCoroutine (loadStage("Stage2-4"));
		}
		else if (StageClick.clickStage == "Day4-2") {
			StartCoroutine (loadStage("Stage2-5"));
		}
		else if (StageClick.clickStage == "Day4-3") {
			StartCoroutine (loadStage("Stage2-6"));
		}

		else if (StageClick.clickStage == "Day5-1") {
			StartCoroutine (loadStage("Stage3-1"));	
		}
		else if (StageClick.clickStage == "Day5-2") {
			StartCoroutine (loadStage("Stage3-2"));
		}
		else if (StageClick.clickStage == "Day5-3") {
			StartCoroutine (loadStage("Stage3-3"));
		}
		else if (StageClick.clickStage == "Day6-1") {
			StartCoroutine (loadStage("Stage3-4"));
		}
		else if (StageClick.clickStage == "Day6-2") {
			StartCoroutine (loadStage("Stage3-5"));
		}
		else if (StageClick.clickStage == "Day6-3") {
			StartCoroutine (loadStage("Stage3-6"));
		}

		else if (StageClick.clickStage == "Day7-1") {
			StartCoroutine (loadStage("Stage4-1"));	
		}
		else if (StageClick.clickStage == "Day7-2") {
			StartCoroutine (loadStage("Stage4-2"));
		}
		else if (StageClick.clickStage == "Day7-3") {
			StartCoroutine (loadStage("Stage4-3"));
		}
		else if (StageClick.clickStage == "Day8-1") {
			StartCoroutine (loadStage("Stage4-4"));
		}
		else if (StageClick.clickStage == "Day8-2") {
			StartCoroutine (loadStage("Stage4-5"));
		}
		else if (StageClick.clickStage == "Day8-3") {
			StartCoroutine (loadStage("Stage4-6"));
		}
		
		else if (StageClick.clickStage == "Day9-1") {
			StartCoroutine (loadStage("Stage5-1"));	
		}
		else if (StageClick.clickStage == "Day9-2") {
			StartCoroutine (loadStage("Stage5-2"));
		}
		else if (StageClick.clickStage == "Day9-3") {
			StartCoroutine (loadStage("Stage5-3"));
		}
		else if (StageClick.clickStage == "Day10-1") {
			StartCoroutine (loadStage("Stage5-4"));
		}
		else if (StageClick.clickStage == "Day10-2") {
			StartCoroutine (loadStage("Stage5-5"));
		}
		else if (StageClick.clickStage == "Day10-3") {
			StartCoroutine (loadStage("Stage5-6"));
		}
		
		else if (StageClick.clickStage == "Day11-1") {
			StartCoroutine (loadStage("Stage6-1"));	
		}
		else if (StageClick.clickStage == "Day11-2") {
			StartCoroutine (loadStage("Stage6-2"));
		}
		else if (StageClick.clickStage == "Day11-3") {
			StartCoroutine (loadStage("Stage6-3"));
		}
		else if (StageClick.clickStage == "Day12-1") {
			StartCoroutine (loadStage("Stage6-4"));
		}
		else if (StageClick.clickStage == "Day12-2") {
			StartCoroutine (loadStage("Stage6-5"));
		}
		else if (StageClick.clickStage == "Day12-3") {
			StartCoroutine (loadStage("Stage6-6"));
		}

		else if (StageClick.clickStage == "Day13-1") {
			StartCoroutine (loadStage("Stage7-1"));	
		}
		else if (StageClick.clickStage == "Day13-2") {
			StartCoroutine (loadStage("Stage7-2"));
		}
		else if (StageClick.clickStage == "Day13-3") {
			StartCoroutine (loadStage("Stage7-3"));
		}
		else if (StageClick.clickStage == "Day14-1") {
			StartCoroutine (loadStage("Stage7-4"));
		}
		else if (StageClick.clickStage == "Day14-2") {
			StartCoroutine (loadStage("Stage7-5"));
		}
		else if (StageClick.clickStage == "Day14-3") {
			StartCoroutine (loadStage("Stage7-6"));
		}

		else if (StageClick.clickStage == "Day15-1") {
			StartCoroutine (loadStage("Stage8-1"));	
		}
		else if (StageClick.clickStage == "Day15-2") {
			StartCoroutine (loadStage("Stage8-2"));
		}
		else if (StageClick.clickStage == "Day15-3") {
			StartCoroutine (loadStage("Stage8-3"));
		}
		else if (StageClick.clickStage == "Day16-1") {
			StartCoroutine (loadStage("Stage8-4"));
		}
		else if (StageClick.clickStage == "Day16-2") {
			StartCoroutine (loadStage("Stage8-5"));
		}
		else if (StageClick.clickStage == "Day16-3") {
			StartCoroutine (loadStage("Stage8-6"));
		}

		else if (StageClick.clickStage == "Day17-1") {
			StartCoroutine (loadStage("Stage9-1"));	
		}
		else if (StageClick.clickStage == "Day17-2") {
			StartCoroutine (loadStage("Stage9-2"));
		}
		else if (StageClick.clickStage == "Day17-3") {
			StartCoroutine (loadStage("Stage9-3"));
		}
		else if (StageClick.clickStage == "Day18-1") {
			StartCoroutine (loadStage("Stage9-4"));
		}
		else if (StageClick.clickStage == "Day18-2") {
			StartCoroutine (loadStage("Stage9-5"));
		}
		else if (StageClick.clickStage == "Day18-3") {
			StartCoroutine (loadStage("Stage9-6"));
		}
	}
	IEnumerator loadStage(string scene) {
        Debug.Log("Starting");
		yield return new WaitForSeconds (2);
        Debug.Log(scene.ToString());
        AsyncOperation async = SceneManager.LoadSceneAsync(scene);
        yield return async;
	}

    /*
	void didFailToLoadInterstitial(CBLocation location, CBImpressionError error) {
		
	}
	void didDismissInterstitial(CBLocation location) {
		
	}
	void didCloseInterstitial(CBLocation location) {

	}
	void didClickInterstitial(CBLocation location) {
		Debug.Log("didClickInterstitial: " + location);
	}
	void didCacheInterstitial(CBLocation location) {
		Debug.Log("didCacheInterstitial: " + location);
	}
	bool shouldDisplayInterstitial(CBLocation location) {
		return true;
	}
	void didDisplayInterstitial(CBLocation location){
		Debug.Log("didDisplayInterstitial: " + location);
	}
	void OnEnable() {
		// Listen to all impression-related events
		Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial += didDismissInterstitial;
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
	}
	void OnDisable() {
		// Remove event handlers
		Chartboost.didFailToLoadInterstitial -= didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial -= didDismissInterstitial;
		Chartboost.didCloseInterstitial -= didCloseInterstitial;
		Chartboost.didClickInterstitial -= didClickInterstitial;
		Chartboost.didCacheInterstitial -= didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial -= shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
	}
    */
}
