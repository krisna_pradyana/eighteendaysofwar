﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WarningClick : MonoBehaviour {
	public GameObject energyPopUpUI;
	public GameObject clearStagePopUpUI;
	public GameObject comingSoonPopUpUI;
	public GameObject welcomePopUpUI;
	private GameObject square, square2, square3;
	private GameObject[] heroSelectorArray;
	private GameObject[] itemSelectorArray;
	public static bool stillShowingWarnings = false;
	public AudioClip soundClick;
	void Start () {
		heroSelectorArray = GameObject.FindGameObjectsWithTag ("HeroSelector");
		itemSelectorArray = GameObject.FindGameObjectsWithTag ("ItemSelector");
		square = GameObject.Find ("Main Camera/Square");
		//square2 = GameObject.Find ("Square2");
		//square3 = GameObject.Find ("Canvas/Square");
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (gameObject.name == "Confirm Container") {
			WarningClick.stillShowingWarnings = false;
            GameFlow.hasPaused = false;
            GameFlow.hasResumed = true;

            if (Application.loadedLevelName == "Stage Selection" || Application.loadedLevelName == "Stage Selection2" || Application.loadedLevelName == "Stage Selection3" ||
			    Application.loadedLevelName == "Stage Selection4" || Application.loadedLevelName == "Stage Selection5" || Application.loadedLevelName == "Stage Selection6" ||
			    Application.loadedLevelName == "Stage Selection7" || Application.loadedLevelName == "Stage Selection8" || Application.loadedLevelName == "Stage Selection9") {
				energyPopUpUI.SetActive(false);
				clearStagePopUpUI.SetActive(false);
				comingSoonPopUpUI.SetActive(false);
				square2.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (gameObject.transform.parent.name == "Rewards Warning UI") {
				//UnityAdsLoading.hasFinishWatchingAds = false;
				energyPopUpUI.SetActive(false);
				if (Application.loadedLevelName == "Purchase Gem") {
					square3.GetComponent<Image>().enabled = false;
				}
				else {
					square2.GetComponent<SpriteRenderer>().enabled = false;
				}

			}
			else if (gameObject.transform.parent.name == "Disconnect Warning") {
				StartCoroutine (playSound(energyPopUpUI));
				Application.LoadLevel("Shop");
				square2.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (gameObject.transform.parent.name == "No Ads Warning") {
				StartCoroutine (playSound(energyPopUpUI));
				Application.LoadLevel("Purchase Gold");
				//square2.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (gameObject.transform.parent.name == "No Ads Warning") {
				StartCoroutine (playSound(energyPopUpUI));
				Application.LoadLevel("Purchase Energy");
				//square2.GetComponent<SpriteRenderer>().enabled = false;
			}
			else if (gameObject.transform.parent.name == "No Ads Warning" && Application.loadedLevelName == "Purchase Gem") {
				energyPopUpUI.SetActive(false);
				square3.GetComponent<Image>().enabled = false;
			}
			else {
				if (gameObject.transform.parent.name == "HeroMustSelect") {
					//StartCoroutine (playSound(energyPopUpUI));
					energyPopUpUI.SetActive (false);
					foreach (GameObject heroSelector in heroSelectorArray) {
						heroSelector.GetComponent<BoxCollider2D>().enabled = true;
					}
					foreach (GameObject itemSelector in itemSelectorArray) {
						itemSelector.GetComponent<BoxCollider2D>().enabled = true;
					}
				}
				else {
				    //square.GetComponent<SpriteRenderer>().enabled = false;
					StartCoroutine (playSound(energyPopUpUI));
					Time.timeScale = 1;
					GameFlow.hasPaused = false;
				}
			}
		}
	}
	IEnumerator playSound(GameObject ui) { 
		GetComponent<AudioSource>().Play();		
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		ui.SetActive (false);
	}
}
