﻿using UnityEngine;
using System.Collections;
using System;
public class CastleEnemy : MonoBehaviour {
	UnitAtt att;
	public static float health;
	public bool isHurt75 = true;
	public bool isHurt50 = false;
	public bool isHurt25 = false;
	public bool isHurt0 = false;
	public static bool hasPlayerWin = false;
	public static bool enemyCastleAttacked = false;
	private int star = 0;
	public GameObject star1, star2, star3, WinUI, gold, timerText;
	public string starStage;
	public string clearStage;
	public GameObject Firework;
	public static float maxHealth;
	public AudioClip winBGM;
	private float bgmVolume;
	public Sprite sprite100, sprite75, sprite50, sprite25, sprite0;
	public int coinFor1Star, coinFor2Star, coinFor3Star;
	private int coin;
	private GameObject square, healthBarEnemy;
	public int timeFor2Star, timeFor3Star;
	public float timer;
	private System.TimeSpan timerFormatted;
	private int victoryCount, totalStars;
	//private GoldSystem goldSystem;
	// Use this for initialization
	void Start () {
        WinUI = GameObject.Find("Win UI");
        WinUI.SetActive(false);

		coin = 0;
		bgmVolume = 1;
		att=GameObject.Find(gameObject.name).GetComponent<UnitAtt>();
		health = att.HP;
		maxHealth = att.HP;
		square = GameObject.Find ("Main Camera/Square");
        //InvokeRepeating ("IncreaseTimer", 3, 1);
        healthBarEnemy = GameObject.FindGameObjectWithTag("HPEnemy");
		victoryCount = PlayerPrefs.GetInt ("Victory");
		//goldSystem = GameObject.FindGameObjectWithTag ("Gold").GetComponent<GoldSystem>();
	}
	
	// Update is called once per frame
	void Update () {

        if(GameFlow.hasPaused == false)
        {
            timer = timer + 1 * Time.deltaTime;
        }

		Vector3 scale = healthBarEnemy.transform.localScale;
		scale.x = (health * 3) / maxHealth;
		if (health <= 0) {
			scale.x = 0;	
		}
		healthBarEnemy.transform.localScale = scale;
		if (PauseClick.enableBGM == false) {
			if (hasPlayerWin == true && bgmVolume > 0) {
				bgmVolume -= 0.01f;
				Camera.main.GetComponent<AudioSource>().volume = bgmVolume;		
			}	
		}
		timerFormatted = System.TimeSpan.FromSeconds(timer);
	}
	public void colldetect(float damage){
		if (health > 0) {
			health -= damage;
			if(health < (att.HP * 0.75) && isHurt75) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite75;
				isHurt75 = false;
				isHurt50 = true;
			}
			else if (health < (att.HP * 0.5) && isHurt50) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite50;
				isHurt50 = false;
				isHurt25 = true;
			}
			else if (health < (att.HP * 0.25) && isHurt25) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite25;
				isHurt25 = false;
			}
			if(health <= 0){	
				//playAnimPlayer();
				if (Application.loadedLevelName != "Survival") {
					isHurt0 = true;
					gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
					Instantiate(Firework, new Vector3((Camera.main.transform.position.x),-5.4f, 0), Quaternion.Euler(270, 0, 0));
					if (Application.loadedLevelName == "Tutorial") {
						TutorialScenario.tutorialHasPlayerWin = true;
					}
					else {
						hasPlayerWin = true;
						GameObject.Find ("Square2").GetComponent<SpriteRenderer>().enabled = true;
						WinUI.SetActive(true);
						GameFlow.hasPaused = true;
						victoryCount += 1;
						PlayerPrefs.SetInt ("Victory", victoryCount);
						//square.GetComponent<SpriteRenderer>().enabled = true;
						countStar();
						
					}
					if (PauseClick.enableBGM == false) {
						StartCoroutine("victorySound");
					}
				}
				else {
					hasPlayerWin = true;
					GameFlow.hasPaused = true;
					GameFlow.unitNumbers = 0;
					Generate.money = 0;
					isHurt75 = true;
					isHurt50 = false;
					isHurt25 = false;
					isHurt0 = false;
					gameObject.GetComponent<SpriteRenderer>().sprite = sprite100;
				}
			}
		} 
	}

	void countStar() {
        gold = GameObject.Find("Reward/Coin Text");
        timerText = GameObject.Find("Time/Time Text");
        star1 = GameObject.Find("Main Camera/Win UI/Star/Star 1");
        star2 = GameObject.Find("Main Camera/Win UI/Star/Star 2");
        star3 = GameObject.Find("Main Camera/Win UI/Star/Star 3");

        if (timer <= timeFor3Star) {
			star = 3;
			coin = coinFor3Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
			star2.GetComponent<SpriteRenderer>().enabled = true;
			star3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerPrefs.SetInt(starStage, star);
        }
		else if (timer <= timeFor2Star) {
			star = 2;
			coin = coinFor2Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
			star3.GetComponent<SpriteRenderer>().enabled = true;
            PlayerPrefs.SetInt(starStage, star);
        }
		else {
			star = 1;
			coin = coinFor1Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
            PlayerPrefs.SetInt(starStage, star);
        }
		//GameObject.Find ("Gold Container").SendMessage ("IncreaseGold", coin);

		gold.GetComponent<EasyFontTextMesh>().Text = coin.ToString();
		timerText.GetComponent<EasyFontTextMesh>().Text = string.Format("{0:D2}:{1:D2}", timerFormatted.Minutes, timerFormatted.Seconds);
        //PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") + coin);
        GoldContainer.addGold(coin);
		PlayerPrefs.SetInt (clearStage, 1);
	}
	IEnumerator victorySound() {
		yield return new WaitForSeconds (2);
		AudioSource.PlayClipAtPoint(winBGM, Camera.main.transform.position);
		Destroy (gameObject);
		//Time.timeScale = 0f;
	}
}
