﻿using UnityEngine;
using System.Collections;

public class Indicator : MonoBehaviour {
	private float speed = 0.1f;
	private float xStart = 0.0f;
	private bool curTouched;
	public GameObject mainCamera;
	public static bool moveLeft = false;
	public static bool moveRight = false;
	public static int rightScreen;
	public static int leftScreen;
	private Vector2 swipeSpeed;
	public static float totalSpeed;
	private float cameraIndicatorSpeed;
	void Start () {
		Scaling ();
	}
	
	// Update is called once per frame
	void Update () {
		if (CameraControl.touchIndicator == true) {
			SwipeIndicator ();	
		}
	}
	void SwipeIndicator()
	{
		if (Input.touchCount == 1)
		{
			// Get the touch info
			Touch t = Input.GetTouch(0);
			// Did the touch action just begin?
			if (t.phase == TouchPhase.Began)
			{
				xStart = t.position.x;
			}
			if (t.phase == TouchPhase.Stationary)
			{
				//moveRight = false;
				//moveLeft = false;
				
			}
			if (t.phase == TouchPhase.Moved && xStart < t.position.x)
			{
				if (transform.position.x < rightScreen+11) {
					swipeSpeed = t.deltaPosition;
					totalSpeed = swipeSpeed.x * speed;
					//moveRight = true;
					//moveLeft = false;
					transform.Translate (Vector2.right * Mathf.Abs(totalSpeed));
					mainCamera.transform.Translate (Vector2.right *Mathf.Abs(totalSpeed)*cameraIndicatorSpeed);
				}
				else {
					transform.Translate (Vector2.right * 0);
				}
			}
			if (t.phase == TouchPhase.Moved && xStart > t.position.x)
			{
				if (transform.position.x > leftScreen-9) {
					swipeSpeed = t.deltaPosition;
					totalSpeed = swipeSpeed.x * speed;
					//moveRight = false;
					//moveLeft = true;
					transform.Translate (-Vector2.right * Mathf.Abs(totalSpeed));
					mainCamera.transform.Translate (-Vector2.right *Mathf.Abs(totalSpeed)*cameraIndicatorSpeed);
				}
				else {
					transform.Translate (Vector2.right * 0);
				}
				
			}
			// Did the touch end?
			if (t.phase == TouchPhase.Ended)
			{
				moveRight = false;
				moveLeft = false;
				xStart = 0.0f;
			}
		}
	}
	void Scaling() {
		if (Camera.main.aspect >= 1.7) {
			//16 : 9
			Camera.main.transform.position = new Vector3 (7, -5.4f, -10);
			leftScreen = 7;
			rightScreen = 148;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/24f;
		}
		else if (Camera.main.aspect >= 1.5) {
			//16 : 10
			Camera.main.transform.position = new Vector3 (4, -5.4f, -10);
			leftScreen = 4;
			rightScreen = 150;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/24f;
		}
		else if (Camera.main.aspect >= 1.3){
			//4 : 3
			leftScreen = 1;
			rightScreen = 156;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/24f;
		}
	}
}
