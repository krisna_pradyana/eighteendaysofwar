﻿using UnityEngine;
using System.Collections;

public class PopupClick : MonoBehaviour {
	private GameObject square;
	private GameObject[] gameSpeedArray;
	private GameObject[] arrayGameController;
	public GameObject PopupUI, energyPopupUI, energyWarningUI, pauseUI;
	//private RegenEnergy regenEnergy;
	private CameraControl cam;
	public AudioClip buttonSound;
	private int hasFinishTutorial;
	// Use this for initialization
	void Start () {

		square = GameObject.Find ("Main Camera/Square");
		arrayGameController = GameObject.FindGameObjectsWithTag ("GameController");
        /*
		if (GameObject.Find("EnergyText") != null)
		{
			regenEnergy = GameObject.Find ("EnergyText").GetComponent<RegenEnergy> ();
		}
        */
		cam = Camera.main.GetComponent<CameraControl>();
		hasFinishTutorial = PlayerPrefs.GetInt ("FinishTutorial");

	}
	
	// Update is called once per frame
	void Update () {
		if (pauseUI != null) {
			pauseUI.SetActive (false);	
		}
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
		if (gameObject.name == "Cancel Container") {
			if (Application.loadedLevelName == "Menu") {
				if (hasFinishTutorial == 0 && MenuClick.tutorial1stTime == true) {
					square.GetComponent<SpriteRenderer>().enabled = false;
					Application.LoadLevel ("MainMenu");
					PlayerPrefs.SetInt ("FinishTutorial", 1);
					//StartCoroutine (playSound(PopupUI));
					PopupUI.SetActive(false);
					GameFlow.hasPaused = false;
				}
				else {
					square.GetComponent<SpriteRenderer>().enabled = false;
					//StartCoroutine (playSound(PopupUI));
					PopupUI.SetActive(false);
					GameFlow.hasPaused = false;
				}

			}
			else if (gameObject.transform.parent.name == "Ads Warning") {
				StartCoroutine (playSound(PopupUI));
				Application.LoadLevel ("Shop");
			}
			else {
				//square.GetComponent<SpriteRenderer>().enabled = false;
				if (GameObject.Find ("ultiArjuna(Clone)") != null) {
					Time.timeScale = 0.5f;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
					Time.timeScale = 1;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
					Time.timeScale = 2;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
					Time.timeScale = 3;
				}
				GameFlow.hasResumed = true;
				GameFlow.hasPaused = false;
				foreach (GameObject gameController in arrayGameController) {
					gameController.GetComponent<BoxCollider2D>().enabled = true;	
				}	
				foreach (GameObject gameSpeed in gameSpeedArray) {
					gameSpeed.GetComponent<BoxCollider2D>().enabled = true;
				}
				StartCoroutine (playSound(PopupUI));
			}

		}
		else if (gameObject.name == "Yes Container") {
			if (Application.loadedLevelName == "Menu") {
				if (gameObject.transform.parent.gameObject.name == "Tutorial Popup UI") {
					Application.LoadLevel ("Loading");
					StageClick.clickStage ="Tutorial";
				}
				else {
					Application.Quit();
				}
			}
			else if (gameObject.transform.parent.name == "Ads Warning") {
				//UnityAdsLoading.adsReward = "Shop";
				StartCoroutine (playSound(PopupUI));
				Application.LoadLevel ("AdMobLoading");
			}
            
			else {
				if (energyPopupUI.activeInHierarchy == true) {
						//square.GetComponent<SpriteRenderer>().enabled = false;
						Application.LoadLevel(Application.loadedLevel);
						GameFlow.hasPaused = false;
						GameFlow.hasResumed = false;
						Unitcooldown.hasdied = "";
						Unitcooldown.hasSummonHero = false;
						Unitcooldown.herosummoned = "";
						Generate.money = 0;
						Generate.increaseFromMiner = 0;
						IncreaseSilver.counterLimit = 0;
						Checkunit.hero1select = "";
						Checkunit.hero2select = "";
						Checkunit.hero3select = "";
						InitUnit.readytoinit = false;
						MoveE.boss1Die = false;
						MoveE.boss2Die = false;
						MoveE.hasBossDie = false;
						Time.timeScale = 1;
                        Skillclick.Useskill = false;
				}
				else {
					//square.GetComponent<SpriteRenderer>().enabled = false;
					Application.LoadLevel("MainMenu");
					GameFlow.hasPaused = false;
					GameFlow.hasResumed = false;
					Unitcooldown.hasdied = "";
					Unitcooldown.hasSummonHero = false;
					Unitcooldown.herosummoned = "";
					Generate.money = 0;
					Generate.increaseFromMiner = 0;
					IncreaseSilver.counterLimit = 0;
					Checkunit.hero1select = "";
					Checkunit.hero2select = "";
					Checkunit.hero3select = "";
					InitUnit.readytoinit = false;
					MoveE.boss1Die = false;
					MoveE.boss2Die = false;
					MoveE.hasBossDie = false;
					Time.timeScale = 1;
				}
			}
            
		}
	}
	IEnumerator playSound(GameObject UI) { 
		GetComponent<AudioSource>().Play();		
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		UI.SetActive (false);
	}
}
