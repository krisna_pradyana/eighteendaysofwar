﻿using UnityEngine;
using System.Collections;

public class CloudAnimation : MonoBehaviour {
	public float speedCloud;
	public bool isMist = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (-Vector2.right * speedCloud * Time.deltaTime);
		if (isMist == true) {
			if (gameObject.transform.position.x <= -80) {
				gameObject.transform.position = new Vector3 (200, 0, 0);	
			}	
		}
		else {
			if (gameObject.transform.position.x <= -40) {
				gameObject.transform.position = new Vector3 (222.82f, 0, 0);	
			}
		}

	}
}
