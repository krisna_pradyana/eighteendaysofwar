﻿using UnityEngine;
using System.Collections;

public class SingleTargetBattleE : MonoBehaviour {
	private UnitAtt att, attfr;
	private float delay = 0, breath, rng, attackanim, cooldown;
	private int knocfriend=1;
	private MoveE mov;
	private Move movE;
	private GameObject target;
	private Animator animEnemy, animPlayer;
	public GameObject explode;
	public bool boss=false;
	// Use this for initialization
	void Start () {
		att = gameObject.GetComponent<UnitAtt>();
		rng = att.RNG;
		mov = gameObject.GetComponent<MoveE>();
		animEnemy = gameObject.GetComponent<Animator> ();
		target = null;
	}
	
	// Update is called once per frame
	void Update () {
		battleon ();
		if (transform.position.x >= 163){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
		}
	}

	void FindClosestEnemy() {
		if(target != null && target.tag == "Player"){
			target = null;
		}
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3.1f){
					target = go;
				}
				else{
					target = null;
				}
				distance = diff;
			}
		}
	}

	void FindPlayerCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("PlayerCastle");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*7){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}

	void FindClosestFriend() {
		knocfriend = 1;
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			attfr = go.GetComponent<UnitAtt>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(attfr.KNOCKBACK !=0 && go != gameObject){
					knocfriend += 1;
				}
				distance = diff;
			}
		}
	}
	
	void battleon(){
		if(boss == true && GetComponent<Rigidbody2D>().velocity != Vector2.right*0 || mov.bulletE.GetComponent<Rigidbody2D>().velocity != Vector2.right*0 &&  boss == true){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
		}
		if (target != null) {
			mov.speed = 0;
			if(target.tag == "PlayerCastle"){
				FindClosestEnemy();
			}
			if(Time.time > delay){
				animEnemy.SetBool("Kill", false);
				animEnemy.SetBool("Attack",true);
				if(attackanim == 0){
					cooldown = 0.9f;
					attackanim=Time.time+cooldown;
				}
				delay = Time.time + att.ATKSPD;
				breath = Time.time + 1f;
				if(att.SPLASH == 0){
					StartCoroutine ("delayBeforeAttack");
					if(att.KNOCKBACK != 0){
						if(target.tag=="Player"){
							animPlayer= target.GetComponent<Animator>();
							animPlayer.SetBool("Knockback",true);
							SingleTargetBattleP seboss = target.GetComponent<SingleTargetBattleP>();
							RangecheckP reboss = target.GetComponent<RangecheckP>();
							bool checkboss=false;
							if(seboss != null){
								checkboss = seboss.boss;
							}
							else if(reboss != null){
								checkboss = reboss.boss;
							}
							if(checkboss == false){
								target.GetComponent<Rigidbody2D>().velocity= -Vector2.right*(3/knocfriend);
								movE = target.GetComponent<Move>();
								movE.bulletP.GetComponent<Rigidbody2D>().velocity= (-Vector2.right*(3/knocfriend))/movE.scalar;
							}
							target = null;
						}
					}
					FindClosestEnemy();
				}
				else{
					FindClosestEnemy();
				}
			}
			else if(Time.time > attackanim){
				attackanim = 0;
				animEnemy.SetBool("Attack", false);
			}
		}
		else{
			animEnemy.SetBool("Attack", false);
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			animEnemy.SetBool("Kill",true);
			animEnemy.SetBool("Knockback",false);
			if(att.KNOCKBACK == 0){
				FindClosestEnemy();
				FindPlayerCastle();
			}
			if(Time.time > breath){
				FindClosestEnemy();
				FindPlayerCastle();
				breath += 0.1f;
				if(mov.speed == 0){
					mov.speed = att.SPD;
				}
			}
		}
	}
	IEnumerator delayBeforeAttack() {
		yield return new WaitForSeconds ((int)1f);
		if (gameObject.tag == "PlayerCastle") {
			int randomX = Random.Range (0, 5);
			int randomY = Random.Range (-4, 3);
			if (explode != null) {
				Instantiate(explode,target.transform.position+new Vector3(randomX,randomY,-10),target.transform.rotation);
			}
			CastleEnemy.enemyCastleAttacked = true;
		}
		else {
			if(target != null){
				if (explode != null) {
					Instantiate(explode,target.transform.position+new Vector3(0,2,-10),target.transform.rotation);
				}
			}
		}
		if (target != null) {
			int dmg = Mathf.RoundToInt(Random.Range(att.ATK,att.ATK*11/10));
			target.gameObject.SendMessage ("colldetect", dmg);	
		}

	}
}
