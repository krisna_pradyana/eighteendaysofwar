﻿using UnityEngine;
using System.Collections;

public class SpawnUnit : MonoBehaviour {
	public GameObject unit, unitcd;
	private SpriteRenderer unitColor;
	public AudioClip soundSpawn, soundCannotSummon;
	private GameObject spawner;
	private bool canSummon;
	void Start() {
		unitColor = gameObject.GetComponent<SpriteRenderer> ();
		spawner = GameObject.Find("SpawnPointP");

	}
	void Update() {
		if (Application.loadedLevelName == "Tutorial") {
			if (TutorialScenario.tutorialCanSummonUnit == true && Generate.money >= unit.GetComponent<UnitAtt>().COST && unitcd.activeSelf == false) {
				canSummon = true;	
				unitColor.color = Color.white;
			}
			else {
				canSummon = false;
				unitColor.color = Color.gray;
			}
		}
		else {
			if (Generate.money >= unit.GetComponent<UnitAtt>().COST && GameFlow.hasPaused == false && unitcd.activeSelf == false && GameFlow.unitNumbers < GameFlow.maxUnitNumbers) {
				canSummon = true;
				unitColor.color = Color.white;
			}
			else if(Cooldown.inCooldown == true) {
				canSummon = false;
				unitColor.color = Color.gray;
			}
			else {
				canSummon = false;
				unitColor.color = Color.gray;
			}
		}


	}
	void OnMouseDown() {
		if (canSummon == true) {
			GameFlow.unitNumbers += 1;
			Generate.money = Generate.money - unit.GetComponent<UnitAtt>().COST;
			Instantiate(unit, spawner.transform.position, spawner.transform.rotation);
			AudioSource.PlayClipAtPoint (soundSpawn, transform.position);
			unitcd.SetActive(true);
			if (Application.loadedLevelName == "Tutorial") {
				if (TutorialScenario.tutorialHasSummonUnit == false) {
					TutorialScenario.tutorialHasSummonUnit = true;
				}
				else if (TutorialScenario.tutorialHasSummonUnit == true && TutorialScenario.tutorialOnlyShowOnce == true && TutorialScenario.tutorialSummon1stTime == false) {
					TutorialScenario.tutorialMoveCameraOnce = true;
					TutorialScenario.tutorialSummon1stTime = true;
				}
			}
		}
		else {
			AudioSource.PlayClipAtPoint (soundCannotSummon, transform.position);
		}
	}
}
