﻿using UnityEngine;
using System.Collections;

public class pierceshoot : MonoBehaviour {
	float atta, init;
	private MoveE move;
	UnitAtt att;
	private int i=0;
	GameObject[] target= new GameObject[12];
	public GameObject hero, explode;
	private GameObject[] gameSpeedArray;
	// Use this for initialization
	void Start () {
		init = gameObject.transform.position.x;
		att = hero.GetComponent<UnitAtt>();
		atta = att.ATK*2.4f;
		gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
	}

	void Update(){
		transform.Translate (Vector2.right * 50 * Time.deltaTime);
		FindClosestEnemy ();
		if (gameObject.transform.position.x >= init + 35f || target[11] != null) {
			if(target[11] != null){
				int dmg = Mathf.RoundToInt(Random.Range(atta,atta*11/10));
				Instantiate (explode, target[11].transform.position+new Vector3(0, 2, -20), target[11].transform.rotation);
				move = target[11].GetComponent<MoveE>();
				target[11].GetComponent<Animator>().SetTrigger("Hurt");
				move.speed = 0;
				target[11].gameObject.SendMessage ("colldetect", dmg);
			}
			if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
				Time.timeScale = 1;
			}
			else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
				Time.timeScale = 2;
			}
			else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
				Time.timeScale = 3;
			}
			Skillclick.Useskill = false;
			Destroy(gameObject);
		}
	}

	void FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		Vector3 pos = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - pos.x);
			if(diff <= 1.5f){
				if(i-1 >= 0){
					bool notyet = false;
					for(int a=0; a < target.Length-1; a++){
						if(target[a] == go){
							notyet = true;
							break;
						}
					}
					if(i <= 11 && !notyet){
						atta -= Mathf.RoundToInt(atta*8/100);
						target[i] = go;
					}
				}
				else{
					if(target[i] == null && go != target[i]){
						target[i] = go;
					}
				}
				damagecalcu();
			}
		}
	}

	void damagecalcu(){
		if(target[i] != null){
			int dmg = Mathf.RoundToInt(Random.Range(atta,atta*11/10));
			Instantiate (explode, target[i].transform.position+new Vector3(0, 2, -20), target[i].transform.rotation);
			move = target[i].GetComponent<MoveE>();
			target[i].GetComponent<Animator>().SetTrigger("Hurt");
			move.speed = 0;
			target[i].gameObject.SendMessage ("colldetect", dmg);
			i+=1;
		}
	}
}
