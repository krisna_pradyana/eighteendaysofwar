﻿using UnityEngine;
using System.Collections;

public class MiniTower : MonoBehaviour {
	UnitAtt att;
	public float health;
	public bool isHurt60 = true;
	public bool isHurt30 = false;
	public bool isHurt0 = false;
	public float maxHealth;
	private GameObject healthBarEnemy;
	public Sprite sprite60, sprite30;
	public GameObject KArcher;
	private CastleEnemy castleEnemy;
	private Animator KArcherAnim;
	public static bool isTowerAlive;
	private GameObject kArcher;
	//private GoldSystem goldSystem;
	// Use this for initialization
	void Start () {
		isTowerAlive = true;
		att=GameObject.Find(gameObject.name).GetComponent<UnitAtt>();
		castleEnemy = GameObject.Find ("EnemyCastle").GetComponent<CastleEnemy>();
		health = att.HP;
		maxHealth = att.HP;
		healthBarEnemy = transform.Find ("healthUnit").gameObject;
		kArcher = Instantiate (KArcher, new Vector3 (66.85f, -5.4f, 0), transform.rotation) as GameObject;
		kArcher.name = "KArcherMiniTower";
		kArcher.tag = "Untagged";
		kArcher.GetComponent<BoxCollider2D> ().enabled = false;
		//KArcher.GetComponent<MoveE> ().bullet = null;
		//goldSystem = GameObject.FindGameObjectWithTag ("Gold").GetComponent<GoldSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 scale = healthBarEnemy.transform.localScale;
		scale.x = (health * 6) / maxHealth;
		if (health <= 0) {
			scale.x = 0;	
		}
		healthBarEnemy.transform.localScale = scale;
	}
	public void colldetect(float damage){
		if (health > 0) {
			health -= damage;
			if(health < (att.HP * 0.6) && isHurt60) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite60;
				isHurt60 = false;
				isHurt30 = true;
			}
			else if (health < (att.HP * 0.3) && isHurt30) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite30;
				isHurt30 = false;
				isHurt0 = true;
			}
			if(health <= 0){		
				//playAnimPlayer();
				isHurt0 = false;
				Destroy(gameObject);
				Destroy(kArcher);
				isTowerAlive = false;
			}
		} 
	}
}
