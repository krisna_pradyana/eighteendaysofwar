﻿using UnityEngine;
using System.Collections;

public class Stockitem : MonoBehaviour {
	private StartGame sg;
	private EasyFontTextMesh eftm;
	// Use this for initialization
	void Start () {
		eftm = GetComponent<EasyFontTextMesh> ();
		sg = transform.root.Find("ItemHeroSelection").GetComponent<StartGame>();
	}

	void Update () {
		if (transform.parent.gameObject == sg.mvspd10 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("MovementSpeed10")) {
			eftm.Text = PlayerPrefs.GetInt("MovementSpeed10").ToString();
		}
		else if(transform.parent.gameObject == sg.mvspd30 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("MovementSpeed30")){
			eftm.Text = PlayerPrefs.GetInt("MovementSpeed30").ToString();
		}
		if (transform.parent.gameObject == sg.atk10 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("Attack10")) {
			eftm.Text = PlayerPrefs.GetInt("Attack10").ToString();
		}
		else if(transform.parent.gameObject == sg.atk30 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("Attack30")){
			eftm.Text = PlayerPrefs.GetInt("Attack30").ToString();
		}
		if (transform.parent.gameObject == sg.hp10 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("HP10")) {
			eftm.Text = PlayerPrefs.GetInt("HP10").ToString();
		}
		else if(transform.parent.gameObject == sg.hp30 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("HP30")){
			eftm.Text = PlayerPrefs.GetInt("HP30").ToString();
		}
		if (transform.parent.gameObject == sg.res10 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("Resource10")) {
			eftm.Text = PlayerPrefs.GetInt("Resource10").ToString();
		}
		else if(transform.parent.gameObject == sg.res30 && int.Parse(eftm.Text) != PlayerPrefs.GetInt("Resource30")){
			eftm.Text = PlayerPrefs.GetInt("Resource30").ToString();
		}
	}
}
