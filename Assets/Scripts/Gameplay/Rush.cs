﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rush : MonoBehaviour {
	private Animator animPlayer;
	private SingleTargetBattleP att;
	public GameObject effect, blast;
	private Vector3 initialloc=new Vector3(0,0,0);
	private GameObject target, black;
	private List<GameObject> swarm;
	private float dmg=0, speed1 = 25, speed2 = 28;
	private Unitcooldown spawn;
	public float range;
	private bool stopskill=false;
	private BoxCollider2D bc;
	private Skillclick sk;
	private Move mov;
	private GameObject[] gameSpeedArray;
	// Use this for initialization
	void Start () {
		swarm = new List<GameObject>();
		spawn = GameObject.Find ("CooldownContainer").GetComponent<Unitcooldown> ();
		animPlayer = gameObject.GetComponent<Animator> ();
		att = GetComponent<SingleTargetBattleP> ();
		bc = GetComponent<BoxCollider2D> ();
		sk = GetComponent<Skillclick> ();
		black = GameObject.Find(sk.blackout.name);
		mov = GetComponent<Move>();
	}
	
	// Update is called once per frame
	void Update () {
		if (dmg != att.atak*2.5f) {
			dmg = att.atak*2.5f;		
		}
		if (Skillclick.gatotskill == true && stopskill == false && Skillclick.Useskill == true && spawn.cdGatotKaca.activeSelf == true && black.GetComponent<SpriteRenderer>().enabled == false) {
			bc.isTrigger = true;
			if(initialloc == new Vector3(0,0,0)){
				initialloc = transform.position;
				range = initialloc.x + range;
				tag = "Untagged";
				att.enabled = false;
				Instantiate(blast, transform.position + new Vector3(0,3f,0),transform.rotation);
			}
			if(transform.position.x > range){
				animPlayer.SetBool("Skill", false);
				animPlayer.SetBool("Backflip", true);
				stopskill = true;
			}
			mov.bulletP.transform.Translate(Vector2.right*speed1/mov.scalar*Time.fixedDeltaTime);
			transform.Translate(Vector2.right*speed1*Time.fixedDeltaTime);
			FindClosestEnemy();
		}
		if (stopskill == true) {
			if(transform.position.x >= initialloc.x){
				mov.bulletP.transform.Translate(-Vector2.right*speed2/mov.scalar *Time.fixedDeltaTime);
				transform.Translate(-Vector2.right*speed2*Time.fixedDeltaTime);
			}
			else{
				gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
				animPlayer.SetBool("Backflip", false);
				initialloc = new Vector3(0,0,0);
				stopskill = false;
				tag = "Player";
				att.enabled = true;
				bc.isTrigger = false;
				mov.speed = mov.sped;
				if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
					Time.timeScale = 1;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
					Time.timeScale = 2;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
					Time.timeScale = 3;
				}
				Skillclick.Useskill = false;
				Skillclick.gatotskill = false;
			}
		}
	}

	void FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		Vector3 pos = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - pos.x);
			if(diff <= 1.5f){
				damagecalcu(go);
			}
		}
	}

	void damagecalcu(GameObject victim){
		bool notyet = false;
		for(int a=0; a < swarm.Count; a++){
			if(swarm[a] == victim){
				notyet = true;
				break;
			}
		}
		if (!notyet) {
			swarm.Add(victim);
			MoveE move;
			Instantiate (effect, victim.transform.position+new Vector3(0, 2, -20), victim.transform.rotation);
			move = victim.GetComponent<MoveE>();
			victim.GetComponent<Animator>().SetTrigger("Hurt");
			move.speed = 0;
			victim.gameObject.SendMessage ("colldetect", dmg);
		}	
	}
}
