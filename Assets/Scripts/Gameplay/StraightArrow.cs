﻿using UnityEngine;
using System.Collections;

public class StraightArrow : MonoBehaviour {
	private float atta,rng,dmg;
	public GameObject unit, effect;
	private UnitAtt att;
	void Start(){
		att = unit.GetComponent<UnitAtt> ();
		atta = att.ATK;
		rng = att.SPLASH;
	}

	// Update is called once per frame
	void Update () {
		if (gameObject.tag == "ProjectileP") {
			transform.Translate (Vector2.right * 25 * Time.deltaTime);		
		}
		else if(gameObject.tag == "ProjectileE"){
			transform.Translate (-Vector2.right * 25 * Time.deltaTime);
		}
	}
	void OnTriggerEnter2D(Collider2D coll){
		dmg = Mathf.RoundToInt(Random.Range(atta,atta*11/10));
		if (coll.gameObject.tag == "Enemy" && gameObject.tag == "ProjectileP" && coll.gameObject.tag == "EnemyCastle" && gameObject.name != "bulletpg") {
			if(rng > 0){
				AreaDamageEnemies();
				Instantiate(effect,coll.transform.position+new Vector3(2f,2f,-9),transform.rotation);
			}
			else{
				coll.GetComponent<MoveE>().colldetect(dmg);
			}
			Destroy(transform.gameObject);
		}
		else if(coll.gameObject.tag == "Player" && gameObject.tag == "ProjectileE" && coll.gameObject.tag == "PlayerCastle" && gameObject.name != "bulleteg") {
			if(rng > 0){
				AreaDamageEnemies();
				Instantiate(effect,coll.transform.position+new Vector3(-2f,2f,-9),transform.rotation);
			}
			else{
				coll.GetComponent<Move>().colldetect(dmg);
			}
			Destroy(transform.gameObject);
		}
		else if(coll.gameObject.tag == "Player" && gameObject.tag == "ProjectileE" && gameObject.name == "bulleteg") {
			Instantiate(effect,coll.transform.position+new Vector3(0,2f,-9),transform.rotation);
			coll.GetComponent<Move>().colldetect(dmg);
			Destroy(transform.parent.gameObject);
		}
		else if(coll.gameObject.tag == "Enemy" && gameObject.tag == "ProjectileP" && gameObject.name == "bulletpg") {
			Instantiate(effect,coll.transform.position+new Vector3(0,2f,-9),transform.rotation);
			coll.GetComponent<MoveE>().colldetect(dmg);
			Destroy(transform.parent.gameObject);
		}
		if (coll.gameObject.tag == "EnemyCastle" && gameObject.tag == "ProjectileP") {
			int randomX = Random.Range (-5, 0);
			int randomY = Random.Range (-4, 3);
			Instantiate(effect,coll.transform.position+new Vector3(randomX,randomY,-10),transform.rotation);
			if (coll.gameObject.name == "EnemyCastle") {
				coll.GetComponent<CastleEnemy>().colldetect(dmg);
			}
			else {
				coll.GetComponent<MiniTower>().colldetect(dmg);
			}
			Destroy(transform.root.gameObject);
		}
		else if (coll.gameObject.tag == "PlayerCastle" && gameObject.tag == "ProjectileE") {
			int randomX = Random.Range (0, 5);
			int randomY = Random.Range (-4, 3);
			Instantiate(effect,coll.transform.position+new Vector3(randomX,randomY,-10),transform.rotation);
			coll.GetComponent<CastlePlayer>().colldetect(dmg);
			Destroy(transform.root.gameObject);
		}
	}

	void OnTriggerStay2D(Collider2D coll){
		dmg = Mathf.RoundToInt(Random.Range(atta,atta*11/10));
		if (coll.gameObject.tag == "Enemy" && gameObject.tag == "ProjectileP" && gameObject.name != "bulletpg") {
			if(rng > 0){
				AreaDamageEnemies();
				Instantiate(effect,transform.position+new Vector3(2f,2f,-9),transform.rotation);
			}
			else{
				coll.GetComponent<MoveE>().colldetect(dmg);
			}
			Destroy(transform.gameObject);
		}
		else if(coll.gameObject.tag == "Player" && gameObject.tag == "ProjectileE" && gameObject.name != "bulleteg") {
			if(rng > 0){
				AreaDamageEnemies();
				Instantiate(effect,transform.position+new Vector3(-2f,2f,-9),transform.rotation);
			}
			else{
				coll.GetComponent<Move>().colldetect(dmg);
			}
			Destroy(transform.gameObject);
		}
	}

	void AreaDamageEnemies()
	{
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(gameObject.transform.position, rng);
		foreach (Collider2D col in objectsInRange)
		{
			if(gameObject.tag=="ProjectileP"){
				MoveE enemy = col.GetComponent<MoveE>();
				if (enemy != null){
					enemy.colldetect(dmg);
				}
			}
			else if(gameObject.tag=="ProjectileE"){
				Move player = col.GetComponent<Move>();
				if(player != null){
					player.colldetect(dmg);
				}
			}
		}
	}
}
