﻿using UnityEngine;
using System.Collections;

public class PauseClick : MonoBehaviour {
	private GameObject[] arrayGameController;
	public GameObject pauseUI, popupUI, creditsUI;
	public static bool enableSFX;
	public static bool enableBGM;
	private GameObject[] gameSpeedArray;
	private GameObject square;
	public GameObject energyPopupUI;
	public AudioClip buttonSound;

    private void Awake()
    {

    }

    // Use this for initialization
    void Start () {
        enableBGM = SettingsScript.enabledBGM;
        enableSFX = SettingsScript.enabledSFX;

        arrayGameController = GameObject.FindGameObjectsWithTag ("GameController");
		square = GameObject.Find ("Main Camera/Square");
        if (enableBGM == false) {
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            pauseUI.transform.Find("BGM Container/Disable").GetComponent<SpriteRenderer>().enabled = true;
            SettingsScript.bgmIcon(true);
		}
        else
        {
            pauseUI.transform.Find("BGM Container/Disable").GetComponent<SpriteRenderer>().enabled = false;
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            SettingsScript.bgmIcon(false);
        }

        if (enableSFX == false)
        {
            pauseUI.transform.Find("Sfx Container/Disable").GetComponent<SpriteRenderer>().enabled = true;
            SettingsScript.sfxIcon(true);
        }
        else
        {
            pauseUI.transform.Find("Sfx Container/Disable").GetComponent<SpriteRenderer>().enabled = false;
            SettingsScript.sfxIcon(false);
        }
    }

    // Update is called once per frame
    void Update () {

	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
		if (gameObject.name == "Resume Container") {
			if (GameObject.Find ("ultiArjuna(Clone)") != null) {
				Time.timeScale = 0.25f;
			}
			else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
				Time.timeScale = 1;
			}
			else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
				Time.timeScale = 2;
			}
			else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
				Time.timeScale = 3;
			}
			foreach (GameObject gameController in arrayGameController) {
				gameController.GetComponent<BoxCollider2D>().enabled = true;	
			}	
			foreach (GameObject gameSpeed in gameSpeedArray) {
				gameSpeed.GetComponent<BoxCollider2D>().enabled = true;
			}
			StartCoroutine (playSound(pauseUI));
			//square.GetComponent<SpriteRenderer>().enabled = false;
			GameFlow.hasResumed = true;
			GameFlow.hasPaused = false;
		}
		else if (gameObject.name == "Retry Container" || gameObject.name == "Restart Container") {
			StartCoroutine (playSound(pauseUI));
			energyPopupUI.SetActive (true);
			Checkunit.hero1select = "";
			Checkunit.hero2select = "";
			Checkunit.hero3select = "";
			InitUnit.readytoinit = false;
		}
		else if (gameObject.name == "Quit Container") {
			StartCoroutine (playSound(pauseUI));
			popupUI.SetActive (true);
		}
		else if (gameObject.name == "Sfx Container") {
			if (enableSFX == false) {
				enableSFX = true;
                SettingsScript.delegateEnableSFX();
				transform.Find("Disable").GetComponent<SpriteRenderer>().enabled = false;
                SettingsScript.sfxIcon(false);
			}
			else {
				enableSFX = false;
                SettingsScript.delegateEnableSFX();
                transform.Find("Disable").GetComponent<SpriteRenderer>().enabled = true;
                SettingsScript.sfxIcon(true);
            }
		}
		else if (gameObject.name == "BGM Container") {
			if (enableBGM == false) {
				enableBGM = true;
                SettingsScript.delegateEnableBGM();
                transform.Find("Disable").GetComponent<SpriteRenderer>().enabled = false;
				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
                SettingsScript.bgmIcon(false);
			}
			else {
				enableBGM = false;
                SettingsScript.delegateEnableBGM();
                transform.Find("Disable").GetComponent<SpriteRenderer>().enabled = true;
				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
                SettingsScript.bgmIcon(true);
            }
		}
		else if (gameObject.name == "Close") {
			StartCoroutine (playSound(pauseUI));
			square.GetComponent<SpriteRenderer>().enabled = false;
            //GameFlow.hasPaused = false;
            GameObject.Find("Main Camera/Square").GetComponent<SpriteRenderer>().enabled = false;
        }
		else if (gameObject.name == "Credits Container") {
			StartCoroutine (playSound(pauseUI));
			creditsUI.SetActive(true);
            //GameFlow.hasPaused = true;
		}
	}
	IEnumerator playSound(GameObject pause) { 
		GetComponent<AudioSource>().Play();		
		yield return new WaitForSeconds(GetComponent<AudioSource>().clip.length);
		pause.SetActive (false);
	}
}
