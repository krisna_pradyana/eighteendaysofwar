﻿using UnityEngine;

public class Generate : MonoBehaviour {
	public static float money;
	public float increase;
	public static float increaseFromMiner;
	private bool initiateItem;
	private float increaseSilverCoin;
	// Use this for initialization
	void Start () {
		initiateItem = false;
		money = 100;
		increaseSilverCoin = PlayerPrefs.GetFloat ("SilverCoin");
		InvokeRepeating("AddToMoney", 3, 3);	
	}

	//money adder for GUIText
	void AddToMoney () {
		if (GameFlow.hasPaused != true) {
			money = money + increase + increaseFromMiner;
			money = Mathf.RoundToInt (money);	
		}
	}
	void Update () {
		if (gameObject.name == "CoinText") {
			gameObject.GetComponent<EasyFontTextMesh>().Text = money.ToString();	
		}
		else if (gameObject.name == "UnitText") {
			gameObject.GetComponent<EasyFontTextMesh>().Text = GameFlow.unitNumbers.ToString()+"/"+GameFlow.maxUnitNumbers.ToString();
		}
		if (initiateItem == false && StartGame.selectionMode == false) {
			increaseSilverCoin = PlayerPrefs.GetFloat ("SilverCoin");
			increase = increase + (increaseFromMiner * increaseSilverCoin);	
			initiateItem = true;
		}
	}
}