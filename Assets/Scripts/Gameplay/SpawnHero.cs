﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SpawnHero : MonoBehaviour {
	public GameObject unit, unitcd, unitskill;
	private SpriteRenderer unitColor;
	public AudioClip soundSpawn, soundCannotSummon;
	private GameObject spawner, temp;
	private bool canSummon;
	private CameraControl cam;

    void Start() {
		GetComponent<AudioSource> ().spatialBlend = 0;
		unitColor = gameObject.GetComponent<SpriteRenderer> ();
		spawner = GameObject.Find("SpawnPointP");
		cam = Camera.main.GetComponent<CameraControl> ();
        if (SceneManager.GetActiveScene().name != "Tutorial")
        {
            temp = GameObject.FindGameObjectWithTag("GameController");
        }
        /*
		if (name == "harjuna") {
			DebugArjun ("Arjuna");
		} else if (name == "hbima") {
			DebugArjun ("Bima");
		} else if (name == "hsrikandi") {
			DebugArjun ("Srikandi");
		}
        */
	}
	
	void Update() {
		if (Application.loadedLevelName == "Tutorial") {
			if(GameFlow.hasPaused == false && unitcd.activeSelf == false && Unitcooldown.hasSummonHero == false && TutorialScenario.tutorialCanSummonHero == true){
				canSummon = true;
				unitColor.color = Color.white;
			}
			else {
				canSummon = false;
				unitColor.color = Color.gray;
			}	
		}
		else {
			if (cam.bosslevel == false && Generate.money >= unit.GetComponent<UnitAtt>().COST && GameFlow.hasPaused == false && unitcd.activeSelf == false && Unitcooldown.hasSummonHero == false) {
				canSummon = true;
				unitColor.color = Color.white;
			}
			else if(cam.bosslevel == true && GameFlow.hasPaused == false && unitcd.activeSelf == false && Unitcooldown.hasSummonHero == false){
				canSummon = true;
				unitColor.color = Color.white;
			}
			else {
				canSummon = false;
				unitColor.color = Color.gray;
			}
		}

	}
	void OnMouseDown() {
		if (canSummon == true && cam.bosslevel == false) {
			Vector3 zposition = new Vector3 (0, 0, 0);	
			Generate.money = Generate.money - unit.GetComponent<UnitAtt>().COST;
			Instantiate(unit, spawner.transform.position+zposition, spawner.transform.rotation);
            if (SceneManager.GetActiveScene().name != "Tutorial")
            {
                temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
            }
			Unitcooldown.hasSummonHero = true;
			if(gameObject.name == "harjuna"){
				Unitcooldown.herosummoned = "arjuna";
			}
			else if(gameObject.name == "hbima"){
				Unitcooldown.herosummoned = "bima";
			}
			else if(gameObject.name == "hsrikandi"){
				Unitcooldown.herosummoned = "srikandi";
			}
			else if(gameObject.name == "hyudhistira"){
				Unitcooldown.herosummoned = "yudistira";
			}
			else if(gameObject.name == "hnakulasadewa"){
				Unitcooldown.herosummoned = "nakusade";
			}
			else if(gameObject.name == "hgatotkaca"){
				Unitcooldown.herosummoned = "gatotkaca";
			}
			else if(gameObject.name == "hdrupada"){
				Unitcooldown.herosummoned = "drupada";
			}
			else if(gameObject.name == "hkresna"){
				Unitcooldown.herosummoned = "kresna";
			}
			else if(gameObject.name == "hanoman"){
				Unitcooldown.herosummoned = "anoman";
			}
			unitskill.SetActive(true);
			gameObject.SetActive(false);
			if (Application.loadedLevelName == "Tutorial") {
				TutorialScenario.tutorialHasSummonHero = true;
			}
		}
		else if(canSummon == true && cam.bosslevel == true){
			Vector3 zposition = new Vector3 (0, 0, 0);
			Vector3 yposition = new Vector3 (0, 0, -7);
			Vector3 xposition = new Vector3 (0, 0, -6);
			if(gameObject.name == "harjuna"){
				Unitcooldown.arjunasummoned = true;
				Instantiate(unit, spawner.transform.position+yposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hbima"){
				Unitcooldown.bimasummoned = true;
				Instantiate(unit, spawner.transform.position+zposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hsrikandi"){
				Unitcooldown.srikandisummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hyudhistira"){
				Unitcooldown.yudissummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hnakulasadewa"){
				Unitcooldown.nakusadesummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hgatotkaca"){
				Unitcooldown.gatotkacasummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hdrupada"){
				Unitcooldown.drupsummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hkresna"){
				Unitcooldown.kressummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			else if(gameObject.name == "hanoman"){
				Unitcooldown.anomsummoned = true;
				Instantiate(unit, spawner.transform.position+xposition, spawner.transform.rotation);
                if (SceneManager.GetActiveScene().name != "Tutorial")
                {
                    temp.GetComponent<AudioSource>().PlayOneShot(soundSpawn);
                }
			}
			unitskill.SetActive(true);
			gameObject.SetActive(false);
		}
		else if(canSummon == false){
			GetComponent<AudioSource>().PlayOneShot (soundCannotSummon);
		}
	}

	void DebugArjun(string name){
		if (unitcd == null) {
			unitcd = transform.parent.parent.Find ("Cooldown/" + name + "cd").gameObject;
		}
		if (unitskill == null) {
			if (name == "Arjuna") {
				unitskill = transform.parent.parent.Find ("Skill/Pierce").gameObject;
			}
			else if (name == "Bima") {
				unitskill = transform.parent.parent.Find ("Skill/Cleave").gameObject;
			}
			else if (name == "Srikandi") {
				unitskill = transform.parent.parent.Find ("Skill/Aura").gameObject;
			}
		}
		unitcd.SetActive (false);
		unitskill.SetActive (false);
	}
}
