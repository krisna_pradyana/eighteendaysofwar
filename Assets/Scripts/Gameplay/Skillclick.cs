﻿using UnityEngine;
using System.Collections;

public class Skillclick : MonoBehaviour {
	Unitcooldown spawn;
	UnitAtt att;
	private string heroname;
	public static bool activatedskill=false, Useskill = false, gatotskill;
	private bool skillready = true;
	public GameObject projectile;
	private GameObject cdskill, guiskill;
	public float duration=0;
	public float range;
	private float delay=0,timeout;
	private Animator animPlayer;
	private AuraAtkSpd auraatkspd;
	private AuraAtk auraatk;
	private AuraHeal auraheal;
	public AudioClip soundSkill, soundSkillInactive;
	private CameraControl cam;
	public GameObject animskill1,animskill2,animskill3,animskill4,animskill5,animskill6,animskill7,animskill8,animskill9;
	public GameObject blackout;
	private GameObject animskill;
	private Vector3 initialpos= Vector3.zero;
	private Move mov;
	private GameObject[] gameSpeedArray = new GameObject[3];
	// Use this for initialization
	void Start () {
		spawn = GameObject.Find ("CooldownContainer").GetComponent<Unitcooldown> ();
		animPlayer = gameObject.GetComponent<Animator> ();
		auraatkspd = gameObject.GetComponentInChildren<AuraAtkSpd> ();
		auraatk = gameObject.GetComponentInChildren<AuraAtk>();
		auraheal = gameObject.GetComponentInChildren<AuraHeal>();
		att = gameObject.GetComponent<UnitAtt> ();
		mov = GetComponent<Move>();
		cam = Camera.main.GetComponent<CameraControl> ();
	}

	void FixedUpdate(){
		if (cam.bosslevel == false) {
			if (Input.GetMouseButtonDown(0)) {
				CastRay();
			}
		}
		else if(cam.bosslevel == true){
			if (Input.GetMouseButtonDown(0)) {
				CastRay();
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (cam.bosslevel == false) {
			if (skillready == false) {
				duration = att.skillduration;
				skillready = true;
			}
			if (Unitcooldown.hasSummonHero == true) {
				if(Unitcooldown.herosummoned == "arjuna"){
					guiskill = spawn.skill1;
					cdskill = spawn.cdArjuna;
					heroname = "arjuna";
					startanimantion();
					if(Time.time >= duration && duration != 0 && animPlayer.GetBool("Skill") == true){
						animPlayer.SetBool("Skill",false);
						mov.sped = mov.initspeed;
						mov.speed = mov.sped;
					}
				}
				else if(Unitcooldown.herosummoned == "bima"){
					guiskill = spawn.skill2;
					cdskill = spawn.cdBima;
					heroname = "bima";
					startanimantion();
					if(duration == 0 && skillready == true){
						att.SPLASH = 0;
					}
				}
				else if(Unitcooldown.herosummoned == "srikandi"){
					guiskill = spawn.skill3;
					cdskill = spawn.cdSrikandi;
					heroname ="srikandi";
					startanimantion();
				}
				else if(Unitcooldown.herosummoned == "yudistira"){
					guiskill = spawn.skill4;
					cdskill = spawn.cdYudistira;
					heroname ="yudistira";
					startanimantion();
				}
				else if(Unitcooldown.herosummoned == "nakusade"){
					guiskill = spawn.skill5;
					cdskill = spawn.cdNakuSade;
					heroname ="nakusade";
					startanimantion();
				}
				else if(Unitcooldown.herosummoned == "gatotkaca"){
					guiskill = spawn.skill6;
					cdskill = spawn.cdGatotKaca;
					heroname ="gatotkaca";
					startanimantion();
					if(tag == "Player" && gatotskill == false && mov.sped != mov.speed){
						mov.speed = mov.sped;
					}
				}
				else if(Unitcooldown.herosummoned == "drupada"){
					guiskill = spawn.skill7;
					cdskill = spawn.cdDrupada;
					heroname ="drupada";
					startanimantion();
				}
				else if(Unitcooldown.herosummoned == "kresna"){
					guiskill = spawn.skill8;
					cdskill = spawn.cdKresna;
					heroname ="kresna";
					startanimantion();
				}
				else if(Unitcooldown.herosummoned == "anoman"){
					guiskill = spawn.skill9;
					cdskill = spawn.cdAnoman;
					heroname ="anoman";
					startanimantion();
				}
			}
		}
		else if(cam.bosslevel == true){
			if (skillready == false) {
				duration = att.skillduration;
				skillready = true;
			}
			if(gameObject.name == "arjuna_Root(Clone)"){
				guiskill = spawn.skill1;
				cdskill = spawn.cdArjuna;
				heroname = "arjuna";
				startanimantion();
				if(Time.time >= duration && duration != 0 && animPlayer.GetBool("Skill") == true){
					animPlayer.SetBool("Skill",false);
					mov.sped = mov.initspeed;
					mov.speed = mov.sped;
				}
			}
			else if(gameObject.name == "BimaRoot(Clone)"){
				guiskill = spawn.skill2;
				cdskill = spawn.cdBima;
				heroname = "bima";
				startanimantion();
				if(duration == 0 && skillready == true){
					att.SPLASH = 0;
				}
			}
			else if(gameObject.name == "srikandiRoot(Clone)"){
				guiskill = spawn.skill3;
				cdskill = spawn.cdSrikandi;
				heroname = "srikandi";
				startanimantion();
			}
			else if(gameObject.name == "Yudhistira_root(Clone)"){
				guiskill = spawn.skill4;
				cdskill = spawn.cdYudistira;
				heroname = "yudistira";
				startanimantion();
			}
			else if(gameObject.name == "nakula_sadewa_root(Clone)"){
				guiskill = spawn.skill5;
				cdskill = spawn.cdNakuSade;
				heroname = "nakusade";
				startanimantion();
			}
			else if(gameObject.name == "gatotkaca_root(Clone)"){
				guiskill = spawn.skill6;
				cdskill = spawn.cdGatotKaca;
				heroname = "gatotkaca";
				startanimantion();
				if(tag == "Player" && gatotskill == false && mov.sped != mov.speed){
					mov.speed = mov.sped;
				}
			}
			else if(gameObject.name == "drupa_root(Clone)"){
				guiskill = spawn.skill7;
				cdskill = spawn.cdDrupada;
				heroname = "drupada";
				startanimantion();
			}
			else if(gameObject.name == "kresna_root(Clone)"){
				guiskill = spawn.skill8;
				cdskill = spawn.cdKresna;
				heroname = "kresna";
				startanimantion();
			}
			else if(gameObject.name == "anomanRoot(Clone)"){
				guiskill = spawn.skill9;
				cdskill = spawn.cdAnoman;
				heroname = "anoman";
				startanimantion();
			}
		}
	}

	void CastRay() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit) {
			if(hit.collider.gameObject == guiskill && cdskill.activeSelf==false){
				if(heroname == "arjuna" && Useskill == false){
					activatedskill = true;
					duration = Time.time + 1;
					if(animskill == null){
						animskill = GameObject.Find(animskill1.name);
					}
					Move mov = GetComponent<Move>();
					mov.sped = 0;
					mov.speed = 0;
					animPlayer.SetBool("Skill",true);
					animPlayer.SetBool("Attack", false);
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					Useskill = true;
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
					StartCoroutine("delaySkill");
				}
				else if(heroname == "bima" && Useskill == false){
					skillready = false;
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill2.name);
					}
					att.SPLASH = 5;
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "srikandi" && Useskill == false){
					auraatkspd.rng = range;
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill3.name);
					}
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "yudistira" && Useskill == false){
					auraatk.rng = range;
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill4.name);
					}
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "nakusade" && Useskill == false){
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill5.name);
					}
					Move mov = GetComponent<Move>();
					mov.sped = 0;
					mov.speed = 0;
					animPlayer.SetBool("Skill",true);
					animPlayer.SetBool("Attack", false);
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					Useskill = true;
					GameObject project = (GameObject) Instantiate(projectile,gameObject.transform.position,gameObject.transform.rotation);
					project.name = projectile.name;
					project.transform.parent = transform;
					project.transform.position = project.transform.position + new Vector3(1,0,0);	
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "drupada" && Useskill == false){
					auraheal.rng = range;
					activatedskill = true;
					animPlayer.SetBool("Skill",true);
					if(animskill == null){
						animskill = GameObject.Find(animskill7.name);
					}
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "gatotkaca" && Useskill == false){
					activatedskill = true;
					gatotskill = true;
					duration = Time.time + 1;
					if(animskill == null){
						animskill = GameObject.Find(animskill6.name);
					}
					Move mov = GetComponent<Move>();
					mov.speed = 0;
					animPlayer.SetBool("Skill",true);
					animPlayer.SetBool("Attack", false);
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					Useskill = true;
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "kresna" && Useskill == false){
					skillready = false;
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill8.name);
					}
					att.KNOCKBACK = att.skillduration;
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else if(heroname == "anoman" && Useskill == false){
					activatedskill = true;
					if(animskill == null){
						animskill = GameObject.Find(animskill9.name);
					}
					Move mov = GetComponent<Move>();
					mov.sped = 0;
					mov.speed = 0;
					animPlayer.SetBool("Skill",true);
					animPlayer.SetBool("Attack", false);
					cdskill.SetActive(true);
					AudioSource.PlayClipAtPoint (soundSkill, new Vector3(0,0,0));
					skillpause();
					Useskill = true;
					GameObject project = (GameObject) Instantiate(projectile,gameObject.transform.position,gameObject.transform.rotation);
					project.name = projectile.name;
					project.transform.parent = transform;
					project.transform.position = project.transform.position + new Vector3(1,0,0);	
					guiskill.GetComponent<SpriteRenderer>().color = new Color(0.63f,0.63f,0.63f,1);
				}
				else{
					AudioSource.PlayClipAtPoint (soundSkillInactive, new Vector3(0,0,0));
				}
			}
		}
	}

	void skillpause(){
		Time.timeScale = 0;
		GameFlow.hasPaused = true;
		GameFlow.hasResumed = false;
	}

	void animasi(){
		Vector3 centerpoint = Camera.main.GetComponent<Camera>().ScreenToWorldPoint (new Vector3 ((Screen.width / 4), Screen.height / 2, 10));
		if (initialpos == Vector3.zero) {
			initialpos = Camera.main.GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (-35, (Screen.height / 2)-3f, 10));
			animskill.transform.position = initialpos;
		}
		if (animskill.transform.position.x < centerpoint.x) {
			animskill.transform.Translate (Vector2.right*2);
		}
	}

	void startanimantion(){
		gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
		if(Time.timeScale == 0){
			if(activatedskill == true && animskill != null){
				animasi ();
				if(delay == 0){
					delay = 2f;
				}
				timeout+=0.03f;
				blackout = GameObject.Find(blackout.name);
				blackout.GetComponent<SpriteRenderer>().enabled = true;
				if(timeout >= delay){
					animskill.transform.position = initialpos-new Vector3(31.9f,0,0);
					GameFlow.hasPaused = false;
					GameFlow.hasResumed = true;
					timeout = 0;
					delay = 0;
					if(Unitcooldown.herosummoned == "arjuna" || gameObject.name == "arjuna_Root(Clone)"){
						Time.timeScale = 0.5f;
					}
					else if(Unitcooldown.herosummoned == "gatotkaca" || gameObject.name == "gatotkaca_root(Clone)"){
						Time.timeScale = 1f;
					}
					else if(Unitcooldown.herosummoned == "nakusade" || gameObject.name == "nakula_sadewa_root(Clone)"){
						Time.timeScale = 0.5f;
					}
					else if(Unitcooldown.herosummoned == "anoman" || gameObject.name == "anomanRoot(Clone)"){
						Time.timeScale = 0.5f;
					}
					else if((Unitcooldown.herosummoned != "gatotkaca" || gameObject.name != "gatotkaca_root(Clone)" || Unitcooldown.herosummoned != "nakusade" || gameObject.name != "nakula_sadewa_root(Clone)"|| Unitcooldown.herosummoned != "arjuna" || gameObject.name != "arjuna_Root(Clone)") && Useskill == false || Unitcooldown.herosummoned != "anoman" || gameObject.name != "anomanRoot(Clone)" && Useskill == false){
						if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
							Time.timeScale = 1;
						}
						else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
							Time.timeScale = 2;
						}
						else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
							Time.timeScale = 3;
						}
					}
					blackout.GetComponent<SpriteRenderer>().enabled = false;
					initialpos = Vector3.zero;
					activatedskill = false;
					animskill = null;
				}
			}
		}
	}

	IEnumerator delaySkill() {
		yield return new WaitForSeconds((int)1);
		Instantiate(projectile,gameObject.transform.position,gameObject.transform.rotation);
	}
}
