﻿using UnityEngine;
using System.Collections;

public class GameFlow : MonoBehaviour {

    #region Delegates
    public delegate void ActivateCount();
    #endregion

    #region Delegate Functions
    public static ActivateCount activateCount;
    #endregion

    public static bool hasPaused = true;
	public static bool hasResumed = false;
	private GameObject[] backgroundArray, arrayPlayer, arrayEnemy, arrayUnitIcon, arrayHeroIcon, gameSpeedArray, arrayAudioTrigger, arrayGameController;
	public GameObject pauseUI, ReadySetGo;
    public GameObject square;
	public static bool reinit;
	public static int unitNumbers;
	public static int maxUnitNumbers;

    private void OnEnable()
    {
        activateCount += ActivateCountdown;
    }

    private void OnDisable()
    {
        activateCount -= ActivateCountdown;
    }
    
    // Use this for initialization
    void Start () {

        ReadySetGo = GameObject.Find("Countdown");
        ReadySetGo.SetActive(false);

		unitNumbers = 0;
		maxUnitNumbers = 15;
		if (Application.loadedLevelName != "Tutorial") {
            return;
		}
		else {
			hasPaused = true;
			Time.timeScale = 0.1f;
		}
		backgroundArray = GameObject.FindGameObjectsWithTag ("Background");

		arrayUnitIcon = GameObject.FindGameObjectsWithTag ("UnitUI");
		arrayHeroIcon = GameObject.FindGameObjectsWithTag ("HeroUI");
        //square = GameObject.Find("Main Camera");

        /*
		if (PauseClick.enableBGM == false) {
			Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;	
		}
        */

        if (StartGame.selectionMode == true || hasPaused == true)
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            if (Skillclick.activatedskill == false && CastleEnemy.hasPlayerWin == false)
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }
            else
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        if (reinit == true)
        {
            arrayHeroIcon = null;
            arrayHeroIcon = GameObject.FindGameObjectsWithTag("HeroUI");
            reinit = false;
        }


    }

    private void LateUpdate()
    {
        arrayPlayer = GameObject.FindGameObjectsWithTag("Player");
        arrayEnemy = GameObject.FindGameObjectsWithTag("Enemy");
        arrayAudioTrigger = GameObject.FindGameObjectsWithTag("AudioTrigger");
        arrayGameController = GameObject.FindGameObjectsWithTag("GameController");

        if (SettingsScript.enabledSFX == false)
        {
            foreach (GameObject player in arrayPlayer)
            {
                player.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            foreach (GameObject enemy in arrayEnemy)
            {
                enemy.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            /*
            foreach (GameObject uniticon in arrayUnitIcon)
            {
                uniticon.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            foreach (GameObject heroicon in arrayHeroIcon)
            {
                heroicon.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            */
            foreach (GameObject audiotrigger in arrayAudioTrigger)
            {
                audiotrigger.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            foreach (GameObject gameController in arrayGameController)
            {
                gameController.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
        }
        else
        {
            foreach (GameObject player in arrayPlayer)
            {
                if (player != null)
                {
                    player.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
                }
            }
            foreach (GameObject enemy in arrayEnemy)
            {
                if (enemy != null)
                {
                    enemy.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
                }
            }
            /*
            foreach (GameObject uniticon in arrayUnitIcon)
            {
                uniticon.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            }
            foreach (GameObject heroicon in arrayHeroIcon)
            {
                heroicon.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            }
            */
            foreach (GameObject audiotrigger in arrayAudioTrigger)
            {
                audiotrigger.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            }
            foreach (GameObject gameController in arrayGameController)
            {
                gameController.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            }
        }
        

    }

	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (Skillclick.activatedskill == false || CastleEnemy.hasPlayerWin == false) {
			//square.GetComponent<SpriteRenderer>().enabled = true;
			gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
			foreach (GameObject gameSpeed in gameSpeedArray) {
				gameSpeed.GetComponent<BoxCollider2D>().enabled = false;
			}
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			hasPaused = true;
			hasResumed = false;
			pauseUI.SetActive (true);
			Time.timeScale = 0;	
		}
	}

    /*
	void OnApplicationFocus (bool focusStatus) {
		//square = GameObject.Find ("Main Camera/Square");
		if (focusStatus != true) {
			//square.GetComponent<SpriteRenderer> ().enabled = true;
			gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
			foreach (GameObject gameSpeed in gameSpeedArray) {
				gameSpeed.GetComponent<BoxCollider2D>().enabled = false;
			}
			gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			hasPaused = true;
			hasResumed = false;
			Time.timeScale = 0;	
		}
		else {
			if (StartGame.selectionMode == false) {
				//square.GetComponent<SpriteRenderer> ().enabled = false;
			}
			else {
				//square.GetComponent<SpriteRenderer> ().enabled = true;
			}
			if (StartGame.selectionMode == false) {
				gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
				if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
					Time.timeScale = 1;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
					Time.timeScale = 2;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
					Time.timeScale = 3;
				}
				//square.GetComponent<SpriteRenderer>().enabled = false;
				pauseUI.SetActive (false);
				hasResumed = true;
				hasPaused = false;
			}
		}
	}
    */

    public void ActivateCountdown()
    {
        ReadySetGo.SetActive(true);
        StartCoroutine(countDown());
    }

	IEnumerator countDown() {
		hasPaused = true;
        Debug.Log("Enable Set Go notification");
		yield return new WaitForSeconds (3);
        hasPaused = false;
        ReadySetGo.SetActive(false);
        //Destroy (ReadySetGo);
		Camera.main.GetComponent<AudioSource> ().enabled = true;
	}
}
