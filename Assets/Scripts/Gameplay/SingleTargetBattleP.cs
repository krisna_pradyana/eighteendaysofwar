﻿using UnityEngine;
using System.Collections;

public class SingleTargetBattleP : MonoBehaviour {
	private UnitAtt att,attfr;
	private int knocfriend=1;
	private float delay = 0, breath, rng, attackanim,cooldown;
	public float atkspd,attack, atak;
	private Move mov;
	private MoveE movE;
	private GameObject target;
	private Animator animPlayer,animEnemy;
	public GameObject explode;
	public bool boss=false;
	private CameraControl cam;
	// Use this for initialization
	void Start () {
		att = gameObject.GetComponent<UnitAtt>();
		rng = att.RNG;
		atak = att.ATK;
		mov = gameObject.GetComponent<Move>();
		animPlayer = gameObject.GetComponent<Animator> ();
		float increaseAttackPower = PlayerPrefs.GetFloat ("AttackPower");
		atak = atak + (atak * increaseAttackPower);
		attack = atak;
		atkspd = att.ATKSPD;
		target = null;
		cam = Camera.main.GetComponent<CameraControl> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (CastleEnemy.hasPlayerWin != true) {
			battleon ();	
		}
		if (transform.position.x <= -8){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
		}
	}

	void FindPlayerCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("EnemyCastle");
		float distance = Mathf.Infinity;
		double check=rng*1.5*7;
		if (rng > 1) {
			check=rng*1.5*4;
		}
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			if(go.name == "MiniTower" || go.name == "MiniTower2"){
				check=rng*1.5*3;
			}
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= check){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}
	
	void FindClosestEnemy() {
		if(target != null && target.tag == "Enemy"){
			target = null;
		}
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3.1f){
					target = go;
				}
				else{
					GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
					mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				}
				distance = diff;
			}
		}
	}

	void FindClosestFriend() {
		knocfriend = 1;
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			attfr = go.GetComponent<UnitAtt>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(attfr.KNOCKBACK !=0 && go != gameObject){
					knocfriend += 1;
				}
				distance = diff;
			}
		}
	}

	void battleon(){
		if(boss == true && GetComponent<Rigidbody2D>().velocity != Vector2.right*0){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
		}
		if (target != null && animPlayer.GetBool("Skill") == false && name != "drupa_root(Clone)" || target != null && name == "drupa_root(Clone)") {
			mov.speed = 0;
			if(Time.time > delay){
				animPlayer.SetBool("Kill", false);
				animPlayer.SetBool("Attack",true);
				if(attackanim == 0){
					if(animPlayer.GetBool("Attackspeed")){
						cooldown = 0.45f;
					}
					else{
						cooldown = 0.9f;
					}
					attackanim=Time.time+cooldown;
				}
				delay = Time.time + atkspd;
				breath += 0.3f;
				if(att.SPLASH == 0){
					StartCoroutine ("delayBeforeAttack");
					if(att.KNOCKBACK != 0){
						if(target.tag == "Enemy"){
							animEnemy= target.GetComponent<Animator>();
							animEnemy.SetBool("Knockback",true);
							SingleTargetBattleE seboss = target.GetComponent<SingleTargetBattleE>();
							RangecheckE reboss = target.GetComponent<RangecheckE>();
							bool checkboss=false;
							if(seboss != null){
								checkboss = seboss.boss;
							}
							else if(reboss != null){
								checkboss = reboss.boss;
							}
							if(checkboss == false){
								target.GetComponent<Rigidbody2D>().velocity= Vector2.right*(3/knocfriend);
								movE = target.GetComponent<MoveE>();
								movE.bulletE.GetComponent<Rigidbody2D>().velocity= (Vector2.right*(3/knocfriend))/movE.scalar;
							}
							target = null;
						}
					}
					FindClosestEnemy();
				}
				else{
					FindClosestEnemy();
				}
			}
			else if(Time.time > attackanim){
				attackanim = 0;
				animPlayer.SetBool("Attack", false);
			}
		}
		else{
			animPlayer.SetBool("Attack", false);
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			animPlayer.SetBool("Kill",true);
			animPlayer.SetBool("Knockback",false);
			if(att.KNOCKBACK == 0){
				FindClosestEnemy();
				FindPlayerCastle();
			}
			if(Time.time > breath){
				delay = Time.time;
				FindClosestEnemy();
				FindPlayerCastle();
				breath = delay + 0.1f;
				if(mov.speed == 0 && target == null){
					mov.speed = mov.sped;
				}
			}
		}
		if(gameObject.name == "gatotkaca_root(Clone)" && Skillclick.Useskill == true && Skillclick.gatotskill == true){
			mov.speed = 0;
		}
		else if(gameObject.name == "nakula_sadewa_root(Clone)" && Skillclick.Useskill == true && Skillclick.gatotskill == false){
			mov.speed = 0;
		}
	}
	IEnumerator delayBeforeAttack() {
		yield return new WaitForSeconds((int)1f);
		if (gameObject.tag == "EnemyCastle") {
			int randomX = Random.Range (-5, 0);
			int randomY = Random.Range (-4, 3);
			if (explode != null) {
				Instantiate(explode,target.transform.position+new Vector3(randomX,randomY,-10),target.transform.rotation);
			}
			CastleEnemy.enemyCastleAttacked = true;
		}
		else {
			if(target != null){
				if (explode != null) {
					Instantiate(explode,target.transform.position+new Vector3(0,2,-10),target.transform.rotation);
				}
			}
		}
		if (target != null) {
			int dmg = Mathf.RoundToInt(Random.Range(attack,attack*11/10));
			target.gameObject.SendMessage ("colldetect", dmg);	
		}
	}
}
