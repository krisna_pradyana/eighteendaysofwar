﻿using UnityEngine;
using System.Collections;

public class BossMode : MonoBehaviour {
	private int star = 0;
	public GameObject star1, star2, star3, WinUI, gold, timerText;
	public string starStage;
	public string clearStage;
	public AudioClip winBGM;
	private float bgmVolume;
    private GameObject healthBarEnemy;
	public int coinFor1Star, coinFor2Star, coinFor3Star;
	private int coin;
	private GameObject square;
	public int timeFor2Star, timeFor3Star;
	public float timer;
	private System.TimeSpan timerFormatted;
	public GameObject Firework;
	private bool hasShowFirework = false;
	private int victoryCount;
	//private GoldSystem goldSystem;
	// Use this for initialization
	void Start () {
        WinUI = GameObject.Find("Win UI");
        WinUI.SetActive(false);

        bgmVolume = 1;
		square = GameObject.Find ("Main Camera/Square");
		InvokeRepeating ("IncreaseTimer", 1, 1);
		victoryCount = PlayerPrefs.GetInt ("Victory");
        //goldSystem = GameObject.FindGameObjectWithTag ("Gold").GetComponent<GoldSystem>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Application.loadedLevelName == "Stage3-6" || Application.loadedLevelName == "Stage5-6") {
			if (MoveE.boss1Die == true && MoveE.boss2Die == true && bgmVolume > 0) {
				bgmVolume -= 0.01f;
				Camera.main.GetComponent<AudioSource>().volume = bgmVolume;
			}
			if (MoveE.boss1Die == true && MoveE.boss2Die == true) {
				WinUI.SetActive(true);
				square.GetComponent<SpriteRenderer>().enabled = true;
				if (hasShowFirework == false) {
					Instantiate(Firework, new Vector3((Camera.main.transform.position.x),-5.4f, 0), Quaternion.Euler(270, 0, 0));
					hasShowFirework = true;
				}
				GameFlow.hasPaused = true;
				countStar();
				victoryCount += 1;
				PlayerPrefs.SetInt ("Victory", victoryCount);
				if (Application.loadedLevelName == "Stage3-6") {
					PlayerPrefs.SetInt ("DoubleTrouble", 1);
				}
				else if (Application.loadedLevelName == "Stage5-6") {
					PlayerPrefs.SetInt ("CoupleOrTrouble", 1);
				}
				StartCoroutine("victorySound");
				MoveE.hasBossDie = false;
				MoveE.boss1Die = false;
				MoveE.boss2Die = false;
			}
			timerFormatted = System.TimeSpan.FromSeconds(timer);
		}
		else {
			if (MoveE.hasBossDie == true && bgmVolume > 0) {
				bgmVolume -= 0.01f;
				Camera.main.GetComponent<AudioSource>().volume = bgmVolume;
			}
			if (MoveE.hasBossDie == true) {
				WinUI.SetActive(true);
				//square.GetComponent<SpriteRenderer>().enabled = true;
				if (hasShowFirework == false) {
					Instantiate(Firework, new Vector3((Camera.main.transform.position.x),-5.4f, 0), Quaternion.Euler(270, 0, 0));
					hasShowFirework = true;
				}
				GameFlow.hasPaused = true;
				countStar();
				if (Application.loadedLevelName == "Stage1-6") {
					PlayerPrefs.SetInt ("TerribleOath", 1);
				}
				else if (Application.loadedLevelName == "Stage2-6") {
					PlayerPrefs.SetInt ("SonOfSurya", 1);
				}
				else if (Application.loadedLevelName == "Stage4-6") {
					PlayerPrefs.SetInt ("Eclipse", 1);
				}
				else if (Application.loadedLevelName == "Stage6-6") {
					PlayerPrefs.SetInt ("CraftyTacticion", 1);
				}
				else if (Application.loadedLevelName == "Stage7-6") {
					PlayerPrefs.SetInt ("TheImmortal", 1);
				}
				else if (Application.loadedLevelName == "Stage8-6") {
					PlayerPrefs.SetInt ("ToughRuler", 1);
				}
				else if (Application.loadedLevelName == "Stage9-6") {
					PlayerPrefs.SetInt ("UnconquerableWarrior", 1);
				}
				victoryCount += 1;
				PlayerPrefs.SetInt ("Victory", victoryCount);
				StartCoroutine("victorySound");
				MoveE.hasBossDie = false;
				MoveE.boss1Die = false;
				MoveE.boss2Die = false;
			}
			timerFormatted = System.TimeSpan.FromSeconds(timer);
		}

	}
	void countStar() {
        gold = GameObject.Find("Reward/Coin Text");
        timerText = GameObject.Find("Time/Time Text");
        star1 = GameObject.Find("Boss Camera/Win UI/Star/Star 1");
        star2 = GameObject.Find("Boss Camera/Win UI/Star/Star 2");
        star3 = GameObject.Find("Boss Camera/Win UI/Star/Star 3");

        if (timer <= timeFor3Star) {
			star = 3;
			coin = coinFor3Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
			star2.GetComponent<SpriteRenderer>().enabled = true;
			star3.GetComponent<SpriteRenderer>().enabled = true;
		}
		else if (timer <= timeFor2Star) {
			star = 2;
			coin = coinFor2Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
			star3.GetComponent<SpriteRenderer>().enabled = true;
		}
		else {
			star = 1;
			coin = coinFor1Star;
			star1.GetComponent<SpriteRenderer>().enabled = true;
		}

        //GameObject.Find("Gold Container").SendMessage("IncreaseGold", coin);
        gold.GetComponent<EasyFontTextMesh>().Text = coin.ToString();
		timerText.GetComponent<EasyFontTextMesh>().Text = string.Format("{0:D2}:{1:D2}", timerFormatted.Minutes, timerFormatted.Seconds);
		//PlayerPrefs.SetInt ("Gold", PlayerPrefs.GetInt ("Gold") + coin);
		PlayerPrefs.SetInt (starStage, star);
		PlayerPrefs.SetInt (clearStage, 1);
	}
	IEnumerator victorySound() {
		yield return new WaitForSeconds (2);
		AudioSource.PlayClipAtPoint(winBGM, Camera.main.transform.position);
		Time.timeScale = 0;
	}
	void IncreaseTimer () {
		timer += 1;
	}
}
