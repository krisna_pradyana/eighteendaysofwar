﻿using UnityEngine;
using System.Collections;

public class RangecheckE : MonoBehaviour {
	private float delay,attackanim;
	UnitAtt att;
	float rng,atta;
	public float atkspd;
	GameObject arrowplay,target,closest, close;
	public GameObject arro;
	MoveE mov;
	Vector3 arrpos, rightch;
	private Animator animEnemy;
	public bool boss=false;
	private CircleCollider2D area;
	// Use this for initialization
	void Start () {
		att = gameObject.GetComponent<UnitAtt>();
		area = gameObject.GetComponentInChildren<CircleCollider2D> ();
		atta = att.ATK;
		rng = att.RNG;
		mov = gameObject.GetComponent<MoveE>();
		animEnemy = gameObject.GetComponent<Animator> ();
		atkspd = att.ATKSPD;
		target = null;
	}
	
	void Update(){
		if (transform.position.x >= 163 && att.KNOCKBACK == 0 && tag == "Enemy"){
			boss = true;	
		}
		else{
			if (gameObject.name == "BheesmaHarpoonRoot(Clone)" || gameObject.name == "BheesmaRoot(Clone)" ||
			    gameObject.name == "Dursi_root(Clone)" || gameObject.name == "jayadrata_root(Clone)" ||
			    gameObject.name == "jayadrata_root 2nd form(Clone)" || gameObject.name == "Karna_Root(Clone)" ||
			    gameObject.name == "Karna_Root 2nd Form(Clone)" || gameObject.name == "ksengkuni_root(Clone)") 
			{
				boss = true;
			}
			else {
				boss = false;
			}
		}
		if (gameObject.name == "KArcherMiniTower" && MiniTower.isTowerAlive == true) {
			animEnemy.SetBool ("Idle", true);
			if (target != null && animEnemy.GetBool("Skill")!= true) {
				if(Time.time > delay){
					animEnemy.SetBool("Attack", true);
					if (attackanim == 0) {
						attackanim = Time.time+0.9f;		
					}
					delay += atkspd;
					shoot();
				}
				else if(Time.time > attackanim){
					attackanim = 0;
					animEnemy.SetBool("Attack", false);
				}
			}
			else {
				FindClosestEnemy();
				delay = Time.time;
				animEnemy.SetBool("Attack",false);
				if(arrowplay != null){
					Destroy(arrowplay.gameObject);	
				}
			}
		}
		else {
			if(boss == true && GetComponent<Rigidbody2D>().velocity != Vector2.right*0){
				GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			}
			if (delay == 0) {
				delay = Time.time;
			}
			if (target != null && animEnemy.GetBool("Skill")!= true) {
				animEnemy.SetBool("Kill", false);
				if(gameObject.name == "ktank_root(Clone)"){
					ClosestEnemyTank();
					if(target == null){
						FindClosestEnem();
					}
				}
				else{
					FindClosestEnemy();
				}
				mov.speed = 0;
				if(gameObject.name == "kshaman_root(Clone)") {
					startheal();
				}
				if(Time.time > delay){
					animEnemy.SetBool("Knockback",false);
					GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
					mov.bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
					animEnemy.SetBool("Attack", true);
					if (attackanim == 0) {
						attackanim = Time.time+0.9f;		
					}
					delay += atkspd;
					if(gameObject.name == "kshaman_root(Clone)"){
						
					}
					else{
						shoot ();
					}
				}
				else if(Time.time > attackanim){
					attackanim = 0;
					animEnemy.SetBool("Attack", false);
				}
			} 
			else if(close != null && target == null){
				mov.speed = 0;
				animEnemy.SetBool("Idle", true);
				if(gameObject.name == "ktank_root(Clone)"){
					ClosestEnemyTank();
				}
				delay = Time.time;
			}
			else {
				animEnemy.SetBool("Attack", false);
				GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				mov.bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				animEnemy.SetBool("Kill",true);
				if(arrowplay != null){
					Destroy(arrowplay.gameObject);
					
				}
				FindPlayerCastle();
				if(gameObject.name == "ktank_root(Clone)"){
					ClosestEnemyTank();
					if(target == null){
						FindClosestEnem();
					}
				}
				else if(gameObject.name == "kshaman_root(Clone)"){
					FindClosestEnemy();
					if(target == null){
						stopheal();
					}
				}
				else{
					FindClosestEnemy();
				}
				delay = Time.time;
				if(mov.speed == 0){
					mov.speed = att.SPD;
				}
			}
		}

	}
	
	void shoot(){
		arrpos = gameObject.transform.position;
		StartCoroutine("delayShooting");
	}

	void FindPlayerCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("PlayerCastle");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*4){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}

	void FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3){
					target = go;
				}
				else{
					target = null;
				}
				distance = diff;
			}
		}
	}

	void FindClosestEnem() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		if (gos.Length == 0) {
			gos = GameObject.FindGameObjectsWithTag("PlayerCastle");
		}
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3){
					close = go;
				}
				distance = diff;
			}
		}
	}
	
	void ClosestEnemyTank(){
		GameObject[] gos;
		target = null;
		gos = GameObject.FindGameObjectsWithTag("Player");
		if (gos.Length == 0) {
			gos = GameObject.FindGameObjectsWithTag("PlayerCastle");
		}
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*6 && diff >= (rng-2)*6){
					target = go;
					close = null;
				}
				if(diff >= (rng-2)*6){
					distance = diff;
				}
			}
		}
	}
	
	void startheal(){
		area.radius = att.SPLASH;
	}
	void stopheal(){
		area.radius = 0.1f;
	}

	IEnumerator delayShooting() {
		if (gameObject.name == "kgunnerRoot(Clone)") {
			yield return new WaitForSeconds ((int)0.8f);	
		}
		else {
			yield return new WaitForSeconds ((int)1f);
		}
		if (arro != null) {
			if (arro.name == "ArrowE" || arro.name == "ArrowBhisma" || arro.name == "ArrowKarna" || arro.name == "ArrowKarna2ndForm") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-10.5f, 8f, 0), arro.transform.rotation) as GameObject;	
			}
			else if (arro.name == "FireBallEnemy") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-4f, 2f, 0), arro.transform.rotation) as GameObject;
			}
			else if (arro.name == "HarpoonE") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-5f, 2f, 0), arro.transform.rotation) as GameObject;
			}
			else if (arro.name == "TankBulletEnemy") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-9.5f, 7.5f, 0), arro.transform.rotation) as GameObject;
			}
			else if (arro.name == "dice_root") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-3f, 1, 0), arro.transform.rotation) as GameObject;
			}
			else {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (-8f, 0, 0), arro.transform.rotation) as GameObject;	
			}		
		}
	}
}
