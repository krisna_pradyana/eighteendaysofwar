﻿using UnityEngine;
using System.Collections;

public class ParallaxScrolling : MonoBehaviour {
	private float speed;
	public bool isGunung;
	public bool isBack;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (isGunung == true) {
			speed = CameraControl.totalSpeed/30;
		}
		if (isBack == true) {
			speed = CameraControl.totalSpeed/3;	
		}
		if (CameraControl.moveRight == true) {
			transform.Translate (Vector2.right * speed);
		}
		if (CameraControl.moveLeft == true) {
			transform.Translate (-Vector2.right * speed * -1);	
		}
	}
}