﻿using UnityEngine;
using System.Collections;

public class InitUnit : MonoBehaviour {

    #region Delegate
    public delegate void InitializeHero();
    public static InitializeHero initializeHero;
    #endregion

    private GameObject container;
	private GameObject unit,lockunit,hero;
	public GameObject placeholder1,placeholder2,placeholder3;
	public GameObject placeskill1,placeskill2,placeskill3;
	public GameObject placeframe1,placeframe2,placeframe3;
	public GameObject placecd1,placecd2,placecd3;
	public GameObject cdarjun,cdbim,cdsri,cdyud,cdnasa,cdgatot,cddrup,cdkres,cdanom;
	public GameObject arjuna, bima, srikandi,yudistira,nakusade, gatotkaca, drupada, kresna, anoman,zero;
	public GameObject skill1, skill2, skill3,skill4,skill5,skill6,skill7,skill8,skill9;
	public GameObject costhero1, costhero2, costhero3;
	public GameObject frameranged, framemelee;
	private CameraControl cam;
	private bool onerun;
	public static bool readytoinit;

    private void OnEnable()
    {
        initializeHero += InitHero;
    }

    private void OnDisable()
    {
        initializeHero -= InitHero;
    }

    // Use this for initialization
    void Start () {
		cam = Camera.main.GetComponent<CameraControl> ();
		if (cam.bosslevel == false) {
			container = GameObject.Find("UnitContainer");
			if (Checkunit.archer == false) {
				unit =container.transform.Find("archer").gameObject;
				lockunit=container.transform.Find("emptyarcher").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.swordman == false) {
				unit =container.transform.Find("swordman").gameObject;
				lockunit=container.transform.Find("emptyswordman").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.gunner == false) {
				unit =container.transform.Find("gunner").gameObject;
				lockunit=container.transform.Find("emptygunner").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.spearman == false) {
				unit =container.transform.Find("spearman").gameObject;
				lockunit=container.transform.Find("emptyspearman").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.mage == false) {
				unit =container.transform.Find("mage").gameObject;
				lockunit=container.transform.Find("emptymage").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.shaman == false) {
				unit =container.transform.Find("shaman").gameObject;
				lockunit=container.transform.Find("emptyshaman").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.elephant == false) {
				unit =container.transform.Find("elephantry").gameObject;
				lockunit=container.transform.Find("emptyelephantry").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
			if (Checkunit.tank == false) {
				unit =container.transform.Find("tank").gameObject;
				lockunit=container.transform.Find("emptytank").gameObject;
				unit.SetActive(false);
				lockunit.SetActive(true);
			}
		}
	}

    public void InitHero()
    {
        if (readytoinit == true && onerun == false)
        {
            containall(Checkunit.hero1select, placeholder1, placeskill1, placeframe1, costhero1, placecd1);
            containall(Checkunit.hero2select, placeholder2, placeskill2, placeframe2, costhero2, placecd2);
            containall(Checkunit.hero3select, placeholder3, placeskill3, placeframe3, costhero3, placecd3);
            onerun = true;
        }
    }

	void containall(string name,GameObject placeholder, GameObject placeskill, GameObject placeframe, GameObject costhero ,GameObject placecd){
		if (name == "") {
			inithero(placeholder, placeskill,placecd, placeframe ,zero, null, null, null);
			costhero.SetActive(false);
		}
		else if(name == "arjuna"){
			inithero(placeholder,placeskill,placecd,placeframe,arjuna, skill1,cdarjun, frameranged);
		}
		else if(name == "bima"){
			inithero(placeholder, placeskill,placecd,placeframe, bima, skill2,cdbim, framemelee);
		}
		else if(name == "srikandi"){
			inithero(placeholder, placeskill,placecd,placeframe,srikandi, skill3,cdsri, frameranged);
		}
		else if(name == "yudistira"){
			inithero(placeholder, placeskill,placecd,placeframe, yudistira, skill4,cdyud, framemelee);
		}
		else if(name == "nakusade"){
			inithero(placeholder, placeskill,placecd,placeframe, nakusade, skill5,cdnasa, framemelee);
		}
		else if(name == "gatot"){
			inithero(placeholder, placeskill,placecd,placeframe, gatotkaca, skill6,cdgatot, framemelee);
		}
		else if(name == "drupada"){
			inithero(placeholder, placeskill,placecd,placeframe, drupada, skill7,cddrup, framemelee);
		}
		else if(name == "kresna"){
			inithero(placeholder, placeskill,placecd,placeframe, kresna, skill8,cdkres, frameranged);
		}
		else if(name == "anoman"){
			inithero(placeholder, placeskill,placecd,placeframe, anoman, skill9,cdanom, framemelee);
		}
	}

	void inithero(GameObject placeholder,GameObject placeskill, GameObject placecd, GameObject placeframe ,GameObject hero, GameObject skillhero, GameObject cdhero, GameObject frame){
		GameObject heroui = hero;
		heroui.transform.position = placeholder.transform.position;
		heroui.name = hero.name;
		heroui.SetActive (true);
		if (skillhero != null) {
			GameObject fr = (GameObject) Instantiate (frame, placeframe.transform.position, placeframe.transform.rotation);
			fr.transform.parent = placeframe.transform.parent;
			fr.transform.localScale = placeframe.transform.localScale;
			fr.SetActive (true);
			SpawnHero sh = heroui.GetComponent<SpawnHero>();
			AudioSource a = heroui.GetComponent<AudioSource>();
			Unitcooldown uc = GameObject.Find ("CooldownContainer").GetComponent<Unitcooldown> ();
			a.volume = placeholder.GetComponent<AudioSource>().volume;
			GameObject skillui = skillhero;
			skillui.transform.position = placeskill.transform.position;
			skillui.SetActive(false);
			skillui.name = skillhero.name;
			sh.unitskill = skillui;
			a = skillui.GetComponent<AudioSource>();
			a.volume = placeskill.GetComponent<AudioSource>().volume;
			GameObject cdui = cdhero;
			cdui.transform.position = placecd.transform.position;
			cdui.SetActive(false);
			cdui.name = cdhero.name;
			sh.unitcd = cdui;
			if(hero == arjuna){
				uc.Arjuna = heroui;
				uc.cdArjuna = cdui;
				uc.skill1 = skillui;
			}
			else if(hero == bima){
				uc.Bima = heroui;
				uc.cdBima = cdui;
				uc.skill2 = skillui;
			}
			else if(hero == srikandi){
				uc.Srikandi = heroui;
				uc.cdSrikandi = cdui;
				uc.skill3 = skillui;
			}
			else if(hero == yudistira){
				uc.Yudistira = heroui;
				uc.cdYudistira = cdui;
				uc.skill4 = skillui;
			}
			else if(hero == nakusade){
				uc.NakuSadev = heroui;
				uc.cdNakuSade = cdui;
				uc.skill5 = skillui;
			}
			else if(hero == gatotkaca){
				uc.GatotKaca = heroui;
				uc.cdGatotKaca = cdui;
				uc.skill6 = skillui;
			}
			else if(hero == drupada){
				uc.Drupada = heroui;
				uc.cdDrupada = cdui;
				uc.skill7 = skillui;
			}
			else if(hero == kresna){
				uc.Kresna = heroui;
				uc.cdKresna = cdui;
				uc.skill8 = skillui;
			}
			else if(hero == anoman){
				uc.Anoman = heroui;
				uc.cdAnoman = cdui;
				uc.skill9 = skillui;
			}
		}
		else{
			GameObject go = (GameObject) Instantiate(heroui, placeholder.transform.position, placeholder.transform.rotation);
			go.transform.position = placeholder.transform.position;
			go.transform.parent = placeframe.transform.parent;
			heroui.name = hero.name;
			go.transform.localScale = placeframe.transform.localScale;
		}
		Destroy(placeholder);
		Destroy(placeskill);
		Destroy(placecd);
		Destroy(placeframe);
	}
}
