﻿using UnityEngine;
using System.Collections;

public class IncreaseSilver : MonoBehaviour {
	public GameObject unitcd;
	public AudioClip soundSpawn, soundCannotSummon;
	public float cost;
	public int summonLimit;
	private bool canSummon;
	private SpriteRenderer unitColor;
	public static int counterLimit;
	public GameObject minerCostText;
	private float money;
	// Use this for initialization
	void Start () {
		counterLimit = 1;
		unitColor = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Generate.money >= cost && GameFlow.hasPaused == false && unitcd.activeSelf == false && counterLimit <= summonLimit) {
			canSummon = true;
			unitColor.color = Color.white;
		}
		else {
			canSummon = false;
			unitColor.color = Color.gray;
		}
	}
	void OnMouseDown () {
		if (canSummon == true) {
			GameFlow.maxUnitNumbers += 1;
			Generate.increaseFromMiner += 5;
			money = Generate.money - cost;
			Generate.money = money;
			counterLimit += 1;
			AudioSource.PlayClipAtPoint (soundSpawn, transform.position);
			unitcd.SetActive(true);

			if (Application.loadedLevelName == "Tutorial" || counterLimit > summonLimit) {
				cost = 0;
			}
			else {
				cost += 10;
			}
			if (cost == 0) {
				minerCostText.GetComponent<EasyFontTextMesh>().Text = " ";
			}
			else {
				minerCostText.GetComponent<EasyFontTextMesh>().Text = cost.ToString();
			}
		}
		else {
			AudioSource.PlayClipAtPoint (soundCannotSummon, transform.position);
		}
	}
}
