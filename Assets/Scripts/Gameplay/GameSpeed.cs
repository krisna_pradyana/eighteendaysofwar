﻿using UnityEngine;
using System.Collections;

public class GameSpeed : MonoBehaviour {
	private int increaseGameSpeed;
	public GameObject gameSpeed1x, gameSpeed2x, gameSpeed3x;
	private float timer;
	// Use this for initialization
	void Start () {
		timer = 3;
		increaseGameSpeed = PlayerPrefs.GetInt ("GameSpeed");
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		if (timer >= 0) {
			gameObject.GetComponent<BoxCollider2D>().enabled = false;	
		}
		else {
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
		if (StartGame.selectionMode == true || GameFlow.hasPaused == true) {
			gameObject.GetComponent<BoxCollider2D>().enabled = false;	
		}
		else {
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
	}
	void OnMouseDown () {
		if (GameFlow.hasPaused == false) {
			if (increaseGameSpeed == 0) {
				Time.timeScale = 1;
			}
			if (increaseGameSpeed == 2) {
				if (gameObject.name == "1x Container") {
					gameSpeed1x.SetActive(false);
					gameSpeed2x.SetActive(true);
					Time.timeScale = 2;
				}
				else if (gameObject.name == "2x Container") {
					gameSpeed2x.SetActive(false);
					gameSpeed1x.SetActive(true);
					Time.timeScale = 1;
				}
			}
			if (increaseGameSpeed == 3) {
				if (gameObject.name == "1x Container") {
					gameSpeed1x.SetActive(false);
					gameSpeed2x.SetActive(true);
					gameSpeed3x.SetActive(false);
					Time.timeScale = 2;	
				}
				else if (gameObject.name == "2x Container") {
					gameSpeed1x.SetActive(false);
					gameSpeed2x.SetActive(false);
					gameSpeed3x.SetActive(true);
					Time.timeScale = 3;
				}
				else if (gameObject.name == "3x Container") {
					gameSpeed3x.SetActive(false);
					gameSpeed2x.SetActive(false);
					gameSpeed1x.SetActive(true);
					Time.timeScale = 1;	
				}
				
			}	
		}

	}
}
