﻿using UnityEngine;
using System.Collections;

public class RangecheckP : MonoBehaviour {
	private float delay, attackanim, cooldown;
	UnitAtt att;
	float rng;
	public float atkspd, atak, atta;
	GameObject arrowplay,target,closest, close;
	public GameObject arro, chakram;
	Move mov;
	Vector3 arrpos, rightch;
	private Animator animPlayer;
	public bool boss=false;
	private CircleCollider2D area;
	// Use this for initialization
	void Start () {
		att = gameObject.GetComponent<UnitAtt>();
		area = gameObject.GetComponentInChildren<CircleCollider2D> ();
		atak = att.ATK;
		float increaseAttackPower = PlayerPrefs.GetFloat ("AttackPower");
		atak = atak + (atak * increaseAttackPower);
		atta = atak;
		rng = att.RNG;
		mov = gameObject.GetComponent<Move>();
		animPlayer = gameObject.GetComponent<Animator> ();
		atkspd = att.ATKSPD;
		target = null;
	}

	void Update(){
		if (transform.position.x <= -8){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;	
		}
		if(boss == true && GetComponent<Rigidbody2D>().velocity != Vector2.right*0){
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
		}
		if (delay == 0) {
			delay = Time.time;	
		}
		if (target != null && animPlayer.GetBool("Skill")!= true) {
			mov.speed = 0;
			animPlayer.SetBool("Kill", false);
			if(gameObject.name == "tank_root(Clone)"){
				ClosestEnemyTank();
				if(target == null){
					FindClosestEnem();
				}
			}
			else{
				FindClosestEnemy();
			}
			if (gameObject.name == "shaman_root(Clone)") {
				startheal();
			}
			if(Time.time > delay){
				animPlayer.SetBool("Knockback",false);
				GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				if(mov.bulletP != null){
					mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				}
				animPlayer.SetBool("Attack", true);
				if(attackanim == 0){
					if(animPlayer.GetBool("Attackspeed")){
						cooldown = 0.45f;
					}
					else{
						cooldown = 0.9f;
					}
					attackanim=Time.time+cooldown;
				}
				delay += atkspd;
				if(gameObject.name == "shaman_root(Clone)"){

				}
				else{
					shoot ();
				}
			}
			else if(Time.time > attackanim){
				attackanim = 0;
				animPlayer.SetBool("Attack", false);
			}
		}
		else if(close != null){
			mov.speed = 0;
			animPlayer.SetBool("Idle", true);
			if(gameObject.name == "tank_root(Clone)"){
				ClosestEnemyTank();
			}
			delay = Time.time;
		}
		else{
			animPlayer.SetBool("Attack", false);
			GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			if(mov.bulletP != null){
				mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			}
			animPlayer.SetBool("Kill",true);
			if(arrowplay != null){
				Destroy(arrowplay.gameObject);
			}
			FindPlayerCastle();
			if(gameObject.name == "tank_root(Clone)"){
				ClosestEnemyTank();
				if(target == null){
					FindClosestEnem();
				}
			}
			else if(gameObject.name == "shaman_root(Clone)"){
				FindClosestEnemy();
				if(target == null){
					stopheal();
				}
			}
			else{
				FindClosestEnemy();
			}
			attackanim = 0;
			delay = Time.time;
			if(mov.speed == 0 && target == null){
				mov.speed = mov.sped;
			}
		}
	}

	void shoot(){
		arrpos = gameObject.transform.position;
		StartCoroutine("delayShooting");
	}

	void FindPlayerCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("EnemyCastle");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*4){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}

	void FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3){
					target = go;
				}
				else{
					GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
					mov.bulletP.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
				}
				distance = diff;
			}
		}
	}

	void FindClosestEnem() {
		GameObject[] gos;
		GameObject mini;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		mini = GameObject.Find("MiniTower");
		if (gos.Length == 0) {
			gos = GameObject.FindGameObjectsWithTag("EnemyCastle");
		}
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*1.5*3){
					close = go;
				}
				distance = diff;
			}
		}
		if (mini != null) {
			float differ = Mathf.Abs(mini.transform.position.x - position.x);
			if(differ <= rng*1.5*3){
				close = mini;
			}		
		}
	}

	void ClosestEnemyTank(){
		GameObject[] gos;
		GameObject mini;
		target = null;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		mini = GameObject.Find("MiniTower");
		if (gos.Length == 0) {
			gos = GameObject.FindGameObjectsWithTag("EnemyCastle");
		}
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= rng*6 && diff >= (rng-2)*6){
					target = go;
					close = null;
				}
				if(diff >= (rng-2)*6){
					distance = diff;
				}
			}
		}
		if (mini != null) {
			float differ = Mathf.Abs(mini.transform.position.x - position.x);
			if(differ <= rng*1.5*3){
				target = mini;
			}		
		}
	}

	void startheal(){
		area.radius = att.SPLASH;
	}
	void stopheal(){
		area.radius = 0.1f;
	}

	IEnumerator delayShooting() {
		if (gameObject.name == "gunnerRoot(Clone)") {
			yield return new WaitForSeconds ((int)0.8f);	
		}
		else {
			yield return new WaitForSeconds ((int)1f);
		}
		if (att.KNOCKBACK != 0) {
			if(chakram != null){
				arrowplay = GameObject.Instantiate(chakram, arrpos+new Vector3 (0f, 2.8f, 0), chakram.transform.rotation) as GameObject;
				att.KNOCKBACK -= 1;
			}		
		}
		else if (arro != null) {
			if (arro.name == "ArrowP" || arro.name == "ArrowArjuna" || arro.name == "ArrowSrikandi") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (8f, -3f, 0), arro.transform.rotation) as GameObject;	
				arrowplay.transform.parent = transform;
			}
			else if (arro.name == "FireBallPlayer") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (8f, 2f, 0), arro.transform.rotation) as GameObject;
			}
			else if (arro.name == "TankBulletPlayer") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (9f, 4.8f, 0), arro.transform.rotation) as GameObject;
			}
			else if (arro.name == "chakram") {
				arrowplay = GameObject.Instantiate(arro, arrpos + new Vector3 (0f, 2.8f, 0), arro.transform.rotation) as GameObject;
			}
			else {
				arrowplay = GameObject.Instantiate(arro, arrpos, arro.transform.rotation) as GameObject;
			}	
		}
	}
}
