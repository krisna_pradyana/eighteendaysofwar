﻿using UnityEngine;
using System.Collections;

public class SurvivalController : MonoBehaviour {
	public GameObject[] enemyList;
	public GameObject waveText, waveTextTop;
	public GameObject bgSunset, bgNight, cloudSunny, cloudSunset, bgMountainSunny, bgMountainSunset, bgMountainNight;
	public GameObject puraNormal, puraRetak, puraHancur;
	public GameObject fgNormal, fgNormalRetak, fgNormalHancur, fgSunset, fgSunsetRetak, fgSunsetHancur, fgNight, fgNightRetak, fgNightHancur;
	public static int totalWave, wave, maxWave;
	private int curhp, curatk, counterWave1, counterWave2, counterWave3;
	private int hpbasefoot, hpbasearcher, hpbasesword, hpbasegun,hpbasespear,hpbasemage, hpbaseshaman, hpbaseelephant, hpbasetank;
	private int atkbasefoot, atkbasearcher, atkbasesword, atkbasegun,atkbasespear,atkbasemage, atkbaseshaman, atkbaseelephant, atkbasetank;
	private int hpivfoot, hpivarcher, hpivsword, hpivgun, hpivspear, hpivmage, hpivshaman, hpivelephant, hpivtank;
	private int atkivfoot, atkivarcher, atkivsword, atkivgun, atkivspear, atkivmage, atkivshaman, atkivelephant, atkivtank;
	private GameObject[] enemies, players, bulletE, bulletP;
	private Unitcooldown ud;
	private string battleCondition;
	// Use this for initialization
	void Start () {
		battleCondition = "Normal";
		counterWave1 = 1;
		counterWave2 = 2;
		counterWave3 = 3;
		wave = 1;
		totalWave = 6;
		maxWave = 100;
		hpbasefoot = 200;
		hpbasearcher = 150;
		hpbasesword = 440;
		hpbasegun = 225;
		hpbasespear = 440;
		hpbasemage = 150;
		hpbaseshaman = 200;
		hpbaseelephant = 1000;
		hpbasetank = 3000;
		
		atkbasefoot = 20;
		atkbasearcher = 18;
		atkbasesword = 44;
		atkbasegun = 27;
		atkbasespear = 44;
		atkbasemage = 40;
		atkbaseshaman = 10;
		atkbaseelephant = 30;
		atkbasetank = 100;
		
		hpivfoot = 20;
		hpivarcher = 15;
		hpivsword = 30;
		hpivgun = 22;
		hpivspear = 30;
		hpivmage = 15;
		hpivshaman = 16;
		hpivelephant = 80;
		hpivtank = 40;
		
		atkivfoot = 3;
		atkivarcher = 2;
		atkivsword = 6;
		atkivgun = 3;
		atkivspear = 6;
		atkivmage = 6;
		atkivshaman = 2;
		atkivelephant = 4;
		atkivtank = 8;
		ud = GameObject.Find ("CooldownContainer").GetComponent<Unitcooldown> ();
		StartCoroutine (survivalWave());
	}
	
	// Update is called once per frame
	void Update () {

	}
	IEnumerator survivalWave() {
		yield return new WaitForSeconds (3);
		while(wave < maxWave) {
			if (wave == counterWave1) {
				cloudSunny.SetActive(true);
				cloudSunset.SetActive(false);
				bgNight.SetActive(false);
				bgSunset.SetActive(false);
				bgMountainNight.SetActive(false);
				bgMountainSunset.SetActive(false);
				bgMountainSunny.SetActive(true);
				if (battleCondition == "Normal") {
					fgNormal.SetActive(true);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Retak") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(true);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Hancur") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(true);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				counterWave1 += 3;
			}
			else if (wave == counterWave2) {
				cloudSunny.SetActive(false);
				cloudSunset.SetActive(true);
				bgSunset.SetActive(true);
				bgNight.SetActive(false);
				bgMountainNight.SetActive(false);
				bgMountainSunset.SetActive(true);
				bgMountainSunny.SetActive(false);
				if (battleCondition == "Normal") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(true);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Retak") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(true);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Hancur") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(true);
					fgNightHancur.SetActive(false);
				}
				counterWave2 += 3;
			}
			else if (wave == counterWave3) {
				cloudSunny.SetActive(false);
				cloudSunset.SetActive(false);
				bgSunset.SetActive(false);
				bgNight.SetActive(true);
				bgMountainNight.SetActive(true);
				bgMountainSunset.SetActive(false);
				bgMountainSunny.SetActive(false);
				if (battleCondition == "Normal") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(true);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Retak") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(true);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(false);
				}
				else if (battleCondition == "Hancur") {
					fgNormal.SetActive(false);
					fgSunset.SetActive(false);
					fgNight.SetActive(false);
					fgNormalRetak.SetActive(false);
					fgSunsetRetak.SetActive(false);
					fgNightRetak.SetActive(false);
					fgNormalHancur.SetActive(false);
					fgSunsetHancur.SetActive(false);
					fgNightHancur.SetActive(true);
				}
				counterWave3 += 3;
			}
			if (wave >= 6 && wave < 14) {
				battleCondition = "Retak";
				puraNormal.SetActive(false);
				puraRetak.SetActive(true);
			}
			else if (wave >= 13) {
				battleCondition = "Hancur";
				puraRetak.SetActive(false);
				puraHancur.SetActive(true);
			}
			while(CastleEnemy.hasPlayerWin == false) {
				while(totalWave > 0) {
					if (wave < 7) {
						int randomEnemy = Random.Range (0, enemyList.Length-5);
						Instantiate(enemyList[randomEnemy], transform.position, transform.rotation);
					}
					else if (wave < 14) {
						int randomEnemy = Random.Range (0, enemyList.Length-3);
						Instantiate(enemyList[randomEnemy], transform.position, transform.rotation);
					}
					else {
						int randomEnemy = Random.Range (0, enemyList.Length);
						Instantiate(enemyList[randomEnemy], transform.position, transform.rotation);
					}
					yield return new WaitForSeconds(1);
					totalWave -= 1;
				}
				totalWave = 6;
				yield return new WaitForSeconds(5);
			}
			enemies = GameObject.FindGameObjectsWithTag ("Enemy");
			players = GameObject.FindGameObjectsWithTag ("Player");
			bulletE = GameObject.FindGameObjectsWithTag ("BulletE");
			bulletP = GameObject.FindGameObjectsWithTag ("BulletP");
			foreach (GameObject enemy in enemies) {
				Destroy(enemy);
			}
			foreach (GameObject player in players) {
				Destroy(player);
			}
			foreach (GameObject bullete in bulletE) {
				Destroy(bullete);
			}
			foreach (GameObject bulletp in bulletP) {
				Destroy(bulletp);
			}
			ResetHeroes();
			wave += 1;
			formula (hpbasefoot, hpivfoot, atkivfoot, wave, atkbasefoot, enemyList[0]);
			formula (hpbasearcher, hpivarcher, atkivarcher, wave, atkbasearcher, enemyList[1]);
			formula (hpbasesword, hpivsword, atkivsword, wave, atkbasesword, enemyList[2]);
			formula (hpbasegun, hpivgun, atkivgun, wave, atkbasegun, enemyList[3]);
			formula (hpbasespear, hpivspear, atkivspear, wave, atkbasespear, enemyList[4]);
			formula (hpbasemage, hpivmage, atkivmage, wave, atkbasemage, enemyList[5]);
			formula (hpbaseshaman, hpivshaman, atkivshaman, wave, atkbaseshaman, enemyList[6]);
			formula (hpbaseelephant, hpivelephant, atkivelephant, wave, atkbaseelephant, enemyList[7]);
			formula (hpbasetank, hpivtank, atkivtank, wave, atkbasetank, enemyList[8]);
			waveText.SetActive(true);
			waveText.GetComponent<EasyFontTextMesh>().Text = "Wave "+wave+"\n"+"Get Ready!";
			waveTextTop.GetComponent<EasyFontTextMesh>().Text = "Wave "+wave;
			yield return new WaitForSeconds(3);
			CastleEnemy.maxHealth += 5000;
			CastleEnemy.health = CastleEnemy.maxHealth;
			CastleEnemy.hasPlayerWin = false;
			GameFlow.hasPaused = false;
			waveText.SetActive(false);
		}

	} 
	void formula(int hpbase,int hpiv, int atkiv, int lv,int atkbase, GameObject unit){
		for (int i = 2; i <= lv; i++) {
			curhp = hpbase +(i*hpiv);
			hpbase = curhp;
			curatk = atkbase + (i*atkiv);	
			atkbase = curatk;
		}
		unit.GetComponent<UnitAtt> ().HP = curhp;
		unit.GetComponent<UnitAtt> ().ATK = curatk;
	}

	void ResetHeroes(){
		Unitcooldown.hasdied = Unitcooldown.herosummoned;
		Unitcooldown.herosummoned="";
		Unitcooldown.hasSummonHero = false;
		Unitcooldown.resetercooldown = 1;
	}
}
