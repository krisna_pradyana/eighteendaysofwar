﻿using UnityEngine;
using System.Collections;

public class MinerLevel : MonoBehaviour {
	public Sprite[] levelMiner;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (IncreaseSilver.counterLimit == 2) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[0];	
		}
		else if (IncreaseSilver.counterLimit == 3) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[1];
		}
		else if (IncreaseSilver.counterLimit == 4) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[2];
		}
		else if (IncreaseSilver.counterLimit == 5) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[3];
		}
		else if (IncreaseSilver.counterLimit == 6) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[4];
		}
		else if (IncreaseSilver.counterLimit == 7) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[5];
		}
		else if (IncreaseSilver.counterLimit == 8) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[6];
		}
		else if (IncreaseSilver.counterLimit == 9) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[7];
		}
		else if (IncreaseSilver.counterLimit == 10) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[8];
		}
		else if (IncreaseSilver.counterLimit == 11) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[9];
		}
		else if (IncreaseSilver.counterLimit == 12) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[10];
		}
		else if (IncreaseSilver.counterLimit == 13) {
			gameObject.GetComponent<SpriteRenderer>().sprite = levelMiner[11];
		}
	}
}
