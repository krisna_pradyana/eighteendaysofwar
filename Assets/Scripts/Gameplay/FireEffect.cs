﻿using UnityEngine;
using System.Collections;

public class FireEffect : MonoBehaviour {
    /*
	public GameObject Fire1, Fire2, Fire3, Fire4, Fire5;
	private CastlePlayer castlePlayer;
	private CastleEnemy castleEnemy;
	private MiniTower miniTower;
	// Use this for initialization
	void Start () {
		if (gameObject.name == "Fire Castle Player") {
			castlePlayer = GameObject.Find ("PlayerCastle").GetComponent<CastlePlayer>();	
		}
		else if (gameObject.name == "Fire Castle Enemy") {
			castleEnemy = GameObject.Find ("EnemyCastle").GetComponent<CastleEnemy>();
		}
		else if (gameObject.name == "Fire Mini Tower") {
			miniTower = GameObject.Find ("MiniTower").GetComponent<MiniTower>();
		}
		else if (gameObject.name == "Fire Mini Tower2") {
			miniTower = GameObject.Find ("MiniTower2").GetComponent<MiniTower>();
		}
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (gameObject.name == "Fire Castle Player")
        {
            if (castlePlayer.isHurt50 == true)
            {
                Fire1.SetActive(true);
            }
            else if (castlePlayer.isHurt25 == true)
            {
                Fire1.SetActive(false);
                Fire2.SetActive(true);
                Fire3.SetActive(true);
            }
            else if (castlePlayer.isHurt0 == true)
            {
                Fire2.SetActive(false);
                Fire3.SetActive(false);
                Fire4.SetActive(true);
                Fire5.SetActive(true);
            }
        }
        else if (gameObject.name == "Fire Castle Enemy")
        {
            if (CastleEnemy.hasPlayerWin == true && Application.loadedLevelName == "Survival")
            {
                Fire1.SetActive(false);
                Fire2.SetActive(false);
                Fire3.SetActive(false);
                Fire4.SetActive(false);
                Fire5.SetActive(false);
            }
            else
            {
                if (castleEnemy.isHurt50 == true)
                {
                    Fire1.SetActive(true);
                }
                else if (castleEnemy.isHurt25 == true)
                {
                    Fire1.SetActive(false);
                    Fire2.SetActive(true);
                    Fire3.SetActive(true);
                }
                else if (castleEnemy.isHurt0 == true)
                {
                    Fire2.SetActive(false);
                    Fire3.SetActive(false);
                    Fire4.SetActive(true);
                    Fire5.SetActive(true);
                }
            }

        }
        else if (gameObject.name == "Fire Mini Tower")
        {
            if (miniTower.isHurt30 == true)
            {
                Fire3.SetActive(true);
            }
            else if (miniTower.isHurt0 == true)
            {
                Fire3.SetActive(false);
                Fire4.SetActive(true);
                Fire5.SetActive(true);
            }
            else
            {
                Fire4.SetActive(false);
                Fire5.SetActive(false);
            }
        }
        else if (gameObject.name == "Fire Mini Tower2")
        {
            if (miniTower.isHurt30 == true)
            {
                Fire3.SetActive(true);
            }
            else if (miniTower.isHurt0 == true)
            {
                Fire3.SetActive(false);
                Fire4.SetActive(true);
                Fire5.SetActive(true);
            }
            else
            {
                Fire4.SetActive(false);
                Fire5.SetActive(false);
            }
        }
	}
    */
}
