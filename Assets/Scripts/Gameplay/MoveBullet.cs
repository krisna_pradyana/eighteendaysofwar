﻿using UnityEngine;
using System.Collections;

public class MoveBullet : MonoBehaviour {
	public bool isPlayer;
	public bool isEnemy;
	void OnCollisionEnter2D(Collision2D coll){	
		if (isPlayer) {
			if (coll.gameObject.tag == "BulletP" || coll.gameObject.tag == "ProjectileP" || coll.gameObject.tag == "CameraPosition") {
				Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(),coll.gameObject.GetComponent<Collider2D>());		
			}	
		}
		if (isEnemy) {
			if (coll.gameObject.tag == "BulletE" || coll.gameObject.tag == "ProjectileE" || coll.gameObject.tag == "CameraPosition" ) {
				Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(),coll.gameObject.GetComponent<Collider2D>());		
			}	
		}
	}
}
