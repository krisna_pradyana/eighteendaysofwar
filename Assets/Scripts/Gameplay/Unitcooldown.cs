﻿using UnityEngine;
using System.Collections;

public class Unitcooldown : MonoBehaviour {
	public float swordcd, footcd, gunnercd, magecd, archcd, spearcd, shamancd, elephantrycd, tankcd, minercd;
	public float arjunacd, srikandicd, bimacd, yudiscd, nakusadecd, gatotkacacd, drupadacd, kresnacd, anomancd;
	public float skillArjunacd, skillBimacd, skillSrikandicd, skillYudiscd, skillnakusadecd, skillgatotkacacd, skilldrupcd, skillkrescd, skillanomcd;
	public GameObject Arjuna, Srikandi, Bima, Yudistira, NakuSadev, GatotKaca, Drupada, Kresna, Anoman;
	public GameObject skill1,skill2,skill3,skill4,skill5,skill6,skill7,skill8,skill9;
	public GameObject cdsword, cdarcher, cdfoot, cdgunner, cdmage, cdminer, cdspear, cdshaman, cdelephantry, cdtank;
	public GameObject cdArjuna, cdBima, cdSrikandi,cdYudistira,cdNakuSade,cdGatotKaca,cdDrupada,cdKresna,cdAnoman;
	public static bool hasSummonHero= false;
	public static string herosummoned, hasdied;
	public static bool arjunasummoned, bimasummoned, srikandisummoned, yudissummoned, nakusadesummoned, gatotkacasummoned, drupsummoned, kressummoned, anomsummoned;
	public static float resetercooldown;

	void Start(){
		resetercooldown = 0;
	}

	void Update(){
		if (hasdied == "arjuna") {
			Arjuna.SetActive(true);
			skill1.SetActive(false);
			cdArjuna.SetActive(false);
			cdArjuna.transform.Find("arjunaon").localScale = new Vector3(1,1,1);
			cdArjuna.SetActive(true);
			arjunasummoned = false;
			Unitcooldown.hasdied = "";
		}
		else if (hasdied == "bima") {
			Bima.SetActive(true);
			skill2.SetActive(false);
			cdBima.SetActive(false);
			cdBima.transform.Find("bimaon").localScale = new Vector3(1,1,1);
			cdBima.SetActive(true);
			bimasummoned = false;
			Unitcooldown.hasdied = "";
		}
		else if (hasdied == "srikandi") {
			Srikandi.SetActive(true);
			skill3.SetActive(false);
			cdSrikandi.SetActive(false);
			cdSrikandi.transform.Find("srikandion").localScale = new Vector3(1,1,1);
			cdSrikandi.SetActive(true);
			srikandisummoned = false;
			Unitcooldown.hasdied = "";
			AuraOffAll();
		}
		else if (hasdied == "yudistira") {
			Yudistira.SetActive(true);
			skill4.SetActive(false);
			cdYudistira.SetActive(false);
			cdYudistira.transform.Find("yudistiraon").localScale = new Vector3(1,1,1);
			cdYudistira.SetActive(true);
			yudissummoned = false;
			Unitcooldown.hasdied = "";
			AuraOffAll();
		}
		else if (hasdied == "nakusade") {
			NakuSadev.SetActive(true);
			skill5.SetActive(false);
			cdNakuSade.SetActive(false);
			cdNakuSade.transform.Find("nakulasadewaon").localScale = new Vector3(1,1,1);
			cdNakuSade.SetActive(true);
			nakusadesummoned = false;
			Unitcooldown.hasdied = "";
		}
		else if (hasdied == "gatotkaca") {
			GatotKaca.SetActive(true);
			skill6.SetActive(false);
			cdGatotKaca.SetActive(false);
			cdGatotKaca.transform.Find("gatotkacaon").localScale = new Vector3(1,1,1);
			cdGatotKaca.SetActive(true);
			gatotkacasummoned = false;
			Unitcooldown.hasdied = "";
		}
		else if (hasdied == "drupada") {
			Drupada.SetActive(true);
			skill7.SetActive(false);
			cdDrupada.SetActive(false);
			cdDrupada.transform.Find("drupadaon").localScale = new Vector3(1,1,1);
			cdDrupada.SetActive(true);
			drupsummoned = false;
			Unitcooldown.hasdied = "";
			AuraOffAll();
		}
		else if (hasdied == "kresna") {
			Kresna.SetActive(true);
			skill8.SetActive(false);
			cdKresna.SetActive(false);
			cdKresna.transform.Find("kresnaon").localScale = new Vector3(1,1,1);
			cdKresna.SetActive(true);
			kressummoned = false;
			Unitcooldown.hasdied = "";
		}
		else if (hasdied == "anoman") {
			Anoman.SetActive(true);
			skill9.SetActive(false);
			cdAnoman.SetActive(false);
			cdAnoman.transform.Find("anomanon").localScale = new Vector3(1,1,1);
			cdAnoman.SetActive(true);
			anomsummoned = false;
			Unitcooldown.hasdied = "";
		}
	}

	void AuraOffAll()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject go in gos) {
			UnitAtt attri = go.GetComponent<UnitAtt>();
			SingleTargetBattleP friend = go.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = go.GetComponent<RangecheckP>();
			if (friend != null){
				if(attri.ATKSPD == friend.atkspd){
					friend.atkspd = attri.ATKSPD;
				}
			}
			else if(friendsr != null){
				if(attri.ATKSPD == friendsr.atkspd){
					friendsr.atkspd = attri.ATKSPD;
				}
			}
		}
	}
}
