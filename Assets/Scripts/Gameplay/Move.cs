﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	UnitAtt att;
	public float speed,sped, initspeed;
	public float health;
	private float maxHealth;
	private Animator animPlayer;
	private Animator animEnemy;
	private GameObject[] enemyArray;
	private bool isHurt80 = true;
	private bool isHurt60 = false;
	private bool isHurt40 = false;
	private bool isHurt20 = false;
	public GameObject bullet;
	private GameObject[] gameSpeedArray;
	public GameObject damagePopup, dead;
	public GameObject bulletP;
	private GameObject spawnerBullet;
	private GameObject damagePopupP;
	public GameObject hero1,hero2,hero3,hero4,hero5,hero6,hero7,hero8,hero9;
	private GameObject spawnerBulletE, spawnerBulletP, spawnPointE, spawnPointP;
	private GameObject healthBarP;
	private float spawnerBulletDistance, spawnPointDistance;
	public float scalar;
	private Vector3 charPosition;
	private Vector3 healthPosition, healthBGPosition;
	private int yposition;
	private bool showHealthBar;
	private CameraControl cam;
	public static int player1 = 0;
	public static int player0 = 0;
	public static int player_1 = 0;
	private AuraHeal aheal;
	void Start(){
		att=GetComponent<UnitAtt>();
		cam = Camera.main.GetComponent<CameraControl> ();
		animPlayer = gameObject.GetComponent<Animator> ();
		aheal = gameObject.GetComponentInChildren<AuraHeal> ();

		spawnerBulletE = GameObject.Find ("SpawnerBulletE");
		spawnerBulletP = GameObject.Find ("SpawnerBulletP");
		spawnerBulletDistance = spawnerBulletE.transform.position.x - spawnerBulletP.transform.position.x;

		spawnPointE = GameObject.Find ("SpawnPointE");
		spawnPointP = GameObject.Find ("SpawnPointP");
		spawnPointDistance = spawnPointE.transform.position.x - spawnPointP.transform.position.x;

		scalar = (spawnPointDistance/spawnerBulletDistance);

		initspeed = att.SPD;
		health = att.HP;
		maxHealth = att.HP;

		float increaseMovementSpeed = PlayerPrefs.GetFloat ("MovementSpeed");
		initspeed = initspeed + (initspeed * increaseMovementSpeed);
		sped = initspeed;
		speed = sped;

		float increaseHealthPoint = PlayerPrefs.GetFloat ("HealthPoint");
		health = health + (health * increaseHealthPoint);

		healthBarP = transform.Find ("healthUnit").gameObject;

		bulletP = GameObject.Instantiate (bullet, spawnerBulletP.transform.position, spawnerBulletP.transform.rotation) as GameObject;
		bulletP.transform.parent = Camera.main.transform;
		showHealthBar = false;
		
		movePlayer ();

	}
	// Update is called once per frame
	void Update () {
		if (CastleEnemy.hasPlayerWin == true || CastlePlayer.hasEnemyWin == true) {
			Destroy (gameObject);
		}
		if (MoveE.hasBossDie == true || (MoveE.boss1Die == true && MoveE.boss2Die == true)) {
			Destroy (gameObject);	
		}
		if (cam.bosslevel == true) {
			if (yposition == 1) {
				//transform.Translate (Vector2.right * speed * Time.deltaTime);
				if (gameObject.transform.position.y <= -11 && gameObject.transform.position.x >=-2.5) {
					transform.Translate (Vector2.up * speed * Time.deltaTime);
					bulletP.transform.Translate (Vector2.right * speed/(spawnPointDistance/spawnerBulletDistance) * Time.deltaTime);
				}	
			}
			if (yposition == -1) {
				//transform.Translate (Vector2.right * speed * Time.deltaTime);
				if (gameObject.transform.position.y >= -13) {
					transform.Translate (-Vector2.up * speed * Time.deltaTime);
					bulletP.transform.Translate (Vector2.right * speed/(spawnPointDistance/spawnerBulletDistance) * Time.deltaTime);
				}	
			}
			if (gameObject.transform.position.x >= 140 && SpawnerE.bossHasAppear == false) {
				transform.Translate (Vector2.right * 0);
				bulletP.transform.Translate (Vector2.right * 0);
				animPlayer.SetBool ("Idle", true);
			}
			else {
				transform.Translate (Vector2.right * speed * Time.deltaTime);
				bulletP.transform.Translate (Vector2.right * speed/(spawnPointDistance/spawnerBulletDistance) * Time.deltaTime);
				animPlayer.SetBool ("Idle", false);
			}
		}
		else {
			transform.Translate (Vector2.right * speed * Time.deltaTime);
			bulletP.transform.Translate (Vector2.right * speed/(spawnPointDistance/spawnerBulletDistance) * Time.deltaTime);
			if (yposition == 2) {
				if (gameObject.transform.position.y <= -9 && gameObject.transform.position.x >=-2.5) {
					transform.Translate (Vector2.up * speed * Time.deltaTime);
				}
			}
			if (yposition == 1) {
				if (gameObject.transform.position.y <= -11 && gameObject.transform.position.x >=-2.5) {
					transform.Translate (Vector2.up * speed * Time.deltaTime);
				}	
			}
			if (yposition == -1) {
				if (gameObject.transform.position.y >= -13) {
					transform.Translate (-Vector2.up * speed * Time.deltaTime);
				}	
			}
			if (yposition == -2) {
				if (gameObject.transform.position.y >= -14) {
					transform.Translate (-Vector2.up * speed * Time.deltaTime);
				}	
			}
		}
		Vector3 scale = healthBarP.transform.localScale;
		if (gameObject.name == "Elephantryroot(Clone)" || gameObject.name == "tank_root(Clone)"||gameObject.name == "drupa_root(Clone)") {
			scale.x = (health * 2) / maxHealth;
			if (health <= 0) {
				scale.x = 0;	
			}
			else if (health >= maxHealth) {
				scale.x = 2;
			}
		}
		else {
			scale.x = (health * 1) / maxHealth;
			if (health <= 0) {
				scale.x = 0;
			}
			else if (health >= maxHealth) {
				scale.x = 1;
			}
		}
		healthBarP.transform.localScale = scale;
		if (CastleEnemy.hasPlayerWin == true) {
			gameObject.GetComponent<AudioSource>().volume = 0;	
		}
		if (showHealthBar == true) {
			gameObject.transform.Find("healthUnit").GetComponent<SpriteRenderer>().enabled = true;
			gameObject.transform.Find("healthUnitBack").GetComponent<SpriteRenderer>().enabled = true;
		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "Player" || coll.gameObject.tag == " tileP" ) {
			Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(),coll.gameObject.GetComponent<Collider2D>());		
		}
	}

	public void colldetect(float damage){
		if (health > 0) {
			health -= damage;
			showHealthBar = true;
			Vector3 yPosition = new Vector3(0, 4, 0);
			damagePopupP = Instantiate (damagePopup, gameObject.transform.position+yPosition, Quaternion.identity) as GameObject;
			damagePopupP.GetComponent<EasyFontTextMesh>().Text = damage.ToString();
			damagePopupP.transform.parent = transform;
			if(health < (att.HP * 0.8) && isHurt80) {
				animPlayer.SetTrigger("Hurt");
				isHurt80 = false;
				isHurt60 = true;
			}
			else if (health < (att.HP * 0.6) && isHurt60) {
				animPlayer.SetTrigger("Hurt");
				isHurt60 = false;
				isHurt40 = true;
			}
			else if (health < (att.HP * 0.4) && isHurt40) {
				animPlayer.SetTrigger("Hurt");
				isHurt40 = false;
				isHurt20 = true;
			}
			else if (health < (att.HP * 0.2) && isHurt20) {
				animPlayer.SetTrigger("Hurt");
				isHurt20 = false;
			}
			if(health <= 0){
				if(aheal != null){
					aheal.AuraOffAll("Player");
				}
				Instantiate(dead,gameObject.transform.position+new Vector3(0,1.5f,0),gameObject.transform.rotation);
				playAnimEnemy();
                Time.timeScale = 1; // reset time to normal state
                Skillclick.Useskill = false; // Reset Skill usage after Dead
                
				Destroy (gameObject);
				Destroy (bulletP);
				if(gameObject == hero1){
					Unitcooldown.hasdied = "arjuna";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero2){
					Unitcooldown.hasdied = "bima";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero3){
					Unitcooldown.hasdied = "srikandi";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero4){
					Unitcooldown.hasdied = "yudistira";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero5){
					if(transform.GetComponentInChildren<Meteodown>() != null){
						Skillclick.Useskill = false;
						gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
						if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
							Time.timeScale = 1;
						}
						else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
							Time.timeScale = 2;
						}
						else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
							Time.timeScale = 3;
						}
					}
					Unitcooldown.hasdied = "nakusade";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero6){
					Unitcooldown.hasdied = "gatotkaca";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero7){
					Unitcooldown.hasdied = "drupada";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero8){
					Unitcooldown.hasdied = "kresna";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else if(gameObject == hero9){
					Skillclick.Useskill = false;
					gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
					if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
						Time.timeScale = 1;
					}
					else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
						Time.timeScale = 2;
					}
					else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
						Time.timeScale = 3;
					}
					Unitcooldown.hasdied = "anoman";
					Unitcooldown.herosummoned="";
					Unitcooldown.hasSummonHero = false;
				}
				else {
					GameFlow.unitNumbers -= 1;
				}
			}
		} 
	}
	void playAnimEnemy() {
		enemyArray = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject enemy in enemyArray) {
			if (enemy != GameObject.Find("EnemyCastle")) {
				animEnemy = enemy.GetComponent<Animator>();
				animEnemy.SetBool("Kill",true);
				animEnemy.speed = 1;
			}
		}
	}
	void movePlayer() {
		Vector3 finalposition = new Vector3(0, 0, 0);
		if (cam.bosslevel == true) {
			if (gameObject == hero1) {
				finalposition = new Vector3 (0, 0, 25);
				yposition = 1;
			}
			if (gameObject == hero2) {
				finalposition = new Vector3 (0, 0, 15);
				yposition = 0;
			}
			if (gameObject == hero3) {
				finalposition = new Vector3 (0, 0, 5);
				yposition = -1;
			}
			if (gameObject == hero4) {
				finalposition = new Vector3 (0, 0, 25);
				yposition = 1;
			}
			if (gameObject == hero5) {
				finalposition = new Vector3 (0, 0, 15);
				yposition = 0;
			}
			if (gameObject == hero6) {
				finalposition = new Vector3 (0, 0, 5);
				yposition = -1;
			}
			if (gameObject == hero7) {
				finalposition = new Vector3 (0, 8, 25);
				yposition = 1;
			}
			if (gameObject == hero8) {
				finalposition = new Vector3 (0, 0, 15);
				yposition = 0;
			}
			if (gameObject == hero9) {
				finalposition = new Vector3 (0, 0, 5);
				yposition = -1;
			}
			gameObject.transform.position += finalposition;
		}
		else {
			Vector3 position = new Vector3 (0, Random.Range(-1, 2), 0);
			if (position.y == 1) {
				finalposition = new Vector3 (0, 0, (player1*0.5f)+55);
				yposition = 1;
				player1 += 2;
				if (player1 >= 40) {
					player1 = 0;
				}
			}
			if (position.y == 0) {
				finalposition = new Vector3 (0, 0, (player0*0.5f)+30);
				yposition = 0;
				player0 += 2;
				if (player0 >= 40) {
					player0 = 0;
				}
			}
			if (position.y == -1) {
				finalposition = new Vector3 (0, 0, (player_1*0.5f)+1);
				yposition = -1;
				player_1 += 2;
				if (player_1 >= 40) {
					player_1 = 0;
				}
			}
			if(gameObject == hero1 || gameObject == hero2 || gameObject == hero3 || 
			   gameObject == hero4 || gameObject == hero5 || gameObject == hero6 || 
			   gameObject == hero8 || gameObject == hero9){
				finalposition = new Vector3 (0, 0, 0);
				yposition = -2;
			}
			if (gameObject == hero7) {
				finalposition = new Vector3 (0, 8, (player1*0.5f)+65);
				yposition = 0;
				player1 += 2;
				if (player1 >= 40) {
					player1 = 0;
				}
			}
			if(gameObject.name == "Elephantryroot(Clone)") {
				finalposition = new Vector3 (0, 0, (player1*0.5f)+65);
				yposition = 1;
				player1 += 2;
				if (player1 >= 40) {
					player1 = 0;
				}
			}
			if(gameObject.name == "tank_root(Clone)") {
				finalposition = new Vector3 (0, 0, (player1*0.5f)+60);
				yposition = 2;
				player1 += 2;
				if (player1 >= 40) {
					player1 = 0;
				}
			}
			gameObject.transform.position += finalposition;
		}
	}
}