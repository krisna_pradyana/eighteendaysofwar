﻿using UnityEngine;
using System.Collections;

public class AuraAtkSpd : MonoBehaviour {
	private UnitAtt att;
	private Skillclick sc;
	private RangecheckP rngch;
	public float rng, atkspeed;
	private CircleCollider2D splas;
	private float duration = 0;
	public GameObject EffectAtkspd;
	// Use this for initialization
	void Start () {
		sc = transform.root.GetComponent<Skillclick> ();
		att = transform.root.GetComponent<UnitAtt>();
		rngch = transform.root.GetComponent<RangecheckP>();
		splas = transform.GetComponent<CircleCollider2D>();
	}
	
	void Update(){
		if(Time.time >= duration && duration != 0){
			rng = 1;
			rngch.atkspd = att.ATKSPD;
			if(gameObject.transform.root.Find("Charm") != null){
				Destroy(gameObject.transform.root.Find("Charm").gameObject);
			}
			AuraOffAll();
			duration = 0;
		}
		if (rng == sc.range) {
			if(rngch.atkspd == att.ATKSPD){
				GameObject ef;
				duration = Time.time + att.skillduration;
				splas.radius = rng;
				rngch.atkspd -= atkspeed;
				ef=Instantiate (EffectAtkspd, transform.position, Quaternion.identity) as GameObject;
				ef.transform.parent = gameObject.transform.root;
				ef.transform.localPosition = new Vector3(0,0.5f,0);
				ef.name = EffectAtkspd.name;
			}
			AuraOn();
			AuraOff();
		}
	}

	void AuraOn()
	{
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(gameObject.transform.position, rng);
		foreach (Collider2D col in objectsInRange)
		{
			UnitAtt attri = col.GetComponent<UnitAtt>();
			SingleTargetBattleP friend = col.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = col.GetComponent<RangecheckP>();
			if (friend != null){
				if(attri.ATKSPD == friend.atkspd){
					GameObject ef;
					ef=Instantiate (EffectAtkspd, col.transform.position, Quaternion.identity) as GameObject;
					ef.transform.parent = col.transform.root;
					ef.transform.localPosition = new Vector3(0,0.5f,0);
					ef.name = EffectAtkspd.name;
					friend.atkspd -= atkspeed;
				}
			}
			else if(friendsr != null){
				if(attri.ATKSPD == friendsr.atkspd){
					GameObject ef;
					ef=Instantiate (EffectAtkspd, col.transform.position, Quaternion.identity) as GameObject;
					ef.transform.parent = col.transform.root;
					ef.transform.localPosition = new Vector3(0,0.5f,0);
					ef.name = EffectAtkspd.name;
					friendsr.atkspd -= atkspeed;
				}
			}
		}
	}

	void AuraOff()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			UnitAtt attri = go.GetComponent<UnitAtt>();
			SingleTargetBattleP friend = go.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = go.GetComponent<RangecheckP>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff > rng*2) {
				if (friend != null){
					if(attri.ATKSPD != friend.atkspd){
						if(go.transform.Find("Charm") != null){
							Destroy(go.transform.Find("Charm").gameObject);
						}
						friend.atkspd = attri.ATKSPD;
					}
				}
				else if(friendsr != null){
					if(attri.ATKSPD != friendsr.atkspd){
						if(go.transform.Find("Charm") != null){
							Destroy(go.transform.Find("Charm").gameObject);
						}
						friendsr.atkspd = attri.ATKSPD;
					}
				}
			}
		}
	}

	void AuraOffAll()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject go in gos) {
			UnitAtt attri = go.GetComponent<UnitAtt>();
			SingleTargetBattleP friend = go.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = go.GetComponent<RangecheckP>();
			if (friend != null){
				if(attri.ATKSPD != friend.atkspd){
					if(go.transform.Find("Charm") != null){
						Destroy(go.transform.Find("Charm").gameObject);
					}
					friend.atkspd = attri.ATKSPD;
				}
			}
			else if(friendsr != null){
				if(attri.ATKSPD != friendsr.atkspd){
					if(go.transform.Find("Charm") != null){
						Destroy(go.transform.Find("Charm").gameObject);
					}
					friendsr.atkspd = attri.ATKSPD;
				}
			}
		}
	}
}
