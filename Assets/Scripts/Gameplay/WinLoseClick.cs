﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class WinLoseClick : MonoBehaviour {
	private GameObject square;
	public string nextLevel;
	//private const string FACEBOOK_APP_ID = "406922682792466";
	//private const string FACEBOOK_URL = "http://www.facebook.com/dialog/feed";

	// Your app’s unique identifier.
	private string AppID = "1518031601790532";
	
	// The link attached to this post.
	private string Link = "https://www.facebook.com/MahabharatWars";
	
	// The URL of a picture attached to this post. The picture must be at least 200px by 200px.
	private string Picture = "http://falkanastudio.com/mwicon.png";
	
	// The name of the link attachment.
	private string Name = "Mahabharat Warriors";
	
	// The caption of the link (appears beneath the link name).
	public string Caption = "I just beat day 1-1. Can you beat it?";
	
	// The description of the link (appears beneath the link caption). 
	private string Description = "Pandawa warriors are waiting for you. Click this link to download Mahabharat Warriors and let us defeat Kurawa Armies together.";
	
	private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	private const string TWEET_LANGUAGE = "en";

    public EnergyContainer EnContainer;
	private CameraControl cam;
	public GameObject energyPopupUI, adsPopupUI;
	// Use this for initialization
	void Start () {
		square = GameObject.Find ("Main Camera/Square");
		if (GameObject.Find("Energy Container") != null)
		{
			EnContainer = GameObject.Find("Energy Container").GetComponent<EnergyContainer> ();
		}
		cam = Camera.main.GetComponent<CameraControl>();
		if (Application.loadedLevelName == "Survival") {
			Caption = "I have survived "+SurvivalController.wave+" wave of Kurawa Armies. Can you survive them?";		
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (gameObject.name == "Retry Container" || gameObject.name == "Restart Container") {
			if (PlayerPrefs.GetInt("Energy") >= cam.retryCost) {
				//square.GetComponent<SpriteRenderer>().enabled = false;
				Application.LoadLevel(Application.loadedLevel);
				GameFlow.hasPaused = false;
				GameFlow.hasResumed = false;
				Unitcooldown.hasdied = "";
				Unitcooldown.hasSummonHero = false;
				Unitcooldown.herosummoned = "";
				CastleEnemy.hasPlayerWin = false;
				Generate.money = 0;
				Generate.increaseFromMiner = 0;
				IncreaseSilver.counterLimit = 0;
                //EnContainer.DecreaseEnergy(cam.retryCost);
				Checkunit.hero1select = "";
				Checkunit.hero2select = "";
				Checkunit.hero3select = "";
				InitUnit.readytoinit = false;
				MoveE.boss1Die = false;
				MoveE.boss2Die = false;
				MoveE.hasBossDie = false;
				Time.timeScale = 1;

			}
			else
            {
				energyPopupUI.SetActive(true);
			}
		}
		else if (gameObject.name == "Quit Container") {
//			int chance = Random.Range (0, 5);
//			if (chance == 2) {
//				energyPopupUI.SetActive (false);
//				adsPopupUI.SetActive (true);
//			}
//			else {
//				Application.LoadLevel("Shop");
//			}
			//square.GetComponent<SpriteRenderer>().enabled = false;
			GameFlow.hasPaused = false;
			GameFlow.hasResumed = false;
			Unitcooldown.hasdied = "";
			Unitcooldown.hasSummonHero = false;
			Unitcooldown.herosummoned = "";
			CastleEnemy.hasPlayerWin = false;
			Generate.money = 0;
			Generate.increaseFromMiner = 0;
			IncreaseSilver.counterLimit = 0;
			MoveE.hasBossDie = false;
			MoveE.boss1Die = false;
			MoveE.boss2Die = false;
			Time.timeScale = 1;
            Application.LoadLevel("MainMenu");
            //UnityAdsLoading.adsReward = "Shop";
            //Application.LoadLevel ("AdMobLoading");
		}
		else if (gameObject.name == "Next Container") {
//			int chance = Random.Range (0, 5);
//			if (chance == 2) {
//				energyPopupUI.SetActive (false);
//				adsPopupUI.SetActive (true);
//			}
//			else {
//				Application.LoadLevel(nextLevel);
//			}



			//square.GetComponent<SpriteRenderer>().enabled = false;
			GameFlow.hasPaused = false;
			GameFlow.hasResumed = false;
			Unitcooldown.hasdied = "";
			Unitcooldown.hasSummonHero = false;
			Unitcooldown.herosummoned = "";
			CastleEnemy.hasPlayerWin = false;
			Generate.money = 0;
			Generate.increaseFromMiner = 0;
			IncreaseSilver.counterLimit = 0;
			Checkunit.playOnce = false;
			MoveE.boss1Die = false;
			MoveE.boss2Die = false;
			MoveE.hasBossDie = false;
			Time.timeScale = 1;
            //GoldContainer.addGold();
            Application.LoadLevel(nextLevel);
            //UnityAdsLoading.adsReward = "Shop";
            //Application.LoadLevel ("AdMobLoading");
		}
		else if (gameObject.name == "FB") {
			ShareScoreOnFB();
			/*ShareToFacebook ("https://www.facebook.com/riady.despada",
			                 "Testing",
			                 "New High Score",
			                 "Just ignore this message. its just a test", 
			                 "http://img1.wikia.nocookie.net/__cb20111029135427/finalfantasy/images/thumb/c/c6/FFXIII-Lightning_CG.png/170px-FFXIII-Lightning_CG.png",
			                 "https://www.facebook.com");*/
		}
		else if (gameObject.name == "Twitter") {
			ShareToTwitter (Caption+"#mahabharatwarriors");
		}
	}
	/*void ShareToFacebook (string linkParameter, string nameParameter, string captionParameter, string descriptionParameter, string pictureParameter, string redirectParameter)
	{
		Application.OpenURL (FACEBOOK_URL + "?app_id=" + FACEBOOK_APP_ID +
		                     "&link=" + WWW.EscapeURL(linkParameter) +
		                     "&name=" + WWW.EscapeURL(nameParameter) +
		                     "&caption=" + WWW.EscapeURL(captionParameter) + 
		                     "&description=" + WWW.EscapeURL(descriptionParameter) + 
		                     "&picture=" + WWW.EscapeURL(pictureParameter) + 
		                     "&redirect_uri=" + WWW.EscapeURL(redirectParameter));
	}*/
	void ShareToTwitter (string textToDisplay)
	{
		Application.OpenURL(TWITTER_ADDRESS +
		                    "?text=" + WWW.EscapeURL(textToDisplay) +
		                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
	}
	void ShareScoreOnFB(){
		Application.OpenURL("https://www.facebook.com/dialog/feed?"+
		                    "app_id="+AppID+
		                    "&link="+Link+
		                    "&picture="+Picture+
		                    "&name="+ReplaceSpace(Name)+
		                    "&caption="+ReplaceSpace(Caption)+
		                    "&description="+ReplaceSpace(Description)+
		                    "&redirect_uri=https://facebook.com/");
	}
	
	string ReplaceSpace (string val) {
		return val.Replace(" ", "%20");    
	}
}
