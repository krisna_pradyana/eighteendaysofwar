﻿using UnityEngine;
using System.Collections;

public class HealthBarEnemy : MonoBehaviour
{
	public GUISkin mySkin;
	public GUIText guiGameOver;
	public Texture2D imageStar;
	private Vector3 pos;
	private Texture2D background;
	private Texture2D foreground;
	private bool hasPlayerWin = false;
	
	public int curHealth = 100;
	public int maxHealth = 100;
	private int star = 0;
	private float healthBarLength;
	void Start()
	{
		healthBarLength = Screen.width / 8;
//		background = new Texture2D(1, 1, TextureFormat.RGB24, false);
//		foreground = new Texture2D(1, 1, TextureFormat.RGB24, false);
//		
//		background.SetPixel(0, 0, Color.red);
//		foreground.SetPixel(0, 0, Color.green);
//		
//		background.Apply();
//		foreground.Apply();
	}
	
	void Update()
	{
		pos = Camera.main.WorldToScreenPoint(transform.position); //The offsets are to position the health bar so it's placed above and centered to the character
		pos.y = Screen.height - pos.y;
	}
	
	void OnGUI()
	{
		GUI.skin = mySkin;
		GUI.Box (new Rect (pos.x-Screen.width/16, pos.y-70, Screen.width/8, 20), curHealth + "/" + maxHealth);
		GUI.Box (new Rect (pos.x-healthBarLength/2, pos.y-70, healthBarLength, 20), "");

		if (hasPlayerWin == true) {
			Time.timeScale = 0;
			GUI.Box (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), "");
			GUI.Label(new Rect(Screen.width/2-Screen.width/16, Screen.height/4+20, Screen.width/8, Screen.height/8), "You Win");
			if (star == 1) {
				GUI.Label(new Rect(Screen.width/4, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
			}
			if (star == 2) {
				GUI.Label(new Rect(Screen.width/4, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
				GUI.Label(new Rect(Screen.width/2-imageStar.width/6, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
			}
			if (star == 3) {
				GUI.Label(new Rect(Screen.width/4, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
				GUI.Label(new Rect(Screen.width/2-imageStar.width/6, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
				GUI.Label(new Rect(Screen.width/2+imageStar.width/6, Screen.height/2-(imageStar.width/6), imageStar.width/3, imageStar.height/3), imageStar);
			}
			if(GUI.Button(new Rect(Screen.width/4, Screen.height/2+(Screen.height/8+10), Screen.width/8, Screen.height/8-10), "Back to Menu")) {
				Application.LoadLevel("Play Game");
			}
			if(GUI.Button(new Rect(Screen.width/2-Screen.width/16, Screen.height/2+(Screen.height/8+10), Screen.width/8, Screen.height/8-10), "Restart")) {
				Application.LoadLevel(Application.loadedLevel);
			}
			if(GUI.Button(new Rect(Screen.width/2+Screen.width/8, Screen.height/2+(Screen.height/8+10), Screen.width/8, Screen.height/8-10), "Share FB")) {
			}
		}
//		Rect box = new Rect(pos.x-50, pos.y-70, 100, 20);
//		GUI.BeginGroup(box);
//		{
//			GUI.DrawTexture(new Rect(0, 0, box.width, box.height), background, ScaleMode.StretchToFill);
//			GUI.DrawTexture(new Rect(0, 0, box.width*health/maxHealth, box.height), foreground, ScaleMode.StretchToFill);
//		}
//		GUI.EndGroup(); ;
	}
	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player") {
			if (curHealth <= 0) {
				int curPlayerHealth = GameObject.Find("GatePlayer").GetComponent<HealthBarPlayer>().curHealth;
				int maxPlayerHealth = GameObject.Find("GatePlayer").GetComponent<HealthBarPlayer>().maxHealth;
				hasPlayerWin = true;
				if (curPlayerHealth == maxPlayerHealth) {
					star = 3;
				}
				if (curPlayerHealth/(float)maxPlayerHealth >= 0.5 && curPlayerHealth/(float)maxPlayerHealth <= 0.9) {
					star = 2;
				}
				if (curPlayerHealth/(float)maxPlayerHealth < 0.5) {
					star = 1;
				}
			}
			else {
				AdjustCurrentHealth (-10);
				Destroy (col.gameObject);
			}

		}
	}
	public void AdjustCurrentHealth (int adj) {
		curHealth += adj;
		if (curHealth < 0) {
			curHealth = 0;		
		}
		if (curHealth > maxHealth) {
			curHealth = maxHealth;		
		}
		healthBarLength = (Screen.width / 8) * (curHealth / (float)maxHealth);
	}
}