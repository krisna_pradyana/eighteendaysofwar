﻿using UnityEngine;
using System.Collections;

public class Pillar : MonoBehaviour {
	private float rng, dmg;
	private CircleCollider2D col;
	private GameObject[] gameSpeedArray;
	private Animator animPlayer;
	private Move mov;
	private GameObject checker;
	// Use this for initialization
	void Start () {
		col = GetComponent<CircleCollider2D> ();
		rng = col.radius;
		mov = transform.root.GetComponent<Move> ();
		dmg = transform.root.GetComponent<SingleTargetBattleP> ().atak*4f;
		animPlayer = transform.root.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale != 0) {
			checker = transform.root.Find("ultiano_000").gameObject;
			if(checker.activeSelf == true){
				gameSpeedArray = GameObject.FindGameObjectsWithTag ("GameSpeed");
				AreaDamage();
				if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "1x Container") {
					Time.timeScale = 1;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "2x Container") {
					Time.timeScale = 2;
				}
				else if (gameSpeedArray[0].activeInHierarchy == true && gameSpeedArray[0].name == "3x Container") {
					Time.timeScale = 3;
				}
				Skillclick.Useskill = false;
				animPlayer.SetBool("Skill",false);
				mov.sped = mov.initspeed;
				Destroy (gameObject);
			}
		}
	}

	void AreaDamage(){
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(transform.root.position, rng+4);
		foreach (Collider2D col in objectsInRange)
		{
			MoveE enemy = col.GetComponent<MoveE>();
			if (enemy != null)
			{
				int damag = Mathf.RoundToInt(Random.Range(dmg,dmg*11/10));
				enemy.colldetect(damag);
			}
		}
	}
}
