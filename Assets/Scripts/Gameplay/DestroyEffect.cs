﻿using UnityEngine;
using System.Collections;

public class DestroyEffect : MonoBehaviour {
	public float delay;
	// Use this for initialization
	void Start () {
		delay =	Time.time + 0.6f;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= delay) {
			Destroy(gameObject);		
		}
	}
}
