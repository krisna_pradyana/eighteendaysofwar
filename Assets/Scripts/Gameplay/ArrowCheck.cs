﻿using UnityEngine;
using System.Collections;

public class ArrowCheck : MonoBehaviour {
	public GameObject parent;
	private GameObject mas, target,root;
	Vector3 tgt, init, rightch;
	float fracComplete =0;
	public GameObject explode;
	public GameObject unit;
	private UnitAtt att;
	private float attack;
	void Start(){
		att = unit.GetComponent<UnitAtt> ();
		attack = att.ATK;
		if (gameObject.tag == "ProjectileP") {
			float increaseAttackPower = PlayerPrefs.GetFloat ("AttackPower");
			attack = attack + (attack * increaseAttackPower);
			mas = transform.parent.Find("Massp").gameObject;	
		}
		else if(gameObject.tag == "ProjectileE"){
			mas = transform.parent.Find("Masse").gameObject;
		}
		init = mas.transform.position;
		root = transform.root.gameObject;
	}

	void FixedUpdate(){
		if (gameObject.tag == "ProjectileP") {
			FindClosestEnemy ();
			FindEnemyCastle();
		}
		else if(gameObject.tag == "ProjectileE"){
			FindClosestPlayer ();
			FindPlayerCastle();
		}
		if (target != null && target.tag != "Untagged") {
			tgt = target.transform.position;
			Arrowtraject();	
		}
		else{
			Destroy(parent.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D coll){
		int dmg = Mathf.RoundToInt(Random.Range(attack,attack*11/10));
		if (coll.gameObject.tag == "Enemy" && gameObject.tag == "ProjectileP") {
			if (explode != null) {
				Instantiate (explode, coll.transform.position+new Vector3(0,2,-10), coll.transform.rotation);
			}
			coll.GetComponent<MoveE>().colldetect(dmg);
			CastleEnemy.enemyCastleAttacked = false;
			Destroy(parent.gameObject);
		}
		else if (coll.gameObject.tag == "Player" && gameObject.tag == "ProjectileE") {
			if (explode != null) {
				Instantiate (explode, coll.transform.position+new Vector3(0,2,-10), coll.transform.rotation);
			}
			coll.GetComponent<Move>().colldetect(dmg);
			Destroy(parent.gameObject);
		}
		if (coll.gameObject.tag == "EnemyCastle" && gameObject.tag == "ProjectileP") {
			int randomX = Random.Range (-5, 0);
			int randomY = Random.Range (-4, 3);
			if (explode != null) {
				Instantiate(explode,target.transform.position+new Vector3(randomX,randomY,-10),target.transform.rotation);
			}
			if (coll.gameObject.name == "EnemyCastle") {
				coll.GetComponent<CastleEnemy>().colldetect(dmg);
			}
			else {
				coll.GetComponent<MiniTower>().colldetect(dmg);
			}
			CastleEnemy.enemyCastleAttacked = true;
			Destroy(parent.gameObject);
		}
		else if (coll.gameObject.tag == "PlayerCastle" && gameObject.tag == "ProjectileE") {
			int randomX = Random.Range (0, 5);
			int randomY = Random.Range (-4, 3);
			if (explode != null) {
				Instantiate(explode,target.transform.position+new Vector3(randomX,randomY,-10),target.transform.rotation);
			}
			coll.GetComponent<CastlePlayer>().colldetect(dmg);
			Destroy(parent.gameObject);
		}
	}

	void FindClosestEnemy() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = Mathf.Infinity;
		Vector3 position = root.transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= 4*1.5*4){
					target = go;
					distance = diff;
				}
			}
		}
	}

	void FindClosestPlayer() {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = root.transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= 4*1.5*4){
					target = go;
					distance = diff;
				}
			}
		}
	}

	void FindPlayerCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("PlayerCastle");
		float distance = Mathf.Infinity;
		Vector3 position = root.transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= 4*1.5*4){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}

	void FindEnemyCastle(){
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("EnemyCastle");
		float distance = Mathf.Infinity;
		Vector3 position = root.transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(diff <= 4*1.5*4){
					if(target == null){
						target = go;
					}
				}
				distance = diff;
			}
		}
	}

	void Arrowtraject(){
		tgt = new Vector3 (tgt.x, tgt.y, init.z);
		rightch = (init + tgt) * 0.5F;
		rightch -= new Vector3(0, 1, 0);
		Vector3 riseRelCenter = init - rightch;
		Vector3 setRelCenter = tgt - rightch;
		fracComplete += 0.02f;
		mas.transform.position = Vector3.Slerp(riseRelCenter, setRelCenter, fracComplete);
		mas.transform.position += rightch;
	}
}