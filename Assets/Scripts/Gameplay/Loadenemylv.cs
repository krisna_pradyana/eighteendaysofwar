﻿using UnityEngine;
using System.Collections;

public class Loadenemylv : MonoBehaviour {
	public int lvkfoot, lvkarcher, lvksword, lvkgun, lvkspear, lvkmage, lvkshaman, lvkelephant, lvktank;
	private int hpbasefoot, hpbasearcher, hpbasesword, hpbasegun,hpbasespear,hpbasemage, hpbaseshaman, hpbaseelephant, hpbasetank;
	private int atkbasefoot, atkbasearcher, atkbasesword, atkbasegun,atkbasespear,atkbasemage, atkbaseshaman, atkbaseelephant, atkbasetank;
	private int hpivfoot, hpivarcher, hpivsword, hpivgun, hpivspear, hpivmage, hpivshaman, hpivelephant, hpivtank;
	private int atkivfoot, atkivarcher, atkivsword, atkivgun, atkivspear, atkivmage, atkivshaman, atkivelephant, atkivtank;
	public GameObject kfoot, karcher, ksword, kgun, kspear, kmage, kshaman, kelephant, ktank;
	private int curhp, curatk;
	// Use this for initialization
	void Start () {
		hpbasefoot = 200;
		hpbasearcher = 150;
		hpbasesword = 440;
		hpbasegun = 225;
		hpbasespear = 440;
		hpbasemage = 150;
		hpbaseshaman = 200;
		hpbaseelephant = 1000;
		hpbasetank = 3000;

		atkbasefoot = 20;
		atkbasearcher = 18;
		atkbasesword = 44;
		atkbasegun = 27;
		atkbasespear = 44;
		atkbasemage = 40;
		atkbaseshaman = 10;
		atkbaseelephant = 30;
		atkbasetank = 100;

		hpivfoot = 20;
		hpivarcher = 15;
		hpivsword = 30;
		hpivgun = 22;
		hpivspear = 30;
		hpivmage = 15;
		hpivshaman = 16;
		hpivelephant = 80;
		hpivtank = 40;

		atkivfoot = 3;
		atkivarcher = 2;
		atkivsword = 6;
		atkivgun = 3;
		atkivspear = 6;
		atkivmage = 6;
		atkivshaman = 2;
		atkivelephant = 4;
		atkivtank = 8;

		if (Application.loadedLevelName == "Stage1-1" || Application.loadedLevelName == "Stage1-2" || Application.loadedLevelName == "Stage1-3") {
			normalize (hpbasefoot, atkbasefoot, kfoot);
			normalize (hpbasearcher, atkbasearcher, karcher);
			normalize (hpbasesword, atkbasesword, ksword);
			normalize (hpbasegun, atkbasegun, kgun);
			normalize (hpbasespear, atkbasespear, kspear);
			normalize (hpbasemage, atkbasemage, kmage);
		}
		else {
			formula (hpbasefoot, hpivfoot, atkivfoot, lvkfoot, atkbasefoot, kfoot);
			formula (hpbasearcher, hpivarcher, atkivarcher, lvkarcher, atkbasearcher, karcher);
			formula (hpbasesword, hpivsword, atkivsword, lvksword, atkbasesword, ksword);
			formula (hpbasegun, hpivgun, atkivgun, lvkgun, atkbasegun, kgun);
			formula (hpbasespear, hpivspear, atkivspear, lvkspear, atkbasespear, kspear);
			formula (hpbasemage, hpivmage, atkivmage, lvkmage, atkbasemage, kmage);
			if (Application.loadedLevel >= 37 && Application.loadedLevel <= 69) {
				formula (hpbaseshaman, hpivshaman, atkivshaman, lvkshaman, atkbaseshaman, kshaman);
				formula (hpbaseelephant, hpivelephant, atkivelephant, lvkelephant, atkbaseelephant, kelephant);
				formula (hpbasetank, hpivtank, atkivtank, lvktank, atkbasetank, ktank);
			}
		}
	}

	void formula(int hpbase,int hpiv, int atkiv, int lv,int atkbase, GameObject unit){
		if (unit != null) {
			for (int i = 2; i <= lv; i++) {
				curhp = hpbase +(i*hpiv);
				hpbase = curhp;
				curatk = atkbase + (i*atkiv);	
				atkbase = curatk;
			}
			unit.GetComponent<UnitAtt> ().HP = curhp;
			unit.GetComponent<UnitAtt> ().ATK = curatk;
		}

	}
	void normalize(int curhp, int curatk, GameObject unit) {
		if (unit != null) {
			unit.GetComponent<UnitAtt> ().HP = curhp;
			unit.GetComponent<UnitAtt> ().ATK = curatk;
		}

	}
}
