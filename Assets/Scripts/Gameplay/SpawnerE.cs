﻿using System.Collections;
using UnityEngine;

public class SpawnerE : MonoBehaviour
{
    #region Delegates
    public delegate void EnemySpawnEvent();
    #endregion

    #region Delegat Functions
    public static EnemySpawnEvent enemySpawn;
    #endregion

    public GameObject BulletE;
    public GUIText waveText;
    public int waveSpawnDelay = 5;      // The amount of time between each spawn.
    private int delayWave;
    public int enemiesSpawnDelay = 1;       // The amount of time before spawning starts.
    public GameObject kfootman, kswordman, karcherman, kgunnerman, kmage, kspearman, kshaman, kelephantry, ktank, boss, boss2;
    public static GameObject instantiatedBoss;
    public int amountEnemiesWave1;
    public int amountEnemiesWave2;
    public int amountEnemiesWave3;
    public int amountEnemiesWave4;
    public int amountEnemiesWave5;
    public int amountEnemiesWave6;
    public int amountEnemiesWave7;
    public int amountEnemiesWave8;

    public int amountBossWave = 1;
    public int amountBossWave2 = 1;

    private int amountKFootman = 0;
    private int amountKArcher = 0;
    private int amountKSwordman = 0;
    private int amountKGunner = 0;
    private int amountKSpearman = 0;
    private int amountKMage = 0;
    private int amountKShaman = 0;
    private int amountKElephantry = 0;
    private int amountKTank = 0;

    private int wave = 1, delay = 0;
    private CameraControl cam;
    public static bool bossHasAppear;

    private void OnEnable()
    {
        enemySpawn += StartToSpawnEnemy;
    }

    private void OnDisable()
    {
        enemySpawn -= StartToSpawnEnemy;
    }

    // Update is called once per frame
    void Start()
    {
        cam = Camera.main.GetComponent<CameraControl>();
        bossHasAppear = false;
        delayWave = 0;
    }

    public void StartToSpawnEnemy()
    {
        if (Application.loadedLevelName == "Stage1-6" || Application.loadedLevelName == "Stage2-6" || Application.loadedLevelName == "Stage3-6" || Application.loadedLevelName == "Stage4-6" ||
            Application.loadedLevelName == "Stage5-6" || Application.loadedLevelName == "Stage6-6" || Application.loadedLevelName == "Stage7-6" || Application.loadedLevelName == "Stage8-6" ||
            Application.loadedLevelName == "Stage9-6" )
        {
            StartCoroutine(BossWave());
        }
        else if (Application.loadedLevelName == "Stage1-1" || Application.loadedLevelName == "Stage1-2" || Application.loadedLevelName == "Stage1-3")
        {
            StartCoroutine(waveDay1());
        }
        else if (Application.loadedLevelName == "Stage1-4" || Application.loadedLevelName == "Stage1-5")
        {
            StartCoroutine("waveDay2");
        }
        else if (Application.loadedLevelName == "Stage2-1" || Application.loadedLevelName == "Stage2-2" || Application.loadedLevelName == "Stage2-3")
        {
            StartCoroutine("waveDay3");
        }
        else if (Application.loadedLevelName == "Stage2-4" || Application.loadedLevelName == "Stage2-5")
        {
            StartCoroutine("waveDay4");
        }
        else if (Application.loadedLevelName == "Stage3-1" || Application.loadedLevelName == "Stage3-2" || Application.loadedLevelName == "Stage3-3")
        {
            StartCoroutine("waveDay5");
        }
        else if (Application.loadedLevelName == "Stage3-4" || Application.loadedLevelName == "Stage3-5")
        {
            StartCoroutine("waveDay6");
        }
        else if (Application.loadedLevelName == "Stage4-1" || Application.loadedLevelName == "Stage4-2" || Application.loadedLevelName == "Stage4-3")
        {
            StartCoroutine("waveDay7");
        }
        else if (Application.loadedLevelName == "Stage4-4" || Application.loadedLevelName == "Stage4-5")
        {
            StartCoroutine("waveDay8");
        }
        else if (Application.loadedLevelName == "Stage5-1" || Application.loadedLevelName == "Stage5-2" || Application.loadedLevelName == "Stage5-3")
        {
            StartCoroutine("waveDay9");
        }
        else if (Application.loadedLevelName == "Stage5-4" || Application.loadedLevelName == "Stage5-5")
        {
            StartCoroutine("waveDay10");
        }
        else if (Application.loadedLevelName == "Stage6-1" || Application.loadedLevelName == "Stage6-2" || Application.loadedLevelName == "Stage6-3")
        {
            StartCoroutine("waveDay11");
        }
        else if (Application.loadedLevelName == "Stage6-4" || Application.loadedLevelName == "Stage6-5")
        {
            StartCoroutine("waveDay12");
        }
        else if (Application.loadedLevelName == "Stage7-1" || Application.loadedLevelName == "Stage7-2" || Application.loadedLevelName == "Stage7-3")
        {
            StartCoroutine("waveDay13");
        }
        else if (Application.loadedLevelName == "Stage7-4" || Application.loadedLevelName == "Stage7-5")
        {
            StartCoroutine("waveDay14");
        }
        else if (Application.loadedLevelName == "Stage8-1" || Application.loadedLevelName == "Stage8-2" || Application.loadedLevelName == "Stage8-3")
        {
            StartCoroutine("waveDay15");
        }
        else if (Application.loadedLevelName == "Stage8-4" || Application.loadedLevelName == "Stage8-5")
        {
            StartCoroutine("waveDay16");
        }
        else if (Application.loadedLevelName == "Stage9-1" || Application.loadedLevelName == "Stage9-2" || Application.loadedLevelName == "Stage9-3")
        {
            StartCoroutine("waveDay17");
        }
        else if (Application.loadedLevelName == "Stage9-4" || Application.loadedLevelName == "Stage9-5")
        {
            StartCoroutine("waveDay18");
        }
    }

    void Update()
    {
        if (TutorialScenario.tutorialCanEnemySpawn == true)
        {
            if (amountKFootman < 8)
            {
                if (delay < 100)
                {
                    delay += 1;
                }
                else
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    delay = 0;
                    amountKFootman += 1;
                }
            }
            else
            {
                TutorialScenario.tutorialEnemyFinishSpawning = true;
            }
        }
        if (CastleEnemy.enemyCastleAttacked == true)
        {
            delayWave = waveSpawnDelay / 2;
        }
        else
        {
            delayWave = waveSpawnDelay;
        }

    }

    IEnumerator BossWave()
    {
        Debug.Log("Start Spawning Boss and enemies");
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage1-6")
        {
            if (wave == 1)
            {
                while (amountEnemiesWave1 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kfootman, transform.position, transform.rotation);
                    amountEnemiesWave1 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountEnemiesWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(karcherman, transform.position, transform.rotation);
                    amountEnemiesWave2 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage2-6")
        {
            if (wave == 1)
            {
                while (amountEnemiesWave1 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kfootman, transform.position, transform.rotation);
                    amountEnemiesWave1 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountEnemiesWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(karcherman, transform.position, transform.rotation);
                    amountEnemiesWave2 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountEnemiesWave3 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kswordman, transform.position, transform.rotation);
                    amountEnemiesWave3 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountEnemiesWave4 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    amountEnemiesWave4 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 5)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage3-6")
        {
            if (wave == 1)
            {
                while (amountEnemiesWave1 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kfootman, transform.position, transform.rotation);
                    amountEnemiesWave1 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountEnemiesWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(karcherman, transform.position, transform.rotation);
                    amountEnemiesWave2 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountEnemiesWave3 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kswordman, transform.position, transform.rotation);
                    amountEnemiesWave3 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountEnemiesWave4 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    amountEnemiesWave4 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 5)
            {
                while (amountEnemiesWave5 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kspearman, transform.position, transform.rotation);
                    amountEnemiesWave5 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 6)
            {
                while (amountEnemiesWave6 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kmage, transform.position, transform.rotation);
                    amountEnemiesWave6 -= 1;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 7)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;

            }
            yield return new WaitForSeconds(4);
            if (wave == 8)
            {
                while (amountBossWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss2, transform.position, transform.rotation);
                    amountBossWave2 -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage4-6")
        {
            if (wave == 1)
            {
                while (amountEnemiesWave1 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kfootman, transform.position, transform.rotation);
                    amountEnemiesWave1 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountEnemiesWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(karcherman, transform.position, transform.rotation);
                    amountEnemiesWave2 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountEnemiesWave3 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kswordman, transform.position, transform.rotation);
                    amountEnemiesWave3 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountEnemiesWave4 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    amountEnemiesWave4 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 5)
            {
                while (amountEnemiesWave5 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kspearman, transform.position, transform.rotation);
                    amountEnemiesWave5 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 6)
            {
                while (amountEnemiesWave6 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(kmage, transform.position, transform.rotation);
                    amountEnemiesWave6 -= 1;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 7)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                yield return new WaitForSeconds(enemiesSpawnDelay);
                Instantiate(kshaman, transform.position, transform.rotation);
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage5-6")
        {
            if (wave == 1)
            {
                while (amountKFootman < 3)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                amountKFootman = 0;
                while (amountKElephantry < 1)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKArcher < 2)
                {
                    Instantiate(karcherman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKArcher += 1;
                }
                amountKArcher = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountKSwordman < 3)
                {
                    Instantiate(kswordman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSwordman += 1;
                }
                amountKSwordman = 0;
                while (amountKElephantry < 1)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKGunner < 2)
                {
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKGunner += 1;
                }
                amountKGunner = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountKSpearman < 2)
                {
                    Instantiate(kspearman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSpearman += 1;
                }
                amountKSpearman = 0;
                while (amountKElephantry < 1)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKMage < 2)
                {
                    Instantiate(kmage, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKMage += 1;
                }
                amountKMage = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(4);
            if (wave == 5)
            {
                while (amountBossWave2 > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss2, transform.position, transform.rotation);
                    amountBossWave2 -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage6-6")
        {
            if (wave == 1)
            {
                while (amountKFootman < 2)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                amountKFootman = 0;
                while (amountKElephantry < 2)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKArcher < 2)
                {
                    Instantiate(karcherman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKArcher += 1;
                }
                amountKArcher = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountKSwordman < 2)
                {
                    Instantiate(kswordman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSwordman += 1;
                }
                amountKSwordman = 0;
                while (amountKElephantry < 1)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKGunner < 2)
                {
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKGunner += 1;
                }
                amountKGunner = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountKSpearman < 1)
                {
                    Instantiate(kspearman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSpearman += 1;
                }
                amountKSpearman = 0;
                while (amountKTank < 1)
                {
                    Instantiate(ktank, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKTank += 1;
                }
                amountKTank = 0;
                while (amountKMage < 1)
                {
                    Instantiate(kmage, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKMage += 1;
                }
                amountKMage = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage7-6")
        {
            if (wave == 1)
            {
                while (amountKFootman < 3)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                amountKFootman = 0;
                while (amountKElephantry < 2)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKArcher < 2)
                {
                    Instantiate(karcherman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKArcher += 1;
                }
                amountKArcher = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountKSwordman < 2)
                {
                    Instantiate(kswordman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSwordman += 1;
                }
                amountKSwordman = 0;
                while (amountKElephantry < 1)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKGunner < 2)
                {
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKGunner += 1;
                }
                amountKGunner = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountKSpearman < 2)
                {
                    Instantiate(kspearman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSpearman += 1;
                }
                amountKSpearman = 0;
                while (amountKTank < 1)
                {
                    Instantiate(ktank, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKTank += 1;
                }
                amountKTank = 0;
                while (amountKMage < 2)
                {
                    Instantiate(kmage, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKMage += 1;
                }
                amountKMage = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage8-6")
        {
            if (wave == 1)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 2)
            {
                while (amountKFootman < 3)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                amountKFootman = 0;
                while (amountKElephantry < 2)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKArcher < 2)
                {
                    Instantiate(karcherman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKArcher += 1;
                }
                amountKArcher = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 3)
            {
                while (amountKSwordman < 2)
                {
                    Instantiate(kswordman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSwordman += 1;
                }
                amountKSwordman = 0;
                while (amountKElephantry < 2)
                {
                    Instantiate(kelephantry, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKElephantry += 1;
                }
                amountKElephantry = 0;
                while (amountKGunner < 2)
                {
                    Instantiate(kgunnerman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKGunner += 1;
                }
                amountKGunner = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
            yield return new WaitForSeconds(waveSpawnDelay);
            if (wave == 4)
            {
                while (amountKSpearman < 2)
                {
                    Instantiate(kspearman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKSpearman += 1;
                }
                amountKSpearman = 0;
                while (amountKTank < 1)
                {
                    Instantiate(ktank, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKTank += 1;
                }
                amountKTank = 0;
                while (amountKMage < 2)
                {
                    Instantiate(kmage, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKMage += 1;
                }
                amountKMage = 0;
                while (amountKShaman < 1)
                {
                    Instantiate(kshaman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKShaman += 1;
                }
                amountKShaman = 0;
                wave += 1;
            }
        }
        else if (Application.loadedLevelName == "Stage9-6")
        {
            if (wave == 1)
            {
                while (amountBossWave > 0)
                {
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    instantiatedBoss = Instantiate(boss, transform.position, transform.rotation);
                    amountBossWave -= 1;
                    bossHasAppear = true;
                }
                wave += 1;
            }
        }
    }
    IEnumerator waveDay1()
    {
        Debug.Log("Spawing Wave");
        yield return new WaitForSeconds(3);
        while (CastleEnemy.hasPlayerWin == false)
        {
            if (Application.loadedLevelName == "Stage1-1")
            {
                while (amountKFootman < 4)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                yield return new WaitForSeconds(delayWave);
                amountKFootman = 0;
            }
            else if (Application.loadedLevelName == "Stage1-2")
            {
                while (amountKFootman < 6)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                yield return new WaitForSeconds(waveSpawnDelay);
                amountKFootman = 0;
            }
            else if (Application.loadedLevelName == "Stage1-3")
            {
                while (amountKFootman < 6)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                yield return new WaitForSeconds(waveSpawnDelay);
                amountKFootman = 0;
            }
        }
    }
    IEnumerator waveDay2()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage1-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                while (amountKFootman < 3)
                {
                    Instantiate(kfootman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKFootman += 1;
                }
                amountKFootman = 0;
                while (amountKArcher < 1)
                {
                    Instantiate(karcherman, transform.position, transform.rotation);
                    yield return new WaitForSeconds(enemiesSpawnDelay);
                    amountKArcher += 1;
                }
                amountKArcher = 0;
                yield return new WaitForSeconds(waveSpawnDelay);
            }
        }
        else if (Application.loadedLevelName == "Stage1-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 1)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 2;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay3()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage2-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 1)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 2;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage2-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 6)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    wave = 2;
                    amountKFootman = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage2-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 6)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    wave = 2;
                    amountKSwordman = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay4()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage2-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKGunner < 1)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    wave = 2;
                    amountKGunner = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage2-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    wave = 2;
                    amountKGunner = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKGunner < 3)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    wave = 1;
                    amountKGunner = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay5()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage3-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    wave = 2;
                    amountKSpearman = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 3)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    wave = 1;
                    amountKArcher = 0;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage3-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 3)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKArcher < 3)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage3-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay6()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage3-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage3-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay7()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage4-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    /*while (amountKFootman < 2) {
						Instantiate(kfootman, transform.position, transform.rotation);
						yield return new WaitForSeconds(enemiesSpawnDelay);
						amountKFootman += 1;
					}
					amountKFootman = 0;*/
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage4-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage4-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay8()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage4-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKShaman < 3)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKGunner < 3)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage4-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay9()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage5-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage5-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSpearman < 4)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKSpearman < 1)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 4)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage5-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay10()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage5-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage5-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay11()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage6-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage6-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    while (amountKShaman < 1)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage6-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSpearman < 3)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay12()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage6-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage6-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKTank < 2)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay13()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage7-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage7-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 6)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 4)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage7-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKSpearman < 3)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay14()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage7-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage7-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKFootman < 6)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKTank < 2)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay15()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage8-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKFootman < 4)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage8-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKShaman < 4)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKArcher < 4)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKSwordman < 2)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKTank < 2)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage8-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 1)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKArcher < 4)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay16()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage8-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSwordman < 1)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSpearman < 3)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage8-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKSwordman < 3)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKSpearman < 3)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay17()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage9-1")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 3)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage9-2")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSpearman < 4)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKSpearman < 1)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 4)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage9-3")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKSwordman < 4)
                    {
                        Instantiate(kswordman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSwordman += 1;
                    }
                    amountKSwordman = 0;
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKSpearman < 2)
                    {
                        Instantiate(kspearman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKSpearman += 1;
                    }
                    amountKSpearman = 0;
                    while (amountKArcher < 2)
                    {
                        Instantiate(karcherman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKArcher += 1;
                    }
                    amountKArcher = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
    IEnumerator waveDay18()
    {
        yield return new WaitForSeconds(3);
        if (Application.loadedLevelName == "Stage9-4")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKFootman < 2)
                    {
                        Instantiate(kfootman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKFootman += 1;
                    }
                    amountKFootman = 0;
                    while (amountKElephantry < 2)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKGunner < 2)
                    {
                        Instantiate(kgunnerman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKGunner += 1;
                    }
                    amountKGunner = 0;
                    while (amountKMage < 2)
                    {
                        Instantiate(kmage, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKMage += 1;
                    }
                    amountKMage = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
        else if (Application.loadedLevelName == "Stage9-5")
        {
            while (CastleEnemy.hasPlayerWin == false)
            {
                if (wave == 1)
                {
                    while (amountKElephantry < 3)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKTank < 1)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 2;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 2)
                {
                    while (amountKTank < 2)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    while (amountKShaman < 2)
                    {
                        Instantiate(kshaman, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKShaman += 1;
                    }
                    amountKShaman = 0;
                    wave = 3;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                else if (wave == 3)
                {
                    while (amountKElephantry < 4)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    wave = 4;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
                if (wave == 4)
                {
                    while (amountKElephantry < 1)
                    {
                        Instantiate(kelephantry, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKElephantry += 1;
                    }
                    amountKElephantry = 0;
                    while (amountKTank < 3)
                    {
                        Instantiate(ktank, transform.position, transform.rotation);
                        yield return new WaitForSeconds(enemiesSpawnDelay);
                        amountKTank += 1;
                    }
                    amountKTank = 0;
                    wave = 1;
                    yield return new WaitForSeconds(waveSpawnDelay);
                }
            }
        }
    }
}
