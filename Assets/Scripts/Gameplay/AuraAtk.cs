﻿using UnityEngine;
using System.Collections;

public class AuraAtk : MonoBehaviour {
	private UnitAtt att;
	private Skillclick sc;
	private SingleTargetBattleP rngch;
	private Move mov;
	public float rng, moveplus;
	private CircleCollider2D splas;
	private float duration, atk;
	public GameObject EffectAtkspd;
	// Use this for initialization
	void Start () {
		duration = 0;
		sc = transform.root.GetComponent<Skillclick> ();
		att = transform.parent.GetComponent<UnitAtt>();
		rngch = transform.root.GetComponent<SingleTargetBattleP>();
		mov = transform.root.GetComponent<Move> ();
		splas = transform.GetComponent<CircleCollider2D>();
		atk = att.skillduration;
	}
	
	void Update(){
		if(Time.time >= duration && duration != 0){
			rng = 1;
			rngch.atak = rngch.attack;
			mov.sped -= moveplus;
			if(mov.speed != mov.sped && mov.speed != 0){
				mov.speed = mov.sped;
			}
			if(gameObject.transform.root.Find("Charge") != null){
				Destroy(gameObject.transform.root.Find("Charge").gameObject);
			}
			AuraOffAll();
			duration = 0;
		}
		if (rng == sc.range) {
			if (gameObject.transform.root.Find ("Charge") == null) {
				if(mov.sped == mov.initspeed){
					GameObject ef;
					duration = Time.time+4;
					splas.radius = rng;
					rngch.atak += Mathf.Ceil((rngch.atak*atk)/100);
					mov.sped += moveplus;
					if(mov.speed != mov.sped && mov.speed != 0){
						mov.speed = mov.sped;
					}
					ef=Instantiate (EffectAtkspd, transform.position, Quaternion.identity) as GameObject;
					ef.transform.parent = gameObject.transform.root;
					ef.transform.localPosition = new Vector3(0,0.5f,0);
					ef.name = EffectAtkspd.name;
				}
			}
			AuraOn();
			AuraOff();
		}
	}
	
	void AuraOn()
	{
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(gameObject.transform.position, rng);
		foreach (Collider2D col in objectsInRange)
		{
			UnitAtt attri = col.GetComponent<UnitAtt>();
			Move mo = col.GetComponent<Move>();
			SingleTargetBattleP friend = col.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = col.GetComponent<RangecheckP>();
			if (friend != null){
				if(friend.transform.root.Find("Charge") == null){
					if(mo.sped == mo.initspeed){
						GameObject ef;
						ef=Instantiate (EffectAtkspd, col.transform.position, Quaternion.identity) as GameObject;
						ef.transform.parent = col.transform.root;
						ef.transform.localPosition = new Vector3(0,0.5f,0);
						ef.name = EffectAtkspd.name;
						friend.atak += Mathf.Ceil((friend.atak*atk)/100);
						mo.sped += moveplus;
						if(mo.speed != mo.sped && mo.speed != 0){
							mo.speed = mo.sped;
						}
					}
				}
			}
			else if(friendsr != null){
				if (friendsr.transform.root.Find ("Charge") == null) {
					if(mo.sped == mo.initspeed){
						GameObject ef;
						ef=Instantiate (EffectAtkspd, col.transform.position, Quaternion.identity) as GameObject;
						ef.transform.parent = col.transform.root;
						ef.transform.localPosition = new Vector3(0,0.5f,0);
						ef.name = EffectAtkspd.name;
						friendsr.atak += Mathf.Ceil((friendsr.atak*atk)/100);
						mo.sped += moveplus;
						if(mo.speed != mo.sped && mo.speed != 0){
							mo.speed = mo.sped;
						}
					}
				}
			}
		}
	}
	
	void AuraOff()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			UnitAtt attri = go.GetComponent<UnitAtt>();
			Move mo = go.GetComponent<Move>();
			SingleTargetBattleP friend = go.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = go.GetComponent<RangecheckP>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff > rng*2) {
				if (friend != null){
					if(mo.sped != mo.initspeed){
						if(go.transform.Find("Charge") != null){
							Destroy(go.transform.Find("Charge").gameObject);
						}
						friend.attack = friend.atak;
						mo.sped = mo.initspeed;
						if(mo.speed != mo.sped && mo.speed != 0){
							mo.speed = mo.sped;
						}
					}
				}
				else if(friendsr != null){
					if(mo.sped != mo.initspeed){
						if(go.transform.Find("Charge") != null){
							Destroy(go.transform.Find("Charge").gameObject);
						}
						friendsr.atta = friendsr.atak;
						mo.sped = mo.initspeed;
						if(mo.speed != mo.sped && mo.speed != 0){
							mo.speed = mo.sped;
						}
					}
				}
			}
		}
	}
	
	void AuraOffAll()
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject go in gos) {
			UnitAtt attri = go.GetComponent<UnitAtt>();
			Move mo = go.GetComponent<Move>();
			SingleTargetBattleP friend = go.GetComponent<SingleTargetBattleP>();
			RangecheckP friendsr = go.GetComponent<RangecheckP>();
			if (friend != null){
				if(mo.sped != mo.initspeed){
					if(go.transform.Find("Charge") != null){
						Destroy(go.transform.Find("Charge").gameObject);
					}
					friend.attack = friend.atak;
					mo.sped = mo.initspeed;
					if(mo.speed != mo.sped && mo.speed != 0){
						mo.speed = mo.sped;
					}
				}
			}
			else if(friendsr != null){
				if(mo.sped != mo.initspeed){
					if(go.transform.Find("Charge") != null){
						Destroy(go.transform.Find("Charge").gameObject);
					}
					friendsr.atta = friendsr.atak;
					mo.sped = mo.initspeed;
					if(mo.speed != mo.sped && mo.speed != 0){
						mo.speed = mo.sped;
					}
				}
			}
		}
	}
}
