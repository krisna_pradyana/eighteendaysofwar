﻿using UnityEngine;
using System.Collections;

public class CastlePlayer : MonoBehaviour {
	UnitAtt att;
	public float health;
	public bool isHurt75 = true;
	public bool isHurt50 = false;
	public bool isHurt25 = false;
	public bool isHurt0 = false;
	private Animator animEnemy;
	private GameObject[] enemyArray;
	public static bool hasEnemyWin;
	public float maxHealth;
	public GUISkin mySkin;
	public GameObject LoseUI, gold, wave, timerText;
	private GameObject healthBarPlayer;
	public AudioClip loseBGM;
	public Sprite sprite75, sprite50, sprite25, sprite0;
	private float bgmVolume;
	private GameObject square;
	public int coinLose, ivSurvival;
	public float timer;
	private System.TimeSpan timerFormatted;
	// Use this for initialization
	void Start () {

        LoseUI = GameObject.Find("Lose UI");
        LoseUI.SetActive(false);

		bgmVolume = 1;
		att=GameObject.Find(gameObject.name).GetComponent<UnitAtt>();
		health = att.HP;
		hasEnemyWin = false;
		maxHealth = att.HP;
		square = GameObject.Find ("Main Camera/Square");
        healthBarPlayer = GameObject.FindGameObjectWithTag("HPPlayer");
		InvokeRepeating ("IncreaseTimer", 3, 1);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 scale = healthBarPlayer.transform.localScale;
		scale.x = (health * 3) / maxHealth;
		if (health <= 0) {
			scale.x = 0;	
		}
		healthBarPlayer.transform.localScale = scale;
		if (PauseClick.enableBGM == false) {
			if (hasEnemyWin == true && bgmVolume > 0) {
				bgmVolume -= 0.01f;
				Camera.main.GetComponent<AudioSource>().volume = bgmVolume;		
			}	
		}
		timerFormatted = System.TimeSpan.FromSeconds(timer);
	}
	public void colldetect(float damage){
		if (health > 0) {
			health -= damage;
			if(health < (att.HP * 0.75) && isHurt75) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite75;
				isHurt75 = false;
				isHurt50 = true;
			}
			else if (health < (att.HP * 0.5) && isHurt50) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite50;
				isHurt50 = false;
				isHurt25 = true;
			}
			else if (health < (att.HP * 0.25) && isHurt25) {
				//animPlayer.SetTrigger("Hurt");
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite25;
				isHurt25 = false;
			}
			if(health <= 0){
				isHurt0 = true;
				gameObject.GetComponent<SpriteRenderer>().sprite = sprite0;
				GameObject.Find ("Square2").GetComponent<SpriteRenderer>().enabled = true;
				hasEnemyWin = true;	
				LoseUI.SetActive(true);
				GameFlow.hasPaused = true;
                /*
				if (Application.loadedLevelName != "Survival") {
					//GameObject.Find ("GoldSystem").SendMessage ("IncreaseGold", coinLose);
					//gold.GetComponent<EasyFontTextMesh>().Text = coinLose.ToString();
				}

				else {
					for (int i = 1; i < SurvivalController.wave; i++) {
						coinLose = coinLose + ((i+1) * ivSurvival);
					}
					GameObject.Find ("GoldSystem").SendMessage ("IncreaseGold", coinLose);
					gold.GetComponent<EasyFontTextMesh>().Text = coinLose.ToString();
					wave.GetComponent<EasyFontTextMesh>().Text = SurvivalController.wave.ToString();
					timerText.GetComponent<EasyFontTextMesh>().Text = string.Format("{0:D2}:{1:D2}", timerFormatted.Minutes, timerFormatted.Seconds);
				}
                */
				GameObject.Find ("Main Camera/Hero Container/Pause Container").GetComponent<BoxCollider2D>().enabled = false;
				//square.GetComponent<SpriteRenderer>().enabled = true;
				if (PauseClick.enableBGM == false) {
					StartCoroutine("defeatSound");
				}
			}
		} 
	}
	IEnumerator defeatSound() {
		yield return new WaitForSeconds (2);
		AudioSource.PlayClipAtPoint(loseBGM, Camera.main.transform.position);
		//Time.timeScale = 0;
	}
	void IncreaseTimer () {
		timer += 1;
	}
}
