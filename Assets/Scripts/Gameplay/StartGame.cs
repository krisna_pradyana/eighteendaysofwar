﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {
	public GameObject mvspd10, mvspd30, atk10, atk30, hp10, hp30, res10, res30, next, square;
	public GameObject hero1, hero2, hero3, hero4, hero5, hero6, hero7, hero8, hero9;
	public GameObject selecthero1, selecthero2, selecthero3;
	public GameObject lockhero;
	public GameObject activateItem, warning;
	private bool chmvspd10=false, chmvspd30=false, chatk10=false, chatk30=false, chhp10=false, chhp30=false, chres10=false, chres30=false;
	private int stockMovementSpeed10, stockMovementSpeed30, stockAttack10, stockAttack30, stockHP10, stockHP30, stockResource10, stockResource30;
	public AudioClip buttonSound, buttonSoundCannot;
	private bool toggleSelectionSpd = false, toggleSelectionAtk = false, toggleSelectionHP = false, toggleSelectionRes = false;
	private bool hasUsedBuffMvSpd = false, hasUsedBuffAtk = false, hasUsedBuffHP = false, hasUsedBuffRes = false;
	private CameraControl cam;
	public static bool selectionMode;
	private GameObject[] heroSelectorArray;
	private GameObject[] itemSelectorArray;

    // Use this for initialization
    void Start () {
        //De selected hero on start
        Debug.Log("Clearing Hero");
        selectedhero(selecthero1, Checkunit.hero1select);
        selectedhero(selecthero2, Checkunit.hero2select);
        selectedhero(selecthero3, Checkunit.hero3select);

        Checkunit.hero1select = "";
        Checkunit.hero2select = "";
        Checkunit.hero3select = "";
        //

        PlayerPrefs.SetFloat("SilverCoin", 0);
		PlayerPrefs.SetFloat("HealthPoint", 0);
		PlayerPrefs.SetFloat("AttackPower", 0);
		PlayerPrefs.SetFloat("MovementSpeed", 0);
		selectionMode = true;
		cam = Camera.main.GetComponent<CameraControl> ();
		Time.timeScale = 1;

		GameFlow.hasPaused = true;
	    GameFlow.hasResumed = false;
		stockMovementSpeed10 = PlayerPrefs.GetInt("MovementSpeed10"); //save stock at here fuck!
		stockMovementSpeed30 = PlayerPrefs.GetInt("MovementSpeed30");
		stockAttack10 = PlayerPrefs.GetInt("Attack10");
		stockAttack30 = PlayerPrefs.GetInt("Attack30");
		stockHP10 = PlayerPrefs.GetInt("HP10");
		stockHP30 = PlayerPrefs.GetInt("HP30");
		stockResource10 = PlayerPrefs.GetInt("Resource10");
		stockResource30 = PlayerPrefs.GetInt("Resource30");
		heroSelectorArray = GameObject.FindGameObjectsWithTag ("HeroSelector");
		itemSelectorArray = GameObject.FindGameObjectsWithTag ("ItemSelector");

		if (Checkunit.bima == true) {
			hero2.SetActive(true);
		}
		if (Checkunit.srikandi == true) {
			hero3.SetActive(true);
		}
		if (Checkunit.yudhistira == true) {
			hero4.SetActive(true);		
		}
		if (Checkunit.nakulasadewa == true) {
			hero5.SetActive(true);		
		}
		if (Checkunit.gatotkaca == true) {
			hero6.SetActive(true);		
		}
		if (Checkunit.drupada == true) {
			hero7.SetActive(true);		
		}
		if (Checkunit.kresna == true) {
			hero8.SetActive(true);		
		}
		if (Checkunit.hanoman == true) {
			hero9.SetActive(true);		
		}

        ///////////////////////////////Audio Code////////////////////////////
        if (SettingsScript.enabledSFX == true)
        {

        }
        else
        {

        }

        if (SettingsScript.enabledBGM == true)
        {
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
        }

    }

	// Update is called once per frame
	void Update () {
		/*if (Input.touchCount == 1) {
			Touch t = Input.GetTouch(0);
			if (t.phase == TouchPhase.Began) {
				CastRay();
			}
		}*/
		if (Input.GetMouseButtonDown(0)) {
			CastRay();
		}
		if (stockMovementSpeed10 <= 0) {
			mvspd10.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockMovementSpeed30 <= 0) {
			mvspd30.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockAttack10 <= 0) {
			atk10.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockAttack30 <= 0) {
			atk30.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockHP10 <= 0) {
			hp10.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockHP30 <= 0) {
			hp30.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockResource10 <= 0) {
			res10.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
		if (stockResource30 <= 0) {
			res30.GetComponent<SpriteRenderer>().color = Color.gray;	
		}
	}   

	void CastRay() {
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit) {
			selectitem(hit);
			selecthero(hit);
			if(hit.collider.gameObject == next){
				if(Checkunit.hero1select != "" || Checkunit.hero2select != "" || Checkunit.hero3select != ""){
					GameFlow.reinit = true;
					InitUnit.readytoinit = true;
                    //Time.timeScale = 1;
                    GameFlow.hasPaused = false;
					gameObject.SetActive(false);

                    ///Start Everything here
                    GameFlow.activateCount();
                    SpawnerE.enemySpawn();
					square.GetComponent<SpriteRenderer>().enabled=false;
					AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					selectionMode = false;
                    InitUnit.initializeHero();
				}
				else{
					foreach (GameObject heroSelector in heroSelectorArray) {
						heroSelector.GetComponent<BoxCollider2D>().enabled = false;
					}
					foreach (GameObject itemSelector in itemSelectorArray) {
						itemSelector.GetComponent<BoxCollider2D>().enabled = false;
					}
					warning.SetActive(true);
				}
			}
		}
	}

	void selectitem (RaycastHit2D hit){
		if(hit.collider.gameObject == mvspd10){
				if (stockMovementSpeed10 != 0 || hasUsedBuffMvSpd == true) {
					if(stockMovementSpeed10 > 0 && chmvspd10 == false && chmvspd30 == false && toggleSelectionSpd == false){
						PlayerPrefs.SetFloat("MovementSpeed", 0.1f);
						stockMovementSpeed10 -=1;
						PlayerPrefs.SetInt("MovementSpeed10",stockMovementSpeed10);
						chmvspd10 = true;
						mvspd10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						mvspd10.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("mvspd10").GetComponent<SpriteRenderer>().enabled = true;
						mvspd30.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionSpd = true;
						hasUsedBuffMvSpd = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockMovementSpeed10 >= 0 && chmvspd30 == false) {
							PlayerPrefs.SetFloat("MovementSpeed", 0);
							stockMovementSpeed10 +=1;
							PlayerPrefs.SetInt("MovementSpeed10",stockMovementSpeed10);
							chmvspd10 = false;
							mvspd10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							mvspd10.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("mvspd10").GetComponent<SpriteRenderer>().enabled = false;
							mvspd30.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionSpd = false;
							hasUsedBuffMvSpd = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
			}
			else if(hit.collider.gameObject == mvspd30){
				if (stockMovementSpeed30 != 0 || hasUsedBuffMvSpd == true) {
					if(stockMovementSpeed30 > 0 && chmvspd10 == false && chmvspd30 == false && toggleSelectionSpd == false){
						PlayerPrefs.SetFloat("MovementSpeed", 0.3f);
						stockMovementSpeed30 -=1;
						PlayerPrefs.SetInt("MovementSpeed30",stockMovementSpeed30);
						chmvspd30 = true;
						mvspd30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						mvspd30.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("mvspd30").GetComponent<SpriteRenderer>().enabled = true;
						mvspd10.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionSpd = true;
						hasUsedBuffMvSpd = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockMovementSpeed30 >= 0 && chmvspd10 == false) {
							PlayerPrefs.SetFloat("MovementSpeed", 0);
							stockMovementSpeed30 +=1;
							PlayerPrefs.SetInt("MovementSpeed30",stockMovementSpeed30);
							chmvspd30 = false;
							mvspd30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							mvspd30.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("mvspd30").GetComponent<SpriteRenderer>().enabled = false;
							mvspd10.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionSpd = false;
							hasUsedBuffMvSpd = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}		
					}
				}
			}
			else if(hit.collider.gameObject == atk10){
				if(stockAttack10 != 0 || hasUsedBuffAtk == true) {
					if(stockAttack10 > 0 && chatk10 == false && chatk30 == false && toggleSelectionAtk == false){
						PlayerPrefs.SetFloat("AttackPower", 0.1f);
						stockAttack10 -=1;
						PlayerPrefs.SetInt("Attack10",stockAttack10);
						chatk10 = true;
						atk10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						atk10.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("atk10").GetComponent<SpriteRenderer>().enabled = true;
						atk30.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionAtk = true;
						hasUsedBuffAtk = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockAttack10 >= 0 && chatk30 == false) {
							PlayerPrefs.SetFloat("AttackPower", 0);
							stockAttack10 +=1;
							PlayerPrefs.SetInt("Attack10",stockAttack10);
							chatk10 = false;
							atk10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							atk10.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("atk10").GetComponent<SpriteRenderer>().enabled = false;
							atk30.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionAtk = false;
							hasUsedBuffAtk = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
			else if(hit.collider.gameObject == atk30){
				if (stockAttack30 != 0 || hasUsedBuffAtk == true) {
					if(stockAttack30 > 0 && chatk10 == false && chatk30 == false && toggleSelectionAtk == false){
						PlayerPrefs.SetFloat("AttackPower", 0.3f);
						stockAttack30 -=1;
						PlayerPrefs.SetInt("Attack30",stockAttack30);
						chatk30 = true;
						atk30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						atk30.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("atk30").GetComponent<SpriteRenderer>().enabled = true;
						atk10.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionAtk = true;
						hasUsedBuffAtk = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockAttack30 >= 0 && chatk10 == false) {
							PlayerPrefs.SetFloat("AttackPower", 0);
							stockAttack30 +=1;
							PlayerPrefs.SetInt("Attack30",stockAttack30);
							chatk30 = false;
							atk30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							atk30.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("atk30").GetComponent<SpriteRenderer>().enabled = false;
							atk10.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionAtk = false;
							hasUsedBuffAtk = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
			else if(hit.collider.gameObject == hp10){
				if (stockHP10 != 0 || hasUsedBuffHP == true) {
					if(stockHP10 > 0 && chhp10 == false && chhp30 == false && toggleSelectionHP == false){
						PlayerPrefs.SetFloat("HealthPoint", 0.1f);
						stockHP10 -=1;
						PlayerPrefs.SetInt("HP10",stockHP10);
						chhp10 = true;
						hp10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						hp10.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("hp10").GetComponent<SpriteRenderer>().enabled = true;
						hp30.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionHP = true;
						hasUsedBuffHP = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockHP10 >= 0 && chhp30 == false) {
							PlayerPrefs.SetFloat("HealthPoint", 0);
							stockHP10 +=1;
							PlayerPrefs.SetInt("HP10",stockHP10);
							chhp10 = false;
							hp10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							hp10.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("hp10").GetComponent<SpriteRenderer>().enabled = false;
							hp30.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionHP = false;
							hasUsedBuffHP = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
			else if(hit.collider.gameObject == hp30){
				if(stockHP30 != 0 || hasUsedBuffHP == true) {
					if(stockHP30 > 0 && chhp10 == false && chhp30 == false && toggleSelectionHP == false){
						PlayerPrefs.SetFloat("HealthPoint", 0.3f);
						stockHP30 -=1;
						PlayerPrefs.SetInt("HP30",stockHP30);
						chhp30 = true;
						hp30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						hp30.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("hp30").GetComponent<SpriteRenderer>().enabled = true;
						hp10.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionHP = true;
						hasUsedBuffHP = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockHP30 >= 0 && chhp10 == false) {
							PlayerPrefs.SetFloat("HealthPoint", 0);
							stockHP30 +=1;
							PlayerPrefs.SetInt("HP30",stockHP30);
							chhp30 = false;
							hp30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							hp30.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("hp30").GetComponent<SpriteRenderer>().enabled = false;
							hp10.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionHP = false;
							hasUsedBuffHP = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
			else if(hit.collider.gameObject == res10){
				if(stockResource10 != 0 || hasUsedBuffRes == true) {
					if(stockResource10 > 0 && chres10 == false && chres30 == false && toggleSelectionRes == false){
						PlayerPrefs.SetFloat("SilverCoin", 0.1f);
						stockResource10 -=1;
						PlayerPrefs.SetInt("Resource10",stockResource10);
						chres10 = true;
						res10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						res10.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("res10").GetComponent<SpriteRenderer>().enabled = true;
						res30.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionRes = true;
						hasUsedBuffRes = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockResource10 >= 0 && chres30 == false) {
							PlayerPrefs.SetFloat("SilverCoin", 0);
							stockResource10 +=1;
							PlayerPrefs.SetInt("Resource10",stockResource10);
							chres10 = false;
							res10.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							res10.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("res10").GetComponent<SpriteRenderer>().enabled = false;
							res30.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionRes = false;
							hasUsedBuffRes = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
			else if(hit.collider.gameObject == res30){
				if(stockResource30 != 0 || hasUsedBuffRes == true) {
					if(stockResource30 > 0 && chres10 == false && chres30 == false && toggleSelectionRes == false){
						PlayerPrefs.SetFloat("SilverCoin", 0.3f);
						stockResource30 -=1;
						PlayerPrefs.SetInt("Resource30",stockResource30);
						chres30 = true;
						res30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = true;
						res30.GetComponent<SpriteRenderer>().color = Color.gray;
						activateItem.transform.Find("res30").GetComponent<SpriteRenderer>().enabled = true;
						res10.GetComponent<SpriteRenderer>().color = Color.gray;
						toggleSelectionRes = true;
						hasUsedBuffRes = true;
						AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
					}
					else{
						if (stockResource30 >= 0 && chres10 == false) {
							PlayerPrefs.SetFloat("SilverCoin", 0);
							stockResource30 +=1;
							PlayerPrefs.SetInt("Resource30",stockResource30);
							chres30 = false;
							res30.transform.Find("Centang").GetComponent<SpriteRenderer>().enabled = false;
							res30.GetComponent<SpriteRenderer>().color = Color.white;
							activateItem.transform.Find("res30").GetComponent<SpriteRenderer>().enabled = false;
							res10.GetComponent<SpriteRenderer>().color = Color.white;
							toggleSelectionRes = false;
							hasUsedBuffRes = false;
							AudioSource.PlayClipAtPoint (buttonSound, new Vector3(0, 0, 0));
						}
						
					}
				}
				
			}
	}

	void selecthero(RaycastHit2D hit){
		if (hit.collider.gameObject == hero1) {
			heroselect("arjuna",hero1);
		}
		else if (hit.collider.gameObject == hero2) {
			heroselect("bima",hero2);
		}
		else if (hit.collider.gameObject == hero3) {
			heroselect("srikandi",hero3);
		}
		else if (hit.collider.gameObject == hero4) {
			heroselect("yudistira",hero4);
		}
		else if (hit.collider.gameObject == hero5) {
			heroselect("nakusade",hero5);
		}
		else if (hit.collider.gameObject == hero6) {
			heroselect("gatot",hero6);
		}
		else if (hit.collider.gameObject == hero7) {
			heroselect("drupada",hero7);
		}
		else if (hit.collider.gameObject == hero8) {
			heroselect("kresna",hero8);
		}
		else if (hit.collider.gameObject == hero9) {
			heroselect("anoman",hero9);
		}
		else if(hit.collider.gameObject == selecthero1){
			selectedhero(selecthero1,Checkunit.hero1select);
			selecthero1.transform.localScale = new Vector3(1,1,0);
			Checkunit.hero1select = "";
		}
		else if(hit.collider.gameObject == selecthero2){
			selectedhero(selecthero2,Checkunit.hero2select);
			selecthero2.transform.localScale = new Vector3(1,1,0);
			Checkunit.hero2select = "";
		}
		else if(hit.collider.gameObject == selecthero3){
			selectedhero(selecthero3,Checkunit.hero3select);
			selecthero3.transform.localScale = new Vector3(1,1,0);
			Checkunit.hero3select = "";
		}
	}

    
	void heroselect(string name, GameObject unit){
		if (Checkunit.hero1select == "") {
			Checkunit.hero1select = name;
			selecthero1.GetComponent<SpriteRenderer>().sprite = unit.GetComponent<SpriteRenderer>().sprite;
			unit.SetActive(false);
			resizing(selecthero1, name);
		}
		else if(Checkunit.hero2select == ""){
			Checkunit.hero2select = name;
			selecthero2.GetComponent<SpriteRenderer>().sprite = unit.GetComponent<SpriteRenderer>().sprite;
			unit.SetActive(false);
			resizing(selecthero2, name);
		}
		else if(Checkunit.hero3select == ""){
			Checkunit.hero3select = name;
			selecthero3.GetComponent<SpriteRenderer>().sprite = unit.GetComponent<SpriteRenderer>().sprite;
			unit.SetActive(false);
			resizing(selecthero3, name);
		}
		else if(Checkunit.hero3select != ""){
			selectedhero(selecthero3, Checkunit.hero3select);
			Checkunit.hero3select = Checkunit.hero2select;
			selecthero3.GetComponent<SpriteRenderer>().sprite = selecthero2.GetComponent<SpriteRenderer>().sprite;
			resizing(selecthero3, Checkunit.hero2select);
			Checkunit.hero2select = Checkunit.hero1select;
			selecthero2.GetComponent<SpriteRenderer>().sprite = selecthero1.GetComponent<SpriteRenderer>().sprite;
			resizing(selecthero2, Checkunit.hero1select);
			Checkunit.hero1select = name;
			selecthero1.GetComponent<SpriteRenderer>().sprite = unit.GetComponent<SpriteRenderer>().sprite;
			resizing(selecthero1, name);
			unit.SetActive(false);
		}
	}

	void resizing(GameObject select, string name){
		if (name == "arjuna" || name == "bima" || name == "srikandi") {
			select.transform.localScale = new Vector3(1,1,0);
		}
		else{
			select.transform.localScale = new Vector3(4,4,0);
			if(select.GetComponent<BoxCollider2D>().size != new Vector2(1,1)){
				select.GetComponent<BoxCollider2D>().size = new Vector2(1,1);
			}
		}
	}

	void selectedhero(GameObject select, string heroname){
		select.GetComponent<SpriteRenderer>().sprite = lockhero.GetComponent<SpriteRenderer>().sprite;
		if (heroname == "arjuna") {
			hero1.SetActive(true);
		}
		else if(heroname == "bima"){
			hero2.SetActive(true);
		}
		else if(heroname == "srikandi"){
			hero3.SetActive(true);
		}
		else if(heroname == "yudistira"){
			hero4.SetActive(true);
		}
		else if(heroname == "nakusade"){
			hero5.SetActive(true);
		}
		else if(heroname == "gatot"){
			hero6.SetActive(true);
		}
		else if(heroname == "drupada"){
			hero7.SetActive(true);
		}
		else if(heroname == "kresna"){
			hero8.SetActive(true);
		}
		else if(heroname == "anoman"){
			hero9.SetActive(true);
		}
	}
}
