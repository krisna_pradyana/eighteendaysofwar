﻿using UnityEngine;
using System.Collections;

public class AuraHeal : MonoBehaviour {
	private UnitAtt att;
	private Move mov;
	private Skillclick sc;
	private bool readytoheal;
	public float rng, heal, loading;
	private CircleCollider2D splas;
	private float duration, delay=0;
	public GameObject HealPopUp, Effectheal;
	private RangecheckP rnchp;
	private RangecheckE rnche;
	private SingleTargetBattleP sngltgt;
	private string sider;
	private Animator animdrupada;
	// Use this for initialization
	void Start () {
		sc = transform.root.GetComponent<Skillclick> ();
		att = transform.root.GetComponent<UnitAtt>();
		mov = transform.root.GetComponent<Move> ();
		splas = transform.GetComponent<CircleCollider2D>();
		rnchp = transform.root.GetComponent<RangecheckP> ();
		rnche = transform.root.GetComponent<RangecheckE> ();
		sngltgt = transform.root.GetComponent<SingleTargetBattleP> ();
		loading = att.ATKSPD;
		rng = att.SPLASH;
		readytoheal = false;
		duration = 0;
		if(rnchp != null){
			sider = "Player";
		}
		else{
			sider = "Enemy";
		}
		if (sc != null) {
			animdrupada = transform.root.GetComponent<Animator>();
			sider = "Player";
		}
	}
	
	void Update(){
		if (sc != null) {
			if (rng == sc.range) {
				if(duration == 0){
					duration = Time.time+att.skillduration;
				}
				Healingprocess();
			}
			else{
				delay = 0;
				AuraOffAll(sider);
			}
			if(Time.time >= duration && duration != 0){
				rng = 1;
				AuraOffAll(sider);
				duration = 0;
				animdrupada.SetBool("Skill",false);
			}
		}
		else{
			if (splas.radius == att.SPLASH) {
				Healingprocess();
			}
			else{
				delay = 0;
				AuraOffAll(sider);
			}
		}
	}

	void Healingprocess(){
		if(sc != null){
			if(heal != sngltgt.atak){
				heal = sngltgt.atak;
			}
			delay = 0;
		}
		else if(rnchp != null){
			if(heal != rnchp.atak){
				heal = rnchp.atak;
			}
		}
		else{
			if(heal != att.ATK){
				heal = att.ATK;
			}
		}
		if(Time.time >= delay){
			if(gameObject.transform.root.Find("healing(Clone)") == null){
				GameObject ef;
				ef=Instantiate (Effectheal, gameObject.transform.position, Quaternion.identity) as GameObject;
				ef.transform.parent = gameObject.transform.root;
			}
			GameObject HealP;
			if(delay != 0){
				delay += loading;
			}
			AuraOn();
			AuraOff(sider);
		}
		if(delay == 0){
			delay= Time.time + loading;
		}
	}
	
	void AuraOn()
	{
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(gameObject.transform.position, rng);
		foreach (Collider2D col in objectsInRange)
		{
			UnitAtt attri = col.GetComponent<UnitAtt>();
			Move friend=col.GetComponent<Move>();
			MoveE friende=col.GetComponent<MoveE>();
			if (friend != null && sider == "Player"){
				if(col.transform.Find("healing(Clone)") == null){
					GameObject ef;
					ef=Instantiate (Effectheal, col.transform.position, Quaternion.identity) as GameObject;
					ef.transform.parent = col.transform;
				}
				if(attri.HP > friend.health){
					GameObject HealP;
					HealP = Instantiate (HealPopUp, col.transform.position+new Vector3(0, 4f, 0), Quaternion.identity) as GameObject;
					HealP.GetComponent<EasyFontTextMesh>().Text = heal.ToString();
					friend.health += heal;
					if(attri.HP < friend.health){
						friend.health = attri.HP;
					}
				}
			}
			else if(friende != null && sider == "Enemy"){
				if(col.transform.Find("healing(Clone)") == null){
					GameObject ef;
					ef=Instantiate (Effectheal, col.transform.position, Quaternion.identity) as GameObject;
					ef.transform.parent = col.transform;
				}
				if(attri.HP > friende.health){
					GameObject HealP;
					HealP = Instantiate (HealPopUp, col.transform.position+new Vector3(0, 4f, 0), Quaternion.identity) as GameObject;
					HealP.GetComponent<EasyFontTextMesh>().Text = heal.ToString();
					friende.health += heal;
					if(attri.HP < friende.health){
						friende.health = attri.HP;
					}
				}
			}
		}
	}

	void AuraOff(string side)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(side);
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			Move friend = go.GetComponent<Move>();
			MoveE friende = go.GetComponent<MoveE>();
			if (diff > rng*2) {
				if(friend != null){
					if(go.transform.Find("healing(Clone)") != null){
						Destroy(go.transform.Find("healing(Clone)").gameObject);
					}
				}
				else{
					if(go.transform.Find("healing(Clone)") != null){
						Destroy(go.transform.Find("healing(Clone)").gameObject);
					}
				}
			}
		}
	}

	public void AuraOffAll(string side)
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag(side);
		foreach (GameObject go in gos) {
			Move friend = go.GetComponent<Move>();
			MoveE friende = go.GetComponent<MoveE>();
			if (friend != null){
				if(go.transform.Find("healing(Clone)") != null){
					Destroy(go.transform.Find("healing(Clone)").gameObject);
				}
			}
			else{
				if(go.transform.Find("healing(Clone)") != null){
					Destroy(go.transform.Find("healing(Clone)").gameObject);
				}
			}
		}
	}
}
