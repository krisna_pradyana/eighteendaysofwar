﻿using UnityEngine;
using System.Collections;

public class Cooldown : MonoBehaviour {
	private float delay, cdspeed, scale, i, curspeed=0;
	Unitcooldown cooldown;
	private GameObject guiskill;
	public static bool inCooldown = false;
	// Use this for initialization
	void Start () {
		cooldown = GameObject.Find ("CooldownContainer").GetComponent<Unitcooldown> ();
		if (gameObject.name == "spearcdon") {
			cdspeed = cooldown.spearcd;
		} 
		else if (gameObject.name == "footmancdon") {
			cdspeed = cooldown.footcd;
		} 
		else if (gameObject.name == "swordcdon") {
			cdspeed = cooldown.swordcd;
		}
		else if(gameObject.name == "archeron"){
			cdspeed = cooldown.archcd;
		}
		else if(gameObject.name == "gunneron"){
			cdspeed = cooldown.gunnercd;
		}
		else if(gameObject.name == "mageon"){
			cdspeed = cooldown.magecd;
		}
		else if(gameObject.name == "shamanon"){
			cdspeed = cooldown.shamancd;
		}
		else if(gameObject.name == "elephantryon"){
			cdspeed = cooldown.elephantrycd;
		}
		else if(gameObject.name == "tankon"){
			cdspeed = cooldown.tankcd;
		}
		else if(gameObject.name == "mineron"){
			cdspeed = cooldown.minercd;
		}
		delay = 0;
		i = 0;
	}

	void OnEnable(){
		reinit ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (curspeed == 0) {
			curspeed = cdspeed;		
		}
		if (gameObject.name == "arjunaon") {
			if(Unitcooldown.herosummoned=="arjuna" || Unitcooldown.arjunasummoned == true){
				cdspeed = cooldown.skillArjunacd;
				guiskill = cooldown.skill1;
			}
			else{
				cdspeed = cooldown.arjunacd;
			}
		}
		else if (gameObject.name == "bimaon") {
			if(Unitcooldown.herosummoned=="bima" || Unitcooldown.bimasummoned == true){
				cdspeed = cooldown.skillBimacd;
				guiskill = cooldown.skill2;
			}
			else{
				cdspeed = cooldown.bimacd;
			}
		}
		else if (gameObject.name == "srikandion") {
			if(Unitcooldown.herosummoned=="srikandi" || Unitcooldown.srikandisummoned == true){
				cdspeed = cooldown.skillSrikandicd;
				guiskill = cooldown.skill3;
			}
			else{
				cdspeed = cooldown.srikandicd;
			}
		}
		else if (gameObject.name == "yudistiraon") {
			if(Unitcooldown.herosummoned=="yudistira" || Unitcooldown.yudissummoned == true){
				cdspeed = cooldown.skillYudiscd;
				guiskill = cooldown.skill4;
			}
			else{
				cdspeed = cooldown.yudiscd;
			}
		}
		else if (gameObject.name == "nakulasadewaon") {
			if(Unitcooldown.herosummoned=="nakusade" || Unitcooldown.nakusadesummoned == true){
				cdspeed = cooldown.skillnakusadecd;
				guiskill = cooldown.skill5;
			}
			else{
				cdspeed = cooldown.nakusadecd;
			}
		}
		else if (gameObject.name == "gatotkacaon") {
			if(Unitcooldown.herosummoned=="gatotkaca" || Unitcooldown.gatotkacasummoned == true){
				cdspeed = cooldown.skillgatotkacacd;
				guiskill = cooldown.skill6;
			}
			else{
				cdspeed = cooldown.gatotkacacd;
			}
		}
		else if (gameObject.name == "drupadaon") {
			if(Unitcooldown.herosummoned=="drupada" || Unitcooldown.drupsummoned == true){
				cdspeed = cooldown.skilldrupcd;
				guiskill = cooldown.skill7;
			}
			else{
				cdspeed = cooldown.drupadacd;
			}
		}
		else if (gameObject.name == "kresnaon") {
			if(Unitcooldown.herosummoned=="kresna" || Unitcooldown.kressummoned == true){
				cdspeed = cooldown.skillkrescd;
				guiskill = cooldown.skill8;
			}
			else{
				cdspeed = cooldown.kresnacd;
			}
		}
		else if (gameObject.name == "anomanon") {
			if(Unitcooldown.herosummoned=="anoman" || Unitcooldown.anomsummoned == true){
				cdspeed = cooldown.skillanomcd;
				guiskill = cooldown.skill9;
			}
			else{
				cdspeed = cooldown.anomancd;
			}
		}
		i +=1/cdspeed;
		if(i >= delay/cdspeed){
			inCooldown = true;
			delay += cdspeed;
			scale = 1/cdspeed;
			if (Unitcooldown.resetercooldown != 0) {
				scale = 0.9f;
				Unitcooldown.resetercooldown = 0;
			}
			if (gameObject.name == "arjunaon" || gameObject.name == "srikandion" || gameObject.name == "bimaon"||gameObject.name == "yudistiraon"||gameObject.name == "nakulasadewaon"||gameObject.name == "gatotkacaon"||gameObject.name == "drupadaon"||gameObject.name == "kresnaon"||gameObject.name == "anomanon") {
				transform.localScale = new Vector3(1,(transform.localScale.y - scale),1);
			}
			else if (gameObject.name == "mineron") {
				transform.localScale = new Vector3(2.5f,(transform.localScale.y - scale),1);
			}
			else {
				transform.localScale = new Vector3(1.8f,(transform.localScale.y - scale),1);
			}

			if(transform.localScale.y <= 0){
				transform.parent.gameObject.SetActive(false);
				if (gameObject.name == "arjunaon" || gameObject.name == "srikandion" || gameObject.name == "bimaon"||gameObject.name == "yudistiraon"||gameObject.name == "nakulasadewaon"||gameObject.name == "gatotkacaon"||gameObject.name == "drupadaon"||gameObject.name == "kresnaon"||gameObject.name == "anomanon") {
					guiskill.GetComponent<SpriteRenderer>().color = Color.white;
					transform.localScale = new Vector3(1,1,1);
				}
				else if (gameObject.name == "mineron") {
					transform.localScale = new Vector3(2.5f,2.1f,1);
				}
				else {
					transform.localScale = new Vector3(1.8f,1.5f,1);
				}
				inCooldown = false;
			}
		}
	}

	void reinit(){
		delay = 0;
		i = 0;
		curspeed = 0;
	}
}
