﻿using UnityEngine;
using System.Collections;

public class SplashArea : MonoBehaviour {
	public float dmg;
	private int knocfriend=1;
	private UnitAtt att, attfr;
	private float rng;
	private float delay=0, attackanim,cooldown;
	private Animator animEnemy,animPlayer;
	public GameObject explode;
	Skillclick sc;
	// Use this for initialization
	void Start () {
		att = transform.root.GetComponent<UnitAtt>();
		sc = transform.root.GetComponent<Skillclick>();
		animPlayer = transform.parent.GetComponent<Animator> ();
		if (transform.root.gameObject.name == "BimaRoot(Clone)") {
			dmg = att.ATK*0.6f;
		}
		else{
			dmg = att.ATK;
		}
	}

	void OnTriggerStay2D(Collider2D coll){
		if (coll.gameObject.tag == "Enemy") {
			if (att.SPLASH != 0) {
				if(delay == 0){
					delay = Time.time;
				}
				rng = att.SPLASH;
				if (Time.time > delay) {
					animPlayer.SetBool("Attack", true);
					delay += att.ATKSPD;
					AreaDamageEnemies ();
					StartCoroutine("explosionarea");
					if(attackanim == 0){
						if(animPlayer.GetBool("Attackspeed")){
							cooldown = 0.45f;
						}
						else{
							cooldown = 1.2f;
						}
						attackanim=Time.time+cooldown;
					}
					if(transform.root.gameObject.name == "BimaRoot(Clone)"){
						sc.duration -= 1;
					}
				}
				else if(Time.time > attackanim){
					attackanim = 0;
					animPlayer.SetBool("Attack", false);
				}
			}
			else{
				if(delay != 0){
					delay = 0;
				}
			}		
		}
	}

	void OnTriggerExit2D(Collider2D coll){
		delay = 0;
	}

	void AreaDamageEnemies()
	{
		Collider2D[] objectsInRange = Physics2D.OverlapCircleAll(gameObject.transform.position, rng);
		foreach (Collider2D col in objectsInRange)
		{
			if(transform.root.gameObject.tag == "Player"){
				MoveE enemy = col.GetComponent<MoveE>();
				if (enemy != null)
				{
					int damag = Mathf.RoundToInt(Random.Range(dmg,dmg*11/10));
					enemy.colldetect(damag);
					animEnemy= col.GetComponent<Animator>();
					animEnemy.SetBool("Knockback", true);
					FindClosestFriend();
					if(att.KNOCKBACK != 0 && Unitcooldown.herosummoned == "bima"){
						enemy.GetComponent<Rigidbody2D>().velocity= Vector2.right*(5/knocfriend);
						enemy.bulletE.GetComponent<Rigidbody2D>().velocity = (Vector2.right*(5/knocfriend))/enemy.scalar;
					}
				}
			}
			else if(transform.root.gameObject.tag == "Enemy"){
				Move player = col.GetComponent<Move>();
				if (player != null)
				{
					int damag = Mathf.RoundToInt(Random.Range(dmg,dmg*11/10));
					player.colldetect(damag);
					animEnemy= col.GetComponent<Animator>();
					animEnemy.SetBool("Knockback", true);
					FindClosestEnemy();
					if(att.KNOCKBACK != 0){
						player.GetComponent<Rigidbody2D>().velocity= Vector2.right*(5/knocfriend);
						player.bulletP.GetComponent<Rigidbody2D>().velocity = (Vector2.right*(5/knocfriend))/player.scalar;
					}
				}
			}
		}
	}

	void FindClosestFriend() {
		knocfriend = 1;
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Player");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			attfr = go.GetComponent<UnitAtt>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(attfr.KNOCKBACK !=0 && go != gameObject){
					knocfriend += 1;
				}
				distance = diff;
			}
		}
	}

	void FindClosestEnemy() {
		knocfriend = 1;
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			attfr = go.GetComponent<UnitAtt>();
			float diff = Mathf.Abs(go.transform.position.x - position.x);
			if (diff < distance) {
				if(attfr.KNOCKBACK !=0 && go != gameObject){
					knocfriend += 1;
				}
				distance = diff;
			}
		}
	}

	IEnumerator explosionarea(){
		yield return new WaitForEndOfFrame();
		Instantiate(explode,transform.position+new Vector3(3,0,-15),transform.rotation);
	}
}
