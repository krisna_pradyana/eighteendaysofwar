﻿using UnityEngine;
using System.Collections;

public class TouchTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		int nbTouches = Input.touchCount;
		if(nbTouches == 1)
		{
			Touch touch = Input.GetTouch(0);
			if(touch.phase == TouchPhase.Began)
			{
				Ray screenRay = Camera.main.ScreenPointToRay(touch.position);
				RaycastHit2D hit = Physics2D.Raycast (screenRay.origin, screenRay.direction, Mathf.Infinity);
				if (hit) {
					if(hit.collider.gameObject.tag == "Indicator") {
						CameraControl.touchIndicator = true;
					}
					else {
						CameraControl.touchIndicator = false;
					}
				}
			}
		}
	}
}
