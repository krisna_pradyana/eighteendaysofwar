﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MoveE : MonoBehaviour {
	UnitAtt att; 
	public float speed;
	public static float speedbullet;
	public float health;
	private float maxHealth;
	private Animator animPlayer;
	private Animator animEnemy;
	private GameObject[] playerArray;
	private bool isHurt80 = true;
	private bool isHurt60 = false;
	private bool isHurt40 = false;
	private bool isHurt20 = false;
	public GameObject bullet;
	public GameObject damagePopup, dead;
	public GameObject bulletE;
	private GameObject spawnerBulletE, spawnerBulletP, spawnPointE, spawnPointP;
	private GameObject healthBarE,damagePopupE;
	private float spawnerBulletDistance, spawnPointDistance;
	public float scalar;
	private Vector3 charPosition;
	private Vector3 healthPosition, healthBGPosition;
	private int yposition;
	private bool showHealthBar = false;
	public GameObject boss1, boss2;
	public static bool hasBossDie = false;
	public static int enemy1 = 1;
	public static int enemy0 = 1;
	public static int enemy_1 = 1;
	public GameObject HealthBarEnemy, HealthBarEnemy2;
	private CameraControl cam;
	public static bool boss1Die = false, boss2Die = false;
	public static int kfootmanCount, karcherCount, kswordmanCount, kgunnerCount, kspearmanCount, kmageCount, kshamanCount, kelephantryCount, ktankCount;
	private AuraHeal aheal;

    void Start(){
        kfootmanCount = PlayerPrefs.GetInt ("FootmanSlayer");
		karcherCount = PlayerPrefs.GetInt ("ArcherSlayer");
		kswordmanCount = PlayerPrefs.GetInt ("SwordmanSlayer");
		kgunnerCount = PlayerPrefs.GetInt ("GunnerSlayer");
		kspearmanCount = PlayerPrefs.GetInt ("SpearmanSlayer");
		kmageCount = PlayerPrefs.GetInt ("MageSlayer");
		kshamanCount = PlayerPrefs.GetInt ("ShamanSlayer");
		kelephantryCount = PlayerPrefs.GetInt ("ElephantrySlayer");
		ktankCount = PlayerPrefs.GetInt ("TankSlayer");
		aheal = gameObject.GetComponentInChildren<AuraHeal> ();

		att=GameObject.Find(gameObject.name).GetComponent<UnitAtt>();
		animEnemy = gameObject.GetComponent<Animator> ();
		cam = Camera.main.GetComponent<CameraControl> ();

		spawnerBulletE = GameObject.Find ("SpawnerBulletE");
		spawnerBulletP = GameObject.Find ("SpawnerBulletP");
		spawnerBulletDistance = spawnerBulletE.transform.position.x - spawnerBulletP.transform.position.x;

		spawnPointE = GameObject.Find ("SpawnPointE");
		spawnPointP = GameObject.Find ("SpawnPointP");
		spawnPointDistance = spawnPointE.transform.position.x - spawnPointP.transform.position.x;

		scalar = (spawnPointDistance/spawnerBulletDistance);

		speed = att.SPD;
		health = att.HP;
		maxHealth = att.HP;

		healthBarE = transform.Find ("healthUnit").gameObject;
		HealthBarEnemy = GameObject.Find ("Main Camera/TopContainer/Health Enemy/Health Bar Enemy");
		HealthBarEnemy2 = GameObject.Find ("Main Camera/TopContainer/Health Enemy 2/Health Bar Enemy");

		bulletE = GameObject.Instantiate(bullet, spawnerBulletE.transform.position, spawnerBulletE.transform.rotation) as GameObject;
		bulletE.transform.parent = Camera.main.transform;
		dead.transform.localScale = new Vector3 (-3, 3, 1);
		moveEnemy ();
		showHealthBar = false;
	}
	// Update is called once per frame
	void Update () {


        //////
        ///     I Need To Fix This Stupidity
        //////
        if (MoveE.hasBossDie == true)
        {
            Debug.Log("BossHas DIED!!!!!!");
            if (SceneManager.GetActiveScene().name == "Stage1-6")
            {
                PlayerPrefs.SetInt("ClearStage6", 1);
                PlayerPrefs.SetInt("StarStage6", 1);
                PlayerPrefs.Save();
            }
        }
        //////
        ///
        //////  


        if (gameObject.name == "KArcherMiniTower" && MiniTower.isTowerAlive == true) {
			speed = 0;
			animEnemy.SetBool ("Idle", true);
			bulletE.GetComponent<SpriteRenderer>().enabled = false;
		}
		if (CastleEnemy.hasPlayerWin == true || CastlePlayer.hasEnemyWin == true) {
			Destroy (gameObject);
		}
		if (MoveE.hasBossDie == true || (MoveE.boss1Die == true && MoveE.boss2Die == true)) {
			Destroy (gameObject);	
		}
		transform.Translate (-Vector2.right * speed * Time.deltaTime);
		if (yposition == 1) {
			if (gameObject.transform.position.y <= -11 && gameObject.transform.position.x <=160) {
				transform.Translate (Vector2.up * speed * Time.deltaTime);
			}	
		}
		if (yposition == -1) {
			if (gameObject.transform.position.y >= -13) {
				transform.Translate (-Vector2.up * speed * Time.deltaTime);
			}	
		}
		if (yposition == -2) {
			if (gameObject.transform.position.y >= -14) {
				transform.Translate (-Vector2.up * speed * Time.deltaTime);
			}	
		}

		bulletE.transform.Translate (-Vector2.right * speed/(spawnPointDistance/spawnerBulletDistance) * Time.deltaTime);
		if (Application.loadedLevelName == "Stage3-6" || Application.loadedLevelName == "Stage5-6") {
			if (gameObject == boss1) {
				Vector3 scale = HealthBarEnemy.transform.localScale;
				scale.x = (health * 3) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				HealthBarEnemy.transform.localScale = scale;	
			}
			if (gameObject == boss2) {
				Vector3 scale = HealthBarEnemy2.transform.localScale;
				scale.x = (health * 3) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				HealthBarEnemy2.transform.localScale = scale;	
			}	
			if (gameObject.name == "klElephantryroot(Clone)" || gameObject.name == "ktank_root(Clone)") {
				Vector3 scale = healthBarE.transform.localScale;
				scale.x = -(health * 2) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				else if (health >= maxHealth) {
					scale.x = 2;
				}
				healthBarE.transform.localScale = scale;
			}
			else {
				Vector3 scale = healthBarE.transform.localScale;
				scale.x = -(health * 1) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				else if (health >= maxHealth) {
					scale.x = 1;
				}
				healthBarE.transform.localScale = scale;
			}
		}
		else {
			if (gameObject == boss1) {
                if(HealthBarEnemy == null)
                {
                    HealthBarEnemy = GameObject.FindGameObjectWithTag("HPEnemy");
                }

                Vector3 scale = HealthBarEnemy.transform.localScale;
				scale.x = (health * 3) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				HealthBarEnemy.transform.localScale = scale;	
			}
			if (gameObject.name == "klElephantryroot(Clone)" || gameObject.name == "ktank_root(Clone)") {
				Vector3 scale = healthBarE.transform.localScale;
				scale.x = -(health * 2) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				else if (health >= maxHealth) {
					scale.x = 2;
				}
				healthBarE.transform.localScale = scale;
			}
			else {
				Vector3 scale = healthBarE.transform.localScale;
				scale.x = -(health * 1) / maxHealth;
				if (health <= 0) {
					scale.x = 0;	
				}
				else if (health >= maxHealth) {
					scale.x = 1;
				}
				healthBarE.transform.localScale = scale;
			}
			if(bulletE.GetComponent<Rigidbody2D>().velocity != Vector2.right*0 && gameObject == boss1 || bulletE.GetComponent<Rigidbody2D>().velocity != Vector2.right*0 && gameObject == boss2){
				bulletE.GetComponent<Rigidbody2D>().velocity = Vector2.right*0;
			}
		}

		if (CastlePlayer.hasEnemyWin == true) {
			gameObject.GetComponent<AudioSource>().volume = 0;	
		}
		if (showHealthBar == true) {
			if (gameObject != boss1 && gameObject != boss2) {
				gameObject.transform.Find("healthUnit").GetComponent<SpriteRenderer>().enabled = true;
				gameObject.transform.Find("healthUnitBack").GetComponent<SpriteRenderer>().enabled = true;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "Enemy" || coll.gameObject.tag == "ProjectileE" || coll.gameObject.tag == "UI") {
			Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(),coll.gameObject.GetComponent<Collider2D>());		
		}
	}

	public void colldetect(float damage){
		if(health > 0) {
			health -= damage;
			showHealthBar = true;
			Vector3 yPosition = new Vector3(0, 4, 0);
			damagePopupE = Instantiate (damagePopup, gameObject.transform.position+yPosition, Quaternion.identity) as GameObject;
			damagePopupE.GetComponent<EasyFontTextMesh>().Text = damage.ToString();
			damagePopupE.transform.parent = transform;
			if(health < (att.HP * 0.8) && isHurt80) {
				animEnemy.SetTrigger("Hurt");
				isHurt80 = false;
				isHurt60 = true;
			}
			else if (health < (att.HP * 0.6) && isHurt60) {
				animEnemy.SetTrigger("Hurt");
				isHurt60 = false;
				isHurt40 = true;
			}
			else if (health < (att.HP * 0.4) && isHurt40) {
				animEnemy.SetTrigger("Hurt");
				isHurt40 = false;
				isHurt20 = true;
			}
			else if (health < (att.HP * 0.2) && isHurt20) {
				animEnemy.SetTrigger("Hurt");
				isHurt20 = false;
			}
			if(health <= 0){
				if(aheal != null){
					aheal.AuraOffAll("Enemy");
				}
				if (gameObject.name == "kfootman_root(Clone)") {
					kfootmanCount += 1;
					PlayerPrefs.SetInt ("FootmanSlayer", kfootmanCount);
				}
				else if (gameObject.name == "kArcherRoot(Clone)") {
					karcherCount += 1;
					PlayerPrefs.SetInt ("ArcherSlayer", karcherCount);
				}
				else if (gameObject.name == "kswordman_Root(Clone)") {
					kswordmanCount += 1;
					PlayerPrefs.SetInt ("SswordmanSlayer", kswordmanCount);
				}
				else if (gameObject.name == "kgunnerRoot(Clone)") {
					kgunnerCount += 1;
					PlayerPrefs.SetInt ("GunnerSlayer", kgunnerCount);
				}
				else if (gameObject.name == "kSpearmanRoot(Clone)") {
					kspearmanCount += 1;
					PlayerPrefs.SetInt ("SpearmanSlayer", kspearmanCount);
				}
				else if (gameObject.name == "kMageRoot(Clone)") {
					kmageCount += 1;
					PlayerPrefs.SetInt ("MageSlayer", kmageCount);
				}
				else if (gameObject.name == "kshaman_root(Clone)") {
					kshamanCount += 1;
					PlayerPrefs.SetInt ("ShamanSlayer", kshamanCount);
				}
				else if (gameObject.name == "klElephantryroot(Clone)") {
					kelephantryCount += 1;
					PlayerPrefs.SetInt ("ElephantrySlayer", kelephantryCount);
				}
				else if (gameObject.name == "ktank_root(Clone)") {
					ktankCount += 1;
					PlayerPrefs.SetInt ("TankSlayer", ktankCount);
				}
				Instantiate(dead,gameObject.transform.position+new Vector3(-0,1.5f,0),gameObject.transform.rotation);
				playAnimPlayer();
				if (Application.loadedLevelName == "Stage3-6" || Application.loadedLevelName == "Stage5-6") {
					if (gameObject == boss1) {
						boss1Die = true;
					}
					if (gameObject == boss2) {
						boss2Die = true;
					}
				}
				else {
					if (gameObject == boss1) {
						hasBossDie = true;
					}
				}
				Destroy (gameObject);
				Destroy (bulletE);
			}
		} 
	}
	void playAnimPlayer() {
		playerArray = GameObject.FindGameObjectsWithTag ("Player");
		foreach (GameObject player in playerArray) {
			if(player != GameObject.Find("PlayerCastle")){
				animPlayer = player.GetComponent<Animator>();
				animPlayer.SetBool("Kill",true);
				animPlayer.speed = 1;
			}
		}
	}
	void moveEnemy() {
		Vector3 finalposition = new Vector3(0, 0, 0);
		Vector3 position = new Vector3 (0, Random.Range(-1, 2), 0);
		if (position.y == 1) {
			finalposition = new Vector3 (0, 0, (enemy1*0.5f)+55);
			yposition = 1;
			enemy1 += 2;
			if (enemy1 >= 40) {
				enemy1 = 0;
			}
		}
		if (position.y == 0) {
			finalposition = new Vector3 (0, 0, (enemy0*0.5f)+30);
			yposition = 0;
			enemy0 += 2;
			if (enemy0 >= 40) {
				enemy0 = 0;
			}
		}
		if (position.y == -1) {
			finalposition = new Vector3 (0, 0, (enemy_1*0.5f)+1);
			yposition = -1;
			enemy_1 += 2;
			if (enemy_1 >= 40) {
				enemy_1 = 0;
			}
		}
		if (cam.bosslevel == true) {
			if (gameObject == boss1) {
				finalposition = new Vector3 (0, 0, 0);
				yposition = -1;	
			}
			if (gameObject == boss2) {
				finalposition = new Vector3 (0, 0, -5);
				yposition = 1;
			}
		}
		if(gameObject.name == "klElephantryroot(Clone)") {
			finalposition = new Vector3 (0, 0, (enemy1*0.5f)+65);
			yposition = 1;
			enemy1 += 2;
			if (enemy1 >= 40) {
				enemy1 = 0;
			}
		}
		if(gameObject.name == "ktank_root(Clone)") {
			finalposition = new Vector3 (0, 0, (enemy1*0.5f)+60);
			yposition = 2;
			enemy1 += 2;
			if (enemy1 >= 40) {
				enemy1 = 0;
			}
		}
		gameObject.transform.position += finalposition;
	}
}
