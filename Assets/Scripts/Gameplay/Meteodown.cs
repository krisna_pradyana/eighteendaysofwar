﻿using UnityEngine;
using System.Collections;

public class Meteodown : MonoBehaviour {
	public GameObject Effect;
	private float speed = 0, range = 0;
	public AudioClip sfxExplosion;
	void Start(){
		if (gameObject.name == "meteor ice root") {
			speed = Random.Range(18,20);
			range = Random.Range(15,17);
		}
		else{
			speed = 20;
			range = 15;
		}
	}

	// Update is called once per frame
	void Update () {
		if (Time.timeScale > 0 && Time.timeScale != 0.5f) {
			Time.timeScale = 0.5f;
		}
		if (Time.timeScale != 0) {
			transform.Translate (-Vector2.up * speed * Time.deltaTime);
			transform.Translate (Vector2.right * range * Time.deltaTime);
			speed +=0.2f;
			range +=0.1f;
			if (transform.localPosition.y <= 1) {
				AudioSource.PlayClipAtPoint (sfxExplosion, transform.position);
				Instantiate (Effect, transform.position, transform.rotation);
				Destroy (gameObject);
			}		
		}
	}
}
