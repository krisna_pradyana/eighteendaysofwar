﻿using UnityEngine;
using System.Collections;

public class DamagePopup : MonoBehaviour {
	private float time_;
	public float time_to_fade;
	private float fadeText;
	private EasyFontTextMesh eft;
	void  Start (){
		time_ = Time.time;
		eft = gameObject.GetComponent<EasyFontTextMesh> ();
	}
	
	void  Update (){
		transform.Translate(new Vector3(0,0.01f,0));
		fadeText = Mathf.Cos((Time.time - time_)*((Mathf.PI/2)/time_to_fade));
		eft.SetColor (new Color (eft.FontColorTop.r,eft.FontColorTop.g ,eft.FontColorTop.b , fadeText));
		Destroy (gameObject,time_to_fade);
	}
}