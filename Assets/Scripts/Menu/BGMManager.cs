﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BGMManager : MonoBehaviour
{
    static BGMManager _instance;

    string loadedScene;
    bool isPlaying;
    public AudioSource[] bgmAudio;
    public AudioSource playedAudio;
    public string currentName;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    private void OnEnable()
    {
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
    }

    private void OnDisable()
    {
        SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
    }

    private void SceneManager_activeSceneChanged(Scene current, Scene next)
    {
        currentName = next.name;
        Debug.Log("Checked Changed Scene");
        if (currentName == "TitleScreen" || currentName == "Achivement")
        {
            Debug.Log("Check this block");
            if (next.name == "TitleScreen" || next.name == "Achivement")
            {
                //return;
                currentName = next.name;
            }
            else
            {

                playedAudio.Stop();
                playedAudio = bgmAudio[(int)BGMCategory.Menu];
                playedAudio.Play();
                currentName = next.name;
            }
        }

        if (currentName == "MainMenu" || currentName == "Purchase Gem" || currentName == "Purchase Gold" || currentName == "StageSelect")
        {
            if(next.name == "MainMenu" || next.name == "Purchase Gem" || next.name == "Purchase Gold" || next.name == "StageSelect")
            {
                //return;
                currentName = next.name;
            }
            else
            {
                Debug.Log("AudioCalled");
                playedAudio.Stop();
                playedAudio = bgmAudio[(int)BGMCategory.Title];
                playedAudio.Play();
                currentName = next.name;
            }
        }
        return;
    }

    private void Start()
    {
        playedAudio = bgmAudio[(int)BGMCategory.Title];
        playedAudio.Play();
        currentName = "TitleScreen";
    }
}
