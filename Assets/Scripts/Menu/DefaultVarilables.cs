﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultVarilables : MonoBehaviour
{
    bool firstPlay;

    void Awake()
    {
        if (Application.isEditor == false)
        {
            if (PlayerPrefs.GetInt("FirstPlay", 1) == 1)
            {
                firstPlay = true;
                PlayerPrefs.SetInt("FirstPlay", 0);
                PlayerPrefs.SetInt("Gem", 0);
                PlayerPrefs.SetInt("Gold", 5000);
                PlayerPrefs.SetInt("Energy", 120);
                PlayerPrefs.Save();
            }
            else
                firstPlay = false;

        }
    }
}
