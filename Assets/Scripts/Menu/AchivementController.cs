﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchivementController : MonoBehaviour {
	private int starStage1, starStage2, starStage3, starStage4, starStage5, starStage6;
	private int starStage7, starStage8, starStage9, starStage10, starStage11, starStage12;
	private int starStage13, starStage14, starStage15, starStage16, starStage17, starStage18;
	private int starStage19, starStage20, starStage21, starStage22, starStage23, starStage24;
	private int starStage25, starStage26, starStage27, starStage28, starStage29, starStage30;
	private int starStage31, starStage32, starStage33, starStage34, starStage35, starStage36;
	private int starStage37, starStage38, starStage39, starStage40, starStage41, starStage42;
	private int starStage43, starStage44, starStage45, starStage46, starStage47, starStage48;
	private int starStage49, starStage50, starStage51, starStage52, starStage53, starStage54;
	public GameObject bar, complete, reward, share, claim, achivement;
	public int achivementLimit, rewardCount;
	private int achivementCount, totalStars;
	private int claimFirstPlay, claimGraduate, claimIronFist, claimBlazingMonkey, claimMahabharatWarrior, claimMahabharatChampion, claimKnight, claimPrince, claimKing, claimAmateur, claimProfessional, claimMaster, claimCaptain, claimCommander, claimGeneral;
	private int claimFootman, claimArcher, claimSwordman, claimGunner, claimSpearman, claimMage, claimShaman, claimElephantry, claimTank;
	private int claimTerribleOath, claimSonOfSurya, claimDoubleTrouble, claimEclipse, claimCoupleOrTrouble, claimCraftyTacticion, claimToughRuler, claimTheImmortal, claimUnconquerableWarrior;
	public static int achivementCompleteCount;
	private int unlockUnit, unlockHero, secondTimePlaying;

    public GoldContainer GoldCntr;

	// Your app’s unique identifier.
	private string AppID = "1518031601790532";
	
	// The link attached to this post.
	private string Link = "https://www.facebook.com/MahabharatWars";
	
	// The URL of a picture attached to this post. The picture must be at least 200px by 200px.
	private string Picture = "http://falkanastudio.com/mwicon.png";
	
	// The name of the link attachment.
	private string Name = "Mahabharat Warriors";
	
	// The caption of the link (appears beneath the link name).
	//private string Caption = "I just beat day 1-1. Can you beat it?";
	
	// The description of the link (appears beneath the link caption). 
	private string Description = "Pandawa warriors are waiting for you. Click this link to download Mahabharat Warriors and let us defeat Kurawa Armies together.";
	
	private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	private const string TWEET_LANGUAGE = "en"; 
	// Use this for initialization
	void Start () {
        GoldCntr = new GoldContainer();

		totalStars = PlayerPrefs.GetInt ("TotalStars");
		starStage1 = PlayerPrefs.GetInt ("StarStage1");
		starStage2 = PlayerPrefs.GetInt ("StarStage2");
		starStage3 = PlayerPrefs.GetInt ("StarStage3");
		starStage4 = PlayerPrefs.GetInt ("StarStage4");
		starStage5 = PlayerPrefs.GetInt ("StarStage5");	
		starStage6 = PlayerPrefs.GetInt ("StarStage6");
		starStage7 = PlayerPrefs.GetInt ("StarStage7");
		starStage8 = PlayerPrefs.GetInt ("StarStage8");
		starStage9 = PlayerPrefs.GetInt ("StarStage9");
		starStage10 = PlayerPrefs.GetInt ("StarStage10");
		starStage11 = PlayerPrefs.GetInt ("StarStage11");	
		starStage12 = PlayerPrefs.GetInt ("StarStage12");
		starStage13 = PlayerPrefs.GetInt ("StarStage13");
		starStage14 = PlayerPrefs.GetInt ("StarStage14");
		starStage15 = PlayerPrefs.GetInt ("StarStage15");
		starStage16 = PlayerPrefs.GetInt ("StarStage16");
		starStage17 = PlayerPrefs.GetInt ("StarStage17");	
		starStage18 = PlayerPrefs.GetInt ("StarStage18");
		starStage19 = PlayerPrefs.GetInt ("StarStage19");
		starStage20 = PlayerPrefs.GetInt ("StarStage20");
		starStage21 = PlayerPrefs.GetInt ("StarStage21");
		starStage22 = PlayerPrefs.GetInt ("StarStage22");
		starStage23 = PlayerPrefs.GetInt ("StarStage23");	
		starStage24 = PlayerPrefs.GetInt ("StarStage24");
		starStage25 = PlayerPrefs.GetInt ("StarStage25");
		starStage26 = PlayerPrefs.GetInt ("StarStage26");
		starStage27 = PlayerPrefs.GetInt ("StarStage27");
		starStage28 = PlayerPrefs.GetInt ("StarStage28");
		starStage29 = PlayerPrefs.GetInt ("StarStage29");	
		starStage30 = PlayerPrefs.GetInt ("StarStage30");
		starStage31 = PlayerPrefs.GetInt ("StarStage31");
		starStage32 = PlayerPrefs.GetInt ("StarStage32");
		starStage33 = PlayerPrefs.GetInt ("StarStage33");
		starStage34 = PlayerPrefs.GetInt ("StarStage34");
		starStage35 = PlayerPrefs.GetInt ("StarStage35");	
		starStage36 = PlayerPrefs.GetInt ("StarStage36");
		starStage37 = PlayerPrefs.GetInt ("StarStage37");
		starStage38 = PlayerPrefs.GetInt ("StarStage38");
		starStage39 = PlayerPrefs.GetInt ("StarStage39");
		starStage40 = PlayerPrefs.GetInt ("StarStage40");
		starStage41 = PlayerPrefs.GetInt ("StarStage41");	
		starStage42 = PlayerPrefs.GetInt ("StarStage42");
		starStage43 = PlayerPrefs.GetInt ("StarStage43");
		starStage44 = PlayerPrefs.GetInt ("StarStage44");
		starStage45 = PlayerPrefs.GetInt ("StarStage45");
		starStage46 = PlayerPrefs.GetInt ("StarStage46");
		starStage47 = PlayerPrefs.GetInt ("StarStage47");	
		starStage48 = PlayerPrefs.GetInt ("StarStage48");
		starStage49 = PlayerPrefs.GetInt ("StarStage49");
		starStage50 = PlayerPrefs.GetInt ("StarStage50");
		starStage51 = PlayerPrefs.GetInt ("StarStage51");
		starStage52 = PlayerPrefs.GetInt ("StarStage52");
		starStage53 = PlayerPrefs.GetInt ("StarStage53");	
		starStage54 = PlayerPrefs.GetInt ("StarStage54");
		totalStars = starStage1+starStage2+starStage3+starStage4+starStage5+starStage6+
				starStage7+starStage8+starStage9+starStage10+starStage11+starStage12+
				starStage13+starStage14+starStage15+starStage16+starStage17+starStage18+
				starStage19+starStage20+starStage21+starStage22+starStage23+starStage24+
				starStage25+starStage26+starStage27+starStage28+starStage29+starStage30+
				starStage31+starStage32+starStage33+starStage34+starStage35+starStage36+
				starStage37+starStage38+starStage39+starStage40+starStage41+starStage42+
				starStage43+starStage44+starStage45+starStage46+starStage47+starStage48+
				starStage49+starStage50+starStage51+starStage52+starStage53+starStage54;
		PlayerPrefs.SetInt ("TotalStars", totalStars);
		achivementCompleteCount = PlayerPrefs.GetInt ("AchievementComplete");

		claimFirstPlay = PlayerPrefs.GetInt ("ClaimFirstPlay");
		claimGraduate = PlayerPrefs.GetInt ("ClaimGraduate");
		claimIronFist = PlayerPrefs.GetInt ("ClaimIronFist");
		claimBlazingMonkey = PlayerPrefs.GetInt ("ClaimBlazingMonkey");
		claimMahabharatWarrior = PlayerPrefs.GetInt ("MahabharatWarrior");
		claimMahabharatChampion = PlayerPrefs.GetInt ("MahabharatChampion");
		claimCaptain = PlayerPrefs.GetInt ("ClaimCaptain");
		claimCommander = PlayerPrefs.GetInt ("ClaimCommander");
		claimGeneral = PlayerPrefs.GetInt ("ClaimGeneral");
		claimKnight = PlayerPrefs.GetInt ("ClaimKnight");
		claimPrince = PlayerPrefs.GetInt ("ClaimPrince");
		claimKing = PlayerPrefs.GetInt ("ClaimKing");
		claimAmateur = PlayerPrefs.GetInt ("ClaimAmateur");
		claimProfessional = PlayerPrefs.GetInt ("ClaimProfessional");
		claimMaster = PlayerPrefs.GetInt ("ClaimMaster");

		claimFootman = PlayerPrefs.GetInt ("ClaimFootman");
		claimArcher = PlayerPrefs.GetInt ("ClaimArcher");
		claimSwordman = PlayerPrefs.GetInt ("ClaimSwordman");
		claimGunner = PlayerPrefs.GetInt ("ClaimGunner");
		claimSpearman = PlayerPrefs.GetInt ("ClaimSpearman");
		claimMage = PlayerPrefs.GetInt ("ClaimMage");
		claimShaman = PlayerPrefs.GetInt ("ClaimShaman");
		claimElephantry = PlayerPrefs.GetInt ("ClaimElephantry");
		claimTank = PlayerPrefs.GetInt ("ClaimTank");

		claimTerribleOath = PlayerPrefs.GetInt ("ClaimTerribleOath");
		claimSonOfSurya = PlayerPrefs.GetInt ("ClaimSonOfSurya");
		claimDoubleTrouble = PlayerPrefs.GetInt ("ClaimDoubleTrouble");
		claimEclipse = PlayerPrefs.GetInt ("ClaimEclipse");
		claimCoupleOrTrouble = PlayerPrefs.GetInt ("ClaimCoupleOrTrouble");
		claimCraftyTacticion = PlayerPrefs.GetInt ("ClaimCraftyTacticion");
		claimToughRuler = PlayerPrefs.GetInt ("ClaimToughRuler");
		claimTheImmortal = PlayerPrefs.GetInt ("ClaimTheImmortal");
		claimUnconquerableWarrior = PlayerPrefs.GetInt ("ClaimUnconquerableWarrior");

		if (PlayerPrefs.GetInt("UnlockHero") == 0 && PlayerPrefs.GetInt("UnlockUnit") == 0) {
			unlockUnit = 1;
			unlockHero = 1;
			PlayerPrefs.SetInt ("UnlockUnit", unlockUnit);
			PlayerPrefs.SetInt ("UnlockHero", unlockHero);
		}
		else {
			unlockUnit = PlayerPrefs.GetInt ("UnlockUnit");
			unlockHero = PlayerPrefs.GetInt ("UnlockHero");
		}

		if (gameObject.name == "First Play") {
			if (claimFirstPlay == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("FirstPlay");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Graduate") {
			if (claimGraduate == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("Graduate");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Iron Fist") {
			if (claimIronFist == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("IronFist");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Blazing Monkey") {
			if (claimBlazingMonkey == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("BlazingMonkey");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Mahabharat Warrior") {
			if (claimMahabharatWarrior == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("TotalStars");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Mahabharat Champion") {
			if (claimMahabharatChampion == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("TotalStars");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Captain") {
			if (claimCaptain == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockUnit");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Commander") {
			if (claimCommander == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockUnit");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "General") {
			if (claimGeneral == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockUnit");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Knight") {
			if (claimKnight == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockHero");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Prince") {
			if (claimPrince == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockHero");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "King") {
			if (claimKing == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnlockHero");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Amateur") {
			if (claimAmateur == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("Victory");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Professional") {
			if (claimProfessional == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("Victory");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Master") {
			if (claimMaster == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("Victory");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}	
		}
		else if (gameObject.name == "Footman") {
			if (claimFootman == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("FootmanSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Archer") {
			if (claimArcher == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("ArcherSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}

		}
		else if (gameObject.name == "Swordman") {
			if (claimSwordman == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("SwordmanSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}

		}
		else if (gameObject.name == "Gunner") {
			if (claimGunner == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("GunnerSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
				
		}
		else if (gameObject.name == "Spearman") {
			if (claimSpearman == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("SpearmanSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Mage") {
			if (claimMage == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("MageSlayer");	
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Shaman") {
			if (claimShaman == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("ShamanSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}

		}
		else if (gameObject.name == "Elephantry") {
			if (claimElephantry == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("ElephantrySlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
			
		}
		else if (gameObject.name == "Tank") {
			if (claimTank == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("TankSlayer");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Terrible Oath") {
			if (claimTerribleOath == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("TerribleOath");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
			
		}
		else if (gameObject.name == "Son of Surya") {
			if (claimSonOfSurya == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("SonOfSurya");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
			
		}
		else if (gameObject.name == "Double Trouble") {
			if (claimDoubleTrouble == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("DoubleTrouble");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Eclipse") {
			if (claimEclipse == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("Eclipse");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
			
		}
		else if (gameObject.name == "Couple or Trouble") {
			if (claimCoupleOrTrouble == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("CoupleOrTrouble");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
			
		}
		else if (gameObject.name == "Crafty Tacticion") {
			if (claimCraftyTacticion == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("CraftyTacticion");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		if (achivementCount >= achivementLimit && gameObject.name != "Share") {
			bar.SetActive(false);
			claim.SetActive(true);
		}
		else if (gameObject.name == "Tough Ruler") {
			if (claimToughRuler == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("ToughRuler");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		if (achivementCount >= achivementLimit && gameObject.name != "Share") {
			bar.SetActive(false);
			claim.SetActive(true);
		}
		else if (gameObject.name == "The Immortal") {
			if (claimTheImmortal == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("TheImmortal");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		else if (gameObject.name == "Unconquerable Warrior") {
			if (claimUnconquerableWarrior == 1) {
				bar.SetActive(false);
				claim.SetActive(false);
				reward.SetActive(false);
				complete.SetActive(true);
				share.SetActive(true);
			}
			else {
				achivementCount = PlayerPrefs.GetInt ("UnconquerableWarrior");
				bar.GetComponentInChildren<Text> ().text = achivementCount.ToString() + " / "+achivementLimit.ToString();
			}
		}
		if (achivementCount >= achivementLimit && gameObject.name != "Share") {
			bar.SetActive(false);
			claim.SetActive(true);
		}
		if (achivementCount >= achivementLimit && gameObject.name != "Share") {
			bar.SetActive(false);
			claim.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		achivement.GetComponent<EasyFontTextMesh> ().Text = achivementCompleteCount + " / 12";
	}
	public void ClaimReward() {
		if (gameObject.name == "First Play") {
            GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimFirstPlay", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Graduate") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimGraduate", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Iron Fist") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimIronFist", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Blazing Monkey") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimBlazingMonkey", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Mahabharat Warrior") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimMahabharatWarrior", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Mahabharat Champion") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimMahabharatChampion", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Captain") {
			PlayerPrefs.SetInt ("MovementSpeed10", rewardCount);
			PlayerPrefs.SetInt ("ClaimCaptain", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Commander") {
			PlayerPrefs.SetInt ("MovementSpeed10", rewardCount);
			PlayerPrefs.SetInt ("ClaimCommander", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "General") {
			PlayerPrefs.SetInt ("MovementSpeed30", rewardCount);
			PlayerPrefs.SetInt ("ClaimGeneral", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Knight") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimKnight", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Prince") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimPrince", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "King") {
			GameObject.Find ("GemContainer").SendMessage ("IncreaseGem", rewardCount);
			PlayerPrefs.SetInt ("ClaimKing", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Amateur") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimAmateur", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Professional") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimProfessional", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Master") {
			GameObject.Find ("GemContainer").SendMessage ("IncreaseGem", rewardCount);
			PlayerPrefs.SetInt ("ClaimMaster", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Footman") {
			PlayerPrefs.SetInt ("Resource10", rewardCount);
			PlayerPrefs.SetInt ("ClaimFootman", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Archer") {
			PlayerPrefs.SetInt ("Resource30", rewardCount);
			PlayerPrefs.SetInt ("ClaimArcher", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Swordman") {
			PlayerPrefs.SetInt ("HP10", rewardCount);
			PlayerPrefs.SetInt ("ClaimSwordman", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Gunner") {
			PlayerPrefs.SetInt ("HP30", rewardCount);
			PlayerPrefs.SetInt ("ClaimGunner", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Spearman") {
			PlayerPrefs.SetInt ("MovementSpeed10", rewardCount);
			PlayerPrefs.SetInt ("ClaimSpearman", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Mage") {
			PlayerPrefs.SetInt ("MovementSpeed30", rewardCount);
			PlayerPrefs.SetInt ("ClaimMage", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Shaman") {
			GameObject.Find ("GemContainer").SendMessage ("IncreaseGem", rewardCount);
			PlayerPrefs.SetInt ("ClaimShaman", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Elephantry") {
			GameObject.Find ("GemContainer").SendMessage ("IncreaseGem", rewardCount);
			PlayerPrefs.SetInt ("ClaimElephantry", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Tank") {
			GameObject.Find ("GemContainer").SendMessage ("IncreaseGem", rewardCount);
			PlayerPrefs.SetInt ("ClaimTank", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Terrible Oath") {
			PlayerPrefs.SetInt ("HP10", rewardCount);
			PlayerPrefs.SetInt ("ClaimTerribleOath", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Son of Surya") {
			PlayerPrefs.SetInt ("Attack10", rewardCount);
			PlayerPrefs.SetInt ("ClaimSonOfSurya", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);
		}
		else if (gameObject.name == "Double Trouble") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimDoubleTrouble", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Eclipse") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimEclipse", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Couple or Trouble") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimCoupleOrTrouble", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Crafty Tacticion") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimCraftyTacticion", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "The Immortal") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimTheImmortal", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Tough Ruler") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimToughRuler", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
		else if (gameObject.name == "Unconquerable Warrior") {
			  GoldCntr.IncreaseGold(rewardCount);
			PlayerPrefs.SetInt ("ClaimUnconquerableWarrior", 1);
			achivementCompleteCount += 1;
			PlayerPrefs.SetInt ("AchievementComplete", achivementCompleteCount);
			claim.SetActive(false);
			reward.SetActive(false);
			complete.SetActive(true);
			share.SetActive(true);	
		}
	}
	public void ShareToTwitter(string Caption)
	{
		if (gameObject.name == "Share") {
			Application.OpenURL(TWITTER_ADDRESS +
			                    "?text=" + WWW.EscapeURL("I have completed "+PlayerPrefs.GetInt("AchievementComplete").ToString()+" achievement"+"#mahabharatwarriors") +
			                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));	
		}
		else {
			Application.OpenURL(TWITTER_ADDRESS +
			                    "?text=" + WWW.EscapeURL(Caption+"#mahabharatwarriors") +
			                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
		}

	}
	public void ShareToFB(string Caption){
		if (gameObject.name == "Share") {
			Application.OpenURL("https://www.facebook.com/dialog/feed?"+
			                    "app_id="+AppID+
			                    "&link="+Link+
			                    "&picture="+Picture+
			                    "&name="+ReplaceSpace(Name)+
			                    "&caption="+ReplaceSpace("I have completed "+PlayerPrefs.GetInt("AchievementComplete").ToString()+" achievement")+
			                    "&description="+ReplaceSpace(Description)+
			                    "&redirect_uri=https://facebook.com/");	
		}
		else {
			Application.OpenURL("https://www.facebook.com/dialog/feed?"+
			                    "app_id="+AppID+
			                    "&link="+Link+
			                    "&picture="+Picture+
			                    "&name="+ReplaceSpace(Name)+
			                    "&caption="+ReplaceSpace(Caption)+
			                    "&description="+ReplaceSpace(Description)+
			                    "&redirect_uri=https://facebook.com/");
		}

	}
	string ReplaceSpace (string val) {
		return val.Replace(" ", "%20");    
	}
}
