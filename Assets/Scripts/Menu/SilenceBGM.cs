﻿using UnityEngine;
using System.Collections;

public class SilenceBGM : MonoBehaviour {
	private GameObject[] arrayBGM;
	// Use this for initialization
	void Awake () {
		arrayBGM = GameObject.FindGameObjectsWithTag ("BGM");
		if (arrayBGM.Length == 1) {
			DontDestroyOnLoad(gameObject);	
		}
		else {
			DestroyObject(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.loadedLevelName != "Purchase Gem") {
			if (PauseClick.enableBGM == true && Application.loadedLevel == 2) {
				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;	
			}
			else if (PauseClick.enableBGM == false && Application.loadedLevel == 2){
				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
			}
            else if (PauseClick.enableBGM == true && (Application.loadedLevelName == "Shop" || Application.loadedLevelName == "Survival Mode" || Application.loadedLevelName == "Community Challenge" ||
			                                           Application.loadedLevelName == "Stage Selection" || Application.loadedLevelName == "Stage Selection2" || Application.loadedLevelName == "Stage Selection3" ||
			                                           Application.loadedLevelName == "Stage Selection4" || Application.loadedLevelName == "Stage Selection5" || Application.loadedLevelName == "Stage Selection6" ||
			                                           Application.loadedLevelName == "Stage Selection7" || Application.loadedLevelName == "Stage Selection8" || Application.loadedLevelName == "Stage Selection9" ||
			                                           Application.loadedLevelName == "Purchase Gem" || Application.loadedLevelName == "Purchase Gold" || Application.loadedLevelName == "Purchase Energy")){
				GameObject.Find ("BGM").GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
			}
            else if (PauseClick.enableBGM == true && (Application.loadedLevelName == "Shop" || Application.loadedLevelName == "Survival Mode" || Application.loadedLevelName == "Community Challenge" ||
			                                           Application.loadedLevelName == "Stage Selection" || Application.loadedLevelName == "Stage Selection2" || Application.loadedLevelName == "Stage Selection3" ||
			                                           Application.loadedLevelName == "Stage Selection4" || Application.loadedLevelName == "Stage Selection5" || Application.loadedLevelName == "Stage Selection6" ||
			                                           Application.loadedLevelName == "Stage Selection7" || Application.loadedLevelName == "Stage Selection8" || Application.loadedLevelName == "Stage Selection9" ||
			                                           Application.loadedLevelName == "Purchase Gem" || Application.loadedLevelName == "Purchase Gold" || Application.loadedLevelName == "Purchase Energy")){
				GameObject.Find ("BGM").GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
			}
//			else if (PauseClick.disableBGM == true && Application.loadedLevel > 9) {
//				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
//			}
//			else if (PauseClick.disableBGM == false && Application.loadedLevel > 9) {
//				Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
//			}
            else if (PauseClick.enableBGM == true && Application.loadedLevel > 15) {
                Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;
            }
            else if (PauseClick.enableBGM == false && Application.loadedLevel > 15) {
                Camera.main.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;
            }
		}
		else {
            /*
			if (Gemshard.showVideoAds == true) {
				GameObject.Find ("BGM").GetComponent<AudioSource>().volume = 0;
			}
			else {
				GameObject.Find ("BGM").GetComponent<AudioSource>().volume = 1;
			}
            */
		}
	}
}
