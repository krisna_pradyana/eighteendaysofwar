﻿using UnityEngine;
using System.Collections;

public class AchivementNotif : MonoBehaviour {
	private int completeFirstPlay, completeGraduate, completeIronFist;
	private int killCountFootman, killCountArcher, killCountSwordman, killCountGunner, killCountSpearman, killCountMage, killCountShaman, killCountElephantry, killCountTank;
	private int unlockUnitCount, unlockHeroCount, victoryCount;
	private int completeTerribleOath, completeSonOfSurya, completeDoubleTrouble, completeEclipse, completeCoupleOrTrouble, completeCraftyTacticion;

	private int claimFirstPlay, claimGraduate, claimIronFist, claimKnight, claimPrince, claimKing, claimAmateur, claimProfessional, claimMaster, claimCaptain, claimCommander, claimGeneral;
	private int claimFootman, claimArcher, claimSwordman, claimGunner, claimSpearman, claimMage, claimShaman, claimElephantry, claimTank;
	private int claimTerribleOath, claimSonOfSurya, claimDoubleTrouble, claimEclipse, claimCoupleOrTrouble, claimCraftyTacticion;
	// Use this for initialization
	void Start () {
		claimFirstPlay = PlayerPrefs.GetInt ("ClaimFirstPlay");
		claimGraduate = PlayerPrefs.GetInt ("ClaimGraduate");
		claimIronFist = PlayerPrefs.GetInt ("ClaimIronFist");
		claimCaptain = PlayerPrefs.GetInt ("ClaimCaptain");
		claimCommander = PlayerPrefs.GetInt ("ClaimCommander");
		claimGeneral = PlayerPrefs.GetInt ("ClaimGeneral");
		claimKnight = PlayerPrefs.GetInt ("ClaimKnight");
		claimPrince = PlayerPrefs.GetInt ("ClaimPrince");
		claimKing = PlayerPrefs.GetInt ("ClaimKing");
		claimAmateur = PlayerPrefs.GetInt ("ClaimAmateur");
		claimProfessional = PlayerPrefs.GetInt ("ClaimProfessional");
		claimMaster = PlayerPrefs.GetInt ("ClaimMaster");
		
		claimFootman = PlayerPrefs.GetInt ("ClaimFootman");
		claimArcher = PlayerPrefs.GetInt ("ClaimArcher");
		claimSwordman = PlayerPrefs.GetInt ("ClaimSwordman");
		claimGunner = PlayerPrefs.GetInt ("ClaimGunner");
		claimSpearman = PlayerPrefs.GetInt ("ClaimSpearman");
		claimMage = PlayerPrefs.GetInt ("ClaimMage");
		claimShaman = PlayerPrefs.GetInt ("ClaimShaman");
		claimElephantry = PlayerPrefs.GetInt ("ClaimElephantry");
		claimTank = PlayerPrefs.GetInt ("ClaimTank");
		
		claimTerribleOath = PlayerPrefs.GetInt ("ClaimTerribleOath");
		claimSonOfSurya = PlayerPrefs.GetInt ("ClaimSonOfSurya");
		claimDoubleTrouble = PlayerPrefs.GetInt ("ClaimDoubleTrouble");
		claimEclipse = PlayerPrefs.GetInt ("ClaimEclipse");
		claimCoupleOrTrouble = PlayerPrefs.GetInt ("ClaimCoupleOrTrouble");
		claimCraftyTacticion = PlayerPrefs.GetInt ("ClaimCraftyTacticion");

		unlockUnitCount = PlayerPrefs.GetInt ("UnlockUnit");
		unlockHeroCount = PlayerPrefs.GetInt ("UnlockHero");
		victoryCount = PlayerPrefs.GetInt ("Victory");
		completeFirstPlay = PlayerPrefs.GetInt ("FirstPlay");
		completeGraduate = PlayerPrefs.GetInt ("Graduate");
		completeIronFist = PlayerPrefs.GetInt ("IronFist");

		killCountFootman = PlayerPrefs.GetInt ("FootmanSlayer");
		killCountArcher = PlayerPrefs.GetInt ("ArcherSlayer");
		killCountSwordman = PlayerPrefs.GetInt ("SwordmanSlayer");
		killCountGunner = PlayerPrefs.GetInt ("GunnerSlayer");
		killCountSpearman = PlayerPrefs.GetInt ("SpearmanSlayer");
		killCountMage = PlayerPrefs.GetInt ("MageSlayer");
		killCountShaman = PlayerPrefs.GetInt ("ShamanSlayer");
		killCountElephantry = PlayerPrefs.GetInt ("ElephantrySlayer");
		killCountTank = PlayerPrefs.GetInt ("TankSlayer");

		completeTerribleOath = PlayerPrefs.GetInt ("TerribleOath");
		completeSonOfSurya = PlayerPrefs.GetInt ("SonOfSurya");
		completeDoubleTrouble = PlayerPrefs.GetInt ("DoubleTrouble");
		completeEclipse = PlayerPrefs.GetInt ("Eclipse");
		completeCoupleOrTrouble = PlayerPrefs.GetInt ("CoupleOrTrouble");
		completeCraftyTacticion = PlayerPrefs.GetInt ("CraftyTacticion");
		if (completeFirstPlay == 1) {
			if (claimFirstPlay == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeGraduate == 1) {
			if (claimGraduate == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeIronFist == 1) {
			if (claimIronFist == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockUnitCount == 3) {
			if (claimCaptain == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockUnitCount == 6) {
			if (claimCommander == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockUnitCount == 9) {
			if (claimGeneral == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockHeroCount == 3) {
			if (claimKnight == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockHeroCount == 6) {
			if (claimPrince == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (unlockHeroCount == 9) {
			if (claimKing == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (victoryCount == 3) {
			if (claimAmateur == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (victoryCount == 6) {
			if (claimProfessional == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (victoryCount == 9) {
			if (claimMaster == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (killCountFootman >= 100) {
			if (claimFootman == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		if (killCountArcher >= 100) {
			if (claimArcher == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		if (killCountSwordman >= 100) {
			if (claimSwordman == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (killCountGunner >= 100) {
			if (claimGunner == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (killCountSpearman >= 100) {
			if (claimSpearman == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		if (killCountMage >= 100) {
			if (claimMage == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		if (killCountShaman >= 100) {
			if (claimShaman == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (killCountElephantry >= 100) {
			if (claimElephantry == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (killCountTank >= 100) {
			if (claimTank == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
		if (completeTerribleOath == 1) {
			if (claimTerribleOath == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeSonOfSurya == 1) {
			if (claimSonOfSurya == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeDoubleTrouble == 1) {
			if (claimDoubleTrouble == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeEclipse == 1) {
			if (claimEclipse == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeCoupleOrTrouble == 1) {
			if (claimCoupleOrTrouble == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
		if (completeCraftyTacticion == 1) {
			if (claimCraftyTacticion == 0) {
				gameObject.GetComponent<SpriteRenderer>().enabled = true;
			}
			else {
				gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}	
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
