﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	private float speed = 0.4f;
	private float xStart = 0.0f;
	private bool curTouched;
	public GameObject cameraIndicator, popupUI;
	public static bool moveLeft = false;
	public static bool moveRight = false;
	public static bool touchIndicator = false;
	public static int rightScreen;
	public static int leftScreen;
	private Vector2 swipeSpeed;
	public static float totalSpeed;
	private float cameraIndicatorSpeed;
	public bool bosslevel = false;
	public bool survivalmode = false;
	public bool tutorial = false;
	public int retryCost;
	private GameObject square;
	private float timer;
    EasyFontTextMesh stageText;

	// Use this for initialization
	void Awake () {
		Destroy (GameObject.Find ("BGM"));
		Destroy (GameObject.Find ("SilenceBGM"));
		//Destroy (GameObject.Find ("GoldSystem"));
		/*if (GameObject.Find("EnergyText") != null)
		{
			GameObject.Find ("EnergyText").SetActive(false);
		}*/
	}
	void Start () {
        //PlayerPrefs.DeleteAll ();
        stageText = GameObject.Find("Middle Container/DayText").GetComponent<EasyFontTextMesh>();
		square = GameObject.Find ("Main Camera/Square");
        stageText.Text = StageManager.loadedLevel.ToString();

		Camera.main.GetComponent<AudioSource> ().volume = 1;
		Scaling ();
		timer = 3;
	}
	
	// Update is called once per frame
	void Update () {

        LockIndicatorAndCamera();

		timer -= Time.deltaTime;
		if (Application.loadedLevelName == "Tutorial") {
			if (TutorialScenario.tutorialStepThree == true && transform.position.x < rightScreen) {
				transform.Translate (Vector2.right * 5);
				cameraIndicator.transform.Translate (Vector2.right * 5/cameraIndicatorSpeed);
			}
			else if (TutorialScenario.tutorialMoveCameraOnce == true && transform.position.x > leftScreen) {
				transform.Translate (-Vector2.right * 5);
				cameraIndicator.transform.Translate (-Vector2.right * 5/cameraIndicatorSpeed);
			}
			else {
				SwipeMainCamera();
			}
			if (TutorialScenario.tutorialSummon1stTime == true && transform.position.x == leftScreen) {
				TutorialScenario.tutorialMoveCameraOnce = false;	
			}
		}
		else {
			if (touchIndicator == false || CastleEnemy.hasPlayerWin == false) {
				if (timer <= 0) {
					SwipeMainCamera();	
				}
			}
		}
	}

    void LockIndicatorAndCamera()
    {
        if(Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left, Space.World);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right, Space.World);
        }


        if (cameraIndicator.transform.localPosition.x < -11.25f)
        {
            cameraIndicator.transform.localPosition = new Vector3(-11.25f, cameraIndicator.transform.localPosition.y, cameraIndicator.transform.localPosition.z);
        }

        if (cameraIndicator.transform.localPosition.x > 11.25f)
        {
            cameraIndicator.transform.localPosition = new Vector3(11.25f, cameraIndicator.transform.localPosition.y, cameraIndicator.transform.localPosition.z);
        }

        //force camera to its border
        if (transform.position.x < 7)
        {
            transform.position = new Vector3(7, transform.position.y, transform.position.z);
        }

        if (transform.position.x > 148)
        {
            transform.position = new Vector3(148, transform.position.y, transform.position.z);
        }
    }

	void SwipeMainCamera()
	{
		if (Input.touchCount == 1)
		{
			// Get the touch info
			Touch t = Input.GetTouch(0);
			// Did the touch action just begin?
			if (t.phase == TouchPhase.Began)
			{
				xStart = t.position.x;

			}
			if (t.phase == TouchPhase.Stationary)
			{
				moveRight = false;
				moveLeft = false;
				
			}
			if (t.phase == TouchPhase.Moved && xStart > t.position.x)
			{
				if (transform.position.x < rightScreen) {
					swipeSpeed = t.deltaPosition;
					totalSpeed = swipeSpeed.x * speed;
					moveRight = true;
					moveLeft = false;
					transform.Translate (Vector2.right * totalSpeed * -1);
					//transform.Translate (Mathf.Abs(t.deltaPosition.x) * speed, transform.position.y, transform.position.z);
					cameraIndicator.transform.Translate (Vector2.right * totalSpeed/cameraIndicatorSpeed * -1);
					//cameraIndicator.transform.Translate (Mathf.Abs(t.deltaPosition.x) * speed/cameraIndicatorSpeed, transform.position.y, transform.position.z);
				}
				else {
					transform.Translate (Vector2.right * 0);
					moveRight = false;
					moveLeft = false;
				}
			}
			else if (t.phase == TouchPhase.Moved && xStart < t.position.x)
			{
				if (transform.position.x > leftScreen) {
					swipeSpeed = t.deltaPosition;
					totalSpeed = swipeSpeed.x * speed;
					moveRight = false;
					moveLeft = true;
					transform.Translate (-Vector2.right * totalSpeed);
					//transform.Translate (-Mathf.Abs(t.deltaPosition.x) * speed, transform.position.y, transform.position.z);
					cameraIndicator.transform.Translate (-Vector2.right * totalSpeed/cameraIndicatorSpeed);
					//cameraIndicator.transform.Translate (-Mathf.Abs(t.deltaPosition.x) * speed/cameraIndicatorSpeed, transform.position.y, transform.position.z);
				}
				else {
					transform.Translate (Vector2.right * 0);
					moveRight = false;
					moveLeft = false;
				}

			}
			// Did the touch end?
			if (t.phase == TouchPhase.Ended)
			{
				moveRight = false;
				moveLeft = false;
				xStart = 0.0f;
			}
		}
	}
	void Scaling() {
		if (Camera.main.aspect >= 1.7) {
			//16 : 9
			Camera.main.transform.position = new Vector3 (7, -5.4f, -10);
			leftScreen = 7;
			rightScreen = 148;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/22f;
		}
		else if (Camera.main.aspect >= 1.5) {
			//16 : 10
			Camera.main.transform.position = new Vector3 (4, -5.4f, -10);
			leftScreen = 4;
			rightScreen = 150;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/22f;
		}
		else if (Camera.main.aspect >= 1.3){
			//4 : 3
			leftScreen = 1;
			rightScreen = 156;
			cameraIndicatorSpeed = (rightScreen - leftScreen)/22f;
		}
	}
}
