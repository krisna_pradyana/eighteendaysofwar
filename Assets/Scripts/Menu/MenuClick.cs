﻿using UnityEngine;
using System.Collections;
//using ChartboostSDK;
using System;

public class MenuClick : MonoBehaviour {
	public string sceneNext;
	public GameObject settingUI;
	public GameObject helpUI;
	public GameObject tutorialUI;
	private int hasFinishTutorial;
	private float xStart;
	private float yStart;
	private Vector3 touchPosition;
	private bool canTouch = false;
	public static bool tutorial1stTime = false, hasShownAds = false;

	// Use this for initialization
	void Awake () {
		Destroy (GameObject.Find ("BGM"));
        /*
		if (hasShownAds == false) {
			Chartboost.showInterstitial(CBLocation.Default);
			hasShownAds = true;
		}
        */
	}
	void Start () {
		Time.timeScale = 1;
		hasFinishTutorial = PlayerPrefs.GetInt ("FinishTutorial");
	}
	void Update () {
		if (Input.touchCount == 1) {
			Touch t = Input.GetTouch(0);
			if (t.phase == TouchPhase.Began && ArtworkAnimation.HasFinishAnimation == true & canTouch == true)
			{
				touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(t.position.x, t.position.y, Camera.main.nearClipPlane));
				xStart = touchPosition.x;
				yStart = touchPosition.y;
                /*
				if(xStart < 5 && yStart < 8) {
					if(hasFinishTutorial == 0) {
						tutorialUI.SetActive(true);
						GameFlow.hasPaused = true;
						tutorial1stTime = true;
					}
					else {
						GetComponent<AudioSource>().Play();
						StartCoroutine("loadShop");
					}
                    
				}\*/
            }
            if (t.phase == TouchPhase.Ended)
			{
				xStart = 0.0f;
				yStart = 0.0f;
			}

            Debug.Log(t);
		}
		if (GameFlow.hasPaused == false) {
			canTouch = true;	
		}
		else {
			canTouch = false;
		}
		if (ArtworkAnimation.HasFinishAnimation == false) {
			gameObject.GetComponent<BoxCollider2D>().enabled = false;	
		}
		else {
			gameObject.GetComponent<BoxCollider2D>().enabled = true;
		}
	}
	void OnMouseDown () {
		if (gameObject.name == "Help Container") {
			GetComponent<AudioSource>().Play ();
			GameObject.Find ("Main Camera/Square").GetComponent<SpriteRenderer>().enabled = true;
			tutorialUI.SetActive (true);
			GameFlow.hasPaused = true;
		}
		else if (gameObject.name == "Option Container") {
			GetComponent<AudioSource>().Play ();
			GameObject.Find ("Main Camera/Square").GetComponent<SpriteRenderer>().enabled = true;
			settingUI.SetActive (true);	
			GameFlow.hasPaused = true;
		}
		else if (gameObject.name == "Achievement Container") {
			GetComponent<AudioSource>().Play ();
			StartCoroutine ("loadStage");
		}
		else if (gameObject.name == "StartButton") {
            if (hasFinishTutorial == 0)
            {
                tutorialUI.SetActive(true);
                GameFlow.hasPaused = true;
                tutorial1stTime = true;
            }
            else
            {
                GetComponent<AudioSource>().Play();
                StartCoroutine("loadStage");
            }
		}
	}
	IEnumerator loadStage() {
		AsyncOperation async = Application.LoadLevelAsync(sceneNext);
		yield return async;
	}
	IEnumerator loadShop() {
		AsyncOperation async = Application.LoadLevelAsync("Shop");
		yield return async;
	}
    /*
	void didFailToLoadInterstitial(CBLocation location, CBImpressionError error) {

	}
	void didDismissInterstitial(CBLocation location) {

	}
	void didCloseInterstitial(CBLocation location) {

	}
	void didClickInterstitial(CBLocation location) {
		Debug.Log("didClickInterstitial: " + location);
	}
	void didCacheInterstitial(CBLocation location) {
		Debug.Log("didCacheInterstitial: " + location);
	}
	bool shouldDisplayInterstitial(CBLocation location) {
		return true;
	}
	void didDisplayInterstitial(CBLocation location){
		Debug.Log("didDisplayInterstitial: " + location);
	}
	void OnEnable() {
		// Listen to all impression-related events
		Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial += didDismissInterstitial;
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
	}
	void OnDisable() {
		// Remove event handlers
		Chartboost.didFailToLoadInterstitial -= didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial -= didDismissInterstitial;
		Chartboost.didCloseInterstitial -= didCloseInterstitial;
		Chartboost.didClickInterstitial -= didClickInterstitial;
		Chartboost.didCacheInterstitial -= didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial -= shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial -= didDisplayInterstitial;
	}
    */
}
