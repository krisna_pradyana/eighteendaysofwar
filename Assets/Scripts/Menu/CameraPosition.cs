﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour {
	public bool isCamPosition1;
	public bool isCamPosition2;
	public bool isCamPosition3;
	public bool isCamPosition4;
	public bool isCamPosition5;
	private float newCamPosition;
	public GameObject cameraIndicator;
	// Use this for initialization
	void Start () {

	}
	void Update () {
		newCamPosition = (CameraControl.rightScreen-CameraControl.leftScreen)/3;
	}
	void OnMouseDown() {
		if (isCamPosition1 == true) {
			Camera.main.transform.position = new Vector3 (CameraControl.leftScreen, Camera.main.transform.position.y, Camera.main.transform.position.z);
			cameraIndicator.transform.position = gameObject.transform.position;
		}
		else if (isCamPosition2 == true) {
			Camera.main.transform.position = new Vector3 (CameraControl.leftScreen+newCamPosition, Camera.main.transform.position.y, Camera.main.transform.position.z);
			cameraIndicator.transform.position = gameObject.transform.position;
		}
		else if (isCamPosition3 == true) {
			Camera.main.transform.position = new Vector3 (CameraControl.rightScreen-newCamPosition, Camera.main.transform.position.y, Camera.main.transform.position.z);
			cameraIndicator.transform.position = gameObject.transform.position;
		}
		else if (isCamPosition4 == true) {
			Camera.main.transform.position = new Vector3 (CameraControl.rightScreen, Camera.main.transform.position.y, Camera.main.transform.position.z);
			cameraIndicator.transform.position = gameObject.transform.position;
		}
		else if (isCamPosition5 == true) {
			Camera.main.transform.position = new Vector3 (CameraControl.rightScreen, Camera.main.transform.position.y, Camera.main.transform.position.z);
			cameraIndicator.transform.position = gameObject.transform.position;
		}
	}
}
