﻿using UnityEngine;
using System.Collections;

public class TutorialUI : MonoBehaviour {
	public Sprite[] TutorialLists;
	private int counter = 1;
	public GameObject helpUI, tutorialUI;
	private int hasFinishTutorial;
	// Use this for initialization
	void Start () {
		hasFinishTutorial = PlayerPrefs.GetInt ("FinishTutorial");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown () {
		GetComponent<AudioSource>().Play ();
		if (counter == 1) {
			gameObject.GetComponent<SpriteRenderer>().sprite = TutorialLists[1];
			counter = 2;
		}
		else if (counter == 2) {
			gameObject.GetComponent<SpriteRenderer>().sprite = TutorialLists[2];
			counter = 3;
		}
		else if (counter == 3) {
			gameObject.GetComponent<SpriteRenderer>().sprite = TutorialLists[3];
			counter = 4;
		}
		else if (counter == 4) {
			gameObject.GetComponent<SpriteRenderer>().sprite = TutorialLists[0];
			helpUI.SetActive (false);
			tutorialUI.SetActive (true);
			counter = 1;
		}
	}
}
