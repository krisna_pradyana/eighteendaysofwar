﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public GameObject tutorial, option, help, achivement;

    private void OnEnable()
    {
          
    }

    public void TutorialPrassed()
    {
        tutorial.SetActive(true);
    }

    public void OptionPressed()
    {
        option.SetActive(true);
    }

    public void HelpPressed()
    {
        help.SetActive(true);
    }

    public void AchivementPressed()
    {
        achivement.SetActive(true);
    }

    public void StartPressed()
    {
        Debug.Log("pressed");
        Application.LoadLevelAsync("Shop");
    }
}
