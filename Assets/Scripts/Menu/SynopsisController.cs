﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SynopsisController : MonoBehaviour {
	public string sceneNext;
	private bool doneFading;
	private Text fadingText;
	public float delay;
	private bool waitText;
	private bool hasTouch;

    LO_LoadScene nextScene;
    LO_SelectStyle selectStyle;
    // Use this for initialization
    void Start () {
		hasTouch = false;
		doneFading = false;
		waitText = true;
		fadingText = gameObject.GetComponent<Text> ();

        nextScene = GameObject.FindObjectOfType<LO_LoadScene>();
        selectStyle = GameObject.FindObjectOfType<LO_SelectStyle>();

        StartCoroutine(loadScene());
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 1 && hasTouch == false) {
			Touch t = Input.GetTouch(0);
			if (t.phase == TouchPhase.Began)
			{
				hasTouch = true;
			}
		}
		if (doneFading == false) {
			if (fadingText.color.a < 1) {
				fadingText.color = new Color(fadingText.color.r, fadingText.color.g, fadingText.color.b, fadingText.color.a + 0.01f); 
			}
			else {
				doneFading = true;
			}
		}
		else {
			if (delay < 0) {
				if (fadingText.color.a > 0) {
					fadingText.color = new Color(fadingText.color.r, fadingText.color.g, fadingText.color.b, fadingText.color.a - 0.01f); 
				}
			}
			else {
				delay -= Time.fixedDeltaTime;
			}
		}
    }


	IEnumerator loadScene() {
        yield return new WaitForSeconds(6f);

        Debug.Log("Has Loading");

        selectStyle.SetStyle("Stock_Style");
        nextScene.ChangeToScene(sceneNext);

        yield return null;
	}
}
