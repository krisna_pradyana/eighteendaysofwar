﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
//using SIS;

public class SplashScreenLoad : MonoBehaviour {
	public float delayTime = 5;
	public bool done = false;
	public string Scene;
	private float speedFade = 1f;
	private float timer;

	void Start()
    { 
    timer = delayTime;

		StartCoroutine ("SomeFunction");
		PlayerPrefs.SetInt ("FirstPlay", 1);
	}

	void Update()
	{
		timer -= Time.fixedDeltaTime;
		if (timer > 0) {
			return;
		}
		if (done) {
            SceneManager.LoadScene(Scene);
		}

	}
	IEnumerator SomeFunction()
	{
		//Do Something Here

		//Some yield operation until it has completed
		yield return null;

		//If we did what we wanted and without error
		done = true;
	}
}
