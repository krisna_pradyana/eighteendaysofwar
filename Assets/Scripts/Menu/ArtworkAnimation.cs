﻿using UnityEngine;
using System.Collections;

public class ArtworkAnimation : MonoBehaviour {
	public GameObject pandawa, kurawa;
	public float animationSpeed;
	public GameObject startButton;
	public static bool HasFinishAnimation;
	public GameObject popupUI;
	private GameObject square;
	private GameObject[] optimize;
	private float delay = 1f;
	// Use this for initialization
	void Start () {
		HasFinishAnimation = false;
		square = GameObject.Find ("Square");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 1) {
			Touch t = Input.GetTouch(0);
			// Did the touch action just begin?
			if (t.phase == TouchPhase.Began)
			{
				if (HasFinishAnimation == false) {
					pandawa.transform.position = new Vector3 (0, pandawa.transform.position.y, pandawa.transform.position.z);
					kurawa.transform.position = new Vector3 (0, kurawa.transform.position.y, kurawa.transform.position.z);
					startButton.SetActive (true);
					HasFinishAnimation = true;
					GameFlow.hasPaused = false;
				}
			}	
		}
		else {
			if (delay < 0) {
				if (HasFinishAnimation == false) {
					if (pandawa.transform.position.x <= 0) {
						pandawa.transform.Translate (Vector2.right * animationSpeed * Time.fixedDeltaTime);
						GameFlow.hasPaused = true;
					}
					else if (kurawa.transform.position.x >= 2) {
						kurawa.transform.Translate (-Vector2.right * animationSpeed * Time.fixedDeltaTime);	
						GameFlow.hasPaused = true;
					}
					else {
						startButton.SetActive (true);
						HasFinishAnimation = true;
						GameFlow.hasPaused = false;
					}
				}
			}
			else {
				delay -= Time.fixedDeltaTime;
			}
		}
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			Time.timeScale = 0;
			square.GetComponent<SpriteRenderer>().enabled = true;
			popupUI.SetActive(true);
			GameFlow.hasPaused = true;
		}

	}
}
