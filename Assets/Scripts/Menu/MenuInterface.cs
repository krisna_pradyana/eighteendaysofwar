﻿using UnityEngine;
using System.Collections;

public class MenuInterface : MonoBehaviour {
	public GUITexture Play;
	public GUITexture Options;
	public GUITexture Help;
	
	public string shopLevel;
	public string optionsLevel;
	public string helpLevel;

	private bool hasClickOptions = false;
	private bool hasClickHelp = false;
	// Update is called once per frame
	void Start () {
		PlayerPrefs.DeleteAll ();
	}
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			if (Play.HitTest(Input.mousePosition, Camera.main))
			{
				Application.LoadLevel(shopLevel);
			}
			if (Options.HitTest(Input.mousePosition, Camera.main))
			{
				//Application.LoadLevel(optionsLevel);
				hasClickOptions = true;
				hasClickHelp = false;
			}
			if (Help.HitTest(Input.mousePosition, Camera.main))
			{
				//Application.LoadLevel(helpLevel);
				hasClickHelp = true;
				hasClickOptions = false;
			}
		}
	}
	void OnGUI () {
		if (hasClickOptions == true) {
			GUI.Box (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), "\n"+"Coming Soon..!!");
			if(GUI.Button(new Rect(Screen.width/2-Screen.width/16, Screen.height/2+(Screen.height/8+10), Screen.width/8, Screen.height/8-10), "Back")) {
				hasClickOptions = false;
			}
				
		}
		if (hasClickHelp == true) {
			GUI.Box (new Rect (Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), "\n"+"Coming Soon..!!");
			if(GUI.Button(new Rect(Screen.width/2-Screen.width/16, Screen.height/2+(Screen.height/8+10), Screen.width/8, Screen.height/8-10), "Back")) {
				hasClickHelp = false;
			}	
		}
	}
}
