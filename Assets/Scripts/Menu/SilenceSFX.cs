﻿using UnityEngine;
using System.Collections;

public class SilenceSFX : MonoBehaviour {
	private GameObject[] arraySFX;
	private GameObject[] arrayAudio, arrayGameController;
	// Use this for initialization
	void Awake () {
		arraySFX = GameObject.FindGameObjectsWithTag ("SFX");
		if (arraySFX.Length == 1) {
			DontDestroyOnLoad(gameObject);	
		}
		else {
			DestroyObject(gameObject);
		}
	}
	void Start () {

	}
	// Update is called once per frame
	void Update () {
		arrayAudio = GameObject.FindGameObjectsWithTag ("AudioTrigger");
		arrayGameController = GameObject.FindGameObjectsWithTag ("GameController");
		if (PauseClick.enableSFX == true) {
			foreach (GameObject sfx in arrayAudio) {
				sfx.GetComponent <AudioSource>().GetComponent<AudioSource>().volume = 0;
			}	
			foreach (GameObject gameController in arrayGameController) {
				gameController.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 0;	
			}
		}
		else if (PauseClick.enableSFX == false) {
			foreach (GameObject sfx in arrayAudio) {
				sfx.GetComponent <AudioSource>().GetComponent<AudioSource>().volume = 1;
			}
			foreach (GameObject gameController in arrayGameController) {
				gameController.GetComponent<AudioSource>().GetComponent<AudioSource>().volume = 1;	
			}
		}
	}
}
