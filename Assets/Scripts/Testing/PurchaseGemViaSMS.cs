﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PurchaseGemViaSMS : MonoBehaviour {
    public int GemID = 0 ;
    Animator windowAnmator;
    SendSMSIAP sendSMS;
    public GameObject WaitingPop, SuccessPop, Failed;
    public float timescaleCheck;
    CekServer cekServer;
    public bool waitingForItem;
    public GemContainer gemSystem;
    public GemUpdate gemupd;

    public TextMesh DebugTxt;

    // Use this for initialization
    void Start () {
        windowAnmator = GetComponent<Animator>();
        sendSMS = FindObjectOfType<SendSMSIAP>();
        cekServer = FindObjectOfType<CekServer>();
        //gemSystem = FindObjectOfType<GemContainer>();

        //gemupd = FindObjectOfType<GemUpdate>();
        /*
        if (sendSMS.success)
        {
            windowAnmator.SetInteger("purchaseID", 2);
        }
        else
        {
            windowAnmator.SetInteger("purchaseID", 4);
        }*/
    }

    void Update() {
        timescaleCheck = Time.timeScale;
        
    }

    public void Gem1() {
        GemID = 1;
        cekServer.doneChecking = false;
        //WaitingPop.SetActive(true);
        //windowAnmator.SetInteger("purchaseID", 1);
        Ya();
    }

    public void Gem2()
    {
        GemID = 2;
        cekServer.doneChecking = false;
        //WaitingPop.SetActive(true);
        //windowAnmator.SetInteger("purchaseID", 1);
        Ya();
    }

    public void Gem3()
    {
        GemID = 3;
        cekServer.doneChecking = false;
        //windowAnmator.SetInteger("purchaseID", 1);
        Ya();
    }

    public void Gem4()
    {
        GemID = 4;
        cekServer.doneChecking = false;
       // WaitingPop.SetActive(true);
        //windowAnmator.SetInteger("purchaseID", 1);
        Ya();
    }

    public void Gem5()
    {
        GemID = 5;
        cekServer.doneChecking = false;
        //WaitingPop.SetActive(true);
        //windowAnmator.SetInteger("purchaseID", 1);
        Ya();
    }

    public void Ya() {
        waitingForItem = true;
            
        if (GemID == 1)
        {
            sendSMS.SenSMSMessage1();
        }
        else if (GemID == 2)
        {
            sendSMS.SenSMSMessage2();
        }
        else if (GemID == 3)
        {
            sendSMS.SenSMSMessage3();
        }
        else if (GemID == 4)
        {
            sendSMS.SenSMSMessage4();
        }
        else if (GemID == 5)
        {
            sendSMS.SenSMSMessage5();
        }

    }

    public void Tidak() {
        windowAnmator.SetInteger("purchaseID", 3);
        Time.timeScale = 1;
    }

    public void Ok() {
        windowAnmator.SetInteger("purchaseID", 0);
        Time.timeScale = 1;
        SceneManager.LoadScene("Shop", LoadSceneMode.Single);
    }

    public void PopUpBerhasil() {
        if (waitingForItem  == true) {
            if (GemID == 1)
            {
                int CurrentGem = PlayerPrefs.GetInt("Gem");
                int GemValue = CurrentGem + 8;
                WaitingPop.SetActive(false);
                SuccessPop.SetActive(true);
                PlayerPrefs.SetInt("Gem", GemValue);
                PlayerPrefs.Save();
            }
            else if (GemID == 2)
            {
                int CurrentGem = PlayerPrefs.GetInt("Gem");
                int GemValue = CurrentGem + 10;
                WaitingPop.SetActive(false);
                SuccessPop.SetActive(true);
                PlayerPrefs.SetInt("Gem", GemValue);
                PlayerPrefs.Save();
            }
            else if (GemID == 3)
            {
                int CurrentGem = PlayerPrefs.GetInt("Gem");
                int GemValue = CurrentGem + 15;
                WaitingPop.SetActive(false);
                SuccessPop.SetActive(true);
                PlayerPrefs.SetInt("Gem", GemValue);
                PlayerPrefs.Save();
            }
            else if (GemID == 4)
            {
                int CurrentGem = PlayerPrefs.GetInt("Gem");
                int GemValue = CurrentGem + 20;
                WaitingPop.SetActive(false);
                SuccessPop.SetActive(true);
                PlayerPrefs.SetInt("Gem", GemValue);
                PlayerPrefs.Save();
            }
            else if (GemID == 5)
            {
                int CurrentGem = PlayerPrefs.GetInt("Gem");
                int GemValue = CurrentGem + 25;
                 WaitingPop.SetActive(false);
                SuccessPop.SetActive(true);
                PlayerPrefs.SetInt("Gem", GemValue);
                PlayerPrefs.Save();
            }
            waitingForItem = false;
        }
        windowAnmator.SetInteger("purchaseID", 2);
        Time.timeScale = 1;
    }

    public void PopUpTidakBerhasil()
    {
        windowAnmator.SetInteger("purchaseID", 3);
        Failed.SetActive(true);
        Time.timeScale = 1;     
    }

    public void PopUpMenungguTransaksiSebelumnya() {
        windowAnmator.SetInteger("purchaseID", 4);
        Failed.SetActive(true);
        Time.timeScale = 1;

    }

}
