﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldUpdate : MonoBehaviour {

    public static int goldValue;
    public Text goldTextUi;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        goldValue = PlayerPrefs.GetInt( "Gold");
        goldTextUi.text = "" + goldValue;
    }
}
