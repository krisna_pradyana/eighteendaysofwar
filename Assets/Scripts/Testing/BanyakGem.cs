﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanyakGem : MonoBehaviour {
    public int banyakGem;

    public TextMesh gemTextMesh;

	// Use this for initialization
	void Start () {
        PlayerPrefs.GetInt("Gem", banyakGem);
	}

    void Update() {
        gemTextMesh.text = "Gem : " + banyakGem;
    }
}
