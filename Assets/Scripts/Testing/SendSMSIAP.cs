﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FourthSky.Android;
using FourthSky.Android.Services;

public class SendSMSIAP : MonoBehaviour {
    public bool success;
    public GameObject WaitingPop;
    public TextMesh Text;

    public string IMEI;
    public int charCount;
    public char[] IMEIChar;

    public int[] kodeTransaksi;
    
    public float purchaseTimeDelay;
    public string currentPurchaseID;

    PurchaseGemViaSMS purchaseSMS;

    // Use this for initialization
    void Awake()
    {

        // Create broadcast receiver
        testBroadcastReceiver = new BroadcastReceiver();
        testBroadcastReceiver.OnReceive += (context, intent) => {
            string action = intent.Call<string>("getAction");

            if (action == testBroadcastAction)
            {
                string message = intent.Call<string>("getStringExtra", "message");

                broadcastMessage = "Broadcast message received: " + message;

            }
            else if (action == BroadcastActions.ACTION_HEADSET_PLUG)
            {
                int headsetState = intent.Call<int>("getIntExtra", "state", 0);

                if (headsetState == 1)
                {
                    string headsetName = intent.Call<string>("getStringExtra", "name");
                    int hasMicrophone = intent.Call<int>("getIntExtra", "microphone", 0);
                    broadcastMessage = "Headset plugged (" + headsetName + " - " + (hasMicrophone == 1 ? " with mic" : "no mic") + ")";

                    Debug.Log("Headset plugged (" + headsetName + " - " + (hasMicrophone == 1 ? " with mic" : "no mic") + ")");
                }
                else
                {
                    broadcastMessage = "Headset unplugged";

                    Debug.Log("Headset unplugged");
                }

            }
            else if (action == BroadcastActions.ACTION_BATTERY_CHANGED)
            {
                int batteryLevel = intent.Call<int>("getIntExtra", "level", 0);
                int batteryMaxLevel = intent.Call<int>("getIntExtra", "scale", 0);
                int isPlugged = intent.Call<int>("getIntExtra", "plugged", 0);

                if (isPlugged == 1)
                {
                    broadcastMessage = "Power plugged (battery level " + batteryLevel + "/" + batteryMaxLevel + ")";
                }
                else
                {
                    broadcastMessage = "Power unplugged (battery level " + batteryLevel + "/" + batteryMaxLevel + ")";
                }
            }
        };

        // Create service connection 
        billingServiceConnection = new ServiceConnection();
        billingServiceConnection.OnServiceConnected += (name, binder) => {
            if (binder == null)
            {
                Debug.Log("Something's wrong");
            }

            billingBinder = IInAppBillingService.Wrap(binder);
            billingMessage = "Billing service enabled";
        };
        billingServiceConnection.OnServiceDisconnected += (name) => {
            billingBinder.Dispose();
            billingBinder = null;
            billingMessage = "Billing service disabled";
        };

        //photoPlane.SetActive(false);
    }

    void Start()
    {
        Text = new TextMesh();

        AndroidJavaObject TM = new AndroidJavaObject("android.telephony.TelephonyManager");
        IMEI = TM.Call<string>("getDeviceId");

        IMEIChar = IMEI.ToCharArray();
        charCount = IMEI.Length;

        Debug.Log(IMEIChar);

        purchaseTimeDelay = PlayerPrefs.GetFloat("PurchaseTime");
        currentPurchaseID = PlayerPrefs.GetString("PurchaseID");

        purchaseSMS = FindObjectOfType<PurchaseGemViaSMS>();
    }

    // Update is called once per frame
    void Update()
    {
        if (purchaseTimeDelay >= 0) {
            purchaseTimeDelay -= Time.deltaTime;
        }

        if (purchaseTimeDelay <= 0) {
            purchaseTimeDelay = 0;            
        }

        //Text.text = "Delay = " + purchaseTimeDelay;

        PlayerPrefs.SetString("PurchaseID", currentPurchaseID);
        PlayerPrefs.SetFloat("PurchaseTime", purchaseTimeDelay);
        PlayerPrefs.Save();
    }

    void OnApplicationQuit()
    {
        // Dispose objects
        if (testBroadcastReceiver != null)
            testBroadcastReceiver.Dispose();

        if (billingServiceConnection != null)
            billingServiceConnection.Dispose();
    }

    // Telephony constants and variables
    public string phoneNumber = "99774";

    string message = "Testing SMS";

    string message1 = "GAMEMU8";
    string message2 = "GAMEMU10";
    string message3 = "GAMEMU15";
    string message4 = "GAMEMU20";
    string message5 = "GAMEMU25";

    string messageStatus = "No messages";

    public void SenSMSMessage1()
    {
        WaitingPop.SetActive(true);
        Debug.Log(IMEI);
        if (purchaseTimeDelay > 0) {
            purchaseSMS.PopUpMenungguTransaksiSebelumnya();
        }
        if (purchaseTimeDelay <= 0) {
            for (int i = 0; i < 6; i++)
            {
                kodeTransaksi[i] = Random.Range(0, 9);
            }

            string purchaseID = "08k" + IMEIChar[charCount - 4] + "" + IMEIChar[charCount - 3] + "" + IMEIChar[charCount - 2] + "" + IMEIChar[charCount - 1] + "" + kodeTransaksi[0] + "" + kodeTransaksi[1] + "" + kodeTransaksi[2] + "" + kodeTransaksi[3] + "" + kodeTransaksi[4] + "" + kodeTransaksi[5];

            message1 = "GAMEMU8 " + purchaseID;

            //Application.OpenURL("http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + purchaseID);

            Telephony.SendSMS(phoneNumber,
                                  message1,
                                  (sentOK) => {
                                      if (sentOK)
                                      {
                                          messageStatus = "Pembelian item sedang diproses";
                                      }
                                      else
                                      {
                                          messageStatus = "Pulsa tidak cukup";
                                      }
                                  },
                (deliveredOK) => {
                    if (deliveredOK)
                    {
                        messageStatus = "Item sudah terbeli";
                        success = true;
                    }
                    else
                    {
                        messageStatus = "Item tidak dapat dibeli";
                        success = false;
                    }
                });
            currentPurchaseID = purchaseID;
            purchaseTimeDelay = 20;
        }
        else if (purchaseTimeDelay == 0)
        {
            WaitingPop.SetActive(false);
        }
    }

    public void SenSMSMessage2()
    {
        WaitingPop.SetActive(true);
        if (purchaseTimeDelay > 0)
        {
            purchaseSMS.PopUpMenungguTransaksiSebelumnya();
        }

        if (purchaseTimeDelay <= 0)
        {
            for (int i = 0; i < 6; i++)
            {
                kodeTransaksi[i] = (int)Random.Range(0, 9);
            }

            string purchaseID = "10k" + IMEIChar[charCount - 4] + "" + IMEIChar[charCount - 3] + "" + IMEIChar[charCount - 2] + "" + IMEIChar[charCount - 1] + "" + kodeTransaksi[0] + "" + kodeTransaksi[1] + "" + kodeTransaksi[2] + "" + kodeTransaksi[3] + "" + kodeTransaksi[4] + "" + kodeTransaksi[5];

            message2 = "GAMEMU10 " + purchaseID;

            
            //Application.OpenURL("http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + purchaseID);

            Telephony.SendSMS(phoneNumber,
                                  message2,
                                  (sentOK) => {
                                      if (sentOK)
                                      {
                                          messageStatus = "Pembelian item sedang diproses";
                                      }
                                      else
                                      {
                                          messageStatus = "Pulsa tidak cukup";
                                      }
                                  },
                (deliveredOK) => {
                    if (deliveredOK)
                    {
                        messageStatus = "Item sudah terbeli";
                        success = true;
                    }
                    else
                    {
                        messageStatus = "Item tidak dapat dibeli";
                        success = false;
                    }
                });

            currentPurchaseID = purchaseID;
            purchaseTimeDelay = 20;
        }
        else if (purchaseTimeDelay == 0)
        {
            WaitingPop.SetActive(false);
        }

    }

    public void SenSMSMessage3()
    {
        WaitingPop.SetActive(true);
        if (purchaseTimeDelay > 0)
        {
            purchaseSMS.PopUpMenungguTransaksiSebelumnya();
        }
        if (purchaseTimeDelay <= 0)
        {
            for (int i = 0; i < 6; i++)
            {
                kodeTransaksi[i] = (int)Random.Range(0, 9);
            }

            string purchaseID = "15k" + IMEIChar[charCount - 4] + "" + IMEIChar[charCount - 3] + "" + IMEIChar[charCount - 2] + "" + IMEIChar[charCount - 1] + "" + kodeTransaksi[0] + "" + kodeTransaksi[1] + "" + kodeTransaksi[2] + "" + kodeTransaksi[3] + "" + kodeTransaksi[4] + "" + kodeTransaksi[5];

            message3 = "GAMEMU15 " + purchaseID;

           // Application.OpenURL("http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + purchaseID);

            Telephony.SendSMS(phoneNumber,
                                  message3,
                                  (sentOK) => {
                                      if (sentOK)
                                      {
                                          messageStatus = "Pembelian item sedang diproses";
                                      }
                                      else
                                      {
                                          messageStatus = "Pulsa tidak cukup";
                                      }
                                  },
                (deliveredOK) => {
                    if (deliveredOK)
                    {
                        messageStatus = "Item sudah terbeli";
                        success = true;
                    }
                    else
                    {
                        messageStatus = "Item tidak dapat dibeli";
                        success = false;
                    }
                });

            currentPurchaseID = purchaseID;
            purchaseTimeDelay = 20;
        }
        else if (purchaseTimeDelay == 0)
        {
            WaitingPop.SetActive(false);
        }
    }

    public void SenSMSMessage4()
    {
        WaitingPop.SetActive(true);
        if (purchaseTimeDelay > 0)
        {
            purchaseSMS.PopUpMenungguTransaksiSebelumnya();
        }
        if (purchaseTimeDelay <= 0)
        {
            for (int i = 0; i < 6; i++)
            {
                kodeTransaksi[i] = (int)Random.Range(0, 9);
            }

            string purchaseID = "20k" + IMEIChar[charCount - 4] + "" + IMEIChar[charCount - 3] + "" + IMEIChar[charCount - 2] + "" + IMEIChar[charCount - 1] + "" + kodeTransaksi[0] + "" + kodeTransaksi[1] + "" + kodeTransaksi[2] + "" + kodeTransaksi[3] + "" + kodeTransaksi[4] + "" + kodeTransaksi[5];

            message4 = "GAMEMU20 " + purchaseID;

            //Application.OpenURL("http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + purchaseID);

            Telephony.SendSMS(phoneNumber,
                                  message4,
                                  (sentOK) => {
                                      if (sentOK)
                                      {
                                          messageStatus = "Pembelian item sedang diproses";
                                      }
                                      else
                                      {
                                          messageStatus = "Pulsa tidak cukup";
                                      }
                                  },
                (deliveredOK) => {
                    if (deliveredOK)
                    {
                        messageStatus = "Item sudah terbeli";
                        success = true;
                    }
                    else
                    {
                        messageStatus = "Item tidak dapat dibeli";
                        success = false;
                    }
                });

            currentPurchaseID = purchaseID;
            purchaseTimeDelay = 20;
        }
        else if (purchaseTimeDelay == 0)
        {
            WaitingPop.SetActive(false);
        }
    }

    public void SenSMSMessage5()
    {
        WaitingPop.SetActive(true);
        if (purchaseTimeDelay > 0)
        {
            purchaseSMS.PopUpMenungguTransaksiSebelumnya();
        }
        if (purchaseTimeDelay <= 0)
        {
            for (int i = 0; i < 6; i++)
            {
                kodeTransaksi[i] = (int)Random.Range(0, 9);
            }

            string purchaseID = "25k" + IMEIChar[charCount - 4] + "" + IMEIChar[charCount - 3] + "" + IMEIChar[charCount - 2] + "" + IMEIChar[charCount - 1] + "" + kodeTransaksi[0] + "" + kodeTransaksi[1] + "" + kodeTransaksi[2] + "" + kodeTransaksi[3] + "" + kodeTransaksi[4] + "" + kodeTransaksi[5];

            message5 = "GAMEMU25 " + purchaseID;

           // Application.OpenURL("http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + purchaseID);

            Telephony.SendSMS(phoneNumber,
                                  message5,
                                  (sentOK) => {
                                      if (sentOK)
                                      {
                                          messageStatus = "Pembelian item sedang diproses";
                                      }
                                      else
                                      {
                                          messageStatus = "Pulsa tidak cukup";
                                      }
                                  },
                (deliveredOK) => {
                    if (deliveredOK)
                    {
                        messageStatus = "Item sudah terbeli";
                        success = true;
                    }
                    else
                    {
                        messageStatus = "Item tidak dapat dibeli";
                        success = false;
                    }
                });

            currentPurchaseID = purchaseID;
            purchaseTimeDelay = 20;
        }
        else if (purchaseTimeDelay == 0)
        {

            WaitingPop.SetActive(false);
        }
    }


    public GameObject photoPlane;
    private string contactInfo;    

    // Broadcast Receiver constants and variables
    private static readonly string testBroadcastAction = "com.unity.bcast.test";
    private static readonly string testBackgroundBroadcastAction = "com.unity.bcast.background.test";

    private BroadcastReceiver testBroadcastReceiver;
    private static string broadcastMessage = "BroadcastReceiver unregistered";    


    // Service Connection and Binder constants and variables
    private static readonly string billingServiceAction = "com.android.vending.billing.InAppBillingService.BIND";

    private ServiceConnection billingServiceConnection;
    private static IInAppBillingService billingBinder;
    private static string billingMessage = "Billing service disabled";
    
}
