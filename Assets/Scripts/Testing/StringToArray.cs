﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringToArray : MonoBehaviour {
    public string strWord = "Pneumonoul";
    public int strCount;
    public char[] characters;
    public char lastChar;

    public int[] kodeTransaksi;

    // Use this for initialization
    void Start () {
        characters = strWord.ToCharArray();

        strCount = strWord.Length;

        lastChar = characters[strCount-1];

        for (int i=0; i<6; i++) {
            kodeTransaksi[i] = Random.Range(0, 9);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
