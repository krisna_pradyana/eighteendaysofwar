﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemUpdate : MonoBehaviour {

    public static int gemValue;
    public Text gemTextUi;                               
    //public TextMesh gemText;

	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
        gemValue = PlayerPrefs.GetInt("Gem");
        gemTextUi.text = "" + gemValue;
	}
}
