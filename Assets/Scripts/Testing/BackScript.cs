﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackScript : MonoBehaviour {
    public string backToScene;

    SendSMSIAP sendSMS;

    // Use this for initialization
    void Start() {
        sendSMS = FindObjectOfType<SendSMSIAP>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(backToScene, LoadSceneMode.Single);
        }

    }

    public void Back()
    {

        SceneManager.LoadScene(backToScene, LoadSceneMode.Single);

    }
}
