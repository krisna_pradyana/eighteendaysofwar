﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGem : MonoBehaviour {
    public int gemValue;

	// Use this for initialization
	void Start () {
        gemValue = PlayerPrefs.GetInt("Gem");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            gemValue++;
            Debug.Log("gem bertambah");
        }

        PlayerPrefs.SetInt("Gem", gemValue);
        PlayerPrefs.Save();
	}
}
