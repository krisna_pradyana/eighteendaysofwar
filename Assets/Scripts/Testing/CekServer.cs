﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LitJson;
using UnityEngine.UI;

public class parseJSON {
    public string msisdnVar;
    public string unique_idVar;
    public string statusVar;
}

public class CekServer : MonoBehaviour {
    PurchaseGemViaSMS purchaseGemPopUp;
    
    public float purchaseTimeDelay;
    public int currentGemshard;
    public SendSMSIAP sendSMS;
    //public Text errorMessage;

    public bool doneChecking; 

    // Use this for initialization
    void Start() {
        purchaseGemPopUp = FindObjectOfType<PurchaseGemViaSMS>();
        currentGemshard = PlayerPrefs.GetInt("Gem");
    }

    IEnumerator DoCheckTheServer () {
        string url = "http://202.43.164.203/mahabharat/check_charging.php?uniqueID=" + sendSMS.currentPurchaseID;
        WWW www = new WWW(url);

        yield return www;

        if (www.error == null)
        {
            Processjson(www.text);
            //errorMessage.text = www.text;
        }
        else
        {
            Debug.Log("ERROR: " + www.error);
            //errorMessage.text = www.error;
        }
    }

    private void Processjson(string jsonString)
    {
        JsonData jsonvale = JsonMapper.ToObject(jsonString);
        parseJSON parsejson;
        parsejson = new parseJSON();
        parsejson.msisdnVar = jsonvale["msisdn"].ToString();
        parsejson.unique_idVar = jsonvale["unique_id"].ToString();
        parsejson.statusVar = jsonvale["status"].ToString();

        if (parsejson.statusVar == "1")
        {
            PlayerPrefs.SetFloat("PurchaseTime", 0);
            #region
            /*
            char[] idChar = parsejson.unique_idVar.ToCharArray();
            int charCount = parsejson.unique_idVar.Length;
            string pay = "" + idChar[charCount-12] + idChar[charCount - 11] + idChar[charCount - 10];
            if (pay == "08k")
            {
                currentGemshard += 8;
            }
            else if (pay == "10k")
            {
                currentGemshard += 10;
            }
            else if (pay == "15k")
            {
                currentGemshard += 15;
            }
            else if (pay == "20k")
            {
                currentGemshard += 20;
            }
            else if (pay == "25k")
            {
                currentGemshard += 25;
            }

            PlayerPrefs.SetInt("Gem", currentGemshard);
            */
            #endregion
            purchaseGemPopUp.PopUpBerhasil();
            doneChecking = true;
        }
        else if(parsejson.statusVar == "3:3:21") {
            purchaseGemPopUp.PopUpTidakBerhasil();
            doneChecking = true;
        }
    }

    void Update() {
        if (sendSMS.purchaseTimeDelay != 0 && doneChecking == false) {
            StartCoroutine("DoCheckTheServer");
        }        

        PlayerPrefs.Save();
    }
}
