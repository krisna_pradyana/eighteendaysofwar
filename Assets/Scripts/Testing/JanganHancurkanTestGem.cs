﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JanganHancurkanTestGem : MonoBehaviour {
    public GameObject[] gemText;

	// Use this for initialization
	void Awake () {
        gemText = GameObject.FindGameObjectsWithTag("Gem Text");

        DontDestroyOnLoad(gemText[0]);

        if (gemText.Length > 1) {
            for (int i=1; i<gemText.Length;i++) {
                Destroy(gemText[i]);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
