using UnityEngine;
using System.Collections;

public class LeftCorner : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float camHalfHeight = Camera.main.orthographicSize;
		float camHalfWidth = Camera.main.aspect * camHalfHeight;
		Bounds bounds = GetComponent<SpriteRenderer>().bounds;
		
		// Set a new vector to the top left of the scene 
		Vector3 topLeftPosition = new Vector3(-camHalfWidth, -camHalfHeight/4, 0) + Camera.main.transform.position; 
		
		// Offset it by the size of the object 
		topLeftPosition += new Vector3(bounds.size.x / 2,bounds.size.y / 2, 10);
		transform.position = topLeftPosition;
	}
}
