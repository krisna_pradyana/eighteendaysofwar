﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPurchasedAds : MonoBehaviour
{
    public Button adsPurhcaseButton;

    private void Start()
    {
        CheckPurchasedAds();
    }

    void CheckPurchasedAds()
    {
        if (!PlayerPrefs.HasKey("SkipAd"))
        {
            adsPurhcaseButton.interactable = true;
        }
        else
        {
            adsPurhcaseButton.interactable = false;
        }
    }

    public void PurchasedAds()
    {
        PlayerPrefs.SetInt("SkipAd", 0);
        PlayerPrefs.Save();
        CheckPurchasedAds();
    }
}
