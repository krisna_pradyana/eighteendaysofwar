//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.34014
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEngine;
using UnityEditor;

public static class AndroidCompilerConstants
{
    public const string ANDROID_SDK = "AndroidSdkRoot";
    public const string ANDROID_NDK = "AndroidNdkRoot";
    public const string ANDROID_PLATFORM = "AndroidPlatform";

    public const string SDK_MANAGER_EXE_PATH = "/SDK Manager.exe\"";
    public const string PLUGINS_PATH = "/Plugins/Android/";
	public const string PLUGINS_JAVA_PATH = PLUGINS_PATH + "java/";
	public const string PLUGINS_JNI_PATH = PLUGINS_PATH + "jni/";
	public const string PLUGINS_NATIVE_LIBS_PATH = PLUGINS_PATH + "libs/";
	public const string PLUGINS_RES_PATH = PLUGINS_PATH + "res/";
	public const string PLUGINS_NATIVE_OBJS_PATH = PLUGINS_PATH + "obj/";
    public const string PLUGINS_PLATFORM_PATH = "/platforms/";

    public const string ANDROID_JAR_NAME = "android.jar";
    public const string UNITY_ANDROID_SYSTEM_JAR_NAME = "unityandroidsystem.jar";

    public const string CLASSES_JAR =
#if UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_5_0 || UNITY_5_1
            "/PlaybackEngines/androidplayer/release/bin/classes.jar";
#else
            "/PlaybackEngines/androidplayer/Variations/mono/Release/Classes/classes.jar";
#endif

    // Save to avoid call in a thread different than main application thread
    public static readonly string APPLICATION_DATA_PATH = Application.dataPath;

    public static readonly string BANNER_PATH = APPLICATION_DATA_PATH + "/AndroidSystem/Editor/Resources/CompilerBanner.png";

    public const string CONFIG_LOCATION_PATH = "Assets/AndroidSystem/Editor/Resources/AndroidCompilerConfig.asset";
    
    public static readonly string ANDROID_PLUGINS_PATH = APPLICATION_DATA_PATH + PLUGINS_PATH;

    public static readonly string ANDROID_GENERATED_JAR_PATH = ANDROID_PLUGINS_PATH + UNITY_ANDROID_SYSTEM_JAR_NAME;

    public static readonly string ANDROID_NATIVE_OBJECTS_PATH = APPLICATION_DATA_PATH + PLUGINS_NATIVE_OBJS_PATH;

}